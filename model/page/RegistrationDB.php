<?php

/**
 * Class RegistrationDB
 */
class RegistrationDB extends MainDB {

    /**
     * @param $col_name
     * @param $input_value
     * @return bool
     */
    public function registered_user_search($col_name, $input_value) {

        $query = "SELECT `ru_id`
                  FROM `registered_user`
                  WHERE `$col_name` = :input_value";
        $array_for_query = ['input_value' => $input_value];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $login
     * @param $password
     * @param $email
     * @param $name
     * @param $surname
     * @param $user_group_id
     * @param $reg_date
     * @param $registration_key
     * @return bool|null
     */
    public function registerNewUser($login, $password, $email, $name, $surname, $user_group_id, $reg_date, $registration_key) {

        $query = "INSERT INTO `user_registration_inf` (`uri_login`, `uri_password`, `uri_email`, `uri_name`, `uri_surname`, `uri_user_group_id`, `uri_reg_date`, `uri_registration_key`, `uri_activated`)
                  VALUES (:uri_login, :uri_password, :uri_email, :uri_name, :uri_surname, :uri_user_group_id, :uri_reg_date, :uri_registration_key, '0')";
        $array_for_query = ['uri_login' => $login,
                            'uri_password' => $password,
                            'uri_email' => $email,
                            'uri_name' => $name,
                            'uri_surname' => $surname,
                            'uri_user_group_id' => $user_group_id,
                            'uri_reg_date' => $reg_date,
                            'uri_registration_key' => $registration_key];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }
}
