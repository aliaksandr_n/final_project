<?php

/**
 * Class UserRightDB
 */
class UserRightDB extends MainDB {
    /**
     * @return bool
     */
    public function readUserRight() {

        $query = "SELECT `ru_id`, `ru_name`, `ru_surname`, `ru_user_right`
                  FROM `registered_user`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $ru_id
     * @param $ru_user_right
     * @return bool
     */
    public function modifyUserRight($ru_id, $ru_user_right) {

        $query = "UPDATE `registered_user`
                  SET `ru_user_right` = :ru_user_right
                  WHERE `ru_id`=:ru_id";
        $array_for_query = ['ru_id'=>$ru_id,
                            'ru_user_right'=>$ru_user_right];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }
}
