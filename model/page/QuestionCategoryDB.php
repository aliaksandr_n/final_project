<?php

/**
 * Class QuestionCategoryDB
 */
class QuestionCategoryDB extends MainDB {

    /**
     * @return bool
     */
    public function readQuestionCategory() {

        $query = "SELECT `qc_id`, `qc_name`, `qc_parent_id`
                  FROM `question_category`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $qc_id
     * @return bool
     */
    public function selectModifiedQuestionCategory($qc_id) {

        $query = "SELECT `qc_id`, `qc_name`, `qc_parent_id`
                  FROM `question_category`
                  WHERE `qc_id` = :qc_id";
        $array_for_query = ['qc_id' => $qc_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $qc_id
     * @return bool
     */
    public function findQuestionCategoryChild($qc_id) {

        $query = "SELECT `qc_id`, `qc_name`
                  FROM `question_category`
                  WHERE `qc_parent_id` = :qc_id";
        $array_for_query = ['qc_id' => $qc_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $qc_id
     * @param $qc_name
     * @param $qc_parent_id
     * @return bool
     */
    public function  modifyQuestionCategory($qc_id, $qc_name, $qc_parent_id) {

        $query = "UPDATE `question_category`
                  SET `qc_name` = :qc_name, `qc_parent_id` = :qc_parent_id
                  WHERE `qc_id`=:qc_id";
        $array_for_query = ['qc_id' => $qc_id,
                            'qc_name' => $qc_name,
                            'qc_parent_id' => $qc_parent_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $qc_name
     * @param $qc_parent_id
     * @return bool|null
     */
    public function addQuestionCategory($qc_name, $qc_parent_id) {

        $query = "INSERT INTO `question_category` (`qc_name`, `qc_parent_id`)
                  VALUES (:qc_name, :qc_parent_id)";
        $array_for_query = ['qc_name' => $qc_name,
                            'qc_parent_id' => $qc_parent_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $qc_id
     * @return bool
     */
    public function deleteQuestionCategory($qc_id) {

        $query = "DELETE FROM `question_category`
                  WHERE `qc_id` = :qc_id";
        $array_for_query = ['qc_id' => $qc_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultDelete($result);
    }
}
