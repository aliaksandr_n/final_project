<?php

/**
 * Class TestJournalDB
 */
class TestJournalDB extends MainDB {

    /**
     * @param $at_creator_regestered_user_id
     * @param $at_id
     * @return bool
     */
    public function checkArrayForCheckingUserRightsForAppointment($at_creator_regestered_user_id, $at_id) {

        $query = "SELECT *
                  FROM `appointed_test`
                  WHERE `at_creator_regestered_user_id` = :at_creator_regestered_user_id AND `at_id` = :at_id";
        $array_for_query = ['at_creator_regestered_user_id' => $at_creator_regestered_user_id,
                            'at_id' => $at_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $utj_appointed_test_id
     * @return bool
     */
    public function getOneAppointmentStatArray($utj_appointed_test_id) {

        $query = "SELECT `tu_id` AS `tuid`, `ru_id` AS `ruid`, `utj_finish_date`, `utj_appointed_test_id` AS `utj_appointed_testid`, `ru_name`, `ru_surname`, `tu_key`, `tu_comment`, `utj_is_finished`, `utj_final_mark`, `utj_final_verdict`, `utj_time_duration`,
                    (SELECT COUNT(*)
                          FROM `user_test_journal`
                          JOIN `user_question_journal` ON `utj_id` = `uqj_user_test_journal_id`
                          WHERE `utj_appointed_test_id` = `utj_appointed_testid`
                          AND (`utj_registered_user_id` IS NOT NULL AND `utj_registered_user_id` = `ruid`)
                           OR (`utj_temp_user_id` IS NOT NULL AND `utj_temp_user_id` = `tuid`)
                    ) AS question_number,
                    (SELECT COUNT(*)
                          FROM `user_question_journal`
                          JOIN `user_test_journal` ON `uqj_user_test_journal_id` = `utj_id`
                          WHERE `utj_appointed_test_id` = `utj_appointed_testid` AND `uqj_user_question_status_id` = '5'
                    ) AS answered,
                    (SELECT COUNT(*)
                          FROM `user_question_journal`
                          JOIN `user_test_journal` ON `uqj_user_test_journal_id` = `utj_id`
                          WHERE `utj_appointed_test_id` = `utj_appointed_testid` AND `uqj_user_question_status_id` = '6'
                    ) AS overtimed,
                    (SELECT COUNT(*)
                          FROM `user_question_journal`
                          JOIN `user_test_journal` ON `uqj_user_test_journal_id` = `utj_id`
                          WHERE `uqj_answer_is_correct` = '1' AND `utj_appointed_test_id` = `utj_appointed_testid`
                    ) AS correct_answers
                  FROM `user_test_journal`
                  LEFT JOIN `registered_user` ON `utj_registered_user_id` = `ru_id`
                  LEFT JOIN `temp_user` ON `utj_temp_user_id` = `tu_id`
                  WHERE `utj_appointed_test_id` = :utj_appointed_test_id";
        $array_for_query = ['utj_appointed_test_id' => $utj_appointed_test_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $at_creator_regestered_user_id
     * @return bool
     */
    public function getGroupsStatArray($at_creator_regestered_user_id) {

        $query = "SELECT `at_id` AS `atid`, `at_creating_time`, `ru_name`, `ru_surname`, `tr_name`, `tr_id`,
                    (SELECT COUNT(*)
                          FROM `user_test_journal`
                          WHERE `utj_appointed_test_id` = `atid`
                    ) AS users_num,
                    (SELECT COUNT(*)
                          FROM `user_test_journal`
                          WHERE `utj_appointed_test_id` = `atid` AND `utj_is_finished` = '1'
                    ) AS finished_users
                  FROM `appointed_test`
                  JOIN `registered_user` ON `at_creator_regestered_user_id` = `ru_id`
                  JOIN `test_repository` ON `at_test_repository_id` = `tr_id`
                  WHERE `at_creator_regestered_user_id` = :at_creator_regestered_user_id";
        $array_for_query = ['at_creator_regestered_user_id' => $at_creator_regestered_user_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @return bool
     */
    public function getAllGroupsStatArray() {

        $query = "SELECT `at_id` AS `atid`, `at_creating_time`, `ru_name`, `ru_surname`, `tr_name`, `tr_id`,
                    (SELECT COUNT(*)
                          FROM `user_test_journal`
                          WHERE `utj_appointed_test_id` = `atid`
                    ) AS users_num,
                    (SELECT COUNT(*)
                          FROM `user_test_journal`
                          WHERE `utj_appointed_test_id` = `atid` AND `utj_is_finished` = '1'
                    ) AS finished_users
                  FROM `appointed_test`
                  JOIN `registered_user` ON `at_creator_regestered_user_id` = `ru_id`
                  JOIN `test_repository` ON `at_test_repository_id` = `tr_id`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }
}
