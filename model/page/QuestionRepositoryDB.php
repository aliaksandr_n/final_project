<?php

/**
 * Class QuestionRepositoryDB
 */
class QuestionRepositoryDB extends MainDB {

    /**
     * @return bool
     */
    public function readQuestionRepository() {

        $query = "SELECT `qr_id`, `qr_name`, `qr_parent_id`
                  FROM `question_repository`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $qr_id
     * @return bool
     */
    public function selectModifiedQuestionRepository($qr_id) {

        $query = "SELECT `qr_id`, `qr_name`, `qr_parent_id`
                  FROM `question_repository`
                  WHERE `qr_id` = :qr_id";
        $array_for_query = ['qr_id' => $qr_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $qr_id
     * @return bool
     */
    public function findQuestionRepositoryChild($qr_id) {

        $query = "SELECT `qr_id`, `qr_name`
                  FROM `question_repository`
                  WHERE `qr_parent_id` = :qr_id";
        $array_for_query = ['qr_id' => $qr_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $qr_id
     * @param $qr_name
     * @param $qr_parent_id
     * @return bool
     */
    public function  modifyQuestionRepository($qr_id, $qr_name, $qr_parent_id) {

        $query = "UPDATE `question_repository`
                  SET `qr_name` = :qr_name, `qr_parent_id` = :qr_parent_id
                  WHERE `qr_id`=:qr_id";
        $array_for_query = ['qr_id' => $qr_id,
                            'qr_name' => $qr_name,
                            'qr_parent_id' => $qr_parent_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $qr_name
     * @param $qr_parent_id
     * @return bool|null
     */
    public function addQuestionRepository($qr_name, $qr_parent_id) {

        $query = "INSERT INTO `question_repository` (`qr_name`, `qr_parent_id`)
                  VALUES (:qr_name, :qr_parent_id)";
        $array_for_query = ['qr_name' => $qr_name,
                            'qr_parent_id' => $qr_parent_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $qr_id
     * @return bool
     */
    public function deleteQuestionRepository($qr_id) {

        $query = "DELETE FROM `question_repository`
                  WHERE `qr_id` = :qr_id";
        $array_for_query = ['qr_id' => $qr_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultDelete($result);
    }
}
