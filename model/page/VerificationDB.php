<?php

/**
 * Class VerificationDB
 */
class VerificationDB extends MainDB {

    /**
     * @param $uri_registration_key
     * @return bool
     */
    public function findVerificationKey($uri_registration_key) {

        $query = "SELECT *
                  FROM `user_registration_inf`
                  WHERE `uri_registration_key` = :uri_registration_key
                  ORDER BY `uri_reg_date` DESC
                  LIMIT 1";
        $array_for_query = ['uri_registration_key' => $uri_registration_key];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $login
     * @param $uri_registered_user_id
     * @param $uri_id
     * @param int $activated
     * @return bool
     */
    public function markOtherKeyAsUsed($login, $uri_registered_user_id, $uri_id, $activated = 1) {

        $query = "UPDATE `user_registration_inf`
                  SET `uri_activated` = :uri_activated, `uri_activated_date` = :uri_activated_date
                  WHERE `uri_login`=:uri_login AND `uri_id`<>:uri_id";
        $array_for_query = ['uri_activated' => $activated,
                            'uri_activated_date' => $uri_registered_user_id,
                            'uri_login' => $login,
                            'uri_id' => $uri_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $activation_date
     * @param $login
     * @param $uri_registered_user_id
     * @param $uri_id
     * @return bool
     */
    public function markKeyAsActivated($activation_date, $login, $uri_registered_user_id, $uri_id) {

        $query = "UPDATE `user_registration_inf`
                  SET `uri_activated` = '1', `uri_activated_date` = :uri_activated_date, `uri_registered_user_id` = :uri_registered_user_id
                  WHERE `uri_login`=:uri_login AND `uri_id`=:uri_id";
        $array_for_query = ['uri_activated_date' => $activation_date,
                            'uri_registered_user_id' => $uri_registered_user_id,
                            'uri_login' => $login,
                            'uri_id' => $uri_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $login
     * @param $password
     * @param $email
     * @param $name
     * @param $surname
     * @param $reg_date
     * @return bool|null
     */
    public function addRegisteredUser($login, $password, $email, $name, $surname, $reg_date) {

        $query = "INSERT INTO `registered_user` (`ru_login`, `ru_password`, `ru_email`, `ru_name`, `ru_surname`, `ru_reg_date`)
                  VALUES (:ru_login, :ru_password, :ru_email, :ru_name, :ru_surname, :ru_reg_date)";
        $array_for_query = ['ru_login' => $login,
                            'ru_password' => $password,
                            'ru_email' => $email,
                            'ru_name' => $name,
                            'ru_surname' => $surname,
                            'ru_reg_date' => $reg_date];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $um_user_group_id
     * @param $um_registered_user_id
     * @return bool|null
     */
    public function addUserGroup($um_user_group_id, $um_registered_user_id) {

        $query = "INSERT INTO `ugroup_member` (`um_user_group_id`, `um_registered_user_id`)
                  VALUES (:um_user_group_id, :um_registered_user_id)";
        $array_for_query = ['um_user_group_id' => $um_user_group_id,
                            'um_registered_user_id' => $um_registered_user_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }
}
