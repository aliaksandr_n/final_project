<?php

/**
 * Class UserJournalDB
 */
class UserJournalDB extends MainDB {

    /**
     * @param $utj_registered_user_id
     * @return bool
     */
    public function selectUserTest($utj_registered_user_id) {

        $query = "SELECT `ru_name`, `ru_surname`, `utj_id`, `tr_name`, `utj_registered_user_id`, `tr_pause_test_permission`,
                      (SELECT COUNT(*)
                          FROM `user_question_journal`
                          WHERE `utj_id` = `uqj_user_test_journal_id`
                      ) AS all_test_sum,
                      (SELECT COUNT(*)
                          FROM `user_question_journal`
                          LEFT JOIN `user_question_status` ON `uqj_user_question_status_id`=`uqs_id`
                          WHERE `utj_id` = `uqj_user_test_journal_id` AND `uqs_name` = 'answered'
                      ) AS answered_test_sum
                  FROM `user_test_journal`
                  LEFT JOIN `appointed_test` ON `utj_appointed_test_id`=`at_id`
                  LEFT JOIN `test_repository` ON `at_test_repository_id`=`tr_id`
                  LEFT JOIN `registered_user` ON `utj_registered_user_id`=`ru_id`
                  WHERE `utj_registered_user_id` = :utj_registered_user_id";
        $array_for_query = ['utj_registered_user_id' => $utj_registered_user_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $utj_id
     * @return bool
     */
    public function selectTestInformation($utj_id) {

        $query = "SELECT `tr_pause_test_permission`, `tr_show_right_answer`, `tr_show_final_mark`, `tr_show_final_verdict`, `tr_success_value_type`, `tr_success_value_num`, `tr_skip_questions`, `tr_control_test_time_per_questions`, `tr_total_time_value`
                  FROM `user_test_journal`
                  LEFT JOIN `appointed_test` ON `utj_appointed_test_id`=`at_id`
                  LEFT JOIN `test_repository` ON `at_test_repository_id`=`tr_id`
                  WHERE `utj_id` = :utj_id";
        $array_for_query = ['utj_id' => $utj_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $uqj_user_test_journal_id
     * @return bool
     */
    public function selectActionQuestion($uqj_user_test_journal_id) {

        $query = "SELECT `uqj_id`, `uqj_answering_duration`, `uqj_last_answering_start`, `qs_id`, `qs_question_type_id`, `uqj_user_question_status_id`, `ugj_paused`, `qs_text`
                  FROM `user_question_journal`
                  JOIN `question_setting` ON `uqj_question_setting_id`=`qs_id`
                  WHERE `uqj_user_question_status_id` = '1' AND `uqj_user_test_journal_id`=:uqj_user_test_journal_id
                  ORDER BY `uqj_q_order_in_user_test`";
        $array_for_query = ['uqj_user_test_journal_id' => $uqj_user_test_journal_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $utj_id
     * @param $utj_registered_user_id
     * @return bool
     */
    public function findUserTestById($utj_id, $utj_registered_user_id) {

        $query = "SELECT *
                  FROM `user_test_journal`
                  WHERE `utj_id` = :utj_id AND `utj_registered_user_id` = :utj_registered_user_id";
        $array_for_query = ['utj_id' => $utj_id,
                            'utj_registered_user_id' => $utj_registered_user_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $uqj_user_test_journal_id
     * @return bool
     */
    public function selectActualQuestions($uqj_user_test_journal_id) {

        $query = "SELECT `uqs_name`, `uqj_id`, `uqj_last_answering_finish`
                  FROM `user_question_journal`
                  JOIN `user_question_status` ON `uqj_user_question_status_id`=`uqs_id`
                  WHERE `uqs_name` <> 'answered' AND `uqs_name` <> 'overtimed' AND `uqj_user_test_journal_id` = :uqj_user_test_journal_id
                  ORDER BY `uqj_q_order_in_user_test`";
        $array_for_query = ['uqj_user_test_journal_id' => $uqj_user_test_journal_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        if ($result->errorCode() <> '00000') {
            return false;
        }
        else {
            $array = $result->fetchAll(PDO::FETCH_GROUP);
            if ($array) {
                return $array;
            }
            else {
                return true;
            }
        }
    }

    /**
     * @param $uqj_id
     * @return bool
     */
    public function getActualQuestionInf($uqj_id) {

        $query = "SELECT * FROM `user_question_journal`
                  JOIN `question_setting` ON `uqj_question_setting_id`=`qs_id`
                  WHERE `uqj_id` = :uqj_id";
        $array_for_query = ['uqj_id' => $uqj_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $av_question_setting_id
     * @return bool
     */
    public function getActualAnswersInf($av_question_setting_id) {

        $query = "SELECT *
                  FROM `answer_variant`
                  WHERE `av_question_setting_id` = :av_question_setting_id
                  ORDER BY `av_order`";
        $array_for_query = ['av_question_setting_id' => $av_question_setting_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $qp_question_setting_id
     * @return bool
     */
    public function getPicturesArray($qp_question_setting_id) {

        $query = "SELECT *
                  FROM `question_picture`
                  WHERE `qp_question_setting_id` = :qp_question_setting_id
                  ORDER BY `qp_order_in_question`";
        $array_for_query = ['qp_question_setting_id' => $qp_question_setting_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $uqj_user_test_journal_id
     * @return bool
     */
    public function countAnswers($uqj_user_test_journal_id) {

        $query = "SELECT COUNT(`uqj_question_setting_id`) AS sum
                  FROM `user_question_journal`
                  WHERE `uqj_user_test_journal_id` = :uqj_user_test_journal_id";
        $array_for_query = ['uqj_user_test_journal_id' => $uqj_user_test_journal_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $uqj_user_test_journal_id
     * @return bool
     */
    public function countRightAnswers($uqj_user_test_journal_id) {

        $query = "SELECT COUNT(`uqj_question_setting_id`) AS sum
                  FROM `user_question_journal`
                  WHERE `uqj_user_test_journal_id` = :uqj_user_test_journal_id AND `uqj_answer_is_correct`='1'";
        $array_for_query = ['uqj_user_test_journal_id' => $uqj_user_test_journal_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $uqj_user_test_journal_id
     * @return bool
     */
    public function countAnsWeight($uqj_user_test_journal_id) {

        $query = "SELECT SUM(`qs_weight`) AS sum FROM `question_setting`
                  JOIN `user_question_journal` ON `qs_id`=`uqj_question_setting_id`
                  WHERE `uqj_user_test_journal_id` = :uqj_user_test_journal_id";
        $array_for_query = ['uqj_user_test_journal_id' => $uqj_user_test_journal_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $uqj_user_test_journal_id
     * @return bool
     */
    public function countRightAnsWeight($uqj_user_test_journal_id) {

        $query = "SELECT SUM(`qs_weight`) AS sum
                  FROM `question_setting`
                  JOIN `user_question_journal` ON `qs_id`=`uqj_question_setting_id`
                  WHERE `uqj_user_test_journal_id` = :uqj_user_test_journal_id AND `uqj_answer_is_correct`='1'";
        $array_for_query = ['uqj_user_test_journal_id' => $uqj_user_test_journal_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $tu_key
     * @return bool
     */
    public function findUserTestJournalId($tu_key) {

        $query = "SELECT `utj_id`
                  FROM `user_test_journal`
                  JOIN `temp_user` ON `utj_temp_user_id` = `tu_id`
                  WHERE `tu_key` = :tu_key";
        $array_for_query = ['tu_key' => $tu_key];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $uqj_user_test_journal_id
     * @return bool
     */
    public function selectUncomplitedTest($uqj_user_test_journal_id) {

        $query = "SELECT COUNT(*) AS sum
                  FROM `user_question_journal`
                  JOIN `user_question_status` ON `uqj_user_question_status_id`=`uqs_id`
                  WHERE `uqj_user_test_journal_id` = :uqj_user_test_journal_id AND `uqs_name` NOT IN ('answered', 'overtimed')";
        $array_for_query = ['uqj_user_test_journal_id' => $uqj_user_test_journal_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $utj_id
     * @return bool
     */
    public function getFinalTestResult($utj_id) {

        $query = "SELECT *
                  FROM `user_test_journal`
                  WHERE `utj_id` = :utj_id";
        $array_for_query = ['utj_id' => $utj_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $utj_id
     * @param $utj_finish_date
     * @param $utj_num_answers
     * @param $utj_num_right_answers
     * @param $utj_final_mark
     * @param $utj_final_verdict
     * @return bool
     */
    public function markTestAsCompleted($utj_id, $utj_finish_date, $utj_num_answers, $utj_num_right_answers, $utj_final_mark, $utj_final_verdict) {

        $query = "UPDATE `user_test_journal`
                  SET `utj_is_finished` = '1', `utj_finish_date` = :utj_finish_date, `utj_num_answers` = :utj_num_answers,
                  `utj_num_right_answers` = :utj_num_right_answers, `utj_final_mark` = :utj_final_mark,
                  `utj_final_verdict` = :utj_final_verdict
                  WHERE `utj_id`=:utj_id";
        $array_for_query = ['utj_id' => $utj_id,
                            'utj_finish_date' => $utj_finish_date,
                            'utj_num_answers' => $utj_num_answers,
                            'utj_num_right_answers' => $utj_num_right_answers,
                            'utj_final_mark' => $utj_final_mark,
                            'utj_final_verdict' => $utj_final_verdict];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $uqj_id
     * @param $uqj_last_answering_start
     * @param $ugj_paused
     * @param string $uqj_user_question_status_id
     * @return bool
     */
    public function markActualQuestion($uqj_id, $uqj_last_answering_start, $ugj_paused, $uqj_user_question_status_id = '1') {

        $query = "UPDATE `user_question_journal`
                  SET `uqj_user_question_status_id` = :uqj_user_question_status_id, `uqj_last_answering_start` = :uqj_last_answering_start, `ugj_paused` = :ugj_paused, `uqj_last_answering_finish` = '0'
                  WHERE `uqj_id`=:uqj_id";
        $array_for_query = ['uqj_id' => $uqj_id,
                            'uqj_user_question_status_id' => $uqj_user_question_status_id,
                            'uqj_last_answering_start' => $uqj_last_answering_start,
                            'ugj_paused' => $ugj_paused];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $uqj_id
     * @param $uqj_answering_duration
     * @param $uqj_last_answering_finish
     * @param $uqj_user_question_status_id
     * @param $ugj_paused
     * @param string $uqj_answer_is_correct
     * @return bool
     */
    public function changeQuestionJournalSetting($uqj_id, $uqj_answering_duration, $uqj_last_answering_finish, $uqj_user_question_status_id, $ugj_paused, $uqj_answer_is_correct = '0') {

        $query = "UPDATE `user_question_journal`
                  SET `uqj_user_question_status_id` = :uqj_user_question_status_id, `uqj_answering_duration` = :uqj_answering_duration, `uqj_last_answering_finish` = :uqj_last_answering_finish, `ugj_paused` = :ugj_paused, `uqj_answer_is_correct` = :uqj_answer_is_correct
                  WHERE `uqj_id` = :uqj_id";
        $array_for_query = ['uqj_id' => $uqj_id,
                            'uqj_answering_duration' => $uqj_answering_duration,
                            'uqj_last_answering_finish' => $uqj_last_answering_finish,
                            'ugj_paused' => $ugj_paused,
                            'uqj_user_question_status_id' => $uqj_user_question_status_id,
                            'uqj_answer_is_correct' => $uqj_answer_is_correct];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $uaj_user_question_journal_id
     * @param $uaj_answer_variant_id
     * @return bool
     */
    public function addUserAnswer($uaj_user_question_journal_id, $uaj_answer_variant_id) {

        $query = "INSERT IGNORE INTO `user_answer_journal` (`uaj_user_question_journal_id`, `uaj_answer_variant_id`)
                  VALUES (:uaj_user_question_journal_id, :uaj_answer_variant_id)";
        $array_for_query = ['uaj_user_question_journal_id' => $uaj_user_question_journal_id,
                            'uaj_answer_variant_id' => $uaj_answer_variant_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        if ($result->errorCode() === '00000') {
            return true;
        }
        else {
            return false;
        }
    }
}
