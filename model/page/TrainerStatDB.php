<?php

/**
 * Class TrainerStatDB
 */
class TrainerStatDB extends MainDB {

    /**
     * @return bool
     */
    public function getUserStat() {

        $query = "SELECT DISTINCT `ru_id` AS `ruid`, `ru_name`, `ru_surname`,
                    (SELECT COUNT(*)
                          FROM `user_test_journal`
                          WHERE `utj_registered_user_id` = `ruid`
                    ) AS tests_appointed,
                    (SELECT COUNT(*)
                          FROM `user_test_journal`
                          WHERE `utj_registered_user_id` = `ruid` AND `utj_is_finished` = '1'
                    ) AS passed_test,
                    (SELECT COUNT(*)
                          FROM `user_test_journal`
                          WHERE `utj_registered_user_id` = `ruid` AND `utj_is_finished` = '1' AND `utj_final_verdict` = '1'
                    ) AS number_of_finished_successfully
                  FROM `appointed_test`
                  JOIN `test_repository` ON `at_test_repository_id` = `tr_id`
                  JOIN `user_test_journal` ON `at_id` = `utj_appointed_test_id`
                  JOIN `registered_user` ON `utj_registered_user_id` = `ru_id`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @return bool
     */
    public function getTestStat() {

        $query = "SELECT DISTINCT `tr_id` AS `trid`, `tr_name`,
                    (SELECT COUNT(*)
                          FROM `user_test_journal`
                          JOIN `appointed_test` ON `utj_appointed_test_id` = `at_id`
                          JOIN `test_repository` ON `at_test_repository_id` = `tr_id`
                          WHERE `tr_id` = `trid`
                    ) AS number_of_appointments,
                    (SELECT COUNT(*)
                          FROM `user_test_journal`
                          JOIN `appointed_test` ON `utj_appointed_test_id` = `at_id`
                          JOIN `test_repository` ON `at_test_repository_id` = `tr_id`
                          WHERE `tr_id` = `trid` AND `utj_is_finished` = '1'
                    ) AS number_of_finished,
                    (SELECT COUNT(*)
                          FROM `user_test_journal`
                          JOIN `appointed_test` ON `utj_appointed_test_id` = `at_id`
                          JOIN `test_repository` ON `at_test_repository_id` = `tr_id`
                          WHERE `tr_id` = `trid` AND `utj_is_finished` = '1' AND `utj_final_verdict` = '1'
                    ) AS passed_successfully
                  FROM `test_repository`
                  JOIN `appointed_test` ON `at_test_repository_id` = `tr_id`
                  JOIN `user_test_journal` ON `at_id` = `utj_appointed_test_id`
                  JOIN `registered_user` ON `utj_registered_user_id` = `ru_id`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }
}
