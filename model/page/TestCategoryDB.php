<?php

/**
 * Class TestCategoryDB
 */
class TestCategoryDB extends MainDB {

    /**
     * @return bool
     */
    public function readTestCategory() {

        $query = "SELECT `tc_id`, `tc_name`, `tc_parent_id`
                  FROM `test_category`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $tc_id
     * @return bool
     */
    public function selectModifiedTestCategory($tc_id) {

        $query = "SELECT `tc_id`, `tc_name`, `tc_parent_id`
                  FROM `test_category`
                  WHERE `tc_id` = :tc_id";
        $array_for_query = ['tc_id' => $tc_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $tc_id
     * @return bool
     */
    public function findTestCategoryChild($tc_id) {

        $query = "SELECT `tc_id`, `tc_name`
                  FROM `test_category`
                  WHERE `tc_parent_id` = :tc_id";
        $array_for_query = ['tc_id' => $tc_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $tc_id
     * @param $tc_name
     * @param $tc_parent_id
     * @return bool
     */
    public function modifyTestCategory($tc_id, $tc_name, $tc_parent_id) {

        $query = "UPDATE `test_category`
                  SET `tc_name` = :tc_name, `tc_parent_id` = :tc_parent_id
                  WHERE `tc_id`=:tc_id";
        $array_for_query = ['tc_id' => $tc_id,
                            'tc_name' => $tc_name,
                            'tc_parent_id' => $tc_parent_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $tc_name
     * @param $tc_parent_id
     * @return bool|null
     */
    public function addTestCategory($tc_name, $tc_parent_id) {

        $query = "INSERT INTO `test_category` (`tc_name`, `tc_parent_id`)
                  VALUES (:tc_name, :tc_parent_id)";
        $array_for_query = ['tc_name' => $tc_name,
                            'tc_parent_id' => $tc_parent_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $tc_id
     * @return bool
     */
    public function deleteTestCategory($tc_id) {

        $query = "DELETE FROM `test_category`
                  WHERE `tc_id` = :tc_id";
        $array_for_query = ['tc_id' => $tc_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultDelete($result);
    }
}
