<?php

/**
 * Class UGroupMemberDB
 */
class UGroupMemberDB extends MainDB {

    /**
     * @return bool
     */
    public function selectAllRegisteredUsers() {

        $query = "SELECT *
                  FROM `registered_user`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @return bool
     */
    public function selectUserByAllGroup() {

        $query = "SELECT *
                  FROM `ugroup_member`
                  JOIN `registered_user` ON `um_registered_user_id`=`ru_id`
                  JOIN `user_group` ON `um_user_group_id`=`ug_id`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $um_user_group_id
     * @return bool
     */
    public function selectUserByOneGroup($um_user_group_id) {

        $query = "SELECT *
                  FROM `ugroup_member`
                  JOIN `registered_user` ON `um_registered_user_id`=`ru_id`
                  JOIN `user_group` ON `um_user_group_id`=`ug_id`
                  WHERE `um_user_group_id` = :um_user_group_id";
        $array_for_query = ['um_user_group_id' => $um_user_group_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $um_registered_user_id
     * @param $um_user_group_id
     * @return bool|null
     */
    public function addUserToUGroupMember($um_registered_user_id, $um_user_group_id) {

        $query = "INSERT INTO `ugroup_member` (`um_user_group_id`, `um_registered_user_id`)
                  VALUES (:um_user_group_id, :um_registered_user_id)";
        $array_for_query = ['um_user_group_id' => $um_user_group_id,
                            'um_registered_user_id' => $um_registered_user_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsertIgnoreDublicate($result);
    }

    /**
     * @param $um_id
     * @return bool
     */
    public function deleteUserFromUGroupMember($um_id) {

        $query = "DELETE FROM `ugroup_member`
                  WHERE `um_id` = :um_id";
        $array_for_query = ['um_id' => $um_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultDelete($result);
    }
}
