<?php

/**
 * Class RecoveryPassDB
 */
class RecoveryPassDB extends MainDB {
    /**
     * @param $recovery_key
     * @return bool
     */
    public function findRecoveryKey($recovery_key) {

        $query = "SELECT *
                  FROM `account_recovery_inf`
                  WHERE `ari_recovery_key` = :ari_recovery_key
                  ORDER BY `ari_recovery_date` DESC LIMIT 1";
        $array_for_query = ['ari_recovery_key' => $recovery_key];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $password
     * @param $user_id
     * @return bool
     */
    public function changeUserPassword($password, $user_id) {

        $query = "UPDATE `registered_user`
                  SET `ru_password` = :ru_password
                  WHERE `ru_id`=:ru_id";
        $array_for_query = ['ru_password' => $password,
                            'ru_id' => $user_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $recovery_row_id
     * @return bool
     */
    public function markKeyAsRecovered($recovery_row_id) {

        $query = "UPDATE `account_recovery_inf`
                  SET ari_recovered='1'
                  WHERE `ari_id`=:ari_id";
        $array_for_query = ['ari_id' => $recovery_row_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }
}
