<?php

/**
 * Class QuestionSettingDB
 */
class QuestionSettingDB extends MainDB {

    /**
     * @return bool
     */
    public function readQuestionType() {

        $query = "SELECT `qt_id`, `qt_name`
                  FROM `question_type`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $qcg_question_category_id
     * @return bool
     */
    public function selectQuestionsByCategory($qcg_question_category_id) {

        $query = "SELECT `qs_id`, `qc_name`, `qt_name`, `qs_text`, `qs_weight`, `qs_answer_time_duration`, `qs_show_random_answers`, `qs_adding_time`, `qs_author_user_id`
                  FROM `question_cat_group`
                  JOIN `question_setting` ON `qcg_question_setting_id` = `qs_id`
                  JOIN `question_type` ON `qs_question_type_id` = `qt_id`
                  JOIN `question_category` ON `qcg_question_category_id` = `qc_id`
                  WHERE `qcg_question_category_id` = :qcg_question_category_id";
        $array_for_query = ['qcg_question_category_id' => $qcg_question_category_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @return bool
     */
    public function selectAllQuestions() {

        $query = "SELECT `qs_id`, `qc_name`, `qt_name`, `qs_text`, `qs_weight`, `qs_answer_time_duration`, `qs_show_random_answers`, `qs_adding_time`, `qs_author_user_id`
                  FROM `question_cat_group`
                  JOIN `question_setting` ON `qcg_question_setting_id` = `qs_id`
                  JOIN `question_type` ON `qs_question_type_id` = `qt_id`
                  JOIN `question_category` ON `qcg_question_category_id` = `qc_id`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $qs_id
     * @return bool
     */
    public function selectOneQuestionSetting($qs_id) {

        $query = "SELECT *
                  FROM `question_setting`
                  WHERE `qs_id` = :qs_id";
        $array_for_query = ['qs_id' => $qs_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $qs_id
     * @return bool
     */
    public function selectAnswerVariants($qs_id) {

        $query = "SELECT *
                  FROM `answer_variant`
                  WHERE `av_question_setting_id` = :qs_id";
        $array_for_query = ['qs_id' => $qs_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $qs_id
     * @return bool
     */
    public function selectQuestionPictures($qs_id) {

        $query = "SELECT *
                  FROM `question_picture`
                  WHERE `qp_question_setting_id` = :qs_id";
        $array_for_query = ['qs_id' => $qs_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $qs_id
     * @return bool
     */
    public function selectQuestionGroup($qs_id) {

        $query = "SELECT *
                  FROM `question_cat_group`
                  WHERE `qcg_question_setting_id` = :qs_id";
        $array_for_query = ['qs_id' => $qs_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $qs_id
     * @param $qs_question_type_id
     * @param $qs_text
     * @param $qs_weight
     * @param $qs_answer_time_duration
     * @param $qs_show_random_answers
     * @param $qs_adding_time
     * @param $qs_author_user_id
     * @return bool
     */
    public function updateQuestion($qs_id, $qs_question_type_id, $qs_text, $qs_weight, $qs_answer_time_duration, $qs_show_random_answers, $qs_adding_time, $qs_author_user_id) {

        $query = "UPDATE `question_setting`
                  SET `qs_question_type_id`=:qs_question_type_id, `qs_text`=:qs_text, `qs_weight`=:qs_weight, `qs_answer_time_duration`=:qs_answer_time_duration, `qs_show_random_answers`=:qs_show_random_answers, `qs_adding_time`=:qs_adding_time, `qs_author_user_id`=:qs_author_user_id
                  WHERE `qs_id`=:qs_id ";
        $array_for_query = ['qs_question_type_id' => $qs_question_type_id,
                            'qs_text' => $qs_text,
                            'qs_weight' => $qs_weight,
                            'qs_answer_time_duration' => $qs_answer_time_duration,
                            'qs_show_random_answers' => $qs_show_random_answers,
                            'qs_adding_time' => $qs_adding_time,
                            'qs_author_user_id' => $qs_author_user_id,
                            'qs_id' => $qs_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $qcg_question_setting_id
     * @param $qcg_question_category_id
     * @return bool
     */
    public function updateQuestionGroup($qcg_question_setting_id, $qcg_question_category_id) {

        $query = "UPDATE `question_cat_group`
                  SET `qcg_question_category_id`=:qcg_question_category_id
                  WHERE `qcg_question_setting_id`=:qcg_question_setting_id";
        $array_for_query = ['qcg_question_setting_id' => $qcg_question_setting_id,
                            'qcg_question_category_id' => $qcg_question_category_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $qs_question_type_id
     * @param $qs_text
     * @param $qs_weight
     * @param $qs_answer_time_duration
     * @param $qs_show_random_answers
     * @param $qs_adding_time
     * @param $qs_author_user_id
     * @return bool|null
     */
    public function addQuestion($qs_question_type_id, $qs_text, $qs_weight, $qs_answer_time_duration, $qs_show_random_answers, $qs_adding_time, $qs_author_user_id) {

        $query = "INSERT INTO `question_setting` (`qs_question_type_id`, `qs_text`, `qs_weight`, `qs_answer_time_duration`, `qs_show_random_answers`, `qs_adding_time`, `qs_author_user_id`)
                  VALUES (:qs_question_type_id, :qs_text, :qs_weight, :qs_answer_time_duration, :qs_show_random_answers, :qs_adding_time, :qs_author_user_id)";
        $array_for_query = ['qs_question_type_id' => $qs_question_type_id,
                            'qs_text' => $qs_text,
                            'qs_weight' => $qs_weight,
                            'qs_answer_time_duration' => $qs_answer_time_duration,
                            'qs_show_random_answers' => $qs_show_random_answers,
                            'qs_adding_time' => $qs_adding_time,
                            'qs_author_user_id' => $qs_author_user_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $av_question_setting_id
     * @param $av_text
     * @param $av_correct
     * @param $av_order
     * @return bool|null
     */
    public function addAnswerVariant($av_question_setting_id, $av_text, $av_correct, $av_order) {

        $query = "INSERT INTO `answer_variant` (`av_question_setting_id`, `av_text`, `av_correct`, `av_order`)
                  VALUES (:av_question_setting_id, :av_text, :av_correct, :av_order)";
        $array_for_query = ['av_question_setting_id' => $av_question_setting_id,
                            'av_text' => $av_text,
                            'av_correct' => $av_correct,
                            'av_order' => $av_order];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $qcg_question_setting_id
     * @param $qcg_question_category_id
     * @return bool|null
     */
    public function addQuestionGroup($qcg_question_setting_id, $qcg_question_category_id) {

        $query = "INSERT INTO `question_cat_group` (`qcg_question_setting_id`, `qcg_question_category_id`)
                  VALUES (:qcg_question_setting_id, :qcg_question_category_id)";
        $array_for_query = ['qcg_question_setting_id' => $qcg_question_setting_id,
                            'qcg_question_category_id' => $qcg_question_category_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $qp_question_setting_id
     * @param $qp_order_in_question
     * @param $qp_name_in_db
     * @param $qp_origin_name
     * @param $qp_file_size
     * @return bool|null
     */
    public function addQuestionPictureDB($qp_question_setting_id, $qp_order_in_question, $qp_name_in_db, $qp_origin_name, $qp_file_size) {

        $query = "INSERT INTO `question_picture` (`qp_question_setting_id`, `qp_order_in_question`, `qp_name_in_db`, `qp_origin_name`, `qp_file_size`)
                  VALUES (:qp_question_setting_id, :qp_order_in_question, :qp_name_in_db, :qp_origin_name, :qp_file_size)";
        $array_for_query = ['qp_question_setting_id' => $qp_question_setting_id,
                            'qp_order_in_question' => $qp_order_in_question,
                            'qp_name_in_db' => $qp_name_in_db,
                            'qp_origin_name' => $qp_origin_name,
                            'qp_file_size' => $qp_file_size];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $av_question_setting_id
     * @return bool
     */
    public function deleteAnswerVariant($av_question_setting_id) {

        $query = "DELETE FROM `answer_variant`
                  WHERE `av_question_setting_id` = :av_question_setting_id";
        $array_for_query = ['av_question_setting_id' => $av_question_setting_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultDelete($result);
    }

    /**
     * @param $qs_id
     * @return bool
     */
    public function deleteQuestion($qs_id) {

        $query = "DELETE FROM `question_setting`
                  WHERE `qs_id` = :qs_id";
        $array_for_query = ['qs_id' => $qs_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultDelete($result);
    }
}
