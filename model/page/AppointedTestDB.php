<?php

/**
 * Class AppointedTestDB
 */
class AppointedTestDB extends MainDB {
    /**
     * @return bool
     */
    public function selectAllRegisteredUsers() {

        $query = "SELECT *
                  FROM `registered_user`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @return bool
     */
    public function selectUserByAllGroup() {

        $query = "SELECT *
                  FROM `ugroup_member`
                  JOIN `registered_user` ON `um_registered_user_id`=`ru_id`
                  JOIN `user_group` ON `um_user_group_id`=`ug_id`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $um_user_group_id
     * @return bool
     */
    public function selectUserByOneGroup($um_user_group_id) {

        $query = "SELECT *
                  FROM `ugroup_member`
                  JOIN `registered_user` ON `um_registered_user_id`=`ru_id`
                  JOIN `user_group` ON `um_user_group_id`=`ug_id`
                  WHERE `um_user_group_id` = :um_user_group_id";
        $array_for_query = ['um_user_group_id' => $um_user_group_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $trg_test_repository_id
     * @return bool
     */
    public function selectQuestionForTest($trg_test_repository_id) {

        $query = "SELECT `trg_question_setting_id`
                  FROM `test_repos_group`
                  WHERE `trg_test_repository_id` = :trg_test_repository_id";
        $array_for_query = ['trg_test_repository_id' => $trg_test_repository_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $um_registered_user_id
     * @param $um_user_group_id
     * @return bool|null
     */
    public function addUserToUGroupMember($um_registered_user_id, $um_user_group_id) {

        $query = "INSERT INTO `ugroup_member` (`um_user_group_id`, `um_registered_user_id`)
                  VALUES (:um_user_group_id, :um_registered_user_id)";
        $array_for_query = ['um_user_group_id' => $um_user_group_id,
                            'um_registered_user_id' => $um_registered_user_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsertIgnoreDublicate($result);
    }

    /**
     * @param $at_test_repository_id
     * @param $at_creating_time
     * @param $at_creator_regestered_user_id
     * @return bool|null
     */
    public function addAppointedTest($at_test_repository_id, $at_creating_time, $at_creator_regestered_user_id) {

        $query = "INSERT INTO `appointed_test` (`at_test_repository_id`, `at_creating_time`, `at_creator_regestered_user_id`)
                  VALUES (:at_test_repository_id, :at_creating_time, :at_creator_regestered_user_id)";
        $array_for_query = ['at_test_repository_id' => $at_test_repository_id,
                            'at_creating_time' => $at_creating_time,
                            'at_creator_regestered_user_id' => $at_creator_regestered_user_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $tu_key
     * @param $tu_comment
     * @return bool|null
     */
    public function addTempKey($tu_key, $tu_comment) {

        $query = "INSERT INTO `temp_user` (`tu_key`, `tu_comment`)
                  VALUES (:tu_key, :tu_comment)";
        $array_for_query = ['tu_key' => $tu_key,
                            'tu_comment' => $tu_comment];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $utj_appointed_test_id
     * @param null $utj_registered_user_id
     * @param null $utj_temp_user_id
     * @return bool|null
     */
    public function addTestUserJournal($utj_appointed_test_id, $utj_registered_user_id = null, $utj_temp_user_id = null) {

        $query = "INSERT INTO `user_test_journal` (`utj_appointed_test_id`, `utj_registered_user_id`, `utj_temp_user_id`)
                  VALUES (:utj_appointed_test_id, :utj_registered_user_id, :utj_temp_user_id)";
        $array_for_query = ['utj_appointed_test_id' => $utj_appointed_test_id,
                            'utj_registered_user_id' => $utj_registered_user_id,
                            'utj_temp_user_id' => $utj_temp_user_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsertIgnoreDublicate($result);
    }

    /**
     * @param $uqj_user_test_journal_id
     * @param $uqj_question_setting_id
     * @param $uqj_q_order_in_user_test
     * @return bool|null
     */
    public function addQuestionJournal($uqj_user_test_journal_id, $uqj_question_setting_id, $uqj_q_order_in_user_test) {

        $query = "INSERT INTO `user_question_journal` (`uqj_user_test_journal_id`, `uqj_question_setting_id`, `uqj_q_order_in_user_test`)
                  VALUES (:uqj_user_test_journal_id, :uqj_question_setting_id, :uqj_q_order_in_user_test)";
        $array_for_query = ['uqj_user_test_journal_id' => $uqj_user_test_journal_id,
                            'uqj_question_setting_id' => $uqj_question_setting_id,
                            'uqj_q_order_in_user_test' => $uqj_q_order_in_user_test];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $um_id
     * @return bool
     */
    public function deleteUserFromUGroupMember($um_id) {

        $query = "DELETE FROM `ugroup_member`
                  WHERE `um_id` = :um_id";
        $array_for_query = ['um_id' => $um_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultDelete($result);
    }
}
