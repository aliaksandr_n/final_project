<?php

/**
 * Class TempUserDB
 */
class TempUserDB extends MainDB {

    /**
     * @return bool
     */
    public function readTempUser() {

        $query = "SELECT *
                  FROM `temp_user`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $tu_id
     * @return bool
     */
    public function selectModifiedTempUser($tu_id) {

        $query = "SELECT *
                  FROM `temp_user`
                  WHERE `tu_id` = :tu_id";
        $array_for_query = ['tu_id' => $tu_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $tu_id
     * @param $tu_key
     * @param $tu_comment
     * @return bool
     */
    public function modifyTempUser($tu_id, $tu_key, $tu_comment) {

        $query = "UPDATE `temp_user`
                  SET `tu_key` = :tu_key, `tu_comment` = :tu_comment
                  WHERE `tu_id`=:tu_id";
        $array_for_query = ['tu_id' => $tu_id,
                            'tu_key' => $tu_key,
                            'tu_comment' => $tu_comment];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $tu_key
     * @param $tu_comment
     * @return bool|null
     */
    public function addTempUser($tu_key, $tu_comment) {

        $query = "INSERT INTO `temp_user` (`tu_key`, `tu_comment`)
                  VALUES (:tu_key, :tu_comment)";
        $array_for_query = ['tu_key' => $tu_key,
                            'tu_comment' => $tu_comment];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $tu_id
     * @return bool
     */
    public function deleteTempUser($tu_id) {

        $query = "DELETE FROM `temp_user`
                  WHERE `tu_id` = :tu_id";
        $array_for_query = ['tu_id' => $tu_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultDelete($result);
    }
}
