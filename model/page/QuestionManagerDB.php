<?php

/**
 * Class QuestionManagerDB
 */
class QuestionManagerDB extends MainDB {

    /**
     * @return bool
     */
    public function selectTestArray() {

        $query = "SELECT `tr_id`, `tr_name`, `tc_name`
                  FROM `test_cat_group`
                  JOIN `test_repository` ON `tcg_test_repository_id`=`tr_id`
                  JOIN `test_category` ON `tcg_test_category_id`=`tc_id`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @return bool
     */
    public function selectQuestionByAllCategory() {

        $query = "SELECT *
                  FROM `question_cat_group`
                  JOIN `question_setting` ON `qcg_question_setting_id` = `qs_id`
                  JOIN `question_category` ON `qcg_question_category_id` = `qc_id`
                  ORDER BY `qcg_question_category_id`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $qcg_question_category_id
     * @return bool
     */
    public function selectQuestionByOneCategory($qcg_question_category_id) {

        $query = "SELECT *
                  FROM `question_cat_group`
                  JOIN `question_setting` ON `qcg_question_setting_id` = `qs_id`
                  JOIN `question_category` ON `qcg_question_category_id` = `qc_id`
                  WHERE `qcg_question_category_id` = :qcg_question_category_id";
        $array_for_query = ['qcg_question_category_id' => $qcg_question_category_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @return bool
     */
    public function selectQuestionByAllRepos() {

        $query = "SELECT *
                  FROM `question_repos_group`
                  JOIN `question_setting` ON `qrg_question_setting_id` = `qs_id`
                  JOIN `question_repository` ON `qrg_question_repository_id` = `qr_id`
                  ORDER BY `qrg_question_repository_id`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $qrg_question_repository_id
     * @return bool
     */
    public function selectQuestionByOneRepos($qrg_question_repository_id) {

        $query = "SELECT *
                  FROM `question_repos_group`
                  JOIN `question_setting` ON `qrg_question_setting_id` = `qs_id`
                  JOIN `question_repository` ON `qrg_question_repository_id` = `qr_id`
                  WHERE `qrg_question_repository_id` = :qrg_question_repository_id";
        $array_for_query = ['qrg_question_repository_id' => $qrg_question_repository_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @return bool
     */
    public function selectQuestionByAllTest() {

        $query = "SELECT *
                  FROM `test_repos_group`
                  JOIN `question_setting` ON `trg_question_setting_id` = `qs_id`
                  JOIN `test_repository` ON `trg_test_repository_id` = `tr_id`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $trg_test_repository_id
     * @return bool
     */
    public function selectQuestionByOneTest($trg_test_repository_id) {

        $query = "SELECT *
                  FROM `test_repos_group`
                  JOIN `question_setting` ON `trg_question_setting_id` = `qs_id`
                  JOIN `test_repository` ON `trg_test_repository_id` = `tr_id`
                  WHERE `trg_test_repository_id` = :trg_test_repository_id";
        $array_for_query = ['trg_test_repository_id' => $trg_test_repository_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $tr_id
     * @return bool
     */
    public function getTestTimeControlSetting($tr_id) {

        $query = "SELECT `tr_control_test_time_per_questions`
                  FROM `test_repository`
                  WHERE `tr_id` = :tr_id";
        $array_for_query = ['tr_id' => $tr_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        if ($result->errorCode() === '00000') {
            $array = $result->fetchAll(PDO::FETCH_ASSOC);
            if (!empty($array)) {
                return $array[0]['tr_control_test_time_per_questions'];
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    /**
     * @param $qs_id
     * @return bool
     */
    public function getQuestionTimeDuration($qs_id) {

        $query = "SELECT `qs_answer_time_duration`
                  FROM `question_setting`
                  WHERE `qs_id` = :qs_id";
        $array_for_query = ['qs_id' => $qs_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        if ($result->errorCode() === '00000') {
            $array = $result->fetchAll(PDO::FETCH_ASSOC);
            return $array[0]['qs_answer_time_duration'];
        }
        else {
            return false;
        }
    }

    /**
     * @param $qrg_question_setting_id
     * @param $qrg_question_repository_id
     * @return bool|null
     */
    public function addQuestionToRepository($qrg_question_setting_id, $qrg_question_repository_id) {

        $query = "INSERT INTO `question_repos_group` (`qrg_question_setting_id`, `qrg_question_repository_id`)
                  VALUES (:qrg_question_setting_id, :qrg_question_repository_id)";
        $array_for_query = ['qrg_question_setting_id' => $qrg_question_setting_id,
                            'qrg_question_repository_id' => $qrg_question_repository_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsertIgnoreDublicate($result);
    }

    /**
     * @param $trg_question_setting_id
     * @param $trg_test_repository_id
     * @return bool|null
     */
    public function addQuestionToTest($trg_question_setting_id, $trg_test_repository_id) {

        $query = "INSERT INTO `test_repos_group` (`trg_question_setting_id`, `trg_test_repository_id`)
                  VALUES (:trg_question_setting_id, :trg_test_repository_id)";
        $array_for_query = ['trg_question_setting_id' => $trg_question_setting_id,
                            'trg_test_repository_id' => $trg_test_repository_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsertIgnoreDublicate($result);
    }

    /**
     * @param $qcg_id
     * @return bool
     */
    public function deleteQuestionFromQuestionCategory($qcg_id) {

        $query = "DELETE FROM `question_cat_group`
                  WHERE `qcg_id` = :qcg_id";
        $array_for_query = ['qcg_id' => $qcg_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultDelete($result);
    }

    /**
     * @param $qrg_id
     * @return bool
     */
    public function deleteQuestionFromQuestionRepository($qrg_id) {

        $query = "DELETE FROM `question_repos_group`
                  WHERE `qrg_id` = :qrg_id";
        $array_for_query = ['qrg_id' => $qrg_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultDelete($result);
    }

    /**
     * @param $trg_id
     * @return bool
     */
    public function deleteQuestionFromTestRepository($trg_id) {

        $query = "DELETE FROM `test_repos_group`
                  WHERE `trg_id` = :trg_id";
        $array_for_query = ['trg_id' => $trg_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultDelete($result);
    }
}
