<?php

/**
 * Class TestRepositoryDB
 */
class TestRepositoryDB extends MainDB {

    /**
     * @return bool
     */
    public function selectTest() {

        $query = "SELECT *
                  FROM `test_repository`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $tcg_test_category_id
     * @return bool
     */
    public function selectTestsByCategory($tcg_test_category_id) {

        $query = "SELECT *
                  FROM `test_cat_group`
                  JOIN `test_repository` ON `tcg_test_repository_id` = `tr_id`
                  JOIN `test_category` ON `tcg_test_category_id` = `tc_id`
                  WHERE `tcg_test_category_id` = :tcg_test_category_id";
        $array_for_query = ['tcg_test_category_id' => $tcg_test_category_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @return bool
     */
    public function selectAllTests() {

        $query = "SELECT *
                  FROM `test_cat_group`
                  JOIN `test_repository` ON `tcg_test_repository_id` = `tr_id`
                  JOIN `test_category` ON `tcg_test_category_id` = `tc_id`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $tr_id
     * @return bool
     */
    public function selectTestById($tr_id) {

        $query = "SELECT *
                  FROM `test_repository`
                  WHERE `tr_id` = :tr_id";
        $array_for_query = ['tr_id' => $tr_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $tr_id
     * @return bool
     */
    public function selectTestCatGroup($tr_id) {

        $query = "SELECT *
                  FROM `test_cat_group`
                  WHERE `tcg_test_repository_id` = :tr_id";
        $array_for_query = ['tr_id' => $tr_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $trg_test_repository_id
     * @return bool
     */
    public function checkAnswerTime($trg_test_repository_id) {

        $query = "SELECT `qs_id`, `qs_text`, `qs_answer_time_duration`
                  FROM `test_repos_group`
                  JOIN `question_setting` ON `trg_question_setting_id`=`qs_id`
                  WHERE `trg_test_repository_id` = :trg_test_repository_id AND `qs_answer_time_duration`=0";
        $array_for_query = ['trg_test_repository_id' => $trg_test_repository_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        if ($result->errorCode() <> '00000') {
            return false;
        }
        else {
            $array = $result->fetchAll(PDO::FETCH_ASSOC);
            if (is_array($array) && !empty($array)) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    /**
     * @param $tr_id
     * @param $tr_name
     * @param $tr_pause_test_permission
     * @param $tr_show_right_answer
     * @param $tr_show_final_mark
     * @param $tr_show_final_verdict
     * @param $tr_number_shown_questions
     * @param $tr_success_value_type
     * @param $tr_success_value_num
     * @param $tr_skip_questions
     * @param $tr_control_test_time_per_questions
     * @param $tr_total_time_value
     * @param $tr_author_id
     * @param $tr_adding_time
     * @return bool
     */
    public function updateTest(
        $tr_id, $tr_name, $tr_pause_test_permission, $tr_show_right_answer, $tr_show_final_mark, $tr_show_final_verdict, $tr_number_shown_questions, $tr_success_value_type, $tr_success_value_num, $tr_skip_questions, $tr_control_test_time_per_questions, $tr_total_time_value, $tr_author_id, $tr_adding_time) {
        $query = "UPDATE `test_repository`
                  SET `tr_name`=:tr_name, `tr_pause_test_permission`=:tr_pause_test_permission, `tr_show_right_answer`=:tr_show_right_answer, `tr_show_final_mark`=:tr_show_final_mark, `tr_show_final_verdict`=:tr_show_final_verdict, `tr_number_shown_questions`=:tr_number_shown_questions, `tr_success_value_type`=:tr_success_value_type, `tr_success_value_num`=:tr_success_value_num, `tr_skip_questions`=:tr_skip_questions, `tr_control_test_time_per_questions`=:tr_control_test_time_per_questions, `tr_total_time_value`=:tr_total_time_value, `tr_author_id`=:tr_author_id, `tr_adding_time`=:tr_adding_time
                  WHERE `tr_id`=:tr_id";
        $array_for_query = ['tr_name' => $tr_name,
                            'tr_pause_test_permission' => $tr_pause_test_permission,
                            'tr_show_right_answer' => $tr_show_right_answer,
                            'tr_show_final_mark' => $tr_show_final_mark,
                            'tr_show_final_verdict' => $tr_show_final_verdict,
                            'tr_number_shown_questions' => $tr_number_shown_questions,
                            'tr_success_value_type' => $tr_success_value_type,
                            'tr_success_value_num' => $tr_success_value_num,
                            'tr_skip_questions' => $tr_skip_questions,
                            'tr_control_test_time_per_questions' => $tr_control_test_time_per_questions,
                            'tr_total_time_value' => $tr_total_time_value,
                            'tr_author_id' => $tr_author_id,
                            'tr_adding_time' => $tr_adding_time,
                            'tr_id' => $tr_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $tcg_test_category_id
     * @param $tcg_test_repository_id
     * @return bool
     */
    public function updateTestGroup($tcg_test_category_id, $tcg_test_repository_id) {

        $query = "UPDATE `test_cat_group`
                  SET `tcg_test_category_id`=:tcg_test_category_id
                  WHERE `tcg_test_repository_id`=:tcg_test_repository_id";
        $array_for_query = ['tcg_test_category_id' => $tcg_test_category_id,
                            'tcg_test_repository_id' => $tcg_test_repository_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $tr_name
     * @param $tr_pause_test_permission
     * @param $tr_show_right_answer
     * @param $tr_show_final_mark
     * @param $tr_show_final_verdict
     * @param $tr_number_shown_questions
     * @param $tr_success_value_type
     * @param $tr_success_value_num
     * @param $tr_skip_questions
     * @param $tr_control_test_time_per_questions
     * @param $tr_total_time_value
     * @param $tr_author_id
     * @param $tr_adding_time
     * @return bool|null
     */
    public function addTest($tr_name, $tr_pause_test_permission, $tr_show_right_answer, $tr_show_final_mark, $tr_show_final_verdict, $tr_number_shown_questions, $tr_success_value_type, $tr_success_value_num, $tr_skip_questions, $tr_control_test_time_per_questions, $tr_total_time_value, $tr_author_id,$tr_adding_time) {

        $query = "INSERT INTO `test_repository` (`tr_name`, `tr_pause_test_permission`, `tr_show_right_answer`, `tr_show_final_mark`, `tr_show_final_verdict`, `tr_number_shown_questions`, `tr_success_value_type`, `tr_success_value_num`, `tr_skip_questions`, `tr_control_test_time_per_questions`, `tr_total_time_value`, `tr_author_id`, `tr_adding_time`)
                  VALUES (:tr_name, :tr_pause_test_permission, :tr_show_right_answer, :tr_show_final_mark, :tr_show_final_verdict, :tr_number_shown_questions, :tr_success_value_type, :tr_success_value_num, :tr_skip_questions, :tr_control_test_time_per_questions, :tr_total_time_value, :tr_author_id, :tr_adding_time)";
        $array_for_query = ['tr_name' => $tr_name,
                            'tr_pause_test_permission' => $tr_pause_test_permission,
                            'tr_show_right_answer' => $tr_show_right_answer,
                            'tr_show_final_mark' => $tr_show_final_mark,
                            'tr_show_final_verdict' => $tr_show_final_verdict,
                            'tr_number_shown_questions' => $tr_number_shown_questions,
                            'tr_success_value_type' => $tr_success_value_type,
                            'tr_success_value_num' => $tr_success_value_num,
                            'tr_skip_questions' => $tr_skip_questions,
                            'tr_control_test_time_per_questions' => $tr_control_test_time_per_questions,
                            'tr_total_time_value' => $tr_total_time_value,
                            'tr_author_id' => $tr_author_id,
                            'tr_adding_time' => $tr_adding_time];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $tcg_test_repository_id
     * @param $tcg_test_category_id
     * @return bool|null
     */
    public function addCategoryToTest($tcg_test_repository_id, $tcg_test_category_id) {

        $query = "INSERT INTO `test_cat_group` (`tcg_test_repository_id`, `tcg_test_category_id`)
                  VALUES (:tcg_test_repository_id, :tcg_test_category_id)";
        $array_for_query = ['tcg_test_repository_id' => $tcg_test_repository_id,
                            'tcg_test_category_id' => $tcg_test_category_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $tr_id
     * @return bool
     */
    public function deleteTest($tr_id) {

        $query = "DELETE FROM `test_repository`
                  WHERE `tr_id` = :tr_id";
        $array_for_query = ['tr_id' => $tr_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultDelete($result);
    }
}
