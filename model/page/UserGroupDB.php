<?php

/**
 * Class UserGroupDB
 */
class UserGroupDB extends MainDB {

    /**
     * @return bool
     */
    public function readUserGroup() {

        $query = "SELECT *
                  FROM `user_group`";
        $result = $this->objConnectionToDB->executeQuery($query);
        return $this->prepareResultSelect($result);
    }

    /**
     * @param $ug_id
     * @return bool
     */
    public function selectModifiedUserGroup($ug_id) {

        $query = "SELECT *
                  FROM `user_group`
                  WHERE `ug_id` = :ug_id";
        $array_for_query = ['ug_id' => $ug_id];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $ug_id
     * @param $ug_name
     * @param $ug_description
     * @return bool
     */
    public function modifyUserGroup($ug_id, $ug_name, $ug_description) {

        $query = "UPDATE `user_group`
                  SET `ug_name` = :ug_name, `ug_description` = :ug_description
                  WHERE `ug_id`=:ug_id";
        $array_for_query = ['ug_id' => $ug_id,
                            'ug_name' => $ug_name,
                            'ug_description' => $ug_description];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * @param $ug_name
     * @param $ug_description
     * @param $ug_author_user_id
     * @return bool|null
     */
    public function addUserGroup($ug_name, $ug_description, $ug_author_user_id) {

        $query = "INSERT INTO `user_group` (`ug_name`, `ug_description`, `ug_author_user_id`)
                  VALUES (:ug_name, :ug_description, :ug_author_user_id)";
        $array_for_query = ['ug_name' => $ug_name,
                            'ug_description' => $ug_description,
                            'ug_author_user_id' => $ug_author_user_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }

    /**
     * @param $ug_id
     * @return bool
     */
    public function deleteUserGroup($ug_id) {

        $query = "DELETE FROM `user_group`
                  WHERE `ug_id` = :ug_id";
        $array_for_query = ['ug_id' => $ug_id];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultDelete($result);
    }
}
