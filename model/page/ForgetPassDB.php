<?php

/**
 * Class ForgetPassDB
 */
class ForgetPassDB extends MainDB {

    /**
     * @param $ru_email
     * @return bool
     */
    public function findRecoveryEmail($ru_email) {

        $query = "SELECT *
                  FROM `registered_user`
                  WHERE `ru_email` = :ru_email";
        $array_for_query = ['ru_email' => $ru_email];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * @param $registered_user_id
     * @param $recovery_key
     * @param $recovery_date
     * @return bool|null
     */
    public function registerRecoveryKey($registered_user_id, $recovery_key, $recovery_date) {

        $query = "INSERT INTO `account_recovery_inf` (`ari_registered_user_id`, `ari_recovery_key`, `ari_recovery_date`, `ari_recovered`)
                  VALUES (:ari_registered_user_id, :ari_recovery_key, :ari_recovery_date, '0')";
        $array_for_query = ['ari_registered_user_id' => $registered_user_id,
                            'ari_recovery_key' => $recovery_key,
                            'ari_recovery_date' => $recovery_date];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultInsert($result);
    }
}
