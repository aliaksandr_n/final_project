<?php

/**
 * Class LoginDB
 */
class LoginDB extends MainDB {

    /**
     * The function check user's login and password in database
     * @param $ru_login
     * @param $ru_password
     * @return bool
     */
    public function checkLoginAndPassInDB($ru_login, $ru_password) {

        $query = "SELECT `ru_name`, `ru_surname`, `ru_login`, `ru_id`, `ru_user_right`
                  FROM `registered_user`
                  WHERE `ru_login`=:ru_login AND `ru_password`=:ru_password";
        $array_for_query = ['ru_login' => $ru_login,
                            'ru_password' => $ru_password];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * The method for checking user's cookies in database
     * @param $ru_auth_key
     * @return bool
     */
    public function findCookieInDb($ru_auth_key) {

        $query = "SELECT `ru_name`, `ru_surname`, `ru_login`, `ru_id`, `ru_user_right`
                  FROM `registered_user`
                  WHERE `ru_auth_key`=:ru_auth_key";
        $array_for_query = ['ru_auth_key' => $ru_auth_key];
        $result= $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultSelectFirstIndex($result);
    }

    /**
     * The function writes key into the database, if the user wants to save his authorization for some time
     * @param $ru_auth_key
     * @param $ru_login
     * @param $ru_password
     * @return bool
     */
    public function rememberUserInDb($ru_auth_key, $ru_login, $ru_password) {

        $query = "UPDATE `registered_user`
                  SET `ru_auth_key`=:ru_auth_key
                  WHERE `ru_login`=:ru_login AND `ru_password`=:ru_password";
        $array_for_query = ['ru_auth_key' => $ru_auth_key,
                            'ru_login' => $ru_login,
                            'ru_password' => $ru_password];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

    /**
     * The function deletes key from the database, if the user wanted to save his authorization for some time and now logs out
     * @param $ru_login
     * @return bool
     */
    public function deleteAuthInDb($ru_login) {

        $query = "UPDATE `registered_user`
                  SET `ru_auth_key`=''
                  WHERE `ru_login`=:ru_login";
        $array_for_query = ['ru_login' => $ru_login];
        $result = $this->objConnectionToDB->executeQuery($query, $array_for_query);
        return $this->prepareResultUpdate($result);
    }

}
