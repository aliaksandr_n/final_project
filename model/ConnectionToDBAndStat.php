<?php
/**
 * This class is used for connection to database and gathering queries' statistic
 *
 * Class ConnectionToDBAndStat
 */
class ConnectionToDBAndStat {
    /**
     * The property, which contains the connection to database
     *
     * @var null
     */
    private $connection = null;
    /**
     * The property with the list of queries, and their time length
     * @var array
     *
     */
    protected $queryList = [];
    /**
     * The property with number of queries, total queries time, min and max-time queries
     * @var array
     */
    protected $queryStat = array('num_of_queries' => 0, 'total_time' => 0, 'min_time' => 0, 'max_time' => 0);
    /**
     * The method creates the connection to database
     * @var null
     */
    protected $lastInsertID = null;

    public static function getInstance() {
        static $instance = null;
        if(is_null($instance)) {
            $instance = new self();
            $instance->establishConnection();
        }
        return $instance;
    }

    private function __construct(){
    }

    private function __clone(){
    }


    public function __destruct()
    {
        if (!is_null($this->connection)) {
            $this->connection = null;
        }
    }

    private function establishConnection() {
        $this->connection = new PDO('mysql:host='.HOST.';dbname='.DB_NAME, DB_USER, DB_PASSWORD);//, array(PDO::ATTR_PERSISTENT => true));
                         //('mysql:dbname='.CONFIG_DB_BASE.';host='.CONFIG_DB_HOST.';port='.CONFIG_DB_PORT.';charset='.CONFIG_DB_CHAR, CONFIG_DB_USER, CONFIG_DB_PASS);
        $this->connection->exec("set names utf8");
    }
    /**
     * The "getter" of the object's property "queryList"
     * @return array
     */
    public function getQueryList() {
        return $this->queryList;
    }
    /**
     * The "getter" of the object's property "queryStat"
     *
     * @return array
     */
    public function getQueryStat() {
        return $this->queryStat;
    }
    /**
     * The "getter" of the object's property "queryStat"
     * @return null
     */
    public function getLastInsertID() {
        return $this->lastInsertID ;
    }
    /**
     * The function for gathering queries and their properties into array
     * @param $time_length
     * @param $log_query_text
     * @param $class_and_function
     */
    public function setQueryList($time_length, $log_query_text, $class_and_function) {
        $i = count($this->queryList);
        $this->queryList[$i] = array('time_length'=> $time_length, 'log_query_text'=> $log_query_text, 'class_and_function'=> $class_and_function);
    }
    /**
     * The function for gathering queries stat into array (number of queries, total queries time, min and max-time queries)
     * @param $time_length
     */
    public function setQueryStat($time_length) {
        //The counter for gueries
        $i = count($this->queryList);
        //Number of queries
        $this->queryStat['num_of_queries'] = $i;
        //Total queries time
        $this->queryStat['total_time'] += $time_length;
        //Min-time query
        if ($this->queryStat['min_time'] == 0) {
            $this->queryStat['min_time'] = $time_length;
        }
        elseif ($this->queryStat['min_time'] > $time_length) {
            $this->queryStat['min_time'] = $time_length;
        }
        //Max-time query
        if ($this->queryStat['max_time'] == 0) {
            $this->queryStat['max_time'] = $time_length;
        }
        elseif ($this->queryStat['max_time'] < $time_length) {
            $this->queryStat['max_time'] = $time_length;
        }
    }
    /**
     * The function for executing queries with gathering the queries' stat
     * @param $query
     * @param array $array_for_query
     * @return mixed
     */
    public function executeQuery($query, $array_for_query = []) {
        //Main action***********************
        $result = $this->connection->prepare($query);

        //The query's text
        $log_query_text = $result->queryString;
        //Start time
        $log_query_start_time = microtime(true);

        //Main action- executing the query***********************
/*
        foreach ($array_for_query as $key => $value) {
            if (!isset($value[2])) {
                $value[2] = gettype($value[1]);
                $array_for_query[$key] = $value;
            }
        }
        foreach ($array_for_query as $v) {
            $type = '';
            switch ($v[2]) {
                case 'boolean': $type = PDO::PARAM_BOOL; break;
                case 'integer': $type = PDO::PARAM_INT; break;
                case 'double': $type = PDO::PARAM_STR; break;
                case 'number': $type = PDO::PARAM_STR; break;
                case 'null': $type = PDO::PARAM_NULL; break;
                case 'lob': $type = PDO::PARAM_LOB; break;
                case 'string': $type = PDO::PARAM_STR; break;
                case 'string': $type = PDO::PARAM_NULL; break;
                default: throw new Exception('Query error. Unsupported data type. Usually this means that you\'ve forgotten to add extra array to query parameters.');
            }
            $result->bindValue($v[0], $v[1], $type);
        }
        $result->execute();
*/
        $result->execute($array_for_query);
        $this->lastInsertID = $this->connection->lastInsertId();
        //Finish time
        $log_query_finish_time = microtime(true);
        //The query's legth
        $time_length = $log_query_finish_time - $log_query_start_time;
        //The query's class and function
        $class_and_function = 'Class: '.__CLASS__." -> function: ".__FUNCTION__;
        //Sending the information into the handle functions
        $this->setQueryList($time_length, $log_query_text, $class_and_function);
        $this->setQueryStat($time_length);
        return $result;
    }
}
