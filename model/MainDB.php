<?php

/**
 * Class MainDB
 */
class MainDB {

    /**
     * @var ConnectionToDBAndStat|null
     */
    protected $objConnectionToDB = null;

    /**
     *
     */
    public function __construct() {

        $this->objConnectionToDB = ConnectionToDBAndStat::getInstance();
    }

    /**
     * @param $result
     * @return bool
     */
    protected function prepareResultSelect($result) {

        if ($result->errorCode() <> '00000') {
            return false;
        }
        else {
            $array = $result->fetchAll(PDO::FETCH_ASSOC);
            if ($array) {
                return $array;
            }
            else {
                return true;
            }
        }
    }

    /**
     * @param $result
     * @return bool
     */
    protected function prepareResultSelectFirstIndex($result) {

        if ($result->errorCode() <> '00000') {
            return false;
        }
        else {
            $array = $result->fetchAll(PDO::FETCH_ASSOC);
            if ($array) {
                return $array[0];
            }
            else {
                return true;
            }
        }
    }

    /**
     * @param $result
     * @return bool
     */
    protected function prepareResultUpdate($result) {

        if ($result->errorCode() === '00000') {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @param $result
     * @return bool|null
     */
    protected function prepareResultInsert($result) {
        if ($result->errorCode() === '00000') {
            return $this->objConnectionToDB->getLastInsertID();
        }
        else {
            return false;
        }
    }

    /**
     * @param $result
     * @return bool|null
     */
    protected function prepareResultInsertIgnoreDublicate($result) {

        if ($result->errorCode() === '00000'){
            return $this->objConnectionToDB->getLastInsertID();
        }
        elseif ($result->errorCode() === '23000') {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @param $result
     * @return bool
     */
    protected function prepareResultDelete($result) {

        if ($result->errorCode() === '00000') {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public  function getRuLabelsForTpl() {

        $query = "SELECT `l_name`, `l_ru_value`
                  FROM `label`";
        $result = $this->objConnectionToDB->executeQuery($query);
        if ($result->errorCode() <> '00000') {
            return false;
        }
        else {
            $array = $result->fetchAll(PDO::FETCH_KEY_PAIR|PDO::FETCH_GROUP);
            if ($array) {
                return $array;
            }
            else {
                return true;
            }
        }
    }

    /**
     * @return bool
     */
    public  function getEngLabelsForTpl() {

        $query = "SELECT `l_name`, `l_eng_value`
                  FROM `label`";
        $result = $this->objConnectionToDB->executeQuery($query);
        if ($result->errorCode() <> '00000') {
            return false;
        }
        else {
            $array = $result->fetchAll(PDO::FETCH_KEY_PAIR|PDO::FETCH_GROUP);
            if ($array) {
                return $array;
            }
            else {
                return true;
            }
        }
    }
}
