<?php
// The constant with host's name for connection to database
define('HOST', 'localhost');
// The constant with database name for connection to database
define('DB_NAME', 'final');
// The constant with user's login for connection to database
define('DB_USER', 'root');
// The constant with user's password for connection to database
define('DB_PASSWORD', '123456');
//Emain for sending registraition e-mails
define('ADMIN_EMAIL', 'admin');
//One symbol for hierarchical lists
define('OPEN_SYMBOL_FOR_HIERARCHIC_LIST', ' - ');
//Index tpl file
define('INDEX_TPL_FILE', 'tpl/Index.tpl');
