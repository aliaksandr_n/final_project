<?php

/**
 * Class ForgetPassController
 */
class ForgetPassController {

    /**
     * @var null
     */
    protected $objForgetPassDB = null; // Reference to the object

    /**
     * @var array
     */
    protected $actionResultMessage = []; // Array with results

    /**
     * @param $obj_forget_pass_DB
     */
    public function __construct($obj_forget_pass_DB) {

        $this->objForgetPassDB = $obj_forget_pass_DB;
    }

    /**
     * @return mixed
     */
    public function getTplArray() {

        $email = null;
        if (!empty($_POST['email'])) {
            $email = $_POST['email'];
        }
        $submit_pressed = null;
        if (!empty($_POST['submit_pressed'])) {
            $submit_pressed = $_POST['submit_pressed'];
        }
        $if_var_email_form = -1;
        if ($submit_pressed==1) {
	        if(empty($email)) {
                array_push ($this->actionResultMessage, array('message' => 'Поле "E-mail" не заполнено',
                    'successfully' => 'empty_email'));
            }
            elseif (!preg_match("/^[a-zA-Z0-9_\.\-]+@([a-zA-Z0-9\-]+\.)+[a-zA-Z]{2,15}$/", $email)) {
                array_push($this->actionResultMessage, array('message' => 'E-mail имеет недопустимий формат (e-mail должен иметь вид name@example.test)',
                    'successfully' => 'incorrect_email'));
            }
            else {
                $result_finding_email = $this->objForgetPassDB->findRecoveryEmail($email);
                if (!is_array($result_finding_email)) {
                    array_push ($this->actionResultMessage, array('message' => 'Пользователь с указанным e-mail отсутствует',
                        'successfully' => 'email_not_find'));
                }
                else {
                    $recovery_id = $result_finding_email['ru_id'];
                    $recovery_date = time();
                    $recovery_key = sha1($email.time());
                    
                    $result_r_key_registration = $this->objForgetPassDB->registerRecoveryKey($recovery_id, $recovery_key, $recovery_date);
                    if ($result_r_key_registration == true) {
                        $mail_sent = $this->sentRecoveryPassEmail($email, $recovery_key);
                        if ($mail_sent == true) {
                            array_push ($this->actionResultMessage, array('message' => 'На указанный вами email отправлено письмо со ссылкой к странице восстановления пароля',
                                'successfully' => '1'));
                            $if_var_email_form = 1;
                        }
                        else {
                            array_push ($this->actionResultMessage, array('message' => 'Не удалось отправить на ваш e-mal письмо со ссылкой на страницу с восстановлением пароля',
                                'successfully' => 'letter_sending_problem'));
                        }
                    }
                    else {
                        array_push ($this->actionResultMessage, array('message' => 'Не удалось зарегистрировать в базе ключ восстановления',
                            'successfully' => 'failed_recovery_key_registration'));
                    }
                }
            }
        }

        $result_array['dynamic_vars']['email'] = '';
        if (!empty($email)) {
            $result_array['dynamic_vars']['email'] = $email;
        }
        $result_array['if_vars']['email_form'] = $if_var_email_form;
        $actionMessages = $this->actionResultMessage;
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        return $result_array;
    }

    /**
     * @param $email
     * @param $recovery_key
     * @return bool
     */
    public function sentRecoveryPassEmail($email, $recovery_key) {

        $to = $email;
        $subject = '=?utf-8?b?'. base64_encode('Восстановление пароля на сайте '.$_SERVER['SERVER_NAME'].''). '?=';
        $message = 'Добрый день. Для востановления пароля пройдите по указанной ссылке '.$_SERVER['SERVER_NAME'].'/index.php?page=recovery_pass&key='.$recovery_key;
        $headers  = 'MIME-Version: 1.0'."\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
        /*$headers .= 'To: user <'.$to.'>'."\r\n";*/
        $headers .= 'From: '.$_SERVER['SERVER_NAME'].'=?utf-8?b?'. base64_encode(' восстановление пароля'). '?='.    ' <'.ADMIN_EMAIL.'@'.$_SERVER['SERVER_NAME'].'>'."\r\n";
        $mail_sent = mail($to,$subject,$message,$headers);

        return $mail_sent;
    }
}
