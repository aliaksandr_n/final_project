<?php

/**
 * Class RecoveryPassController
 */
class RecoveryPassController {
    /**
     * Reference to the object
     * @var null
     */
    protected $objRecoveryPassDB = null;
    /**
     * Array with results
     * @var array
     */
    protected $actionResultMessage = [];

    /**
     * @param $obj_recovery_pass_DB
     */
    public function __construct($obj_recovery_pass_DB) {

        $this->objRecoveryPassDB = $obj_recovery_pass_DB;
    }

    /**
     * @return mixed
     */
    public function getTplArray() {

        $key = null;
        if (!empty($_GET['key'])) {
            $key = $_GET['key'];
        }
        $password = null;
        if (!empty($_POST['password'])) {
            $password = $_POST['password'];
        }
        $password2 = null;
        if (!empty($_POST['password2'])) {
            $password2 = $_POST['password2'];
        }
        $submit_pressed = null;
        if (!empty($_POST['submit_pressed'])) {
            $submit_pressed = $_POST['submit_pressed'];
        }
        $if_var_new_password_form = -1;
        if (empty($key)) {
            array_push ($this->actionResultMessage, array('message' => 'Вы попали на эту страницу способом, который не был предусмотрен.<br />
                                            Если вы хотите восстановить пароль- перейдите по данной <a href="index.php?page=forget_pass">ссылке</a>',
                'successfully' => 'empty_recovery_key'));
        }
        else {
            $result_finding_key = $this->objRecoveryPassDB->findRecoveryKey($key);
            if (!is_array($result_finding_key)) {
                array_push ($this->actionResultMessage, array('message' => 'Ключ для восстановления пароля не найден.<br />
                                                Попробуйте <a href="index.php?page=forget_pass">восстановить пароль</a> еще раз',
                    'successfully' => 'non-existent_key'));
            }
            else {
                if ($result_finding_key['ari_recovered'] == 1) {
                    array_push ($this->actionResultMessage, array('message' => 'Ссылка уже была использована для восстановления пароля.<br />
                                                    Попробуйте <a href="index.php?page=forget_pass">восстановить пароль</a> еще раз',
                        'successfully' => 'used_key'));
                }
                else {
                    $if_var_new_password_form = 1;
                    if ($submit_pressed == 1) {
                        $checking_result = $this->checkInputData($password, $password2);
                        if ($checking_result == 1) {
                            $user_id = $result_finding_key['ari_registered_user_id'];
                            $recovery_row_id = $result_finding_key['ari_id'];
                            $password = sha1($password);
                            $result_changing_pass = $this->objRecoveryPassDB->changeUserPassword($password, $user_id);
                            if ($result_changing_pass == true) {
                                $if_var_new_password_form = -1;
                                $result_marking_used_key = $this->objRecoveryPassDB->markKeyAsRecovered($recovery_row_id);
                                if ($result_marking_used_key == false) {
                                    array_push ($this->actionResultMessage, array('message' => 'Ваш пароль восстановлен, но не удалось дезактивировать ключ восстановления в базе. Ничего страшного, в случае повтора подобного сообение- сообщите администратору',
                                        'successfully' => 'error_marking_used_key'));
                                }
                                else {
                                    array_push ($this->actionResultMessage, array('message' => 'Ваш пароль восстановлен. Попробуйте войти в систему используя новый пароль',
                                        'successfully' => 'non-existent_key'));
                                }
                            }
                            else {
                                array_push ($this->actionResultMessage, array('message' => 'Не удалось изменить пароль. Попробуйте еще раз',
                                    'successfully' => 'failed_pass_changing'));
                            }
                        }
                    }
                }
            }
        }
        $result_array['dynamic_vars']['key'] = '';
        if (!empty($key)) {
            $result_array['dynamic_vars']['key'] = $key;
        }
        $result_array['dynamic_vars']['password'] = '';
        if (!empty($password)) {
            $result_array['dynamic_vars']['password'] = $password;
        }
        $result_array['dynamic_vars']['password2'] = '';
        if (!empty($password2)) {
            $result_array['dynamic_vars']['password2'] = $password2;
        }
        $result_array['if_vars']['new_password_form'] = $if_var_new_password_form;
        $actionMessages = $this->actionResultMessage;
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        return $result_array;
    }

    /**
     * @param $password
     * @param $password2
     * @return int
     */
    public function checkInputData ($password, $password2) {

        $correct_input_data = 1;
        if (empty($password)) {
            array_push ($this->actionResultMessage, array('message' => 'Поле "Пароль" не заполнено',
                'successfully' => 'empty_password'));
            $correct_input_data = 0;
        }
        elseif (!preg_match("/\A(\w){6,20}\Z/", $password)) {
            array_push ($this->actionResultMessage, array('message' => 'Пароль не соотвествует требованим безопасности. Пароль должен быть не менее 6 символов и содержать только буквы, цифры и символ подчеркивания',
                'successfully' => 'incorrect_password'));
            $correct_input_data = 0;
        }
        if (empty($password2)) {
            array_push ($this->actionResultMessage, array('message' => 'Поле с подтвержением пароля не заполнено',
                'successfully' => 'incorrect_password2'));
            $correct_input_data = 0;
        }
        elseif ($password != $password2) {
            array_push ($this->actionResultMessage, array('message' => 'Введенные пароли не совпадают',
                'successfully' => 'different_passwords'));
            $correct_input_data = 0;
        }
        return $correct_input_data;
    }
}
