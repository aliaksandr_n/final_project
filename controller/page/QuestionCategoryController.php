<?php

/**
 * Class QuestionCategoryController
 */
class QuestionCategoryController {

    /**
     * @var null
     */
    protected $objQuestionCategoryDB = null; // Reference to the object

    /**
     * @var array
     */
    protected $questionCategoryArray = []; // Array with categories array

    /**
     * @var array
     */
    protected $actionResultMessage = []; // Array with results

    /**
     * @var array
     */
    protected $selectedModifiedQuestionCategory = array('qc_id' => null, 'qc_name' => null, 'qc_parent_id' => null); // Array with id

    /**
     * @return array
     */
    public function getQuestionCategoryArray() {
        $questionCategoryArray = $this->questionCategoryArray;
        return $questionCategoryArray;
    }

    /**
     * @return array
     */
    public function getActionResultMessage() {
        $actionResultMessage = $this->actionResultMessage;
        return $actionResultMessage;
    }

    /**
     * @return array
     */
    public function getSelectedModifiedQuestionCategory() {
        $selectedModifiedQuestionCategory = $this->selectedModifiedQuestionCategory;
        return $selectedModifiedQuestionCategory;
    }

    /**
     * @param $obj_question_category_DB
     */
    public function __construct($obj_question_category_DB )
    {
        $this->objQuestionCategoryDB = $obj_question_category_DB;
    }

    /**
     * @return mixed
     */
    public function getTplArray() {

        $action = null;
        if (!empty($_GET['action'])) {
            $action = $_GET['action'];
        }
        $qc_id = null;
        if (!empty($_GET['qc_id'])) {
            $qc_id = $_GET['qc_id'];
        }
        elseif (!empty($_POST['qc_id'])) {
            $qc_id = $_POST['qc_id'];
        }
        $qc_name = null;
        if (!empty($_POST['qc_name'])) {
            $qc_name = $_POST['qc_name'];
        }
        $qc_parent_id = 0;
        if (!empty($_POST['qc_parent_id'])) {
            $qc_parent_id = $_POST['qc_parent_id'];
        }
        //----------------------------------
        if (!empty($action)) {
            if (($action == 'add') && (!empty($qc_name))) {
                $this->addQuestionCategory($qc_name, $qc_parent_id);
            }
            elseif (($action == 'modify_tlp') && (!empty($qc_id))) {
                $this->selectModifiedQuestionCategory($qc_id);
            }
            elseif (($action == 'modify') && (!empty($qc_name))) {
                $this->modifyQuestionCategory($qc_id, $qc_name, $qc_parent_id);
            }
            elseif (($action == 'delete') && (!empty($qc_id))) {
                $this->deleteQuestionCategory($qc_id);
            }
        }
        $this->readQuestionCategory();
        $modified_category_array = $this->selectedModifiedQuestionCategory;
        $actionMessages = $this->actionResultMessage;
        $questionCategory = $this->questionCategoryArray;
        if (is_array($questionCategory)) {
            $question_category_array = $this->prepareParentList($questionCategory, 'qc');
        }
        //----------------------------------
        $result_array['dynamic_vars']['qc_open_symbol'] = OPEN_SYMBOL_FOR_HIERARCHIC_LIST;
        $result_array['dynamic_vars']['qc_id'] = '';
        if (!empty($qc_id)) {
            $result_array['dynamic_vars']['qc_id'] = $qc_id;
        }
        $result_array['dynamic_vars']['selectedModifiedQuestionCategory'] = '';
        if (!empty($modified_category_array['qc_name'])) {
            $result_array['dynamic_vars']['selectedModifiedQuestionCategory'] = $modified_category_array['qc_name'];
        }
        $result_array['foreach_arrs']['question_category_array'] = [];
        if (!empty($question_category_array)) {
            $result_array['foreach_arrs']['question_category_array'] = $question_category_array;
        }
        $result_array['if_vars']['question_category_action'] = false;
        if (!empty($qc_id) && (!empty($qc_id)) && ($action == 'modify_tlp')) {
            $result_array['if_vars']['question_category_action'] = $qc_id;
        }
        $result_array['if_vars']['qc_parent_id'] = false;
        if (!empty($modified_category_array['qc_parent_id'])) {
            $result_array['if_vars']['qc_parent_id'] = $modified_category_array['qc_parent_id'];
        }
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        return $result_array;
    }

    /**
     *
     */
    public function readQuestionCategory() {

        $result = $this->objQuestionCategoryDB->readQuestionCategory();
        if (is_array($result)) {
            $this->questionCategoryArray = $result;
            //array_push ($this->actionResultMessage, array('message' => 'Список категорий успешно сформирован',
            //                                         'successfully' => '1'));
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' => 'Список категорий пуст',
                                                     'successfully' => 'empty_category_array'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'При формировании списка категорий возникли проблемы',
                                                     'successfully' => 'failed_reading'));
        }
    }

    /**
     * @param $qc_name
     * @param $qc_parent_id
     */
    public function addQuestionCategory($qc_name, $qc_parent_id) {

        $result = $this->objQuestionCategoryDB->addQuestionCategory($qc_name, $qc_parent_id);
        if ($result) {
            array_push ($this->actionResultMessage, array('message' => 'Категория была успешно добавлена',
                                                     'successfully' => '1'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Проблемы с добавлением категории',
                                                     'successfully' => 'failed_adding'));
        }
    }

    /**
     * @param $qc_id
     */
    public function selectModifiedQuestionCategory($qc_id) {

        $result = $this->objQuestionCategoryDB->selectModifiedQuestionCategory($qc_id);
        if ($result) {
            array_push ($this->actionResultMessage, array('message' => 'Категория готова к редактированию',
                                                     'successfully' => '1'));
            $this->selectedModifiedQuestionCategory['qc_id'] = $result['qc_id'];
            $this->selectedModifiedQuestionCategory['qc_name'] = $result['qc_name'];
            $this->selectedModifiedQuestionCategory['qc_parent_id'] = $result['qc_parent_id'];
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Вы не можете внести изменения в указанную категорию, т.к. она либо уже отсуствует на сервере, либо сущестует несколько категорий с одинаковым идентификатором',
                                                     'successfully' => 'doubled_category'));
        }
    }

    /**
     * @param $qc_id
     * @param $qc_name
     * @param $qc_parent_id
     */
    public function modifyQuestionCategory($qc_id, $qc_name, $qc_parent_id) {

        $result = $this->objQuestionCategoryDB->modifyQuestionCategory($qc_id, $qc_name, $qc_parent_id);
        if ($result) {
            array_push ($this->actionResultMessage, array('message' => 'Категория была успешно изменена',
                                                     'successfully' => '1'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Проблемы с изменением категории',
                                                     'successfully' => 'failed_modifying'));
        }
    }

    /**
     * @param $qc_id
     */
    public function deleteQuestionCategory($qc_id) {

        $result = $this->objQuestionCategoryDB->findQuestionCategoryChild($qc_id);
        if(is_array($result)) {
            array_push ($this->actionResultMessage, array('message' => 'Категория не может быть удалена, т.к. у нее есть подкатегории',
                                                     'successfully' => 'error_deleting_parent_with_child'));
        }
        else {
            $result = $this->objQuestionCategoryDB->deleteQuestionCategory($qc_id);
            if ($result) {
                array_push ($this->actionResultMessage, array('message' => 'Категория была успешно удалена',
                                                         'successfully' => '1'));
            }
            else {
                array_push ($this->actionResultMessage, array('message' => 'Проблемы с удалением категории, сообщите о проблеме администратору',
                                                         'successfully' => 'failed_deleting'));
            }
        }
    }

    /**
     * @param $category
     * @param $table_index
     * @param array $category_copy
     * @param int $first_parent
     * @param null $open_symbol
     * @return array
     */
    public function prepareParentList ($category, $table_index, $category_copy = [], $first_parent = 0, $open_symbol = null) {

        $open_symbol .= '{DV="'.$table_index.'_open_symbol"}';//&nbsp;
        foreach ($category as $key => $value) {
            if ($value[$table_index.'_parent_id'] == $first_parent) {
                $category_copy[$key] = $value;
                $category_copy[$key][$table_index.'_open_value'] = $open_symbol;
                $category_copy = $this->prepareParentList($category, $table_index, $category_copy, $value[$table_index.'_id'], $open_symbol);
            }
        }
        return $category_copy;
    }
}
