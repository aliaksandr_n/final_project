<?php

/**
 * Class UserGroupController
 */
class UserGroupController {

    /**
     * Reference to the object
     * @var null
     */
    protected $objUserGroupDB = null;

    /**
     * Array with results
     * @var array
     */
    protected $actionResultMessage = [];

    /**
     * @var array
     */
    protected $userGroupArray = [];

    public function __construct($obj_user_group_DB) {

        $this->objUserGroupDB = $obj_user_group_DB;
    }

    /**
     * @return array
     */
    public function getActionResultMessage() {
        $actionResultMessage = $this->actionResultMessage;
        return $actionResultMessage;
    }

    /**
     * @return array
     */
    public function getUserGroupArray() {
        $userGroupArray = $this->userGroupArray;
        return $userGroupArray;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getTplArray($user_id) {

        $action = null;
        if (!empty($_GET['action'])) {
            $action = $_GET['action'];
        }
        $ug_id = null;
        if (!empty($_POST['ug_id'])) {
            $ug_id = $_POST['ug_id'];
        }
        elseif (!empty($_GET['ug_id'])) {
            $ug_id = $_GET['ug_id'];
        }
        if (($action == 'add-or-modify') || ($action == 'modify')) {
            $if_var_include_search_result = false;
            $if_vars_include_user_group_form = true;
            if ($action == 'add-or-modify') {
                $ug_name = null;
                if (!empty($_POST['ug_name'])) {
                    $ug_name = $_POST['ug_name'];
                }
                $ug_description = null;
                if (!empty($_POST['ug_description'])) {
                    $ug_description = $_POST['ug_description'];
                }
                $submit_pressed = null;
                if (!empty($_POST['submit_pressed'])) {
                    $submit_pressed = $_POST['submit_pressed'];
                }
                $first_action = 'add-or-modify';
                if (!empty($_POST['first_action'])) {
                    $first_action = $_POST['first_action'];
                }
            }
            elseif ($action == 'modify') {
                $ug_name = null;
                $ug_description = null;
                $selecting_result = $this->objUserGroupDB->selectModifiedUserGroup($ug_id);
                if (!empty($selecting_result) && !empty($selecting_result['ug_name'])) {
                    $ug_name = $selecting_result['ug_name'];
                    $ug_description = $selecting_result['ug_description'];
                    array_push ($this->actionResultMessage, array('message' => 'Группа готова к просмотру и редактированию',
                        'successfully' => '1'));
                }
                elseif ($selecting_result === true) {
                    array_push ($this->actionResultMessage, array('message' => 'Искомая группа не найдена',
                        'successfully' => 'empty_result'));
                    $this->readUserGroup();
                    $if_var_include_search_result = true;
                    $if_vars_include_user_group_form = false;
                }
                elseif ($selecting_result === false) {
                    array_push ($this->actionResultMessage, array('message' => 'Не удалось выбрать информацию по выбранной группе',
                        'successfully' => 'selecting_failed'));
                    $this->readUserGroup();
                    $if_var_include_search_result = true;
                    $if_vars_include_user_group_form = false;
                }
                $first_action = 'modify';
                if (!empty($_POST['first_action'])) {
                    $first_action = $_POST['first_action'];
                }
            }
            if (!empty($submit_pressed) && ($submit_pressed == '1')) {
                $correct_input_data = $this->checkInputData($ug_name);
                if ($correct_input_data === 1) {
                    if ($first_action == 'add-or-modify') {
                        $ug_author_user_id = $user_id;
                        $adding_result = $this->objUserGroupDB->addUserGroup($ug_name, $ug_description, $ug_author_user_id);
                        if ($adding_result == false) {
                            array_push($this->actionResultMessage, array('message' => 'Не удалось добавить группу. Попробуйте еще раз',
                                'successfully' => 'adding_failed'));
                        }
                        else {
                            array_push($this->actionResultMessage, array('message' => 'Группа успешно добавлена',
                                'successfully' => '1'));
                        }
                        $if_var_include_search_result = true;
                        $if_vars_include_user_group_form = false;
                        $this->readUserGroup();
                    }
                    elseif ($first_action == 'modify') {
                        $modifying_result = $this->objUserGroupDB->modifyUserGroup($ug_id, $ug_name, $ug_description);
                        if ($modifying_result == false) {
                            array_push($this->actionResultMessage, array('message' => 'Не удалось сохранить изменения свойств группы. Попробуйте еще раз',
                                'successfully' => 'modifying_failed'));
                        }
                        else {
                            array_push($this->actionResultMessage, array('message' => 'Группа испешно изменена',
                                'successfully' => '1'));
                        }
                        $if_var_include_search_result = true;
                        $if_vars_include_user_group_form = false;
                        $this->readUserGroup();
                    }
                }
            }
        }
        else {
            $if_var_include_search_result = true;
            $if_vars_include_user_group_form = false;

            if ($action == 'delete') {
                $result_deleting_user_group = $this->objUserGroupDB->deleteUserGroup($ug_id);
                if ($result_deleting_user_group == false) {
                    array_push ($this->actionResultMessage, array('message' => 'Не удалось удалить группу. Попробуйте еще раз',
                        'successfully' => 'deleting_failed'));
                    $if_var_include_search_result = true;
                    $if_vars_include_user_group_form = false;
                    $this->readUserGroup();
                }
                else {
                    array_push ($this->actionResultMessage, array('message' => 'Группа была успешно удаленa',
                        'successfully' => '1'));
                    $if_var_include_search_result = true;
                    $if_vars_include_user_group_form = false;
                    $this->readUserGroup();
                }
            }
            $this->readUserGroup();
        }
        //----------------------------------
        $result_array['if_vars']['include_search_result'] = $if_var_include_search_result;
        $result_array['if_vars']['include_user_group_form'] = $if_vars_include_user_group_form;

        $result_array['dynamic_vars']['first_action'] = '';
        if (!empty($first_action)) {
            $result_array['dynamic_vars']['first_action'] = $first_action;
        }

        $result_array['dynamic_vars']['ug_name'] = '';
        if (!empty($ug_name)) {
            $result_array['dynamic_vars']['ug_name'] = $ug_name;
        }
        $result_array['dynamic_vars']['ug_description'] = '';
        if (!empty($ug_description)) {
            $result_array['dynamic_vars']['ug_description'] = $ug_description;
        }
        $result_array['dynamic_vars']['ug_id'] = '';
        if (!empty($ug_id)) {
            $result_array['dynamic_vars']['ug_id'] = $ug_id;
        }
        $result_array['dynamic_vars']['first_action'] = '';
        if (!empty($first_action)) {
            $result_array['dynamic_vars']['first_action'] = $first_action;
        }

        $result_array['foreach_arrs']['user_group_array'] = [];
        if (!empty($this->userGroupArray)) {
            $result_array['foreach_arrs']['user_group_array'] = $this->userGroupArray;
        }

        $actionMessages = $this->actionResultMessage;
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        return $result_array;
    }

    /**
     * @param $ug_name
     * @return int
     */
    public function checkInputData($ug_name) {

        $correct_input_data = 1;
        if (empty($ug_name)) {
            array_push($this->actionResultMessage, array('message' => 'Поле с названием группы не заполнено',
                'successfully' => 'empty_text'));
            $correct_input_data = 0;
        }
        return $correct_input_data;
    }

    /**
     *
     */
    public function readUserGroup() {

        $result = $this->objUserGroupDB->readUserGroup();
        if (is_array($result)) {
            $this->userGroupArray = $result;
            //array_push ($this->actionResultMessage, array('message' => 'Список групп успешно сформирован',
            //    'successfully' => '1'));
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' => 'Список групп пуст',
                'successfully' => 'empty_ugroup_array'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'При формировании списка групп возникли проблемы',
                'successfully' => 'failed_reading'));
        }
    }
}
