<?php

/**
 * Class AppointedTestController
 *
 * This class appoints test to selected users.
 * He also can generate temp keys and attach a test to them.
 */
class AppointedTestController {

    /**
     * Reference to the object with queries to the db, which connected with that class-controller.
     *
     * @var null
     */
    protected $objAppointedTestDB = null;

    /**
     * The array with user's groups.
     *
     * @var array
     */
    protected $userGroupArray = [];

    /**
     * The array with test categories.
     *
     * @var array
     */
    protected $testCategoryArray = [];

    /**
     * The array with test's names and information.
     *
     * @var array
     */
    protected $testRepositoryArray = [];

    /**
     * The service array with service messages to inform the user.
     *
     * @var array
     */
    protected $actionResultMessage = []; // Array with results.

    public function __construct($obj_appointed_test_DB, $user_group_array, $test_category_array, $test_repository_array) {

        $this->objAppointedTestDB = $obj_appointed_test_DB;
        $this->userGroupArray = $user_group_array;
        $this->testCategoryArray = $test_category_array;
        $this->testRepositoryArray = $test_repository_array;
    }

    /**
     * Main method declares vars and operate with other methods.
     *
     * @return mixed
     *  The array with vars for the template.
     */
    public function getTplArray($user_id) {

        $user_group_DB = $this->userGroupArray;
        $user_group_tpl_search = [];
        if ($user_group_DB) {
            $user_group_tpl_search = $user_group_DB;
            foreach ($user_group_tpl_search as $key => $value) {
                $user_group_tpl_search[$key]['selected_user_group'] = null;
            }
        }
        $user_group_tpl_action = $user_group_tpl_search;
        //----------------------------------
        $user_group_input = 0;
        if (isset($_GET['user_group'])) {
            $user_group_input = $_GET['user_group'];
            foreach ($user_group_tpl_search as $key => $value) {
                if ($value['ug_id'] == $user_group_input) {
                    $user_group_tpl_search[$key]['selected_user_group'] = "selected='selected'";
                }
            }
        }
        //----------------------------------
        $test_category_array = [];
        if ($this->testCategoryArray) {
            $test_category_array = $this->testCategoryArray;
            foreach ($test_category_array as $key => $value) {
                $test_category_array[$key]['selected_test_category'] = null;
            }
        }
        $result_array['dynamic_vars']['tc_open_symbol'] = '-';
        //----------------------------------
        $selected_t_category = 0;
        if (isset($_POST['t_category_test_appointment'])) {
            $selected_t_category = $_POST['t_category_test_appointment'];
            foreach ($test_category_array as $key => $value) {
                if ($value['ug_id'] == $selected_t_category) {
                    $test_category_array[$key]['selected_t_category'] = "selected='selected'";
                }
            }
        }
        //----------------------------------
        $test_repository_array = [];
        if ($this->testRepositoryArray) {
            $test_repository_array = $this->testRepositoryArray;
            foreach ($test_repository_array as $key => $value) {
                $test_repository_array[$key]['selected_test_repository'] = null;
            }
        }
        $result_array['dynamic_vars']['tr_open_symbol'] = '-';
        //----------------------------------
        $selected_t_repository = 0;
        if (isset($_POST['t_repository_test_appointment'])) {
            $selected_t_repository = $_POST['t_repository_test_appointment'];
            foreach ($test_repository_array as $key => $value) {
                if ($value['ug_id'] == $selected_t_repository) {
                    $test_repository_array[$key]['selected_t_repository'] = "selected='selected'";
                }
            }
        }
        //----------------------------------
        $action = null;
        if (!empty($_GET['action'])) {
            $action = $_GET['action'];
        }
        $search_type = 1;
        if (!empty($_GET['search_type'])) {
            $search_type = $_GET['search_type'];
        }
        $u_action = null;
        if (isset($_POST['u_action'])) {
            $u_action = $_POST['u_action'];
        }
        $checked_user = [];
        if (!empty($_POST['user'])) {
            $checked_user = $_POST['user'];
        }
        $submit_pressed = null;
        if (!empty($_POST['submit_pressed'])) {
            $submit_pressed = $_POST['submit_pressed'];
        }
        $key_number = 5;
        if (!empty($_GET['key_number'])) {
            $key_number = $_GET['key_number'];
        }
        elseif (!empty($_POST['key_number'])) {
            $key_number = $_POST['key_number'];
        }
        $u_group_search = null;
        if (isset($_POST['u_group_action'])) {
            $u_group_search = $_POST['u_group_action'];
            foreach ($user_group_tpl_action as $key => $value) {
                if ($value['ug_id'] == $u_group_search) {
                    $user_group_tpl_action[$key]['selected_user_group'] = "selected='selected'";
                }
            }
        }
        $temp_key_comment = [];
        if (!empty($_POST['temp_key_comment'])) {
            $temp_key_comment = $_POST['temp_key_comment'];
        }
        $temp_key_name = [];
        if (!empty($_POST['temp_key_name'])) {
            $temp_key_name = $_POST['temp_key_name'];
        }
        $temp_key_checked = [];
        if (!empty($_POST['temp_key_checked'])) {
            $temp_key_checked = $_POST['temp_key_checked'];
        }
        $appointed_test = [];
        if (!empty($_POST['appointed_test'])) {
            $appointed_test = $_POST['appointed_test'];
        }
        //----------------------------------
        $result_array['if_vars']['include_search_form'] = false;
        $result_array['if_vars']['include_result_form'] = false;
        //----------------------------------
        if ($action == 'search') {
            $result_array['if_vars']['include_search_form'] = true;
            if ($search_type == 1) {
                $search_form_type = "1AllUsers";
                $result_array['if_vars']['include_result_form'] = true;
                $user_array = $this->selectUsers();
                if (!empty($user_array) && is_array($user_array)) {
                    if (!empty($submit_pressed)) {
                        $result_checked_user = $this->checkAdmitedUsers($checked_user);
                        if ($result_checked_user == 1) {
                            foreach ($checked_user as $key => $value) {
                                $checked_user[$key] = $key;
                            }
                            $chekingResult = $this->checkInuptDataForAction($appointed_test);
                            if ($chekingResult == 1) {
                                $this->doAction($appointed_test, $checked_user, 'reg_users', $user_id);
                            }
                            $user_array = $this->selectUsers();
                            if (!empty($user_array) && is_array($user_array)) {
                                $user_array = $this->prepareCheckedUsers($checked_user, $user_array, 'ru_id');
                            }
                        }
                    }
                }
  
            }
            elseif ($search_type == 2) {
                $search_form_type = "2ByGroup";
                $result_array['if_vars']['include_result_form'] = true;
                $user_array = $this->selectUserByGroup($user_group_input);
                if (!empty($user_array) && is_array($user_array)) {
                    if (!empty($submit_pressed)) {
                        $result_checked_user = $this->checkAdmitedUsers($checked_user);
                        if ($result_checked_user == 1) {
                            $checked_user = $this->getCheckedUserId($checked_user, $user_array);
                            $chekingResult = $this->checkInuptDataForAction($appointed_test);
                            if ($chekingResult == 1) {
                                $this->doAction($appointed_test, $checked_user, 'reg_users', $user_id);
                            }
                            $user_array = $this->selectUserByGroup($user_group_input);
                            if (!empty($user_array) && is_array($user_array)) {
                                $user_array = $this->prepareCheckedUsers($checked_user, $user_array, 'um_id');
                            }
                        }
                    }
                }
            }
            elseif ($search_type == 3) {
                $search_form_type = "3TempKey";
                $result_array['if_vars']['include_result_form'] = true;
                $result_checked_user_key_number = $this->checkKeyNumber($key_number);
                if ($result_checked_user_key_number == 1) {
                    $temp_key_array = [];
                    if (!empty($submit_pressed)) {
                        for ($i=1; $i <= $key_number; $i++ ) {
                            $temp_key_array[$i]['key_index'] = $i;
                            $temp_key_array[$i]['key_name'] = $temp_key_name[$i];
                            $temp_key_array[$i]['key_comment'] = $temp_key_comment[$i];
                            if (!empty($temp_key_checked[$i])) {
                                $temp_key_array[$i]['key_checked'] = 'checked="checked"';
                            }
                            else {
                                $temp_key_array[$i]['key_checked'] = null;
                            }
                        }
                        if (count($temp_key_checked) < 1) {
                            array_push ($this->actionResultMessage, array('message' =>  'В табличной части не отмечены ключи, с которыми необходимо совершить действие',
                                'successfully' => 'error_questions_list'));
                        }
                        else {
                            $emptyCheckedKeyName = 0;
                            foreach ($temp_key_checked as $key=>$value) {
                                if (empty($temp_key_name[$key])) {
                                    $emptyCheckedKeyName = 1;
                                }
                            }
                            if ($emptyCheckedKeyName == 1) {
                                array_push ($this->actionResultMessage, array('message' =>  'В табличной части для отмеченных ключей, значение ключа пустое',
                                    'successfully' => 'error_questions_list'));
                            }
                            else {
                                $chekingResult = $this->checkInuptDataForAction($appointed_test);
                                if ($chekingResult == 1) {
                                    $added_temp_key_id = [];
                                    $error_counter = 0;
                                    foreach ($temp_key_array as $key=>$value) {
                                        $added_id = $this->objAppointedTestDB->addTempKey($value['key_name'], $value['key_comment']);
                                        if ($added_id) {
                                            array_push($added_temp_key_id, $added_id);
                                        }
                                        else {
                                            $error_counter = 1;
                                        }
                                    }
                                    if ($error_counter == 0) {
                                        $this->doAction($appointed_test, $added_temp_key_id, 'temp_users', $user_id);
                                    }
                                    else {
                                        array_push ($this->actionResultMessage, array('message' => 'Не удалось добавить временные ключи',
                                            'successfully' => 'failed_adding'));
                                    }
                                }
                            }
                        }
                    }
                    else {
                        for ($i=1; $i <= $key_number; $i++ ) {
                            $temp_key_array[$i]['key_index'] = $i;
                            $temp_key_array[$i]['key_name'] = sha1(mt_rand(1, 1000000000) . mt_rand(1000000, 5555555) . mt_rand(1, 1000000000));
                            $temp_key_array[$i]['key_comment'] = null;
                            $temp_key_array[$i]['key_checked'] = null;
                        }
                    }
                }
            }
            else {
                array_push($this->actionResultMessage, array('message' => 'Необходимо выбрать один из вариантов поиска',
                    'successfully' => 'bad_search_variant'));
                $result_array['if_vars']['include_search_form'] = false;
            }

        }

        $result_array['foreach_arrs']['test_category_array'] = $test_category_array;
        $result_array['foreach_arrs']['test_repository_array'] = $test_repository_array;

        $result_array['foreach_arrs']['user_array'] = [];
        $result_array['foreach_arrs']['temp_key_array'] = [];
        $result_array['if_vars']['empty_dir'] = false;
        if (!empty($user_array) && is_array($user_array)) {
            $result_array['foreach_arrs']['user_array'] = $user_array;
        }
        elseif (!empty($temp_key_array)) {
            $result_array['foreach_arrs']['temp_key_array'] = $temp_key_array;
        }
        else {
            $result_array['if_vars']['empty_dir'] = true;
        }
        $result_array['dynamic_vars']['key_number'] = "";
        if(isset($key_number)) {
            $result_array['dynamic_vars']['key_number'] = $key_number;
        }
        //----------------------------------
        $result_array['dynamic_vars']['selected_action_1'] = "";
        $result_array['dynamic_vars']['selected_action_2'] = "";
        $result_array['dynamic_vars']['selected_action_3'] = "";
        if(isset($u_action)) {
            $result_array['dynamic_vars']['selected_action_'.$u_action] = 'selected="selected"';
        }
        //----------------------------------
        $result_array['dynamic_vars']['get_user_group'] = "";
        if(isset($user_group_input)) {
            $result_array['dynamic_vars']['get_user_group'] = "&user_group=" . $user_group_input;
        }
        $result_array['dynamic_vars']['get_key_number'] = "";
        if(isset($key_number)) {
            $result_array['dynamic_vars']['get_key_number'] = "&key_number=" . $key_number;
        }
        //----------------------------------
        $result_array['dynamic_vars']['checked_1_search_type'] = '';
        $result_array['dynamic_vars']['checked_2_search_type'] = '';
        $result_array['dynamic_vars']['checked_3_search_type'] = '';
        if  (in_array($search_type, array(1, 2, 3))) {
            $result_array['dynamic_vars']['checked_'.$search_type.'_search_type'] = 'checked="checked"';
        }
        //----------------------------------
        $result_array['dynamic_vars']['search_form_type'] = '';
        if (!empty($search_form_type)){
            $result_array['dynamic_vars']['search_form_type'] = $search_form_type;
        }
        $result_array['foreach_arrs']['user_group_array_search'] = $user_group_tpl_search;
        $result_array['foreach_arrs']['user_group_array_action'] = $user_group_tpl_action;
        //----------------------------------
        $actionMessages = $this->actionResultMessage;
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        return $result_array;
    }

    /**
     * Get users array from db.
     *
     * @return mixed
     *  Users array or error-sign (true, false).
     */
    public function selectUsers() {

        $result = $this->objAppointedTestDB->selectAllRegisteredUsers();
        if ($result === false) {
            array_push ($this->actionResultMessage, array('message' =>  'При формировании списка пользователей возникли проблемы',
                'successfully' => 'error_questions_list'));
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' =>  'В базе отсуствуют пользователи',
                'successfully' => 'error_empty_array'));
        }
        else {
            foreach ($result as $key => $value) {
                $result[$key]['user_checked'] = null;
            }
            //array_push ($this->actionResultMessage, array('message' =>  'Список вопросов сформирован',
            //    'successfully' => '1'));
        }
        return $result;
    }

    /**
     * Get users which were added to any group. Can select users from all or certain group.
     * If a user doesn't consist at any group? he won't be selected.
     * 
     * @param $um_user_group_id
     *  The id of user group, from witch we want to get users.
     * 
     * @return mixed
     *  Users array or error-sign (true, false).
     */
    public function selectUserByGroup($um_user_group_id) {

        if ($um_user_group_id == 0) {
            $result = $this->objAppointedTestDB->selectUserByAllGroup();
        }
        else {
            $result = $this->objAppointedTestDB->selectUserByOneGroup($um_user_group_id);
        }
        if ($result === false) {
            array_push ($this->actionResultMessage, array('message' =>  'При формировании списка пользователей возникли проблемы',
                'successfully' => 'error_questions_list'));
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' =>  'В базе отсуствуют пользователи',
                'successfully' => 'error_empty_array'));
        }
        else {
            foreach ($result as $key => $value) {
                $result[$key]['user_checked'] = null;
            }
            //array_push ($this->actionResultMessage, array('message' =>  'Список вопросов сформирован',
            //    'successfully' => '1'));
        }
        return $result;
    }

    /**
     * Return array with id of users, which were checked in the table.
     * 
     * @param $checked_user
     *  The array with users, which were checked in the table.
     * @param $user_array
     *   Users array with information from db (including users' id)
     * @return mixed.
     *  The array with checked user and with their id.
     */
    public function getCheckedUserId ($checked_user, $user_array) {

        foreach ($user_array as $key => $value) {
            $users_id = $value['um_id'];
            if (array_key_exists($users_id, $checked_user)) {
                $checked_user[$users_id] = $value['ru_id'];
            }
        }
        return $checked_user;
    }

    /**
     * Mark checked users (in table) for the template.
     * 
     * @param $checked_user
     *  The array with users, which were checked in the table.
     * @param $user_array
     *  Users array with information from db.
     * @param $el_id_index
     * The prefix of checked element (user id or user group id).
     * @return mixed
     *  The array with checked user.
     */
    public function prepareCheckedUsers($checked_user, $user_array, $el_id_index){

        if (!empty($checked_user)) {
            $checked_user = $_POST['user'];
            foreach ($user_array as $key => $value) {
                if (array_key_exists($value[$el_id_index], $checked_user)) {
                    $user_array[$key]['user_checked'] = "checked='checked'";
                }
            }
        }
        return $user_array;
    }

    /**
     * Check declaration of goal test (which would be added to the users).
     * 
     * @param $appointed_test
     *  Goal test.
     * @return int
     *  Checking result.
     */
    public function checkInuptDataForAction($appointed_test) {

        $correct_input_data = 1;
        if (empty($appointed_test)) {
            array_push($this->actionResultMessage, array('message' => 'Не указан назначаемый тест',
                'successfully' => 'empty_action'));
            $correct_input_data = 0;
        }
        return $correct_input_data;
    }

    /**
     * Check if there was admitted as minimum 1 users in the table
     *
     * @param $checked_user
     *  The array of admitted users
     * @return int
     *  Checking result.
     */
    public function checkAdmitedUsers($checked_user) {

        $correct_input_data = 1;
        if (empty($checked_user) || !is_array($checked_user)) {
            array_push($this->actionResultMessage, array('message' => 'В табличной части не отмечены пользователи, с которыми необходимо совершить действие',
                'successfully' => 'empty_question_array'));
            $correct_input_data = 0;
        }
        return $correct_input_data;
    }

    /**
     * Check input number of generated keys.
     *
     * @param $key_number
     *  The number of keys, which should be generated.
     * @return int
     *  If the was any mistake or everything is correct.
     */
    public function checkKeyNumber($key_number) {

        $correct_input_data = 1;
        if (empty($key_number)) {
            array_push($this->actionResultMessage, array('message' => 'Не указано количество генерируемых ключей',
                'successfully' => 'empty_key_number'));
            $correct_input_data = 0;
        }
        elseif (empty((int)$key_number) || $key_number < 1){
        array_push($this->actionResultMessage, array('message' => 'В поле с количеством генерируемых ключей должно быть указано положительное число',
            'successfully' => 'empty_is_not_numeric'));
        $correct_input_data = 0;
        }
        elseif ($key_number > 35){
            array_push($this->actionResultMessage, array('message' => 'За раз можно создать не более 35 ключей',
                'successfully' => 'empty_is_not_numeric'));
            $correct_input_data = 0;
        }
        return $correct_input_data;
    }

    /**
     * Core method of the class. Make main action with user - add them to the test.
     *
     * @param $appointed_test
     *  The goal test.
     * @param $user_array
     *  The array with user, which are have to be added to the test.
     * @param $user_type
     *  Kind of the test receiver: registered users or temp users (temp keys).
     * @param $user_id
     *  User, who adds users to the group.
     */
    public function doAction($appointed_test, $user_array, $user_type, $user_id) {//temp_users reg_users

        //----------------------------------
        $testRepositoryArray = $this->testRepositoryArray;
        $appointed_test_inf = [];
        foreach ($testRepositoryArray as $key=>$value) {
            if ($value['tr_id'] == $appointed_test) {
                $appointed_test_inf = $value;
            }
        }
        $number_shown_questions = $appointed_test_inf['tr_number_shown_questions'];
        $test_q_array = $this->objAppointedTestDB->selectQuestionForTest($appointed_test_inf['tr_id']);
        if (empty($test_q_array) || !is_array($test_q_array)) {
            array_push ($this->actionResultMessage, array('message' => 'К тесту не найдены вопросы',
                'successfully' => 'failed_adding'));
        }
        else {
            $creating_time = time();
            $adding_appoint_result = $this->objAppointedTestDB->addAppointedTest($appointed_test, $creating_time, $user_id);
            if (!$adding_appoint_result) {
                array_push($this->actionResultMessage, array('message' => 'Проблемы с добавлением записи с тестом',
                    'successfully' => 'failed_adding'));
            }
            //----------------------------------
            $registered_user_id = null;
            $temp_user_id = null;
            $error_counter = 0;
            $test_user_journal_id_array = [];
            if ($user_type == 'temp_users') {
                $temp_user_id  = $user_array;
                foreach ($temp_user_id as $key=>$value) {
                    $test_user_journal_id = $this->objAppointedTestDB->addTestUserJournal($adding_appoint_result, $registered_user_id, $value );
                    if ($test_user_journal_id) {
                        array_push($test_user_journal_id_array, $test_user_journal_id);
                    }
                    elseif ($test_user_journal_id == false) {
                        $error_counter = 1;
                    }
                }
            }
            elseif ($user_type == 'reg_users') {
                $registered_user_id = $user_array;
                foreach ($registered_user_id as $key=>$value) {
                    $test_user_journal_id = $this->objAppointedTestDB->addTestUserJournal($adding_appoint_result, $value, $temp_user_id);
                    if (!empty($test_user_journal_id) && $test_user_journal_id !== true) {
                        array_push($test_user_journal_id_array, $test_user_journal_id);
                    }
                    elseif ($test_user_journal_id == false) {
                        $error_counter = 1;
                    }
                }
            }
            if ($error_counter == 1) {
                array_push ($this->actionResultMessage, array('message' => 'Не удалось добавить запись в журнал пользовательских тестов',
                    'successfully' => 'failed_adding'));
            }
            else {
                $test_question_array = [];
                foreach($test_q_array as $key=>$value) {
                    array_push($test_question_array, $value['trg_question_setting_id']);
                }
                shuffle($test_question_array);
                if (($number_shown_questions <= count($test_question_array)) && ($number_shown_questions > 1)) {
                    $test_question_array = array_slice($test_question_array, 0, $number_shown_questions);
                }
                elseif ($number_shown_questions == 1) {
                    $test_question_array = array(array_slice($test_question_array, 0, $number_shown_questions));
                }
                else {
                    $test_question_array = array_slice($test_question_array, 0, count($test_question_array));
                }
                $error_counter = 0;
                foreach ($test_user_journal_id_array as $key => $value) {
                    foreach ($test_question_array as $key2 => $value2) {
                        $adding_question_journal = $this->objAppointedTestDB->addQuestionJournal($value, $value2, $key2+1);
                        if ($adding_question_journal == false) {
                            $error_counter = 1;
                        }
                    }
                }
                if ($error_counter == 0) {
                    array_push($this->actionResultMessage, array('message' => 'Тесты назначены пользователям успешно',
                        'successfully' => '1'));
                } else {
                    array_push($this->actionResultMessage, array('message' => 'Не удалось добавить вопросы в журнал вопросов',
                        'successfully' => 'failed_adding'));
                }
            }
        }
    }

    /**
     * Delete users from users group
     *
     * @param $key
     *  User id
     * @return mixed
     *  Return query result (true or false)
     */
    protected function deleteUserFromUGroupMember($key){

            $result = $this->objAppointedTestDB->deleteUserFromUGroupMember($key);
            return $result;
    }
}
