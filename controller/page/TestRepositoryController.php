<?php

/**
 * Class TestRepositoryController
 */
class TestRepositoryController {

    /**
     * @var null
     */
    protected $objTestRepositoryDB = null;

    /**
     * Reference to the object
     * @var array
     */
    protected $questionTypeArray = [];

    /**
     * Array with categories array
     * @var array
     */
    protected $testCategoryArray = [];

    /**
     * Array with results
     * @var array
     */
    protected $actionResultMessage = [];

    public function __construct($obj_test_repository_DB, $test_category_array = []) {
        $this->objTestRepositoryDB = $obj_test_repository_DB;
        $this->testCategoryArray = $this->prepareParentList($test_category_array, 'tc');
    }

    /**
     * @return array
     */
    public function getActionResultMessage() {
        $actionResultMessage = $this->actionResultMessage;
        return $actionResultMessage;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getTplArray($user_id) {

        $test_category = $this->testCategoryArray;
        $test_category_tpl = [];
        if ($test_category) {
            $test_category_tpl = $test_category;
            foreach ($test_category_tpl as $key => $value) {
                $test_category_tpl[$key]['selected_test_category'] = '';
            }
        }
        $result_array['dynamic_vars']['tc_open_symbol'] = '-';
        //----------------------------------
        $result_array['if_vars']['include_modify_form'] = false;
        $result_array['if_vars']['include_search_form'] = true;
        $result_array['if_vars']['include_list_form'] = false;
        if (!empty($_GET['action'])) {
            $action = $_GET['action'];
            if (($action == 'open-test-form') || ($action == 'modify')) {
                $result_array['if_vars']['include_modify_form'] = true;
                $result_array['if_vars']['include_search_form'] = false;
                $result_array['if_vars']['include_list_form'] = false;
                $first_action = $action;
                if ($action == 'open-test-form') {
                    //-------------------------
                    $tr_id = null;
                    if (!empty($_POST['tr_id'])) {
                        $tr_id = $_POST['tr_id'];
                    }
                    $tr_name = null;
                    if (!empty($_POST['tr_name'])) {
                        $tr_name = $_POST['tr_name'];
                    }
                    $tr_pause_test_permission = 0;
                    if (!empty($_POST['tr_pause_test_permission'])) {
                        $tr_pause_test_permission = 1;
                    }
                    $tr_show_right_answer = 0;
                    if (!empty($_POST['tr_show_right_answer'])) {
                        $tr_show_right_answer = 1;
                    }
                    $tr_show_final_mark = 0;
                    if (!empty($_POST['tr_show_final_mark'])) {
                        $tr_show_final_mark = 1;
                    }
                    $tr_show_final_verdict = 0;
                    if (!empty($_POST['tr_show_final_verdict'])) {
                        $tr_show_final_verdict = 1;
                    }
                    $tr_number_shown_questions = 0;
                    if (!empty($_POST['tr_number_shown_questions'])) {
                        $tr_number_shown_questions = $_POST['tr_number_shown_questions'];
                    }
                    $tr_success_value_type = 0;
                    if (!empty($_POST['tr_success_value_type'])) {
                        $tr_success_value_type = $_POST['tr_success_value_type'];
                    }
                    $tr_skip_questions = null;
                    if (!empty($_POST['tr_skip_questions'])) {
                        $tr_skip_questions = 1;
                    }
                    else {
                        $tr_skip_questions = 0;
                    }
                    $tr_control_test_time_per_questions = 0;
                    if (!empty($_POST['tr_control_test_time_per_questions'])) {
                        $tr_control_test_time_per_questions = $_POST['tr_control_test_time_per_questions'];
                    }
                    $tr_total_time_value = 0;
                    if (!empty($_POST['tr_total_time_value'])) {
                        $tr_total_time_value = $_POST['tr_total_time_value'];
                    }
                    $tr_success_value_num = 0;
                    if (!empty($_POST['tr_success_value_num'])) {
                        $tr_success_value_num = $_POST['tr_success_value_num'];
                    }
                    $tCategory = null;
                    if (!empty($_POST['tCategory'])) {
                        $tCategory = $_POST['tCategory'];
                        foreach ($test_category_tpl as $key => $value) {
                            if ($value['tc_id'] == $tCategory) {
                                $test_category_tpl[$key]['selected_test_category'] = "selected='selected'";
                            }
                        }
                    }
                }
                elseif ($action == 'modify') {
                    $tr_id = null;
                    if (!empty($_GET['tr_id'])) {
                        $tr_id = $_GET['tr_id'];
                        $test_array = $this->objTestRepositoryDB->selectTestById($tr_id);
                        $test_group = $this->objTestRepositoryDB->selectTestCatGroup($tr_id);
                        //-------------------------
                        $tr_name = null;
                        if (!empty($test_array['tr_name'])) {
                            $tr_name = $test_array['tr_name'];
                        }
                        $tr_pause_test_permission = null;
                        if (!empty($test_array['tr_pause_test_permission'])) {
                            $tr_pause_test_permission = $test_array['tr_pause_test_permission'];
                        }
                        $tr_show_right_answer = null;
                        if (!empty($test_array['tr_show_right_answer'])) {
                            $tr_show_right_answer = $test_array['tr_show_right_answer'];
                        }
                        $tr_show_final_mark = null;
                        if (!empty($test_array['tr_show_final_mark'])) {
                            $tr_show_final_mark = $test_array['tr_show_final_mark'];
                        }
                        $tr_show_final_verdict = null;
                        if (!empty($test_array['tr_show_final_verdict'])) {
                            $tr_show_final_verdict = $test_array['tr_show_final_verdict'];
                        }
                        $tr_number_shown_questions = null;
                        if (!empty($test_array['tr_number_shown_questions'])) {
                            $tr_number_shown_questions = $test_array['tr_number_shown_questions'];
                        }
                        $tr_success_value_type = null;
                        if (!empty($test_array['tr_success_value_type'])) {
                            $tr_success_value_type = $test_array['tr_success_value_type'];
                        }
                        $tr_success_value_num = null;
                        if (!empty($test_array['tr_success_value_num'])) {
                            $tr_success_value_num = $test_array['tr_success_value_num'];
                        }
                        $tr_skip_questions = null;
                        if (!empty($test_array['tr_skip_questions'])) {
                            $tr_skip_questions = $test_array['tr_skip_questions'];
                        }
                        $tr_control_test_time_per_questions = '0';
                        if (!empty($test_array['tr_control_test_time_per_questions'])) {
                            $tr_control_test_time_per_questions = $test_array['tr_control_test_time_per_questions'];
                        }
                        $tr_total_time_value = null;
                        if (!empty($test_array['tr_total_time_value'])) {
                            $tr_total_time_value = $test_array['tr_total_time_value'];
                        }
                        $tCategory = null;
                        if (!empty($test_group['tcg_test_category_id'])) {
                            $tCategory = $test_group['tcg_test_category_id'];
                            foreach ($test_category_tpl as $key => $value) {
                                if ($value['tc_id'] == $tCategory) {
                                    $test_category_tpl[$key]['selected_test_category'] = "selected='selected'";
                                }
                            }
                        }
                    }
                }
                //-------------------------
                $result_array['dynamic_vars']['tr_name'] = '';
                if (!empty($tr_name)) {
                    $result_array['dynamic_vars']['tr_name'] = $tr_name;
                }
                $result_array['foreach_arrs']['tCategory'] = [];
                if (!empty($tCategory)) {
                    $result_array['dynamic_vars']['tCategory'] = $tCategory;
                }
                $result_array['dynamic_vars']['selected_stop_test_permission'] = '';
                if (!empty($tr_pause_test_permission)) {
                    $result_array['dynamic_vars']['selected_stop_test_permission'] = 'checked="checked"';
                }
                $result_array['dynamic_vars']['selected_show_right_answer'] = '';
                if (!empty($tr_show_right_answer)) {
                    $result_array['dynamic_vars']['selected_show_right_answer'] = 'checked="checked"';
                }
                $result_array['dynamic_vars']['selected_show_final_mark'] = '';
                if (!empty($tr_show_final_mark)) {
                    $result_array['dynamic_vars']['selected_show_final_mark'] = 'checked="checked"';
                }
                $result_array['dynamic_vars']['selected_show_final_verdict'] = '';
                if (!empty($tr_show_final_verdict)) {
                    $result_array['dynamic_vars']['selected_show_final_verdict'] = 'checked="checked"';
                }
                $result_array['dynamic_vars']['tr_number_shown_questions'] = '';
                if (!empty($tr_number_shown_questions)) {
                    $result_array['dynamic_vars']['tr_number_shown_questions'] = $tr_number_shown_questions;
                }
                $result_array['dynamic_vars']['selected_skip_questions'] = '';
                if (!empty($tr_skip_questions)) {
                    $result_array['dynamic_vars']['selected_skip_questions'] = 'checked="checked"';
                }
                $result_array['dynamic_vars']['selected_success_value_type1'] = '';
                $result_array['dynamic_vars']['selected_success_value_type2'] = '';
                $result_array['dynamic_vars']['selected_success_value_type3'] = '';
                if (!empty($tr_success_value_type)) {
                    if ($tr_success_value_type === '1') {
                        $result_array['dynamic_vars']['selected_success_value_type1'] = 'selected="selected"';
                    }
                    elseif ($tr_success_value_type === '2') {
                        $result_array['dynamic_vars']['selected_success_value_type2'] = 'selected="selected"';
                    }
                    elseif ($tr_success_value_type === '3') {
                        $result_array['dynamic_vars']['selected_success_value_type3'] = 'selected="selected"';
                    }
                }
                $result_array['dynamic_vars']['tr_success_value_num'] = '';
                if (!empty($tr_success_value_num)) {
                    $result_array['dynamic_vars']['tr_success_value_num'] = $tr_success_value_num;
                }
                $result_array['dynamic_vars']['selected_control_test_time_per_questions0'] = 'selected="selected"';
                $result_array['dynamic_vars']['selected_control_test_time_per_questions1'] = '';
                if ($tr_control_test_time_per_questions == 1) {
                    $result_array['dynamic_vars']['selected_control_test_time_per_questions0'] = '';
                    $result_array['dynamic_vars']['selected_control_test_time_per_questions1'] = 'selected="selected"';
                }
                $result_array['dynamic_vars']['tr_total_time_value'] = '';
                if (!empty($tr_total_time_value)) {
                    $result_array['dynamic_vars']['tr_total_time_value'] = $tr_total_time_value;
                }
                $result_array['dynamic_vars']['tr_id'] = '';
                if (!empty($tr_id)) {
                    $result_array['dynamic_vars']['tr_id'] = $tr_id;
                }
                //-------------------------
                if (!empty($_POST['button_pressed']) && ($_POST['button_pressed'] == '1')) {
                    if (!empty($_POST['first_action'])) {
                        $first_action = $_POST['first_action'];
                    }
                    $correct_input_data = $this->checkInputData($tr_name, $tCategory, $tr_number_shown_questions, $tr_success_value_type, $tr_success_value_num, $tr_control_test_time_per_questions, $tr_total_time_value);
                    if ($correct_input_data === 1) {
                        $tr_adding_time = time();
                        $tr_author_id = $user_id;

                        if ($first_action == 'open-test-form') {
                            $testId = $this->objTestRepositoryDB->addTest(
                                $tr_name, $tr_pause_test_permission, $tr_show_right_answer, $tr_show_final_mark, $tr_show_final_verdict,
                                $tr_number_shown_questions, $tr_success_value_type, $tr_success_value_num, $tr_skip_questions,
                                $tr_control_test_time_per_questions, $tr_total_time_value, $tr_author_id, $tr_adding_time
                            );
                            if ($testId === false) {
                                array_push($this->actionResultMessage, array('message' => 'Тест не был добавлен в базу, попробуйте еще раз и в случае повтора ошибки-сообщите администратору',
                                    'successfully' => 'adding_question_false'));
                            }
                            else {
                                $trg_test_repository_id = $testId;
                                $testAddingResult = $this->objTestRepositoryDB->addCategoryToTest($trg_test_repository_id, $tCategory);
                                if ($testAddingResult === false) {
                                    array_push($this->actionResultMessage, array('message' => 'Тест был добавлен в базу, но к нему не удалось добавить категорию. Попробуйте еще раз и в случае повтора ошибки-сообщите администратору',
                                        'successfully' => 'adding_test_false'));
                                }
                                else {
                                    array_push($this->actionResultMessage, array('message' => 'Тест был успешно добавлен в базу',
                                        'successfully' => '1'));
                                    $result_array['if_vars']['include_modify_form'] = false;
                                    $result_array['if_vars']['include_search_form'] = true;
                                    $result_array['if_vars']['include_list_form'] = false;
                                }
                            }
                        }
                        elseif ($first_action == 'modify') {
                            $incorrect_answer_time = false;
                            if ($tr_control_test_time_per_questions == 1) {
                                $incorrect_answer_time = $this->objTestRepositoryDB->checkAnswerTime($tr_id);
                            }
                            if ($incorrect_answer_time == true) {
                                array_push($this->actionResultMessage, array('message' => 'Тесту не может быть присвоен контроль "Времени по каждому вопросу", т.к. в тесте уже существуют вопросы, без проставленного времени на вопрос. Вначале необходимо проставить во всех вопросах теста время, а затем изменять данную настройку теста',
                                    'successfully' => 'incorrect_answer_time'));
                            }
                            else {
                                $updateResult = $this->objTestRepositoryDB->updateTest(
                                    $tr_id, $tr_name, $tr_pause_test_permission, $tr_show_right_answer, $tr_show_final_mark,
                                    $tr_show_final_verdict, $tr_number_shown_questions, $tr_success_value_type, $tr_success_value_num,
                                    $tr_skip_questions, $tr_control_test_time_per_questions, $tr_total_time_value, $tr_author_id, $tr_adding_time
                                );
                                if ($updateResult === false) {
                                    array_push($this->actionResultMessage, array('message' => 'Тест не был изменен в базе, попробуйте еще раз и в случае повтора ошибки-сообщите администратору',
                                        'successfully' => 'update_test_false'));
                                } else {
                                    $updatingTestGroup = $this->objTestRepositoryDB->updateTestGroup($tCategory, $tr_id);
                                    if ($updatingTestGroup === false) {
                                        array_push($this->actionResultMessage, array('message' => 'Возникли проблемы с изменением группы тестов. Попробуйте еще раз и в случае повтора ошибки-сообщите администратору',
                                            'successfully' => 'update_test_false'));
                                    } else {
                                        array_push($this->actionResultMessage, array('message' => 'Тест успешно изменен',
                                            'successfully' => '1'));
                                        $result_array['if_vars']['include_modify_form'] = false;
                                        $result_array['if_vars']['include_search_form'] = true;
                                        $result_array['if_vars']['include_list_form'] = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            elseif (($action == 'search') || ($action == 'delete')) {
                $result_array['if_vars']['include_modify_form'] = false;
                $result_array['if_vars']['include_search_form'] = true;
                $result_array['if_vars']['include_list_form'] = true;

                if ($action == 'delete') {
                    if (!empty($_GET['tr_id'])) {
                        $test_id = $_GET['tr_id'];
                    }
                    $this->deleteTest($test_id);
                }
                $tCategory = null;
                if (!empty($_GET['tCategory'])) {
                    $tCategory = $_GET['tCategory'];
                    foreach ($test_category_tpl as $key => $value) {
                        if ($value['tc_id'] == $tCategory) {
                            $test_category_tpl[$key]['selected_question_repository'] = "selected='selected'";
                        }
                    }
                }
                $testsArray = [];
                $testsArray = $this->getTestsArray($tCategory);
            }
            $result_array['dynamic_vars']['first_action'] = '';
            if (!empty($first_action)) {
                $result_array['dynamic_vars']['first_action'] = $first_action;
            }
            $result_array['foreach_arrs']['testsArray'] = [];
            if (!empty($testsArray) && is_array($testsArray)) {
                $result_array['foreach_arrs']['testsArray'] = $this->prepareTplTestsArray($testsArray);
            }
        }

        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($this->actionResultMessage)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $this->actionResultMessage;
        }
        if(!empty($tCategory)) {
            foreach ($test_category_tpl as $key => $value) {
                if ($value['tc_id'] == $tCategory) {
                    $test_category_tpl[$key]['selected_test_category'] = "selected='selected'";
                }
            }
        }
        $result_array['foreach_arrs']['test_category_array'] = $test_category_tpl;
        return $result_array;
    }

    /**
     * @param int $tcg_test_category_id
     * @return mixed
     */
    public function getTestsArray($tcg_test_category_id = 0) {

        if ($tcg_test_category_id == 0) {
            $result = $this->objTestRepositoryDB->selectAllTests();
        }
        else {
            $result = $this->objTestRepositoryDB->selectTestsByCategory($tcg_test_category_id);
        }
        if ($result === false) {
            array_push ($this->actionResultMessage, array('message' =>  'При формировании списка тестов возникли проблемы',
                'successfully' => 'error_questions_list'));
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' =>  'В базе отсуствуют тесты по выбранной категории',
                'successfully' => 'error_empty_array'));
        }
        return $result;
    }

    /**
     * @param $category
     * @param $table_index
     * @param array $category_copy
     * @param int $first_parent
     * @param null $open_symbol
     * @return array
     */
    public function prepareParentList ($category, $table_index, $category_copy = [], $first_parent = 0, $open_symbol = null) {

        $open_symbol .= '{DV="'.$table_index.'_open_symbol"}';//&nbsp;
        foreach ($category as $key => $value) {
            if ($value[$table_index.'_parent_id'] == $first_parent) {
                $category_copy[$key] = $value;
                $category_copy[$key][$table_index.'_open_value'] = $open_symbol;
                $category_copy = $this->prepareParentList($category, $table_index, $category_copy, $value[$table_index.'_id'], $open_symbol);
            }
        }
        return $category_copy;
    }

    /**
     * @param $tr_name
     * @param $tCategory
     * @param $tr_number_shown_questions
     * @param $tr_success_value_type
     * @param $tr_success_value_num
     * @param $tr_control_test_time_per_questions
     * @param $tr_total_time_value
     * @return int
     */
    public function checkInputData ($tr_name, $tCategory, $tr_number_shown_questions, $tr_success_value_type, $tr_success_value_num, $tr_control_test_time_per_questions, $tr_total_time_value) {

        $correct_input_data = 1;
        if (empty($tr_name)) {
            array_push($this->actionResultMessage, array('message' => 'Поле с названием теста не заполнено',
                'successfully' => 'empty_test_name'));
            $correct_input_data = 0;
        }
        if (empty($tCategory)) {
            array_push($this->actionResultMessage, array('message' => 'Категория теста не указана',
                'successfully' => 'empty_test_category'));
            $correct_input_data = 0;
        }
        if (!empty($tr_number_shown_questions)) {
            if (!is_numeric($tr_number_shown_questions) || $tr_number_shown_questions < 0) {
                array_push($this->actionResultMessage, array('message' => 'Поле с количeством вопросов в тесте должно быть положительным числом',
                    'successfully' => 'incorrect_number_shown_questions'));
                $correct_input_data = 0;
            }
            if (is_numeric($tr_number_shown_questions) && $tr_number_shown_questions > 0 && ($tr_success_value_type == 3)) {
                array_push($this->actionResultMessage, array('message' => 'Правильный ответ не может определяться по "весу вопросов", если в тесте выводятся не все вопросы, а некоторое их число',
                    'successfully' => 'incorrect_number_shown_questions'));
                $correct_input_data = 0;
            }
        }
        if (empty($tr_success_value_type)) {
            array_push($this->actionResultMessage, array('message' => 'В поле "Определение прав. ответа" не выбрано значение',
                'successfully' => 'empty_tr_success_value_type'));
            $correct_input_data = 0;
        }
        if (!empty($tr_success_value_num)) {
            if (!is_numeric($tr_success_value_num) || $tr_success_value_num < 0) {
                array_push($this->actionResultMessage, array('message' => 'Поле со значением, необходимым для прохождения теста должно быть положительным числом',
                    'successfully' => 'incorrect_tr_success_value_num'));
                $correct_input_data = 0;
            }
        }
        if (($tr_control_test_time_per_questions == 0) && empty($tr_total_time_value)) {
            array_push($this->actionResultMessage, array('message' => 'Поле со значением времени на тест должно быть заполнено, т.к. контролируется "общее время теста"',
                'successfully' => 'incorrect_tr_total_time_value'));
            $correct_input_data = 0;
        }
        if (!empty($tr_total_time_value)) {
            if (!is_numeric($tr_total_time_value) || $tr_total_time_value < 0) {
                array_push($this->actionResultMessage, array('message' => 'Поле со значением времени на тест должно быть положительным числом',
                    'successfully' => 'incorrect_tr_total_time_value'));
                $correct_input_data = 0;
            }
        }
        return $correct_input_data;
    }

    /**
     * @param $testsArray
     * @return mixed
     */
    public function prepareTplTestsArray($testsArray) {

        foreach ($testsArray as $key => &$value) {
            if ($value['tr_pause_test_permission'] == 1) {
                $value['tr_pause_test_permission'] = "Да";
            }
            else {
                $value['tr_pause_test_permission'] = "Нет";
            }
            if ($value['tr_show_right_answer'] == 1) {
                $value['tr_show_right_answer'] = "Да";
            }
            else {
                $value['tr_show_right_answer'] = "Нет";
            }
            if ($value['tr_show_final_mark'] == 1) {
                $value['tr_show_final_mark'] = "Да";
            }
            else {
                $value['tr_show_final_mark'] = "Нет";
            }
            if ($value['tr_show_final_verdict'] == 1) {
                $value['tr_show_final_verdict'] = "Да";
            }
            else {
                $value['tr_show_final_verdict'] = "Нет";
            }
            if ($value['tr_success_value_type'] == 1) {
                $value['tr_success_value_type'] = "По количеству вопросов";
            }
            elseif ($value['tr_success_value_type'] == 2) {
                $value['tr_success_value_type'] = "По проценту вопросов";
            }
            elseif ($value['tr_success_value_type'] == 3) {
                $value['tr_success_value_type'] = "По весу вопросов";
            }
            if ($value['tr_number_shown_questions'] == 0) {
                $value['tr_number_shown_questions'] = "Все";
            }
            if ($value['tr_skip_questions'] == 1) {
                $value['tr_skip_questions'] = "Да";
            }
            else {
                $value['tr_skip_questions'] = "Нет";
            }
            if ($value['tr_control_test_time_per_questions'] == 1) {
                $value['tr_control_test_time_per_questions'] = "Да";
            }
            else {
                $value['tr_control_test_time_per_questions'] = "Нет";
            }
            if ($value['tr_total_time_value'] == 0) {
                $value['tr_total_time_value'] = "Не установлено";
            }
            $value['tr_adding_time'] = date('Y.m.d H:i:s', $value['tr_adding_time']);
            if ($value['tr_adding_time'] == 0) {
                $value['tr_adding_time'] = "No time";
            }
        }
        unset($value);
        return $testsArray;
    }

    /**
     * @param $tr_id
     */
    public function deleteTest($tr_id) {

        $result = $this->objTestRepositoryDB->deleteTest($tr_id);
        if ($result) {
            array_push ($this->actionResultMessage, array('message' => 'Тест был успешно удален',
                'successfully' => '1'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Проблемы с удалением теста, сообщите о проблеме администратору',
                'successfully' => 'failed_deleting'));
        }
    }
}
