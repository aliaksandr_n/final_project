<?php

/**
 * Class AppointedTestController
 *
 * This class generates user's right table and provides their changing.
 */
class UserRightController {

    /**
     * Reference to the object with queries to the db, which connected with that class-controller.
     *
     * @var null
     */
    protected $objUserRightDB = null;

    /**
     * The service array with service messages to inform the user.
     *
     * @var array
     */
    protected $actionResultMessage = [];

    public function __construct($obj_user_right_DB ) {
        $this->objUserRightDB = $obj_user_right_DB;
    }

    /**
     * Main method which declares vars and operate with other methods.
     *
     * @return mixed
     *  The array with vars for the template
     */
    public function getTplArray() {

        $action = null;
        if (!empty($_POST['action'])) {
            $action = $_POST['action'];
        }
        $trainer_right = [];
        if (!empty($_POST['trainer_right'])) {
            $trainer_right = $_POST['trainer_right'];
        }
        $manager_right = [];
        if (!empty($_POST['manager_right'])) {
            $manager_right = $_POST['manager_right'];
        }
        $admin_right = [];
        if (!empty($_POST['admin_right'])) {
            $admin_right = $_POST['admin_right'];
        }
        $all_users = [];
        if (!empty($_POST['all_users'])) {
            $all_users = $_POST['all_users'];
        }
        if (!empty($action)) {
            $this->modifyUserRight($trainer_right, $manager_right, $admin_right, $all_users);
        }
        $user_right_array = $this->getUserRight();
        $result_array['foreach_arrs']['user_right_array'] = [];
        if (!empty($user_right_array)) {
            $result_array['foreach_arrs']['user_right_array'] = $user_right_array;
        }
        $actionMessages = $this->actionResultMessage;
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        return $result_array;
    }

    /**
     * The method gets users rights from db abd prepares array for the table.
     */
    public function getUserRight() {

        $result = $this->objUserRightDB->readUserRight();
        if (is_array($result)) {
            foreach ($result as $key=>$value) {
                if ($value['ru_user_right'] & Router::TRAINER_RIGHT) {
                    $result[$key]['trainer_right_checked'] = 'checked="checked"';
                }
                else {
                    $result[$key]['trainer_right_checked'] = '';
                }
                if ($value['ru_user_right'] & Router::MANAGER_RIGHT) {
                    $result[$key]['manager_right_checked'] = 'checked="checked"';
                }
                else {
                    $result[$key]['manager_right_checked'] = '';
                }
                if ($value['ru_user_right'] & Router::ADMIN_RIGHT) {
                    $result[$key]['admin_right_checked'] = 'checked="checked"';
                }
                else {
                    $result[$key]['admin_right_checked'] = '';
                }
            }
            return $result;
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' => 'Пользователи отсутсвуют в базе',
                'successfully' => 'empty_category_array'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'При формировании таблицы прав пользователей возникли проблемы',
                'successfully' => 'failed_reading'));
        }
    }

    /**
     * The method update user's rights in db.
     *
     * @param $trainer_right
     *  Array with user, who was admitted as a trainer in the table.
     * @param $manager_right
     *  Array with user, who was admitted as a manager in the table.
     * @param $admin_right
     *  Array with user, who was admitted as an admin in the table.
     * @param $all_users
     *  Array with all user from the table.
     */
    public function modifyUserRight($trainer_right, $manager_right, $admin_right, $all_users) {

        $error_exist = 0;
        foreach ($all_users as $key=>$value) {
            $new_right = Router::USER_RIGHT;
            if (array_key_exists ($key, $trainer_right)) {
                $new_right = $new_right | Router::TRAINER_RIGHT;
            }
            if (array_key_exists ($key, $manager_right)) {
                $new_right = $new_right | Router::MANAGER_RIGHT;
            }
            if (array_key_exists ($key, $admin_right)) {
                $new_right = $new_right | Router::ADMIN_RIGHT;
            }
            $result = $this->objUserRightDB->modifyUserRight($key, $new_right);
            if ($result == false) {
                $error_exist = 1;
            }
        }
        if ($error_exist == 0) {
            array_push ($this->actionResultMessage, array('message' => 'Права была успешно изменены. Для того, чтобы у пользователя отразились изменения прав, ему необходимо перезати в систему',
                'successfully' => '1'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'При изменении прав пользователей возникли проблемы',
                'successfully' => 'failed_modifying'));
        }
    }
}
