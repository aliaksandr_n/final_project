<?php

/**
 * Class VerificationController
 *
 * Confirm user registration after sending email with confirm reference.
 */
class VerificationController {

    /**
     * Reference to the object with queries to the db, which connected with that class-controller.
     *
     * @var null
     */
    protected $objVerificationDB = null;

    /**
     * The service array with service messages to inform the user.
     *
     * @var array
     */
    protected $actionResultMessage = [];

    public function __construct($obj_verification_DB) {

        $this->objVerificationDB = $obj_verification_DB;
    }

    /**
     * Main method of the class
     *
     * @return mixed
     *  Result array for template
     */
    public function getTplArray() {

        $key = null;
        if (!empty($_GET['key'])) {
            $key = $_GET['key'];

            $result_key_finding = $this->objVerificationDB->findVerificationKey($key);
            if (!is_array($result_key_finding)) {
                array_push ($this->actionResultMessage, array('message' => 'Ключ для восстановления пароля не найден.<br />
                    Попробуйте <a href="index.php?page=registration">зарегистрироваться</a> еще раз.<br />
                    В случае повторных проблем обратитесь к администратору',
                    'successfully' => 'key_is_not_exist'));
            }
            else {
                $activated = $result_key_finding['uri_activated'];
                if ($activated == 1) {
                    array_push ($this->actionResultMessage, array('message' => 'Данная учетная запись уже была активирована',
                        'successfully' => 'account_activated'));
                }
                else {
                    $login = $result_key_finding['uri_login'];
                    $password = $result_key_finding['uri_password'];
                    $email = $result_key_finding['uri_email'];
                    $name = $result_key_finding['uri_name'];
                    $surname = $result_key_finding['uri_surname'];
                    $reg_date = $result_key_finding['uri_reg_date'];
                    $uri_user_group_id = $result_key_finding['uri_user_group_id'];
                    $reg_id = $result_key_finding['uri_id'];

                    $result_adding_user = $this->objVerificationDB->addRegisteredUser($login, $password, $email, $name, $surname, $reg_date);
                    if ($result_adding_user == false) {
                        array_push ($this->actionResultMessage, array('message' => 'Не удалось добавить пользователе в базу',
                            'successfully' => 'user_adding_failed'));
                    }
                    else {
                        if (!empty($uri_user_group_id)) {
                            $result_adding_ugroup = $this->objVerificationDB->addUserGroup($result_adding_user, $uri_user_group_id);
                            if ($result_adding_ugroup == false) {
                                array_push ($this->actionResultMessage, array('message' => 'Пользователь добавлен, но не удалось добавить пользователя в группу',
                                    'successfully' => 'group_adding_failed'));
                            }
                        }
                        $user_id = $result_adding_user;
                        $activation_date = time();
                        $result_key_act = $this->objVerificationDB->markKeyAsActivated($activation_date, $login, $user_id, $reg_id);
                        $result_other_key_act = $this->objVerificationDB->markOtherKeyAsUsed($login, $user_id, $reg_id);

                        if ($result_key_act == true  && $result_other_key_act == true) {
                            array_push($this->actionResultMessage, array('message' => 'Вы успешно зарегистрированы. Попробуйте <a href="index.php?page=login">войти</a> в систему используя новый пароль',
                                'successfully' => '1'));
                        }
                        else {
                            array_push($this->actionResultMessage, array('message' => 'Пользователь зарегистрирован, но возникли проблемы с дизактивацией временного ключа ',
                                'successfully' => 'error_disact_key'));
                        }
                    }
                }
            }
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Вы попали на эту страницу способом, который не был предусмотрен авторами сайта.<br />
                                        Если вы хотите зарегистрироваться- перейдите по данной <a href="index.php?page=registration">ссылке</a>',
                'successfully' => 'letter_dont_send'));
        }

        $actionMessages = $this->actionResultMessage;
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        return $result_array;
    }
}
