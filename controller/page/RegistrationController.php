<?php

/**
 * Class RegistrationController
 */
class RegistrationController {

    /**
     * Reference to the object
     * @var null
     */
    protected $objRegistrationDB = null;

    /**
     * Array with results
     * @var array
     */
    protected $actionResultMessage = []; //

    /**
     * @var array
     */
    protected $userGroupArray = [];

    public function __construct($obj_registration_DB, $user_group_array) {

        $this->objRegistrationDB = $obj_registration_DB;
        $this->userGroupArray = $user_group_array;
    }

    /**
     * @return mixed
     */
    public function getTplArray() {
        $login = null;
        if (!empty($_POST['login'])) {
            $login = $_POST['login'];
        }
        $password = null;
        if (!empty($_POST['password'])) {
            $password = $_POST['password'];
        }
        $password2 = null;
        if (!empty($_POST['password2'])) {
            $password2 = $_POST['password2'];
        }
        $email = null;
        if (!empty($_POST['email'])) {
            $email = $_POST['email'];
        }
        $name = null;
        if (!empty($_POST['name'])) {
            $name = $_POST['name'];
        }
        $surname = null;
        if (!empty($_POST['surname'])) {
            $surname = $_POST['surname'];
        }
        $user_group_DB = $this->userGroupArray;
        $user_group_array = [];
        if ($user_group_DB && is_array($user_group_DB)) {
            $user_group_array = $user_group_DB;
            foreach ($user_group_array as $key => $value) {
                $user_group_array[$key]['selected_group'] = null;
            }
        }
        $user_group = null;
        if (isset($_POST['user_group'])) {
            $user_group = $_POST['user_group'];
            foreach ($user_group_array as $key => $value) {
                if ($value['ug_id'] == $user_group) {
                    $user_group_array[$key]['selected_group'] = "selected='selected'";
                }
            }
        }
        $submit_pressed = null;
        if (!empty($_POST['submit_pressed'])) {
            $submit_pressed = $_POST['submit_pressed'];
        }
        $sha1_password = sha1($password);
        $reg_date = time();
        $if_var_registered = -1;

        if ($submit_pressed == 1) {
            $checking_result = $this->checkInputData($login, $password, $password2, $email, $name, $surname, $user_group);
            if ($checking_result == 1) {
                $result_search_login = $this->objRegistrationDB->registered_user_search("ru_login",$login);
    			if ($result_search_login == false) {
                    array_push ($this->actionResultMessage, array('message' => 'При проверке пользователей с указанным логином возникли ошибки',
                        'successfully' => 'letter_dont_send'));
                }
                elseif (!empty($result_search_login) && is_array($result_search_login)) {
                        array_push ($this->actionResultMessage, array('message' => 'Пользователь с таким логином уже зарегистрирован в системе',
                            'successfully' => 'user_exist'));
                }
    			else {
        			$result_search_email = $this->objRegistrationDB->registered_user_search("ru_email",$email);
                    if ($result_search_email == false) {
                        array_push ($this->actionResultMessage, array('message' => 'При проверке пользователей с указанным e-mail возникли ошибки',
                            'successfully' => 'finding_email_problem'));
                    }
                    elseif (!empty($result_search_email) && is_array($result_search_email)) {
                        array_push ($this->actionResultMessage, array('message' => 'Пользователь с таким e-mail уже зарегистрирован в системе',
                            'successfully' => 'email_exist'));
                    }
                    else {
                        $registration_key= sha1($login.$email.time());
                        $result_reg_user = $this->objRegistrationDB->registerNewUser($login, $sha1_password, $email, $name, $surname, $user_group, $reg_date, $registration_key);
                        $if_var_registered = 1;
                        if (empty($result_reg_user)) {
                            array_push ($this->actionResultMessage, array('message' => 'Не удалось добавить данные на сервер. Попробуйте позже, либо напишите администратору',
                                'successfully' => 'error_adding_user'));
                        }
                        else {
                            $mail_sent = $this->sentRegistrationEmail($email, $registration_key);
                            if ($mail_sent == true) {
                                array_push ($this->actionResultMessage, array('message' => 'На ваш почтовый ящик было выслано письмо. Пройдите по ссылке, указанной в письме для завершения регистрации',
                                    'successfully' => '1'));
                            }
                            else {
                                array_push ($this->actionResultMessage, array('message' => 'Не удалось отправить на ваш e-mal письмо для подтвержения регмистрации. Попробуйте зарегистрироваться еще раз. В случае повторого возникновения ошибки- обратитесь к администратору',
                                    'successfully' => 'letter_sending_problem'));
                            }
                        }
                    }
                }
    		}
        }
        $result_array['dynamic_vars']['login'] = '';
        if (!empty($login)) {
            $result_array['dynamic_vars']['login'] = $login;
        }
        $result_array['dynamic_vars']['password'] = '';
        if (!empty($password)) {
            $result_array['dynamic_vars']['password'] = $password;
        }
        $result_array['dynamic_vars']['password2'] = '';
        if (!empty($password2)) {
            $result_array['dynamic_vars']['password2'] = $password2;
        }
        $result_array['dynamic_vars']['email'] = '';
        if (!empty($email)) {
            $result_array['dynamic_vars']['email'] = $email;
        }
        $result_array['dynamic_vars']['name'] = '';
        if (!empty($name)) {
            $result_array['dynamic_vars']['name'] = $name;
        }
        $result_array['dynamic_vars']['surname'] = '';
        if (!empty($surname)) {
            $result_array['dynamic_vars']['surname'] = $surname;
        }
        $result_array['dynamic_vars']['user_group'] = '';
        if (!empty($user_group)) {
            $result_array['dynamic_vars']['user_group'] = $user_group;
        }
        $result_array['foreach_arrs']['user_group_array'] = $user_group_array;
        $result_array['if_vars']['registered'] = $if_var_registered;

        $actionMessages = $this->actionResultMessage;
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        return $result_array;
    }

    /**
     * @param $login
     * @param $password
     * @param $password2
     * @param $email
     * @param $name
     * @param $surname
     * @param $user_group
     * @return int
     */
    public function checkInputData($login, $password, $password2, $email, $name, $surname, $user_group) {

        $correct_input_data = 1;
        if (empty($login)) {
            array_push ($this->actionResultMessage, array('message' => 'Поле "Логин" не заполнено',
                'successfully' => 'empty_login'));
            $correct_input_data = 0;
        }
        elseif (!preg_match("/^\w{3,}$/", $login)) {
            array_push ($this->actionResultMessage, array('message' => 'В поле "Логин" введены недопустимые символы (только буквы, цифры и подчеркивание), либо логин меньше 3 символов',
                'successfully' => 'incorrect_login'));
            $correct_input_data = 0;
        }
        if (empty($password)) {
            array_push ($this->actionResultMessage, array('message' => 'Поле "Пароль" не заполнено',
                'successfully' => 'empty_password'));
            $correct_input_data = 0;
        }
        elseif (!preg_match("/\A(\w){6,20}\Z/", $password)) {
            array_push ($this->actionResultMessage, array('message' => 'Пароль не соотвествует требованим безопасности. Пароль должен быть не менее 6 символов и содержать только буквы, цифры и символ подчеркивания',
                'successfully' => 'incorrect_password'));
            $correct_input_data = 0;
        }
        if (empty($password2)) {
            array_push ($this->actionResultMessage, array('message' => 'Поле с подтвержением пароля не заполнено',
                'successfully' => 'incorrect_password2'));
            $correct_input_data = 0;
        }
        elseif ($password != $password2) {
            array_push ($this->actionResultMessage, array('message' => 'Введенные пароли не совпадают',
                'successfully' => 'different_passwords'));
            $correct_input_data = 0;
        }
        if (empty($email)) {
            array_push ($this->actionResultMessage, array('message' => 'Поле "E-mail" не заполнено',
                'successfully' => 'empty_email'));
            $correct_input_data = 0;
        }
        elseif (!preg_match("/^[a-zA-Z0-9_\.\-]+@([a-zA-Z0-9\-]+\.)+[a-zA-Z]{2,15}$/", $email)) {
            array_push ($this->actionResultMessage, array('message' => 'E-mail имеет недопустимий формат (e-mail должен иметь вид name@example.test)',
                'successfully' => 'incorrect_email'));
            $correct_input_data = 0;
        }
        if (empty($name)) {
            array_push ($this->actionResultMessage, array('message' => 'Поле "Имя" не заполнено',
                'successfully' => 'empty_name'));
            $correct_input_data = 0;
        }
        if (empty($surname)) {
            array_push ($this->actionResultMessage, array('message' => 'Поле "Фамилия" не заполнено',
                'successfully' => 'empty_surname'));
            $correct_input_data = 0;
        }
        if (empty($user_group)) {
            array_push ($this->actionResultMessage, array('message' => 'Поле "Группа" не заполнено',
                'successfully' => 'empty_user_group'));
            $correct_input_data = 0;
        }
        return $correct_input_data;
    }

    /**
     * @param $email
     * @param $registration_key
     * @return bool
     */
    public function sentRegistrationEmail ($email, $registration_key) {

        $to = $email;
        $subject = '=?utf-8?b?'. base64_encode('Регистрация на сайте '.$_SERVER['SERVER_NAME'].''). '?=';
        $message = 'Добрый день. Ваш ящик был указан при регистрации на сайте '.$_SERVER['SERVER_NAME'].'. Для окончания регистрации пройдите по указанной ссылке: '.$_SERVER['SERVER_NAME'].'/index.php?page=verification&key='.$registration_key.'. Если вы получили данное письмо по ошибке- проигнорируйте его. В случае неоднократного получения аналогичных писем- сообщите администратору сайта.';
        $headers  = 'MIME-Version: 1.0'."\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
        /*$headers .= 'To: user <'.$to.'>'."\r\n";*/
        $headers .= 'From: '.$_SERVER['SERVER_NAME'].'=?utf-8?b?'. base64_encode(' регистрация'). '?='.    ' <'.ADMIN_EMAIL.'@'.$_SERVER['SERVER_NAME'].'>'."\r\n";
        $mail_sent = mail($to,$subject,$message,$headers);

        return $mail_sent;
    }
}
