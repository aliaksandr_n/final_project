<?php

/**
 * Class TrainerStatController
 */
class TrainerStatController {

    /**
     * @var null
     */
    protected $objTrainerStatDB = null;

    /**
     * @var array
     */
    protected $actionResultMessage = [];

    public function __construct($obj_trainer_stat_DB) {

        $this->objTrainerStatDB = $obj_trainer_stat_DB;
    }

    /**
     * @return mixed
     */
    public function getTplArray() {

        $include_user_stat = false;
        $include_test_stat = false;
        $search_type = 1;

        if (!empty($_GET['search_type'])) {
            if ($_GET['search_type'] == '1') {

                $groups_stat_array = $this->objTrainerStatDB->getUserStat();
                if (!empty($groups_stat_array) && is_array($groups_stat_array)) {
                    //array_push($this->actionResultMessage, array('message' => 'Таблица со статистикой тестов успешно сформирована',
                    //    'successfully' => '1'));
                }
                elseif ($groups_stat_array == true) {
                    array_push($this->actionResultMessage, array('message' => 'Таблица со статистикой тестов пуста',
                        'successfully' => 'failed_adding'));
                }
                else {
                    array_push($this->actionResultMessage, array('message' => 'При формировании таблицы со статистикой тестов возникли проблемы',
                        'successfully' => 'failed_adding'));
                }
                $include_user_stat = true;
                $include_test_stat = false;
                $search_type = 1;
            }
            elseif ($_GET['search_type'] == '2') {

                $groups_stat_array = $this->objTrainerStatDB->getTestStat();
                if (!empty($groups_stat_array) && is_array($groups_stat_array)) {
                    //array_push($this->actionResultMessage, array('message' => 'Таблица со статистикой пользователей успешно сформирована',
                    //    'successfully' => '1'));
                }
                elseif ($groups_stat_array == true) {
                    array_push($this->actionResultMessage, array('message' => 'Таблица со статистикой пользователей пуста',
                        'successfully' => 'failed_adding'));
                }
                else {
                    array_push($this->actionResultMessage, array('message' => 'При формировании таблицы со статистикой пользователей возникли проблемы',
                        'successfully' => 'failed_adding'));
                }
                $include_user_stat = false;
                $include_test_stat = true;
                $search_type = 2;
            }
        }


        $result_array['dynamic_vars']['include_user_stat'] = $include_user_stat;
        $result_array['dynamic_vars']['include_user_stat'] = $include_user_stat;

        $result_array['if_vars']['include_user_stat'] = $include_user_stat;
        $result_array['if_vars']['include_test_stat'] = $include_test_stat;

        $result_array['dynamic_vars']['checked_1_search_type'] = '';
        if (!empty($search_type) && ($search_type == 1)) {
            $result_array['dynamic_vars']['checked_1_search_type'] = 'checked="checked"';
        }
        $result_array['dynamic_vars']['checked_2_search_type'] = '';
        if (!empty($search_type) && ($search_type == 2)) {
            $result_array['dynamic_vars']['checked_2_search_type'] = 'checked="checked"';
        }

        $result_array['foreach_arrs']['groups_stat_array'] = '';
        if (!empty($groups_stat_array)) {
            $result_array['foreach_arrs']['groups_stat_array'] = $groups_stat_array;
        }
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($this->actionResultMessage)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $this->actionResultMessage;
        }
        return $result_array;
    }
}
