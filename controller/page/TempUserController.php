<?php

/**
 * Class TempUserController
 */
class TempUserController {

    /**
     * Reference to the object
     * @var null
     */
    protected $objTempUserDB = null;

    /**
     * Array with temp user array
     * @var array
     */
    protected $tempUserArray = [];

    /**
     * Array with results
     * @var array
     */
    protected $actionResultMessage = [];

    /**
     * @return array
     */
    public function getTempUserArray() {
        $tempUserArray = $this->tempUserArray;
        return $tempUserArray;
    }

    /**
     * @return array
     */
    public function getActionResultMessage() {
        $actionResultMessage = $this->actionResultMessage;
        return $actionResultMessage;
    }

    public function __construct($obj_temp_user_DB ) {
        $this->objTempUserDB = $obj_temp_user_DB;
    }

    /**
     * @return mixed
     */
    public function getTplArray() {

        $action = null;
        if (!empty($_GET['action'])) {
            $action = $_GET['action'];
        }
        $tu_id = null;
        if (!empty($_GET['tu_id'])) {
            $tu_id = $_GET['tu_id'];
        }
        elseif (!empty($_POST['tu_id'])) {
            $tu_id = $_POST['tu_id'];
        }
        $tu_key = null;
        if (!empty($_POST['tu_key'])) {
            $tu_key = $_POST['tu_key'];
        }
        $tu_comment = '';
        if (!empty($_POST['tu_comment'])) {
            $tu_comment = $_POST['tu_comment'];
        }
        $modified_temp_user_array = [];
        //----------------------------------

        if (!empty($action)) {
            switch ($action) {
                case 'add':
                    if (!empty($tu_key)) {
                        $this->addTempUser($tu_key, $tu_comment);
                    }
                    break;
                case 'modify_tlp':
                    if (!empty($tu_id)) {
                        $modified_temp_user_array = $this->selectModifiedTempUser($tu_id);
                    }
                    break;
                case 'modify':
                    if (!empty($tu_key)) {
                    $result = $this->modifyTempUser($tu_id, $tu_key, $tu_comment);
                        if ($result == true ) {
                            $hide_modify_form = true;
                        }
                    }
                    break;
                case 'delete':
                    if (!empty($tu_id)) {
                        $this->deleteTempUser($tu_id);
                    }
                    break;
            }
        }

        $this->readTempUser();
        $actionMessages = $this->actionResultMessage;
        $temp_user = $this->tempUserArray;
        $new_key = sha1(mt_rand(1, 1000000000).mt_rand(1000000, 5555555).mt_rand(1, 1000000000));
        //----------------------------------
        $result_array['dynamic_vars']['tu_id'] = '';
        if (!empty($tu_id)) {
            $result_array['dynamic_vars']['tu_id'] = $tu_id;
        }
        $result_array['dynamic_vars']['selected_tu_key'] = '';
        if (!empty($modified_temp_user_array['tu_key'])) {
            $result_array['dynamic_vars']['selected_tu_key'] = $modified_temp_user_array['tu_key'];
        }
        $result_array['dynamic_vars']['new_key'] = '';
        if (!empty($new_key)) {
            $result_array['dynamic_vars']['new_key'] = $new_key;
        }
        $result_array['dynamic_vars']['selected_tu_comment'] = '';
        if (!empty($modified_temp_user_array['tu_comment'])) {
            $result_array['dynamic_vars']['selected_tu_comment'] = $modified_temp_user_array['tu_comment'];
        }
        $result_array['foreach_arrs']['temp_user_array'] = [];
        if (!empty($temp_user)) {
            $result_array['foreach_arrs']['temp_user_array'] = $temp_user;
        }
        $result_array['if_vars']['temp_user_action'] = false;
        if (!empty($tu_id)) {
            $result_array['if_vars']['temp_user_action'] = $tu_id;
        }
        if (!empty($hide_modify_form) && ($hide_modify_form === true)) {
            $result_array['if_vars']['temp_user_action'] = false;
        }
        $result_array['if_vars']['tu_comment'] = false;
        if (!empty($modified_temp_user_array['tu_comment'])) {
            $result_array['if_vars']['tu_comment'] = $modified_temp_user_array['tu_comment'];
        }
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        return $result_array;
    }

    /**
     *
     */
    public function readTempUser() {

        $result = $this->objTempUserDB->readTempUser();
        if (is_array($result)) {
            $this->tempUserArray = $result;
            //array_push ($this->actionResultMessage, array('message' => 'Список одноразовых ключей успешно сформирован',
            //                                         'successfully' => '1'));
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' => 'Список одноразовых ключей пуст',
                                                     'successfully' => 'empty_temp_user_array'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'При формировании списка одноразовых ключей возникли проблемы',
                                                     'successfully' => 'failed_reading'));
        }
    }

    /**
     * @param $tu_key
     * @param $tu_comment
     */
    public function addTempUser($tu_key, $tu_comment) {

        $result = $this->objTempUserDB->addTempUser($tu_key, $tu_comment);
        if ($result) {
            array_push ($this->actionResultMessage, array('message' => 'Одноразовый ключ был успешно добавлен',
                                                     'successfully' => '1'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Проблемы с добавлением одноразового ключа',
                                                     'successfully' => 'failed_adding'));
        }
    }

    /**
     * @param $tu_id
     * @return bool
     */
    public function selectModifiedTempUser($tu_id) {

        $result = $this->objTempUserDB->selectModifiedTempUser($tu_id);
        if ($result) {
            array_push ($this->actionResultMessage, array('message' => 'Одноразовый ключ готов к редактированию',
                                                     'successfully' => '1'));
            return $result;
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Вы не можете внести изменения в указанный одноразовый ключ, т.к. он либо уже отсуствует на сервере, либо сущестует несколько одноразовых ключей с одинаковым идентификатором',
                                                     'successfully' => 'doubled_temp_user'));
            return false;
        }
    }

    /**
     * @param $tu_id
     * @param $tu_key
     * @param $tu_comment
     * @return bool
     */
    public function modifyTempUser($tu_id, $tu_key, $tu_comment) {

        $result = $this->objTempUserDB->modifyTempUser($tu_id, $tu_key, $tu_comment);
        if ($result) {
            array_push ($this->actionResultMessage, array('message' => 'Одноразовый ключ был успешно изменен',
                                                     'successfully' => '1'));
            return true;
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Проблемы с изменением одноразового ключа',
                                                     'successfully' => 'failed_modifying'));
            return false;
        }
    }

    /**
     * @param $tu_id
     */
    public function deleteTempUser($tu_id) {

        $result = $this->objTempUserDB->deleteTempUser($tu_id);
        if ($result) {
            array_push ($this->actionResultMessage, array('message' => 'Одноразовый ключ был успешно удален',
                                                     'successfully' => '1'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Проблемы с удалением одноразового ключа, сообщите о проблеме администратору',
                                                     'successfully' => 'failed_deleting'));
        }
    }
}
