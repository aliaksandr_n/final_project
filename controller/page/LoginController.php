<?php
/**
 * This class is used for user's authorization
 *
 * Class LoginController
 */
class LoginController {

    /**
     * The reference to object "LoginDB", which is used only by class "Login"
     * @var
     */
    protected $objLoginDb;

    /**
     * This property contains user's login after authorization
     *
     * @var
     */
    protected $login;

    /**
     * This property contains whether user's login authorization  should be saved for some time or not
     * @var
     */
    protected $rememberMe;

    /**
     * This property contains user's id from database after authorization
     * @var
     */
    protected $userId;

    /**
     * This property contains user's right from database after authorization
     *
     * @var
     */
    protected $userRight;

    /**
     * This property contains the message, which user gets after successful or not successful authorization
     * @var string
     */
    protected $message;

    /**
     * The "getter" of the object's property "login"
     * @return mixed
     */
    public function getLogin(){
        return $this->login;
    }

    /**
     * The "getter" of the object's property "rememberMe"
     * @return mixed
     */
    public function getRememberMe(){
        return $this->rememberMe;
    }

    /**
     * The "getter" of the object's property "message"
     * @return string
     */
    public function getMessage(){
        return $this->message;
    }

    /**
     * The "getter" of the object's property "userId"
     * @return mixed
     */
    public function getUserId(){
        return $this->userId;
    }
    /**
     * The "getter" of the object's property "userRight"
     * @return mixed
     */
    public function getUserRight(){
        return $this->userRight;
    }
    /**
     * The method create the class "LoginDB" and check, whether the user is authorized
     * @param $obj_login_DB
     */
    public function __construct($obj_login_DB) {

        $this->objLoginDb = $obj_login_DB;
        $this->check_cookie();
        $this->message = $this->checkSession();
    }

    /**
     * @return mixed
     */
    public function getTplArray() {

        $result_array['dynamic_vars']['user_login'] = false;
        if (!empty($_SESSION['user_login'])) {
            $result_array['dynamic_vars']['user_login'] = $_SESSION['user_login'];
        }
        $result_array['dynamic_vars']['user_name'] = false;
        if (!empty($_SESSION['user_name'])) {
            $result_array['dynamic_vars']['user_name'] = $_SESSION['user_name'];
        }
        $result_array['dynamic_vars']['user_surname'] = false;
        if (!empty($_SESSION['user_surname'])) {
            $result_array['dynamic_vars']['user_surname'] = $_SESSION['user_surname'];
        }
        $result_array['dynamic_vars']['checked_button'] = false;
        if (!empty($this->rememberMe)) {
            $result_array['dynamic_vars']['checked_button'] = 'checked="checked"';
        }
        $result_array['if_vars']['login_status'] = false;
        if (!empty($this->message)) {
            $result_array['if_vars']['login_status'] = $this->message;
        }
        $result_array['dynamic_vars']['log_in'] = "Unaccepted";
        if ($this->message == 'accepted') {
            $result_array['dynamic_vars']['log_in'] = "Accepted";
        }
        return $result_array;
    }

    /**
     * The method for user input and output to / from the system
     * @return string
     */
    protected function checkSession() {

        $login = 0;
        if (!empty($_POST['login'])) {
            $login = trim($_POST['login']);
        }
        $pass  = 0;
        if (!empty($_POST['pass'])) {
            $pass = trim($_POST['pass']);
        }
        $remember_me  = false;
        if (!empty($_POST['remember_me'])) {
            $remember_me = true;
        }
        //If the user authorizated- put his  data into the properties of this the class and form the "message"
        if ((isset($_SESSION['user_logged_in'])) && ($_SESSION['user_logged_in'] === true)) {
            $message = 'accepted';
            $this->login = $_SESSION['user_login'];
            $this->userId = $_SESSION['user_id'];
            $this->userRight = $_SESSION['user_right'];
            //If the user log out, destroy session, change session's properties and form the "message"
            if ((isset($_GET['action'])) && ($_GET['action'] === 'logout')) {
                setcookie('remember_me', '-', time()-1209600);
                $this->objLoginDb->deleteAuthInDb($_SESSION['user_login']);
                $_SESSION['user_logged_in'] = false;
                session_destroy();
                unset($_SESSION);
                $message = 'log_out';
                //Split this class to session-cookies= class and login-class and delete next two lines in future
                header("Location: ".$_SERVER['HTTP_REFERER']);
                exit;
            }
        }
        //If the user doesn't authorizated- show him authorization form. If he sent to us the login and password- check it
        else {
            //If he sent to us the login and password- check it in database
            if (!empty($login)&& !empty($pass)) {
                $pass = sha1($pass);
                $result = $this->objLoginDb->checkLoginAndPassInDB($login, $pass);
                //If the login and password are correct- change session's properties
                if (count($result) > 1) {
                    //If user wants to save his authorization for some time- write his key to database
                    if ($remember_me === true) {
                        $auth = sha1(mt_rand(1, 1000000000).mt_rand(1000000, 5555555).mt_rand(1, 1000000000));
                        $this->objLoginDb->rememberUserInDb($auth, $login, $pass);
                        setcookie('remember_me', $auth, time()+1209600);
                    }
                    $_SESSION['user_logged_in'] = true;
                    $_SESSION['user_name'] = $result['ru_name'];
                    $_SESSION['user_surname']= $result['ru_surname'];
                    $_SESSION['user_login'] = $result['ru_login'];
                    $_SESSION['user_id'] = $result['ru_id'];
                    $_SESSION['user_right'] = $result['ru_user_right'];
                    $message = 'accepted';
                    $this->login = $_SESSION['user_login'];
                    $this->userId = $_SESSION['user_id'];
                    $this->userRight = $_SESSION['user_right'];
                    $this->rememberMe = $remember_me;
                }
                //If user's login or password are incorrect- write to the user about incorrect information
                else {
                    $_SESSION['user_logged_in'] = false;
                    $message = 'login_failed';
                }
            }
            //If the login and password weren't send- show usual login form
            else {
                $message = 'unaccepted';
            }
        }
        return $message;
    }

    /**
     * The method for checking user's cookies in database and change sessions properties
     */
    private function check_cookie() {
        //If  cookies are- check it
        if (isset($_COOKIE['remember_me'])) {
            $auth = $_COOKIE['remember_me'];
            $result = $this->objLoginDb->findCookieInDb($auth);
            //If  the cookies were found in the database- change session properties
            if (count($result) > 0) {
                $_SESSION['user_logged_in'] = true;
                $_SESSION['user_name'] = $result['ru_name'];
                $_SESSION['user_surname'] = $result['ru_surname'];
                $_SESSION['user_id'] = $result['ru_id'];
                $_SESSION['user_login'] = $result['ru_login'];
                $_SESSION['user_right'] = $result['ru_user_right'];
            } else {
                $_SESSION['user_logged_in'] = false;
            }
        }
    }
}
