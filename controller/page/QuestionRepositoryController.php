<?php

/**
 * Class QuestionRepositoryController
 */
class QuestionRepositoryController {


    /**
     * Reference to the object
     * @var null
     */
    protected $objQuestionRepositoryDB = null;
    /**
     * Array with repositories array
     * @var array
     */
    protected $questionRepositoryArray = [];
    /**
     * Array with results
     * @var array
     */
    protected $actionResultMessage = [];

    /**
     * @var array
     */
    protected $selectedModifiedQuestionRepository = array('qr_id' => null, 'qr_name' => null, 'qr_parent_id' => null); // Array with id

    /**
     * @return array
     */
    public function getQuestionRepositoryArray() {
        $questionRepositoryArray = $this->questionRepositoryArray;
        return $questionRepositoryArray;
    }

    /**
     * @return array
     */
    public function getActionResultMessage() {
        $actionResultMessage = $this->actionResultMessage;
        return $actionResultMessage;
    }

    /**
     * @return array
     */
    public function getSelectedModifiedQuestionRepository() {
        $selectedModifiedQuestionRepository = $this->selectedModifiedQuestionRepository;
        return $selectedModifiedQuestionRepository;
    }

    /**
     * @param $obj_question_repository_DB
     */
    public function __construct($obj_question_repository_DB )
    {
        $this->objQuestionRepositoryDB = $obj_question_repository_DB;
    }

    /**
     * @return mixed
     */
    public function getTplArray() {

        $action = null;
        if (!empty($_GET['action'])) {
            $action = $_GET['action'];
        }
        $qr_id = null;
        if (!empty($_GET['qr_id'])) {
            $qr_id = $_GET['qr_id'];
        }
        elseif (!empty($_POST['qr_id'])) {
            $qr_id = $_POST['qr_id'];
        }
        $qr_name = null;
        if (!empty($_POST['qr_name'])) {
            $qr_name = $_POST['qr_name'];
        }
        $qr_parent_id = 0;
        if (!empty($_POST['qr_parent_id'])) {
            $qr_parent_id = $_POST['qr_parent_id'];
        }
        //----------------------------------
        if (!empty($action)) {
            if (($action == 'add') && (!empty($qr_name))) {
                $this->addQuestionRepository($qr_name, $qr_parent_id);
            }
            elseif (($action == 'modify_tlp') && (!empty($qr_id))) {
                $this->selectModifiedQuestionRepository($qr_id);
            }
            elseif (($action == 'modify') && (!empty($qr_name))) {
                $this->modifyQuestionRepository($qr_id, $qr_name, $qr_parent_id);
            }
            elseif (($action == 'delete') && (!empty($qr_id))) {
                $this->deleteQuestionRepository($qr_id);
            }
        }
        $this->readQuestionRepository();
        $modified_repository_array = $this->selectedModifiedQuestionRepository;
        $actionMessages = $this->actionResultMessage;
        $questionRepository = $this->questionRepositoryArray;
        if (is_array($questionRepository)) {
            $question_repository_array = $this->prepareParentList($questionRepository, 'qr');
        }
        //----------------------------------
        $result_array['dynamic_vars']['qr_open_symbol'] = OPEN_SYMBOL_FOR_HIERARCHIC_LIST;
        $result_array['dynamic_vars']['qr_id'] = '';
        if (!empty($qr_id)) {
            $result_array['dynamic_vars']['qr_id'] = $qr_id;
        }
        $result_array['dynamic_vars']['selectedModifiedQuestionRepository'] = '';
        if (!empty($modified_repository_array['qr_name'])) {
            $result_array['dynamic_vars']['selectedModifiedQuestionRepository'] = $modified_repository_array['qr_name'];
        }
        $result_array['foreach_arrs']['question_repository_array'] = [];
        if (!empty($question_repository_array)) {
            $result_array['foreach_arrs']['question_repository_array'] = $question_repository_array;
        }
        $result_array['if_vars']['question_repository_action'] = false;
        if (!empty($qr_id) && (!empty($qr_id)) && ($action == 'modify_tlp')) {
            $result_array['if_vars']['question_repository_action'] = $qr_id;
        }
        $result_array['if_vars']['qr_parent_id'] = false;
        if (!empty($modified_repository_array['qr_parent_id'])) {
            $result_array['if_vars']['qr_parent_id'] = $modified_repository_array['qr_parent_id'];
        }
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        return $result_array;
    }

    /**
     *
     */
    public function readQuestionRepository() {

        $result = $this->objQuestionRepositoryDB->readQuestionRepository();
        if (is_array($result)) {
            $this->questionRepositoryArray = $result;
            //array_push ($this->actionResultMessage, array('message' => 'Список наборов успешно сформирован',
            //    'successfully' => '1'));
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' => 'Список наборов пуст',
                'successfully' => 'empty_repository_array'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'При формировании списка наборов возникли проблемы',
                'successfully' => 'failed_reading'));
        }
    }

    /**
     * @param $qr_name
     * @param $qr_parent_id
     */
    public function addQuestionRepository($qr_name, $qr_parent_id) {

        $result = $this->objQuestionRepositoryDB->addQuestionRepository($qr_name, $qr_parent_id);
        if ($result) {
            array_push ($this->actionResultMessage, array('message' => 'Набор был успешно добавлен',
                'successfully' => '1'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Проблемы с добавлением набора',
                'successfully' => 'failed_adding'));
        }
    }

    /**
     * @param $qr_id
     */
    public function selectModifiedQuestionRepository($qr_id) {

        $result = $this->objQuestionRepositoryDB->selectModifiedQuestionRepository($qr_id);
        if ($result) {
            array_push ($this->actionResultMessage, array('message' => 'Набор готов к редактированию',
                'successfully' => '1'));
            $this->selectedModifiedQuestionRepository['qr_id'] = $result['qr_id'];
            $this->selectedModifiedQuestionRepository['qr_name'] = $result['qr_name'];
            $this->selectedModifiedQuestionRepository['qr_parent_id'] = $result['qr_parent_id'];
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Вы не можете внести изменения в указанный набор, т.к. он либо уже отсуствует на сервере, либо сущестует несколько наборов с одинаковым идентификатором',
                'successfully' => 'doubled_repository'));
        }
    }

    /**
     * @param $qr_id
     * @param $qr_name
     * @param $qr_parent_id
     */
    public function modifyQuestionRepository($qr_id, $qr_name, $qr_parent_id) {

        $result = $this->objQuestionRepositoryDB->modifyQuestionRepository($qr_id, $qr_name, $qr_parent_id);
        if ($result) {
            array_push ($this->actionResultMessage, array('message' => 'Набор был успешно изменен',
                'successfully' => '1'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Проблемы с изменением набора',
                'successfully' => 'failed_modifying'));
        }
    }

    /**
     * @param $qr_id
     */
    public function deleteQuestionRepository($qr_id) {

        $result = $this->objQuestionRepositoryDB->findQuestionRepositoryChild($qr_id);
        if(is_array($result)) {
            array_push ($this->actionResultMessage, array('message' => 'Набор не может быть удален, т.к. у него есть поднаборы',
                'successfully' => 'error_deleting_parent_with_child'));
        }
        else {
            $result = $this->objQuestionRepositoryDB->deleteQuestionRepository($qr_id);
            if ($result) {
                array_push ($this->actionResultMessage, array('message' => 'Набор был успешно удален',
                    'successfully' => '1'));
            }
            else {
                array_push ($this->actionResultMessage, array('message' => 'Проблемы с удалением набора, сообщите о проблеме администратору',
                    'successfully' => 'failed_deleting'));
            }
        }
    }

    /**
     * @param $repository
     * @param $table_index
     * @param array $repository_copy
     * @param int $first_parent
     * @param null $open_symbol
     * @return array
     */
    public function prepareParentList ($repository, $table_index, $repository_copy = [], $first_parent = 0, $open_symbol = null) {

        $open_symbol .= '{DV="'.$table_index.'_open_symbol"}';//&nbsp;
        foreach ($repository as $key => $value) {
            if ($value[$table_index.'_parent_id'] == $first_parent) {
                $repository_copy[$key] = $value;
                $repository_copy[$key][$table_index.'_open_value'] = $open_symbol;
                $repository_copy = $this->prepareParentList($repository, $table_index, $repository_copy, $value[$table_index.'_id'], $open_symbol);
            }
        }
        return $repository_copy;
    }
}
