<?php

/**
 * This class can show the questions from sets and tests and can copy or move questions from any set or  test to any set or test
 *
 * Class QuestionManagerController
 */
class QuestionManagerController {

    /**
     * Reference to the object with queries to the db, which connected with that class-controller.
     *
     * @var null
     */
    protected $objTestRepositoryDB = null; // Reference to the object

    /**
     * The array with question's categories.
     *
     * @var array
     */
    protected $questionCategoryArray = []; // Array with categories array

    /**
     * The array with question's sets.
     *
     * @var array
     */
    protected $questionRepositoryArray = [];

    /**
     * The array with tests' categories.
     *
     * @var array
     */
    protected $testCategoryArray = [];

    /**
     * The service array with service messages to inform the user.
     *
     * @var array
     */
    protected $actionResultMessage = []; // Array with results

    public function __construct($obj_question_manager_DB, $question_category_array, $question_repository_array, $test_category_array) {

        $this->objTestRepositoryDB = $obj_question_manager_DB;
        $this->questionCategoryArray = $this->prepareParentList($question_category_array, 'qc');
        $this->questionRepositoryArray = $this->prepareParentList($question_repository_array, 'qr');
        $this->testCategoryArray = $this->prepareParentList($test_category_array, 'tc');
    }

    /**
     * "Getter" for "actionResultMessage" property.
     *
     * @return array
     */
    public function getActionResultMessage() {
        $actionResultMessage = $this->actionResultMessage;
        return $actionResultMessage;
    }

    /**
     * Main method declares vars and operate with other methods.
     *
     * @return mixed
     */
    public function getTplArray() {

        $q_category_DB = $this->questionCategoryArray;
        $q_category_tpl_search = [];
        if ($q_category_DB) {
            $q_category_tpl_search = $q_category_DB;
            foreach ($q_category_tpl_search as $key => $value) {
                $q_category_tpl_search[$key]['selected_question_type'] = null;
            }
        }
        $result_array['dynamic_vars']['qc_open_symbol'] = '-';
        //----------------------------------
        $q_category_input = null;
        if (isset($_GET['q_category'])) {
            $q_category_input = $_GET['q_category'];
            foreach ($q_category_tpl_search as $key => $value) {
                if ($value['qc_id'] == $q_category_input) {
                    $q_category_tpl_search[$key]['selected_question_type'] = "selected='selected'";
                }
            }
        }
        //----------------------------------
        $q_repository_DB = $this->questionRepositoryArray;
        $q_repository_tpl_search = [];
        if ($q_repository_DB) {
            $q_repository_tpl_search = $q_repository_DB;
            foreach ($q_repository_tpl_search as $key => $value) {
                $q_repository_tpl_search[$key]['selected_question_repository'] = null;
            }
        }
        $q_repository_tpl_action = $q_repository_tpl_search;
        $result_array['dynamic_vars']['qr_open_symbol'] = '-';
        //----------------------------------
        $q_repository_input = null;
        if (isset($_GET['q_repository'])) {
            $q_repository_input = $_GET['q_repository'];
            foreach ($q_repository_tpl_search as $key => $value) {
                if ($value['qr_id'] == $q_repository_input) {
                    $q_repository_tpl_search[$key]['selected_question_repository'] = "selected='selected'";
                }
            }
        }
        //----------------------------------
        $t_repository_DB = $this->getTestArray();
        $t_repository_tpl_search = [];
        if ($t_repository_DB) {
            $t_repository_tpl_search = $t_repository_DB;
            foreach ($t_repository_tpl_search as $key => $value) {
                $t_repository_tpl_search[$key]['selected_test_category'] = null;
            }
        }
        //Make array copy
        $t_repository_tpl_action = $t_repository_tpl_search;
        //----------------------------------
        $t_repository_input = null;
        if (isset($_GET['t_repository'])) {
            $t_repository_input = $_GET['t_repository'];
            foreach ($t_repository_tpl_search as $key => $value) {
                if ($value['tr_id'] == $t_repository_input) {
                    $t_repository_tpl_search[$key]['selected_test_category'] = "selected='selected'";
                }
            }
        }
        //----------------------------------
        $action = null;
        if (!empty($_GET['action'])) {
            $action = $_GET['action'];
        }
        $search_type = 1;
        if (!empty($_GET['search_type'])) {
            $search_type = $_GET['search_type'];
        }
        $q_action = 1;
        if (isset($_POST['q_action'])) {
            $q_action = $_POST['q_action'];
        }
        $checked_question = [];
        if (!empty($_POST['question'])) {
            $checked_question = $_POST['question'];
        }
        $submit_pressed = null;
        if (!empty($_POST['submit_pressed'])) {
            $submit_pressed = $_POST['submit_pressed'];
        }
        $q_repository_search = null;
        if (isset($_POST['q_repository_action'])) {
            $q_repository_search = $_POST['q_repository_action'];
            foreach ($q_repository_tpl_action as $key => $value) {
                if ($value['qr_id'] == $q_repository_search) {
                    $q_repository_tpl_action[$key]['selected_question_repository'] = "selected='selected'";
                }
            }
        }
        $t_repository_search = null;
        if (isset($_POST['t_repository_action'])) {
            $t_repository_search = $_POST['t_repository_action'];
            foreach ($t_repository_tpl_action as $key => $value) {
                if ($value['tr_id'] == $t_repository_search) {
                    $t_repository_tpl_action[$key]['selected_test_category'] = "selected='selected'";
                }
            }
        }
        //----------------------------------
        $result_array['if_vars']['include_search_form'] = false;
        $result_array['if_vars']['include_result_form'] = false;
        //----------------------------------
        if ($action == 'search') {
            $result_array['if_vars']['include_search_form'] = true;
            if ($search_type == 1) {
                $search_form_type = "1AllByCat";
                if (isset($q_category_input)) {
                    $result_array['if_vars']['include_result_form'] = true;
                    $question_array = $this->selectQuestionByCategory($q_category_input);
                    if (!empty($question_array) && is_array($question_array)) {
                        if (!empty($submit_pressed)) {
                            $result_checked_question = $this->checkAdmitedQuestions($checked_question);
                            if ($result_checked_question == 1) {
                                $checked_question = $this->getCheckedQuestionId($checked_question, $question_array, 'qcg_id', 'qcg_question_setting_id');
                                $correct_answer_time = $this->checkAnswerTime($checked_question, $t_repository_search, $q_action);
                                if ($correct_answer_time == true) {
                                    if (isset($q_action)) {
                                        $chekingResult = $this->checkInuptDataFotAction($q_action, $q_repository_search, $t_repository_search);
                                        if ($chekingResult == 1) {
                                            $this->doAction($q_action, $q_repository_search, $t_repository_search, $checked_question, $search_type);
                                        }
                                    }
                                    $question_array = $this->selectQuestionByCategory($q_category_input);
                                    if (!empty($question_array) && is_array($question_array)) {
                                        $question_array = $this->prepareCheckedQuestion($checked_question, $question_array, 'qcg_id');
                                    }
                                }
                            }
                        }
                    }
                }
            }
            elseif ($search_type == 2) {
                $search_form_type = "2ByRep";
                if (isset($q_repository_input)) {
                    $result_array['if_vars']['include_result_form'] = true;
                    $question_array = $this->selectQuestionByRepos($q_repository_input);
                    if (!empty($question_array) && is_array($question_array)) {
                        if (!empty($submit_pressed)) {
                            $result_checked_question = $this->checkAdmitedQuestions($checked_question);
                            if ($result_checked_question == 1) {
                                $checked_question = $this->getCheckedQuestionId($checked_question, $question_array, 'qrg_id', 'qrg_question_setting_id');
                                $correct_answer_time = $this->checkAnswerTime($checked_question, $t_repository_search, $q_action);
                                if ($correct_answer_time == true) {
                                    if (isset($q_action)) {
                                        $chekingResult = $this->checkInuptDataFotAction($q_action, $q_repository_search, $t_repository_search);
                                        if ($chekingResult == 1) {
                                            $this->doAction($q_action, $q_repository_search, $t_repository_search, $checked_question, $search_type);
                                        }
                                    }
                                    $question_array = $this->selectQuestionByRepos($q_repository_input);
                                    if (!empty($question_array) && is_array($question_array)) {
                                        $question_array = $this->prepareCheckedQuestion($checked_question, $question_array, 'qrg_id');
                                    }
                                }
                            }
                        }
                    }
                }
            }
            elseif ($search_type == 3) {
                $search_form_type = "3ByTest";
                if (isset($t_repository_input)) {
                    $result_array['if_vars']['include_result_form'] = true;
                    $question_array = $this->selectQuestionByTest($t_repository_input);
                    if (!empty($question_array) && is_array($question_array)) {
                        if (!empty($submit_pressed)) {
                            $result_checked_question = $this->checkAdmitedQuestions($checked_question);
                            if ($result_checked_question == 1) {
                                $checked_question = $this->getCheckedQuestionId($checked_question, $question_array, 'trg_id', 'trg_question_setting_id');
                                $correct_answer_time = $this->checkAnswerTime($checked_question, $t_repository_search, $q_action);
                                if ($correct_answer_time == true) {
                                    if (isset($q_action)) {
                                        $chekingResult = $this->checkInuptDataFotAction($q_action, $q_repository_search, $t_repository_search);
                                        if ($chekingResult == 1) {
                                            $this->doAction($q_action, $q_repository_search, $t_repository_search, $checked_question, $search_type);
                                        }
                                    }
                                    $question_array = $this->selectQuestionByTest($t_repository_input);
                                    if (!empty($question_array) && is_array($question_array)) {
                                        $question_array = $this->prepareCheckedQuestion($checked_question, $question_array, 'trg_id');
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                array_push($this->actionResultMessage, array('message' => 'Необходимо выбрать один из вариантов поиска',
                    'successfully' => 'bad_search_variant'));
                $result_array['if_vars']['include_search_form'] = false;
            }

        }
        $result_array['dynamic_vars']['checked_action_1'] = "";
        $result_array['dynamic_vars']['checked_action_2'] = "";
        $result_array['dynamic_vars']['checked_action_3'] = "";
        $result_array['dynamic_vars']['checked_action_4'] = "";
        $result_array['dynamic_vars']['checked_action_5'] = "";
        if(isset($q_action)) {
            $result_array['dynamic_vars']['checked_action_'.$q_action] = 'checked="checked"';
        }
        //----------------------------------
        $result_array['dynamic_vars']['get_string_q_category'] = "";
        if(isset($q_category_input)) {
            $result_array['dynamic_vars']['get_string_q_category'] = "&q_category=" . $q_category_input;
        }
        $result_array['dynamic_vars']['get_string_q_repository'] = "";
        if(isset($q_repository_input)) {
            $result_array['dynamic_vars']['get_string_q_repository'] = "&q_repository=" . $q_repository_input;
        }
        $result_array['dynamic_vars']['get_string_t_repository'] = "";
        if(isset($t_repository_input)) {
            $result_array['dynamic_vars']['get_string_t_repository'] = "&t_repository=" . $t_repository_input;
        }
        //----------------------------------
        $result_array['dynamic_vars']['checked_1_search_type'] = '';
        $result_array['dynamic_vars']['checked_2_search_type'] = '';
        $result_array['dynamic_vars']['checked_3_search_type'] = '';
        if  ($search_type == 1) {
            $result_array['dynamic_vars']['checked_1_search_type'] = 'checked="checked"';
        }
        elseif  ($search_type == 2) {
            $result_array['dynamic_vars']['checked_2_search_type'] = 'checked="checked"';
        }
        elseif  ($search_type == 3) {
            $result_array['dynamic_vars']['checked_3_search_type'] = 'checked="checked"';
        }
        //----------------------------------
        $result_array['foreach_arrs']['question_array'] = [];
        $result_array['if_vars']['empty_dir'] = false;
        if (!empty($question_array) && is_array($question_array)) {
            $result_array['foreach_arrs']['question_array'] = $question_array;
        }
        else {
            $result_array['if_vars']['empty_dir'] = true;
        }
        //----------------------------------
        $result_array['dynamic_vars']['search_form_type'] = '';
        if (!empty($search_form_type)){
            $result_array['dynamic_vars']['search_form_type'] = $search_form_type;
        }
        $result_array['foreach_arrs']['question_category_array_search'] = $q_category_tpl_search;
        $result_array['foreach_arrs']['question_repository_array_search'] = $q_repository_tpl_search;
        $result_array['foreach_arrs']['test_repository_array_search'] = $t_repository_tpl_search;

        $result_array['foreach_arrs']['question_repository_array_action'] = $q_repository_tpl_action;
        $result_array['foreach_arrs']['test_repository_array_action'] = $t_repository_tpl_action;
        //----------------------------------
        $actionMessages = $this->actionResultMessage;
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        return $result_array;
    }

    /**
     * This method gets test array from db
     */
    public function getTestArray() {

        $result = $this->objTestRepositoryDB->selectTestArray();
        if (is_array($result)) {
            //array_push ($this->actionResultMessage, array('message' => 'Список категорий успешно сформирован',
            //                                         'successfully' => '1'));
            return $result;
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' => 'Список категорий пуст',
                'successfully' => 'empty_test_array'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'При формировании списка категорий возникли проблемы',
                'successfully' => 'error_get_test_array'));
        }
    }

    /**
     * This service method prepare hierarchic(al) list of categories and sets
     */
    public function prepareParentList ($category, $table_index, $category_copy = [], $first_parent = 0, $open_symbol = null) {

        $open_symbol .= '{DV="'.$table_index.'_open_symbol"}';//&nbsp;
        foreach ($category as $key => $value) {
            if ($value[$table_index.'_parent_id'] == $first_parent) {
                $category_copy[$key] = $value;
                $category_copy[$key][$table_index.'_open_value'] = $open_symbol;
                $category_copy = $this->prepareParentList($category, $table_index, $category_copy, $value[$table_index.'_id'], $open_symbol);
            }
        }
        return $category_copy;
    }

    /**
     * This method selects question categories from db
     */
    public function selectQuestionByCategory($q_category_input) {

        if ($q_category_input == 0) {
            $result = $this->objTestRepositoryDB->selectQuestionByAllCategory();
        }
        else {
            $result = $this->objTestRepositoryDB->selectQuestionByOneCategory($q_category_input);
        }
        if ($result === false) {
            array_push ($this->actionResultMessage, array('message' =>  'При формировании списка вопросов возникли проблемы',
                'successfully' => 'error_questions_list'));
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' =>  'В базе отсуствуют вопросы по выбранной категории',
                'successfully' => 'error_empty_array'));
        }
        else {
            foreach ($result as $key => $value) {
                $result[$key]['question_checked'] = null;
            }
            //array_push ($this->actionResultMessage, array('message' =>  'Список вопросов сформирован',
            //    'successfully' => '1'));
        }
        return $result;
    }

    /**
     * This method selects question sets from db
     */
    public function selectQuestionByRepos($q_repository_input) {

        if ($q_repository_input == 0) {
            $result = $this->objTestRepositoryDB->selectQuestionByAllRepos();
        }
        else {
            $result = $this->objTestRepositoryDB->selectQuestionByOneRepos($q_repository_input);
        }
        if ($result === false) {
            array_push ($this->actionResultMessage, array('message' =>  'При формировании списка вопросов возникли проблемы',
                'successfully' => 'error_questions_list'));
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' =>  'В базе отсуствуют вопросы по выбранному набору',
                'successfully' => 'error_empty_array'));
        }
        else {
            foreach ($result as $key => $value) {
                $result[$key]['question_checked'] = null;
            }
            //array_push ($this->actionResultMessage, array('message' =>  'Список вопросов сформирован',
            //    'successfully' => '1'));
        }
        return $result;
    }

    /**
     * This method selects from db tests from tests (from one or more tests)
     */
    public function selectQuestionByTest($t_repository_input) {

        if ($t_repository_input == 0) {
            $result = $this->objTestRepositoryDB->selectQuestionByAllTest();
        }
        else {
            $result = $this->objTestRepositoryDB->selectQuestionByOneTest($t_repository_input);
        }
        if ($result === false) {
            array_push ($this->actionResultMessage, array('message' =>  'При формировании списка вопросов возникли проблемы',
                'successfully' => 'error_questions_list'));
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' =>  'В базе отсуствуют вопросы по выбранному тесту',
                'successfully' => 'error_empty_array'));
        }
        else {
            //array_push ($this->actionResultMessage, array('message' =>  'Список вопросов сформирован',
            //    'successfully' => '1'));
            foreach ($result as $key => $value) {
                $result[$key]['question_checked'] = null;
            }
        }
        return $result;
    }

    /**
     * This method add question Id to array with selected questions
     *
     * @param $checked_question
     * @param $question_array
     * @param $group_index
     * @param $question_index
     * @return mixed
     */
    public function getCheckedQuestionId ($checked_question, $question_array, $group_index, $question_index) {

        foreach ($question_array as $key => $value) {
            $question_id = $value[$group_index];
            if (array_key_exists($question_id, $checked_question)) {
                $checked_question[$question_id] = $value[$question_index];
            }
        }
        return $checked_question;
    }

    /**
     * This method admits selected questions in array
     *
     * @param $checked_question
     * @param $question_array
     * @param $el_id_index
     * @return mixed
     */
    public function prepareCheckedQuestion($checked_question, $question_array, $el_id_index){

        if (!empty($checked_question)) {
            $checked_question = $_POST['question'];
            foreach ($question_array as $key => $value) {
                if (array_key_exists($value[$el_id_index], $checked_question)) {
                    $question_array[$key]['question_checked'] = "checked='checked'";
                }
            }
        }
        return $question_array;
    }

    /**
     * This method checks  input data before action
     *
     * @param $q_action
     * @param $q_repository_search
     * @param $t_repository_search
     * @return int
     */
    public function checkInuptDataFotAction($q_action, $q_repository_search, $t_repository_search) {

        $correct_input_data = 1;
        if (empty($q_action)) {
            array_push($this->actionResultMessage, array('message' => 'Поле с необходимым действием не заполнено',
                'successfully' => 'empty_action'));
            $correct_input_data = 0;
        }
        if (($q_action <> 5) && (empty($q_repository_search) && empty($t_repository_search))) {
            array_push($this->actionResultMessage, array('message' => 'Поле с информацией куда копируется (перемещается) вопрос не заполнено',
                'successfully' => 'empty_q_or_t_array'));
            $correct_input_data = 0;
        }
        return $correct_input_data;
    }

    /**
     * This method control time property in tests: if test should control time, added question should control time too.
     *
     * @param $checked_question
     * @param $t_repository_search
     * @param $q_action
     * @return bool
     */
    public function checkAnswerTime($checked_question, $t_repository_search, $q_action) {

        if ($q_action == 1) {
            return true;
        }
        else {
            $test_checking_result = $this->objTestRepositoryDB->getTestTimeControlSetting($t_repository_search);
            if ($test_checking_result === false) {
                array_push($this->actionResultMessage, array('message' => 'Не удалось получить настройки времени выполнения теста',
                    'successfully' => 'empty_action'));
                return false;
            } elseif ($test_checking_result == 0) {
                return true;
            } elseif ($test_checking_result == 1) {
                $error_counter = 0;
                $bad_question_counter = 0;
                foreach ($checked_question as $key => $value) {
                    $question_checking_result = $this->objTestRepositoryDB->getQuestionTimeDuration($value);
                    if ($question_checking_result == false) {
                        $error_counter++;
                    } elseif ($question_checking_result == 0) {
                        $bad_question_counter++;
                    }
                }
                if ($error_counter > 0) {
                    array_push($this->actionResultMessage, array('message' => 'Вопросы не могут быть добавлены в выбранный тест, т.к. в выбранном тесте ведется учет времени ответа ПО КАЖДОМУ ВОПРОСУ, однако не во всех выбранных вопросах проставлено время, отведенное на конкретный вопрос',
                        'successfully' => 'incorrect_answer_time'));
                    return false;
                } elseif ($bad_question_counter > 0) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    /**
     * This method check whether questions from table selected or not
     *
     * @param $checked_question
     * @return int
     */
    public function checkAdmitedQuestions($checked_question) {

        $correct_input_data = 1;
        if (empty($checked_question) || !is_array($checked_question)) {
            array_push($this->actionResultMessage, array('message' => 'В табличной части не отмечены вопросы, с которыми необходимо совершить действие',
                'successfully' => 'empty_question_array'));
            $correct_input_data = 0;
        }
        return $correct_input_data;
    }

    /**
     * This method do main action with questions (moving and coping)
     *
     * @param $q_action
     * @param $q_repository_search
     * @param $t_repository_search
     * @param $checked_question
     * @param $search_type
     */
    public function doAction($q_action, $q_repository_search, $t_repository_search, $checked_question, $search_type) {

        $error_adding_counter = 0;
        $error_deleting_counter = 0;
        if ($q_action == 1) {//Copy to set
            foreach ($checked_question as $key => $value) {
                $adding_result = $this->objTestRepositoryDB->addQuestionToRepository($value, $q_repository_search);
                if ($adding_result == false) {
                    $error_adding_counter++;
                }
            }
            if ($error_adding_counter === 0) {
                array_push ($this->actionResultMessage, array('message' => 'Вопросы был успешно добавлены в набор',
                    'successfully' => '1'));
            }
            else {
                array_push ($this->actionResultMessage, array('message' => 'Не удалось добавить вопросы в набор',
                    'successfully' => 'error_adding_repository'));
            }
        }
        elseif ($q_action == 2) {//Copy to test
            foreach ($checked_question as $key => $value) {
                $adding_result = $this->objTestRepositoryDB->addQuestionToTest($value, $t_repository_search);
                if ($adding_result == false) {
                    $error_adding_counter++;
                }
            }
            if ($error_adding_counter === 0) {
                array_push ($this->actionResultMessage, array('message' => 'Вопросы был успешно добавлены в тест',
                    'successfully' => '1'));
            }
            else {
                array_push ($this->actionResultMessage, array('message' => 'Не удалось добавить вопросы в тест',
                    'successfully' => 'error_adding_repository'));
            }
        }
        elseif ($q_action == 3) {//Move to set
            foreach ($checked_question as $key => $value) {
                $adding_result = $this->objTestRepositoryDB->addQuestionToRepository($value, $q_repository_search);
                if ($adding_result == false) {
                    $error_adding_counter++;
                }
                $deleting_result = $this->deleteQuestionFrom($search_type, $key);
                if ($deleting_result == false) {
                    $error_deleting_counter++;
                }
            }
            if ($error_adding_counter === 0) {
                array_push ($this->actionResultMessage, array('message' => 'Вопросы был успешно добавлены в набор',
                    'successfully' => '1'));
            }
            else {
                array_push ($this->actionResultMessage, array('message' => 'Не удалось добавить вопросы в набор',
                    'successfully' => 'error_adding_repository'));
            }
            if ($error_deleting_counter == 0) {
                array_push ($this->actionResultMessage, array('message' => 'Вопрос был успешно удален из первоначального источника (набора, либо теста)',
                    'successfully' => '1'));
            }
            else {
                array_push ($this->actionResultMessage, array('message' => 'Не удалось удалить вопрос',
                    'successfully' => 'error_delete'));
            }
        }
        elseif ($q_action == 4) {//move to test
            foreach ($checked_question as $key => $value) {
                $adding_result = $this->objTestRepositoryDB->addQuestionToTest($value, $t_repository_search);
                if ($adding_result == false) {
                    $error_adding_counter++;
                }
                $deleting_result = $this->deleteQuestionFrom($search_type, $key);
                if ($deleting_result == false) {
                    $error_deleting_counter++;
                }
            }
            if ($error_adding_counter === 0) {
                array_push ($this->actionResultMessage, array('message' => 'Вопросы был успешно добавлены в тест',
                    'successfully' => '1'));
            }
            else {
                array_push ($this->actionResultMessage, array('message' => 'Не удалось добавить вопросы в тест',
                    'successfully' => 'error_adding_repository'));
            }
            if ($error_deleting_counter == 0) {
                array_push ($this->actionResultMessage, array('message' => 'Вопрос был успешно удален из первоначального источника (набора, либо теста)',
                    'successfully' => '1'));
            }
            else {
                array_push ($this->actionResultMessage, array('message' => 'Не удалось удалить вопрос',
                    'successfully' => 'error_delete'));
            }
        }
        elseif ($q_action == 5) {//Удалить из тек.набора
            foreach ($checked_question as $key => $value) {
                $deleting_result = $this->deleteQuestionFrom($search_type, $key);
                if ($deleting_result == false) {
                    $error_deleting_counter++;
                }
            }
            if ($error_deleting_counter == 0) {
                array_push ($this->actionResultMessage, array('message' => 'Вопрос был успешно удален',
                    'successfully' => '1'));
            }
            else {
                array_push ($this->actionResultMessage, array('message' => 'Не удалось удалить вопрос',
                    'successfully' => 'error_delete'));
            }
        }
    }

    /**
     * This method delete questions from db
     *
     * @param $search_type
     * @param $key
     * @return mixed
     */
    protected function deleteQuestionFrom($search_type, $key){

        if ($search_type == 2) {
            $result = $this->objTestRepositoryDB->deleteQuestionFromQuestionRepository($key);
            return $result;
        } elseif ($search_type == 3) {
            $result = $this->objTestRepositoryDB->deleteQuestionFromTestRepository($key);
            return $result;
        }
    }
}