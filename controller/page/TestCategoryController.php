<?php

/**
 * Class TestCategoryController
 */
class TestCategoryController {

    /**
     * Reference to the object
     * @var null
     */
    protected $objTestCategoryDB = null;

    /**
     * Array with categories array
     * @var array
     */
    protected $testCategoryArray = [];

    /**
     * Array with results
     * @var array
     */
    protected $actionResultMessage = [];

    /**
     * Array with id
     * @var array
     */
    protected $selectedModifiedTestCategory = array('tc_id' => null, 'tc_name' => null, 'tc_parent_id' => null);

    /**
     * @return array
     */
    public function getTestCategoryArray() {
        $testCategoryArray = $this->testCategoryArray;
        return $testCategoryArray;
    }

    /**
     * @return array
     */
    public function getActionResultMessage() {
        $actionResultMessage = $this->actionResultMessage;
        return $actionResultMessage;
    }

    /**
     * @return array
     */
    public function getSelectedModifiedTestCategory() {
        $selectedModifiedTestCategory = $this->selectedModifiedTestCategory;
        return $selectedModifiedTestCategory;
    }

    public function __construct($obj_test_category_DB )
    {
        $this->objTestCategoryDB = $obj_test_category_DB;
    }

    /**
     * @return mixed
     */
    public function getTplArray() {

        $action = null;
        if (!empty($_GET['action'])) {
            $action = $_GET['action'];
        }
        $tc_id = null;
        if (!empty($_GET['tc_id'])) {
            $tc_id = $_GET['tc_id'];
        }
        elseif (!empty($_POST['tc_id'])) {
            $tc_id = $_POST['tc_id'];
        }
        $tc_name = null;
        if (!empty($_POST['tc_name'])) {
            $tc_name = $_POST['tc_name'];
        }
        $tc_parent_id = 0;
        if (!empty($_POST['tc_parent_id'])) {
            $tc_parent_id = $_POST['tc_parent_id'];
        }
        //----------------------------------
        if (!empty($action)) {
            if (($action == 'add') && (!empty($tc_name))) {
                $this->addTestCategory($tc_name, $tc_parent_id);
            }
            elseif (($action == 'modify_tlp') && (!empty($tc_id))) {
                $this->selectModifiedTestCategory($tc_id);
            }
            elseif (($action == 'modify') && (!empty($tc_name))) {
                $this->modifyTestCategory($tc_id, $tc_name, $tc_parent_id);
            }
            elseif (($action == 'delete') && (!empty($tc_id))) {
                $this->deleteTestCategory($tc_id);
            }
        }
        $this->readTestCategory();
        $modified_category_array = $this->selectedModifiedTestCategory;
        $actionMessages = $this->actionResultMessage;
        $test_category_array = $this->testCategoryArray;
        //----------------------------------
        $result_array['dynamic_vars']['tc_open_symbol'] = OPEN_SYMBOL_FOR_HIERARCHIC_LIST;
        $result_array['dynamic_vars']['tc_id'] = '';
        if (!empty($tc_id)) {
            $result_array['dynamic_vars']['tc_id'] = $tc_id;
        }
        $result_array['dynamic_vars']['selectedModifiedTestCategory'] = '';
        if (!empty($modified_category_array['tc_name'])) {
            $result_array['dynamic_vars']['selectedModifiedTestCategory'] = $modified_category_array['tc_name'];
        }
        $result_array['foreach_arrs']['test_category_array'] = [];
        if (!empty($test_category_array)) {
            $result_array['foreach_arrs']['test_category_array'] = $test_category_array;
        }
        $result_array['if_vars']['test_category_action'] = false;
        if (!empty($tc_id) && (!empty($tc_id)) && ($action == 'modify_tlp')) {
            $result_array['if_vars']['test_category_action'] = $tc_id;
        }
        $result_array['if_vars']['tc_parent_id'] = false;
        if (!empty($modified_category_array['tc_parent_id'])) {
            $result_array['if_vars']['tc_parent_id'] = $modified_category_array['tc_parent_id'];
        }
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        return $result_array;
    }

    /**
     *
     */
    public function readTestCategory() {

        $result = $this->objTestCategoryDB->readTestCategory();
        if (is_array($result)) {
            $result = $this->prepareParentList($result, 'tc');
            $this->testCategoryArray = $result;
            //array_push ($this->actionResultMessage, array('message' => 'Список категорий успешно сформирован',
            //    'successfully' => '1'));
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' => 'Список категорий пуст',
                'successfully' => 'empty_category_array'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'При формировании списка категорий возникли проблемы',
                'successfully' => 'failed_reading'));
        }
    }

    /**
     * @param $tc_name
     * @param $tc_parent_id
     */
    public function addTestCategory($tc_name, $tc_parent_id) {

        $result = $this->objTestCategoryDB->addTestCategory($tc_name, $tc_parent_id);
        if ($result) {
            array_push ($this->actionResultMessage, array('message' => 'Категория была успешно добавлена',
                'successfully' => '1'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Проблемы с добавлением категории',
                'successfully' => 'failed_adding'));
        }
    }

    /**
     * @param $tc_id
     */
    public function selectModifiedTestCategory($tc_id) {

        $result = $this->objTestCategoryDB->selectModifiedTestCategory($tc_id);
        if ($result) {
            array_push ($this->actionResultMessage, array('message' => 'Категория готова к редактированию',
                'successfully' => '1'));
            $this->selectedModifiedTestCategory['tc_id'] = $result['tc_id'];
            $this->selectedModifiedTestCategory['tc_name'] = $result['tc_name'];
            $this->selectedModifiedTestCategory['tc_parent_id'] = $result['tc_parent_id'];
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Вы не можете внести изменения в указанную категорию, т.к. она либо уже отсуствует на сервере, либо сущестует несколько категорий с одинаковым идентификатором',
                'successfully' => 'doubled_category'));
        }
    }

    /**
     * @param $tc_id
     * @param $tc_name
     * @param $tc_parent_id
     */
    public function modifyTestCategory($tc_id, $tc_name, $tc_parent_id) {

        $result = $this->objTestCategoryDB->modifyTestCategory($tc_id, $tc_name, $tc_parent_id);
        if ($result) {
            array_push ($this->actionResultMessage, array('message' => 'Категория была успешно изменена',
                'successfully' => '1'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' => 'Проблемы с изменением категории',
                'successfully' => 'failed_modifying'));
        }
    }

    /**
     * @param $tc_id
     */
    public function deleteTestCategory($tc_id) {

        $result = $this->objTestCategoryDB->findTestCategoryChild($tc_id);
        if(is_array($result)) {
            array_push ($this->actionResultMessage, array('message' => 'Категория не может быть удалена, т.к. у нее есть подкатегории',
                'successfully' => 'error_deleting_parent_with_child'));
        }
        else {
            $result = $this->objTestCategoryDB->deleteTestCategory($tc_id);
            if ($result) {
                array_push ($this->actionResultMessage, array('message' => 'Категория была успешно удалена',
                    'successfully' => '1'));
            }
            else {
                array_push ($this->actionResultMessage, array('message' => 'Проблемы с удалением категории, сообщите о проблеме администратору',
                    'successfully' => 'failed_deleting'));
            }
        }
    }

    /**
     * @param $category
     * @param $table_index
     * @param array $category_copy
     * @param int $first_parent
     * @param null $open_symbol
     * @return array
     */
    public function prepareParentList ($category, $table_index, $category_copy = [], $first_parent = 0, $open_symbol = null) {

        $open_symbol .= '{DV="'.$table_index.'_open_symbol"}';//&nbsp;
        foreach ($category as $key => $value) {
            if ($value[$table_index.'_parent_id'] == $first_parent) {
                $category_copy[$key] = $value;
                $category_copy[$key][$table_index.'_open_value'] = $open_symbol;
                $category_copy = $this->prepareParentList($category, $table_index, $category_copy, $value[$table_index.'_id'], $open_symbol);
            }
        }
        return $category_copy;
    }
}
