<?php

/**
 * Class QuestionSettingController
 */
class QuestionSettingController {
    /**
     * Reference to the object
     * @var null
     */
    protected $objQuestionSettingDB = null;
    /**
     * Array with categories array
     * @var array
     */
    protected $questionTypeArray = [];
    /**
     * Array with results
     * @var array
     */
    protected $actionResultMessage = [];
/*
    protected $lastRequestId = array('action' => null, 'id' => null); // Array with id

    protected $selectedModifiedTestCategory = array('tc_id' => null, 'tc_name' => null, 'tc_parent_id' => null); // Array with id
*/
    public function __construct($obj_question_setting_DB) {
        $this->objQuestionSettingDB = $obj_question_setting_DB;
        $this->readQuestionType();
    }

    /**
     * @return array
     */
    public function getQuestionTypeArray() {
        $questionTypeArray = $this->questionTypeArray;
        return $questionTypeArray;
    }

    /**
     * @return array
     */
    public function getActionResultMessage() {
        $actionResultMessage = $this->actionResultMessage;
        return $actionResultMessage;
    }

    /**
     * @param $obj_question_category_controller
     * @param $user_id
     * @return mixed
     */
    public function getTplArray($obj_question_category_controller, $user_id) {

        $result_array['if_vars']['include_question_table'] = false;
        $result_array['if_vars']['include_search_panel'] = true;
        $result_array['if_vars']['include_search_result'] = false;
        //----------------------------------
        $category = $obj_question_category_controller->getQuestionCategoryArray();
        $category = $this->prepareParentList($category, 'qc');
        $category_tpl = [];
        if ($category) {
            $category_tpl = $category;
            foreach ($category_tpl as $key => $value) {
                $category_tpl[$key]['selected_question_category'] = null;
            }
        }
        $result_array['dynamic_vars']['qc_open_symbol'] = '-';

        //----------------------------------
        if (!empty($_GET['action'])) {
            $action = $_GET['action'];
            if (($action == 'add-or-modify') || ($action == 'show')) {
                //-----------------------------------------
                $result_array['if_vars']['include_question_table'] = true;
                $result_array['if_vars']['include_search_panel'] = false;
                $result_array['if_vars']['include_search_result'] = false;
                //-----------------------------------------
                $first_action = $action;
                if ($action == 'add-or-modify') {
                    $qt_id = '1';
                    if (!empty($_POST['qt_id'])) {
                        $qt_id = $_POST['qt_id'];
                    }
                    $qs_id = null;
                    if (!empty($_POST['qs_id'])) {
                        $qs_id = $_POST['qs_id'];
                    }
                }
                if ($action == 'show') {
                    $qs_id = null;
                    if (!empty($_GET['qs_id'])) {
                        $qs_id = $_GET['qs_id'];
                        $answer_variants = $this->objQuestionSettingDB->selectAnswerVariants($qs_id);
                        $question_pictures = $this->objQuestionSettingDB->selectQuestionPictures($qs_id);
                        $question_group = $this->objQuestionSettingDB->selectQuestionGroup($qs_id);
                        $question_settings = $this->objQuestionSettingDB->selectOneQuestionSetting($qs_id);
                        $qt_id = $question_settings['qs_question_type_id'];
                    }
                }
                $result_array['dynamic_vars']['question_form_tpl'] = null;
                if ($qt_id == '1') $result_array['dynamic_vars']['question_form_tpl'] = "1OneAnswer";
                elseif ($qt_id == '2') $result_array['dynamic_vars']['question_form_tpl'] = "2SomeAnswers";
                elseif ($qt_id == '3') $result_array['dynamic_vars']['question_form_tpl'] = "100UnderConstruction";
                elseif ($qt_id == '4') $result_array['dynamic_vars']['question_form_tpl'] = "100UnderConstruction";
                elseif ($qt_id == '5') $result_array['dynamic_vars']['question_form_tpl'] = "100UnderConstruction";
                elseif ($qt_id == '6') $result_array['dynamic_vars']['question_form_tpl'] = "100UnderConstruction";
                elseif ($qt_id == '7') $result_array['dynamic_vars']['question_form_tpl'] = "100UnderConstruction";
                elseif ($qt_id == '8') $result_array['dynamic_vars']['question_form_tpl'] = "100UnderConstruction";
                elseif ($qt_id == '9') $result_array['dynamic_vars']['question_form_tpl'] = "100UnderConstruction";
                $result_array['dynamic_vars']['question_type'] = $qt_id;
                //-----------------------------------------
                $result_array['dynamic_vars']['qs_weight'] = '';
                $result_array['dynamic_vars']['qs_answer_time_duration'] = '';
                $result_array['foreach_arrs']['radio_button_array'] = [];
                $result_array['foreach_arrs']['checkbox_array'] = [];
                $result_array['foreach_arrs']['pictures_array'] = [];
                //-----------------------------------------
                if ($action == 'add-or-modify') {
                    $qs_text = '';
                    if (!empty($_POST['qs_text'])) {
                        $qs_text = $_POST['qs_text'];
                    }
                    $qs_weight = 50;
                    if (!empty($_POST['qs_weight'])) {
                        $qs_weight = $_POST['qs_weight'];
                    }
                    elseif (!empty($_POST['button_pressed']) && ($_POST['button_pressed'] == 1)) {
                        $qs_weight = '';
                    }
                    $qs_answer_time_duration = '';
                    if (!empty($_POST['qs_answer_time_duration'])) {
                        $qs_answer_time_duration = $_POST['qs_answer_time_duration'];
                    }
                    $qs_show_random_answers = 1;
                    if (empty($_POST['qs_show_random_answers']) && (!empty($_POST['button_pressed'])) && ($_POST['button_pressed'] == 1)) {
                        $qs_show_random_answers = 0;
                    }
                    $qCategory = null;
                    if(!empty($_POST['qCategory'])) {
                        $qCategory = $_POST['qCategory'];
                    }
                    //Show answers
                    $answer_array = [];
                    $num_answers = 5;
                    if(!empty($_POST['av_text'])) {
                        $answer_array = $_POST['av_text'];
                        $num_answers = (count($answer_array));
                    }
                    $answers_variants = [];
                    for ($i = 1; $i <= $num_answers; $i++) {
                        $answers_variants[$i]['av_order_value'] = $i;
                        if ($qt_id == 1) {
                            if (!empty($_POST['qs_correct']) && ($_POST['qs_correct'] == $i)) {
                                $answers_variants[$i]['check_correct'] = "checked='checked'";
                            }
                            else {
                                $answers_variants[$i]['check_correct'] = 0;
                            }
                        }
                        elseif ($qt_id == 2) {
                            if (!empty($_POST['qs_correct']) && (array_key_exists($i, $_POST['qs_correct']))) {
                                $answers_variants[$i]['check_correct'] = "checked='checked'";
                            }
                            else {
                                $answers_variants[$i]['check_correct'] = '';
                            }
                        }

                        if (!empty($answer_array[$i])) {
                            $answers_variants[$i]['answer_text'] = $answer_array[$i];
                        } else {
                            $answers_variants[$i]['answer_text'] = null;
                        }
                    }
                    //Show answers -end
                    $qs_correct = null;
                    if(!empty($_POST['qs_correct'])) {
                        $qs_correct = $_POST['qs_correct'];
                    }

                    $av_text = null;
                    if(!empty($_POST['av_text'])) {
                        $av_text = $_POST['av_text'];
                    }
                }
                if ($action == 'show') {
                    $qs_text = '';
                    if (!empty($question_settings['qs_text'])) {
                        $qs_text = $question_settings['qs_text'];
                    }
                    $qs_weight = '';
                    if (!empty($question_settings['qs_weight'])) {
                        $qs_weight = $question_settings['qs_weight'];
                    }
                    $qs_answer_time_duration = '';
                    if (!empty($question_settings['qs_answer_time_duration'])) {
                        $qs_answer_time_duration = $question_settings['qs_answer_time_duration'];
                    }
                    $qs_show_random_answers = 0;
                    if (!empty($question_settings['qs_show_random_answers'])) {
                        $qs_show_random_answers = $question_settings['qs_show_random_answers'];
                    }
                    $qCategory = null;
                    if (!empty($question_group['qcg_question_category_id'])) {
                        $qCategory = $question_group['qcg_question_category_id'];
                    }
                    //Show answers
                    $max_order = 0;
                    $orders = [];
                    foreach ($answer_variants as $key=> $value) {
                        if ($value['av_order'] > $max_order) {
                            $max_order = $value['av_order'];
                            if ($max_order < 5) {
                                $max_order = 5;
                            }
                        }
                        array_push($orders, $value['av_order']);
                    }
                    for ($i = 1; $i <= $max_order; $i++) {
                        $answers_variants[$i]['av_order_value'] = $i;
                        if (in_array($i, $orders)) {
                            $key = array_search($i, $orders);
                            if ($answer_variants[$key]['av_correct'] == 1) {
                                $answers_variants[$i]['check_correct'] = "checked='checked'";
                            }
                            else {
                                if ($qt_id == 1) {
                                    $answers_variants[$i]['check_correct'] = 0;
                                }
                                elseif ($qt_id == 2) {
                                    $answers_variants[$i]['check_correct'] = '';
                                }
                            }
                            $answers_variants[$i]['answer_text'] = $answer_variants[$key]['av_text'];
                        }
                        else {
                            if ($qt_id == 1) {
                                $answers_variants[$i]['check_correct'] = 0;
                            }
                            elseif ($qt_id == 2) {
                                $answers_variants[$i]['check_correct'] = '';
                            }
                            $answers_variants[$i]['answer_text'] = null;
                        }
                    }
                    //Show answers -end
                    $qs_correct = null;
                    $av_text = null;
                }
                //----------------------------------
                $result_array['dynamic_vars']['qs_text'] = $qs_text;
                $result_array['dynamic_vars']['qs_weight'] = $qs_weight;
                $result_array['dynamic_vars']['qs_answer_time_duration'] = $qs_answer_time_duration;
                if ($qs_show_random_answers ==1)  {
                    $result_array['dynamic_vars']['qs_show_random_answers'] = 'checked="checked"';
                }
                else {
                    $result_array['dynamic_vars']['qs_show_random_answers'] = '';
                }
                if ($qt_id == 1) {
                    $result_array['foreach_arrs']['radio_button_array'] = $answers_variants;
                }
                elseif ($qt_id == 2) {
                    $result_array['foreach_arrs']['checkbox_array'] = $answers_variants;
                }
                $qType = $this->getQuestionTypeArray();
                if ($qType) {
                    foreach ($qType as $key=>$value) {
                        if ($value['qt_id'] == $qt_id)  {
                            $qType[$key]['selected_question_type'] = "selected='selected'";
                        }
                        else {
                            $qType[$key]['selected_question_type'] = null;
                        }
                    }
                }
                $result_array['foreach_arrs']['question_type_array'] = $qType;
                //----------------------------------
                $num_pictures = 1;
                if(!empty($_FILES['qs_name_in_db']['name'])) {
                    $num_pictures = (count($_FILES['qs_name_in_db']['name']));
                }
                for($i=1; $i<=$num_pictures; $i++) {
                    $result_array['foreach_arrs']['pictures_array'][$i]['qp_number_in_question'] = $i;
                    /*
                    if (!empty($_FILES['qs_name_in_db']['name']))  {
                        $result_array['foreach_arrs']['pictures_array'][$i]['picture_name_in_db'] = $_FILES['qs_name_in_db']['name'][$i];
                    }
                    else {
                        $result_array['foreach_arrs']['pictures_array'][$i]['picture_name_in_db'] = null;
                    }
                    */
                }
                //-----------------------------------------
                if (!empty($_POST['button_pressed']) && ($_POST['button_pressed'] == '1')) {
                    $correct_input_data = $this->checkInputData($qs_text, $qs_correct, $av_text, $qt_id, $qs_weight, $qCategory, $qs_answer_time_duration);
                    if (!empty($_POST['first_action'])) {
                        $first_action = $_POST['first_action'];
                    }
                    if ($correct_input_data === 1) {
                        if ($first_action <> 'show') {
                            $qs_adding_time = time();
                            $qs_author_user_id = $user_id;
                            $questionId = $this->objQuestionSettingDB->addQuestion($qt_id, $qs_text, $qs_weight, $qs_answer_time_duration, $qs_show_random_answers, $qs_adding_time, $qs_author_user_id);
                            if ($questionId === false) {
                                array_push($this->actionResultMessage, array('message' => 'Вопрос не был добавлен в базу, попробуйте еще раз и в случае повтора ошибки-сообщите администратору',
                                    'successfully' => 'adding_question_false'));
                            }
                            else {
                                $adding_question = 0;
                                $av_question_setting_id = $questionId;
                                foreach ($answers_variants as $key => $value) {
                                    if (!empty($value['answer_text'])) {
                                        $av_text = $value['answer_text'];
                                        $av_correct = 0;
                                        if (!empty($value['check_correct'])) {
                                            $av_correct = 1;
                                        }
                                        $av_order = $value['av_order_value'];
                                        $answerAddingResult = $this->objQuestionSettingDB->addAnswerVariant($av_question_setting_id, $av_text, $av_correct, $av_order);
                                        if ($answerAddingResult === false) {
                                            array_push($this->actionResultMessage, array('message' => 'Вопрос добавлен в базу, но ответ не был добавлен в базу, но ответ добавить не удалось. Попробуйте найти вопрос в списке вопрсов и добавить к нему ответы еще раз. В случае повтора ошибки-сообщиете администратору',
                                                'successfully' => 'adding_question_false'));
                                            $adding_question = -1;
                                            break;
                                        }
                                    }
                                }
                                //-------------------------
                                $qcg_question_setting_id = $questionId;
                                $answerAddingResult = $this->objQuestionSettingDB->addQuestionGroup($qcg_question_setting_id, $qCategory);
                                $adding_question_group = 0;
                                if ($answerAddingResult === false) {
                                    array_push($this->actionResultMessage, array('message' => 'Вопрос добавлен в базу, к нему не была добавлена группа вопросов. Попробуйте еще раз и в случае повтора ошибки-сообщите администратору',
                                        'successfully' => 'adding_question_false'));
                                    $adding_question_group = -1;
                                }
                                //-------------------------
                                $adding_picture = 0;
                                if (!empty($_FILES['qs_name_in_db']['name'])) {
                                    $adding_picture = $this->addFileToDir($questionId);
                                }
                                //-------------------------
                                if (($adding_question <> -1) && ($adding_picture <> -1) && ($adding_question_group <> -1)) {
                                    array_push($this->actionResultMessage, array('message' => 'Вопрос успешно добавлен в базу',
                                        'successfully' => '1'));
                                    $result_array['if_vars']['include_question_table'] = false;
                                    $result_array['if_vars']['include_search_panel'] = true;
                                    $result_array['if_vars']['include_search_result'] = false;
                                    $qCategory = null;
                                }
                            }
                        }
                        else {
                            $qs_adding_time = time();
                            $qs_author_user_id = $user_id;
                            $questionId = $this->objQuestionSettingDB->updateQuestion($qs_id, $qt_id, $qs_text, $qs_weight, $qs_answer_time_duration, $qs_show_random_answers, $qs_adding_time, $qs_author_user_id);
                            if ($questionId === false) {
                                array_push($this->actionResultMessage, array('message' => 'Вопрос не был изменен в базе, попробуйте еще раз и в случае повтора ошибки-сообщите администратору',
                                    'successfully' => 'update_question_false'));
                            }
                            else {
                                $adding_question = 0;
                                $this->objQuestionSettingDB->deleteAnswerVariant($qs_id);
                                foreach ($answers_variants as $key => $value) {
                                    if (!empty($value['answer_text'])) {
                                        $av_text = $value['answer_text'];
                                        $av_correct = 0;
                                        if (!empty($value['check_correct'])) {
                                            $av_correct = 1;
                                        }
                                        $av_order = $value['av_order_value'];
                                        $answerAddingResult = $this->objQuestionSettingDB->addAnswerVariant($qs_id, $av_text, $av_correct, $av_order);
                                        if ($answerAddingResult === false) {
                                            array_push($this->actionResultMessage, array('message' => 'Вопрос был изменен, но не удалось изменить варианты ответов. Попробуйте изменить нварианты ответов еще раз. В случае повтора ошибки-сообщиете администратору',
                                                'successfully' => 'update_question_false'));
                                            $adding_question = -1;
                                            break;
                                        }
                                    }
                                }
                                //-------------------------
                                $answerAddingResult = $this->objQuestionSettingDB->updateQuestionGroup($qs_id, $qCategory);
                                $adding_question_group = 0;
                                if ($answerAddingResult === false) {
                                    array_push($this->actionResultMessage, array('message' => 'Возникли проблемы с изменением группы вопросов. Попробуйте еще раз и в случае повтора ошибки-сообщите администратору',
                                        'successfully' => 'adding_question_false'));
                                    $adding_question_group = -1;
                                }
                                //-------------------------
                                $adding_picture = 0;
                                /*
                                if (!empty($_FILES['qs_name_in_db']['name'])) {
                                    $adding_picture = $this->addFileToDir($questionId);
                                }
                                */
                                //-------------------------
                                if (($adding_question <> -1) && ($adding_picture <> -1) && ($adding_question_group <> -1)) {
                                    array_push($this->actionResultMessage, array('message' => 'Вопрос успешно изменен',
                                        'successfully' => '1'));
                                    $result_array['if_vars']['include_question_table'] = false;
                                    $result_array['if_vars']['include_search_panel'] = true;
                                    $result_array['if_vars']['include_search_result'] = false;
                                    $qCategory = null;
                                }
                            }
                        }
                    }
                }
            }
            elseif (($action == 'search') || ($action == 'delete')) {
                $result_array['if_vars']['include_question_table'] = false;
                $result_array['if_vars']['include_search_panel'] = true;
                $result_array['if_vars']['include_search_result'] = true;

                if ($action == 'delete') {
                    if (!empty($_GET['qs_id'])) {
                        $question_id = $_GET['qs_id'];
                    }
                    $this->deleteQuestion($question_id);
                }
                $qCategory = null;
                if (!empty($_GET['qCategory'])) {
                    $qCategory = $_GET['qCategory'];
                    foreach ($category_tpl as $key => $value) {
                        if ($value['qc_id'] == $qCategory) {
                            $category_tpl[$key]['selected_question_category'] = "selected='selected'";
                        }
                    }
                }
                $questionsArray = [];
                $questionsArray = $this->getQuestionsArray($qCategory);
                $result_array['foreach_arrs']['questionArray'] = [];
                if (is_array($questionsArray)) {
                    $result_array['foreach_arrs']['questionArray'] = $this->prepareTplQuestionsArray($questionsArray);
                }
            }
        }
        $result_array['dynamic_vars']['first_action'] = '';
        if (!empty($first_action)) {
            $result_array['dynamic_vars']['first_action'] = $first_action;
        }
        $result_array['dynamic_vars']['qs_id'] = '';
        if (!empty($qs_id)) {
            $result_array['dynamic_vars']['qs_id'] = $qs_id;
        }
        $result_array['dynamic_vars']['action'] = '';
        if (!empty($action)) {
            $result_array['dynamic_vars']['action'] = $action;
        }
        $actionMessages = $this->getActionResultMessage();
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        if(!empty($qCategory)) {
            foreach ($category_tpl as $key => $value) {
                if ($value['qc_id'] == $qCategory) {
                    $category_tpl[$key]['selected_question_category'] = "selected='selected'";
                }
            }
        }
        $result_array['foreach_arrs']['question_category_array'] = $category_tpl;
        return $result_array;
    }

    /**
     * @param $category
     * @param $table_index
     * @param array $category_copy
     * @param int $first_parent
     * @param null $open_symbol
     * @return array
     */
    public function prepareParentList ($category, $table_index, $category_copy = [], $first_parent = 0, $open_symbol = null) {

        $open_symbol .= '{DV="'.$table_index.'_open_symbol"}';//&nbsp;
        foreach ($category as $key => $value) {
            if ($value[$table_index.'_parent_id'] == $first_parent) {
                $category_copy[$key] = $value;
                $category_copy[$key][$table_index.'_open_value'] = $open_symbol;
                $category_copy = $this->prepareParentList($category, $table_index, $category_copy, $value[$table_index.'_id'], $open_symbol);
            }
        }
        return $category_copy;
    }

    /**
     *
     */
    public function readQuestionType() {

        $result = $this->objQuestionSettingDB->readQuestionType();
        if ($result) {
            $this->questionTypeArray = $result;
            //array_push ($this->actionResultMessage, array('message' =>  'Список типов вопросов успешно сформирован',
            //                                                'successfully' => '1'));
        }
        else {
            array_push ($this->actionResultMessage, array('message' =>  'При формировании списка типов вопросов возникли проблемы',
                                                            'successfully' => '-1'));
        }
    }

    /**
     * @param $question_id
     * @return bool
     */
    public function deleteQuestion($question_id) {
        if (!empty($question_id)) {
            $result = $this->objQuestionSettingDB->deleteQuestion($question_id);
            if ($result === false) {
                array_push ($this->actionResultMessage, array('message' =>  'При удалении вопроса возникли проблемы',
                                                                'successfully' => 'error_questions_delete'));
                return false;
            }
            else {
                array_push($this->actionResultMessage, array('message' => 'Вопрос был успешно удален из базы',
                    'successfully' => 'error_empty_array'));
                $directory = 'question_file_dir/question_id_' . $question_id;
                $this->deleteDirWIthFiles($directory);
                return true;
            }
        }
        else {
            array_push ($this->actionResultMessage, array('message' =>  'Не был получен номер вопроса, необходимый для удаления, попробуйте еще раз',
                                                            'successfully' => 'deleted_q_id_missed'));
            return false;
        }
    }

    /**
     * @param $directory
     */
    public function deleteDirWithFiles($directory) {

        if (is_dir($directory)) {
            $dir = opendir($directory);
            while ($file = readdir($dir)) {
                if (is_file($directory . "/" . $file)) {
                    unlink($directory . "/" . $file);
                }
                else if (is_dir($directory."/".$file) && ($file != ".") && ($file != "..")) {
                    deleteDirWIthFiles ($directory."/".$file);
                }
            }
            closedir($dir);
            $deleteResult = rmdir($directory);
            if ($deleteResult) {
                array_push($this->actionResultMessage, array('message' => 'Директория с рисунками была удалена',
                    'successfully' => 'adding_question_false'));
            }
            else {
                array_push($this->actionResultMessage, array('message' => 'Не удалось удалить директорию с файлами',
                    'successfully' => 'adding_question_false'));
            }
        }
    }

    /**
     * @param $qCategory
     * @return mixed
     */
    public function getQuestionsArray($qCategory) {
        if (!empty($qCategory)) {
            $question_category_id = $qCategory;
            $result = $this->objQuestionSettingDB->selectQuestionsByCategory($question_category_id);
        } else {
            $result = $this->objQuestionSettingDB->selectAllQuestions();
        }
        if ($result === false) {
            array_push ($this->actionResultMessage, array('message' =>  'При формировании списка вопросов возникли проблемы',
                                                            'successfully' => 'error_questions_list'));
        } elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' =>  'В базе отсуствуют вопросы по выбранной категории',
                                                            'successfully' => 'error_empty_array'));
        } else {
            //array_push ($this->actionResultMessage, array('message' =>  'Список вопросов сформирован',
            //                                                'successfully' => '1'));
        }
        return $result;
    }

    /**
     * @param $questionsArray
     * @return mixed
     */
    public function prepareTplQuestionsArray($questionsArray)
    {
        foreach ($questionsArray as $key => &$value) {
            if ($value['qs_show_random_answers'] == 1) {
                $value['qs_show_random_answers'] = "Да";
            } else {
                $value['qs_show_random_answers'] = "Нет";
            }
            $value['qs_adding_time'] = date('Y.m.d H:i:s', $value['qs_adding_time']);
            if ($value['qs_answer_time_duration'] == 0) {
                $value['qs_answer_time_duration'] = "No time";
            }
        }
        unset($value);
        return $questionsArray;
    }

    /**
     * @param $questionId
     * @return int
     */
    public function addFileToDir($questionId) {
        $allowed_picture_ext = ['png', 'jpg', 'jpeg', 'gif', 'bmp']; //delete

        $adding_picture = 0;
        $num_files = count($_FILES['qs_name_in_db']['name']);
        for ($i=1; $i<=$num_files; $i++) {
            if(!empty($_FILES['qs_name_in_db']['name'][$i])) {
                $qp_origin_name = $_FILES['qs_name_in_db']['name'][$i];
                $input_file_type = pathinfo($qp_origin_name, PATHINFO_EXTENSION);
                $input_file_name = pathinfo($qp_origin_name, PATHINFO_FILENAME);
                $qp_name_in_db = sha1($input_file_name) . "." . $input_file_type;

                if (in_array($input_file_type, $allowed_picture_ext)) {
                    $qp_file_size = $_FILES['qs_name_in_db']['size'][$i];
                    $qp_question_setting_id = $questionId;
                    $qp_order_in_question = $i;
                    $tmp_name = $qp_file_size = $_FILES['qs_name_in_db']['tmp_name'][$i];
                    if (!is_dir('question_file_dir/question_id_' . $questionId)) {
                        mkdir('question_file_dir/question_id_' . $questionId, 0755);
                    }
                    $result = move_uploaded_file($tmp_name, 'question_file_dir/question_id_' . $questionId . '/' . $qp_name_in_db);
                    if ($result) {
                        $answerQuestionPictureDB = $this->objQuestionSettingDB->addQuestionPictureDB($qp_question_setting_id, $qp_order_in_question, $qp_name_in_db, $qp_origin_name, $qp_file_size);
                        if ($answerQuestionPictureDB == false) {
                            array_push ($this->actionResultMessage, array('message' =>  'Файл №'.$i.' не был добавлен в базу. Попробуйте найти вопрос в списке вопрсов и добавить к нему ответы еще раз. В случае повтора ошибки-сообщиете администратору',
                                'successfully' => 'adding_question_false'));
                            $adding_picture = -1;
                        }
                    }
                    else {
                        array_push ($this->actionResultMessage, array('message' =>  'Не удалось скопировать файл на сервер',
                            'successfully' => 'coping_picture_failed'));
                        $adding_picture = -1;
                    }
                }
                else {
                    array_push ($this->actionResultMessage, array('message' =>  'Расширение файла "'.$qp_origin_name.'" не поддерживаeтся',
                        'successfully' => 'coping_unallowed_ext'));
                }
            }
        }
        return $adding_picture;
    }

    /**
     * @param $qs_text
     * @param $qs_correct
     * @param $av_text
     * @param $qt_id
     * @param $qs_weight
     * @param $qCategory
     * @param $qs_answer_time_duration
     * @return int
     */
    public function checkInputData($qs_text, $qs_correct, $av_text, $qt_id, $qs_weight, $qCategory, $qs_answer_time_duration) {
        $correct_input_data = 1;
        if (empty($qs_text)) {
            array_push($this->actionResultMessage, array('message' => 'Поле с текстом вопроса не заполнено',
                'successfully' => 'empty_text'));
            $correct_input_data = 0;
        }
        if (empty($qs_correct)) {
            array_push($this->actionResultMessage, array('message' => 'Правильный ответ не указан',
                'successfully' => 'empty_correct'));
            $correct_input_data = 0;
        }
        if ($qt_id == 1) {
            if (empty($av_text[$qs_correct])) {
                array_push($this->actionResultMessage, array('message' => 'Для правильного ответа не написан вопрос',
                    'successfully' => 'empty_correct_answer'));
                $correct_input_data = 0;
            }
        }
        elseif ($qt_id == 2) {
            $empty_answer_counter = 0;
            if (!empty($qs_correct)) {
                foreach ($qs_correct as $key => $value) {
                    if (empty($av_text[$key])) {
                        $empty_answer_counter = 1;
                    }
                }
            }
            else {
                $empty_answer_counter = 1;
            }
            if ($empty_answer_counter == 1) {
                array_push($this->actionResultMessage, array('message' => 'Для правильного ответа не написан вопрос',
                    'successfully' => 'empty_correct_answer'));
                $correct_input_data = 0;
            }
        }
        if (!empty($qs_weight)) {
            if (!is_numeric($qs_weight) || $qs_weight < 0 || $qs_weight > 100) {
                array_push($this->actionResultMessage, array('message' => 'Поле с весом должно быть числом в диапазоне от 0 до 100',
                    'successfully' => 'empty_weight'));
                $correct_input_data = 0;
            }
        } else {
            array_push($this->actionResultMessage, array('message' => 'Поле с весом вопроса не заполнено',
                'successfully' => 'empty_weight'));
            $correct_input_data = 0;
        }
        if (!empty($qs_answer_time_duration)) {
            if (!is_numeric($qs_answer_time_duration) || $qs_answer_time_duration < 0) {
                array_push($this->actionResultMessage, array('message' => 'Поле с временем ответа должно быть положительным числом',
                    'successfully' => 'empty_weight'));
                $correct_input_data = 0;
            }
        }
        if (empty($qCategory)) {
            array_push($this->actionResultMessage, array('message' => 'Категория вопроса не указана',
                'successfully' => 'empty_question_category'));
            $correct_input_data = 0;
        }
        return $correct_input_data;
    }
}