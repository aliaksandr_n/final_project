<?php

/**
 * Class UserJournalController
 */
class UserJournalController {

    /**
     * @var null
     */
    protected $objUserJournalDB = null;
    /**
     * @var array
     */
    protected $actionResultMessage = [];
    /**
     * @var null
     */
    protected $userId = null;
    /**
     * @var null
     */
    protected $userTestJournalId = null;

    public function __construct($obj_user_journal_DB, $user_id) {

        $this->objUserJournalDB = $obj_user_journal_DB;
        $this->userId = $user_id;
    }

    /**
     * @return mixed
     */
    public function getTplArray() {
        $user_id = $this->userId;
        //----------------------------------

        $input_answers_radio = null;
        if (!empty($_POST['input_answers_radio'])) {
            $input_answers_radio = $_POST['input_answers_radio'];
        }
        $input_answers_checkbox = [];
        if (!empty($_POST['input_answers_checkbox'])) {
            $input_answers_checkbox = $_POST['input_answers_checkbox'];
        }
        $actual_time = time();
        $show_right_answer = 0;

        $action = null;
        if (!empty($_POST['action'])) {
            $action = $_POST['action'];
        }
        elseif (!empty($_GET['action'])) {
            $action = $_GET['action'];
        }

        $show_user_test = false;
        $show_answer_result = false;
        $show_test_result = false;
        if (!empty($user_id) ) {
            $show_test_list = true;
            $show_temp_key = false;
        }
        else {
            $show_test_list= false;
            $show_temp_key = true;
        }

        $search_test = $this->findTest($action);
        $utj_id = $this->userTestJournalId;

        $selecting_test_information = [];
        $actual_question_inf = [];
        $pictures_array = [];
        $actual_answers_array = [];
        if ($search_test == 1) {
            //Select test setting information
            $selecting_test_information = $this->objUserJournalDB->selectTestInformation($utj_id);
            if (empty($selecting_test_information) || !is_array($selecting_test_information)) {
                array_push($this->actionResultMessage, array('message' => 'Не удалось извлечь информацию о тесте',
                    'successfully' => 'failed_adding'));
            }
            else {
                if (!empty($action)) {
                    $selecting_actual_question = $this->objUserJournalDB->selectActionQuestion($utj_id);
                    if (empty($selecting_actual_question) || !is_array($selecting_actual_question)) {
                        array_push($this->actionResultMessage, array('message' => 'Не удалось найти обрабатываемый вопрос',
                            'successfully' => 'failed_adding'));
                    }
                    else {
                        $answering_duration = $selecting_actual_question['uqj_answering_duration'] + ($actual_time - $selecting_actual_question['uqj_last_answering_start']);
                        $question_paused = $selecting_actual_question['ugj_paused'];
                        //Skip question if it needed
                        if ($question_paused == 0) {
                            if ($action == 'skip') {
                                $this->doActionSkip($selecting_test_information, $selecting_actual_question, $answering_duration, $actual_time, $question_paused);
                            }
                            elseif ($action == 'pause') {
                                $this->doActionPause($selecting_test_information, $selecting_actual_question, $answering_duration, $actual_time);
                            }
                            elseif ($action == 'answer') {
                                $question_type = $selecting_actual_question['qs_question_type_id'];
                                if ($question_type == 1) {
                                    if (empty($input_answers_radio)) {
                                        array_push($this->actionResultMessage, array('message' => 'Необходимо указать правильный ответ',
                                            'successfully' => 'failed_adding'));
                                    }
                                    else {
                                        $question_id = $selecting_actual_question['qs_id'];
                                        $answers_for_checking = $this->objUserJournalDB->getActualAnswersInf($question_id);
                                        if (empty($answers_for_checking)) {
                                            array_push($this->actionResultMessage, array('message' => 'При выборке информации с ответами возникли проблемы',
                                                'successfully' => 'failed_adding'));
                                        }
                                        else {
                                            $adding_user_answer = $this->objUserJournalDB->addUserAnswer($selecting_actual_question['uqj_id'], $input_answers_radio);
                                            if ($adding_user_answer == false) {
                                                array_push($this->actionResultMessage, array('message' => 'При добавлении ответа пользователя возникли проблемы',
                                                    'successfully' => 'failed_adding'));
                                            }
                                            else {
                                                $right_answer = $this->getFinalVerdictQuestionType1($answers_for_checking, $input_answers_radio);

                                                $answering_question = $this->objUserJournalDB->changeQuestionJournalSetting($selecting_actual_question['uqj_id'], $answering_duration, $actual_time, '5', $question_paused, $right_answer);
                                                if ($answering_question == false) {
                                                    array_push($this->actionResultMessage, array('message' => 'При попытке пометить вопрос как выполненный возникли проблемы',
                                                        'successfully' => 'failed_adding'));
                                                }
                                                else {
                                                    //array_push($this->actionResultMessage, array('message' => 'Ответ был принят',
                                                    //    'successfully' => 'failed_adding'));
                                                    $show_right_answer = $selecting_test_information['tr_show_right_answer'];
                                                    $this->markTestStatIfFinished($utj_id, $selecting_test_information);
                                                }
                                            }
                                        }
                                    }
                                }
                                elseif ($question_type == 2) {
                                    if (empty($input_answers_checkbox) || !is_array($input_answers_checkbox)) {
                                        array_push($this->actionResultMessage, array('message' => 'Необходимо указать правильный ответ',
                                            'successfully' => 'failed_adding'));
                                    }
                                    else {
                                        $question_id = $selecting_actual_question['qs_id'];
                                        $answers_for_checking = $this->objUserJournalDB->getActualAnswersInf($question_id);
                                        if (empty($answers_for_checking) || !is_array($answers_for_checking)) {
                                            array_push($this->actionResultMessage, array('message' => 'При выборке информации с ответами возникли проблемы',
                                                'successfully' => 'failed_adding'));
                                        }
                                        else {
                                            $error_counter = 0;
                                            foreach ($answers_for_checking as $key => $value) {
                                                $adding_user_answer = $this->objUserJournalDB->addUserAnswer($selecting_actual_question['uqj_id'], $key);
                                                if ($adding_user_answer == false) {
                                                    $error_counter = 1;
                                                }
                                            }
                                            if ($error_counter == 1) {
                                                array_push($this->actionResultMessage, array('message' => 'При добавлении ответов пользователя возникли проблемы',
                                                    'successfully' => 'failed_adding'));
                                            }
                                            else {
                                                $right_answer = $this->getFinalVerdictQuestionType2($input_answers_checkbox, $answers_for_checking);

                                                $answering_question = $this->objUserJournalDB->changeQuestionJournalSetting($selecting_actual_question['uqj_id'], $answering_duration, $actual_time, '5', $question_paused, $right_answer);
                                                if ($answering_question == false) {
                                                    array_push($this->actionResultMessage, array('message' => 'При попытке пометить вопрос как выполненный возникли проблемы',
                                                        'successfully' => 'failed_adding'));
                                                }
                                                else {
                                                    //array_push($this->actionResultMessage, array('message' => 'Ответ был принят',
                                                    //    'successfully' => 'failed_adding'));
                                                    $show_right_answer = $selecting_test_information['tr_show_right_answer'];
                                                    $this->markTestStatIfFinished($utj_id, $selecting_test_information);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            if ($action == 'resume') {
                                $this->doActionResume($selecting_test_information, $selecting_actual_question, $actual_time);
                            }
                        }
                    }
                }
                if ($show_right_answer == 0) {
                    //Finding the actual question
                    $actual_question_array = $this->objUserJournalDB->selectActualQuestions($utj_id);
                    if (!empty($actual_question_array) && is_array($actual_question_array)) {
                        $actual_question_utj_id = $this->getActualQuestionId($actual_question_array);
                        //Select actual question information
                        $actual_question_inf = $this->objUserJournalDB->getActualQuestionInf($actual_question_utj_id);
                        if (empty($actual_question_inf) || !is_array($actual_question_inf)) {
                            array_push($this->actionResultMessage, array('message' => 'При выборке информации по вопросу возникли проблемы',
                                'successfully' => 'failed_adding'));
                        }
                        else {
                            $question_id = $actual_question_inf['qs_id'];
                            $actual_answers_array = $this->objUserJournalDB->getActualAnswersInf($question_id);
                            if (empty($actual_answers_array) || !is_array($actual_answers_array)) {
                                array_push($this->actionResultMessage, array('message' => 'При выборке информации с ответами возникли проблемы',
                                    'successfully' => 'failed_adding'));
                            }
                            else {
                                $pictures_array = $this->objUserJournalDB->getPicturesArray($question_id);
                                if ($pictures_array == false) {
                                    array_push($this->actionResultMessage, array('message' => 'При выборке информации с рисунками возникли проблемы',
                                        'successfully' => 'failed_adding'));
                                }
                                else {
                                    //Change answers order, if it's needed
                                    if ($actual_question_inf['qs_show_random_answers'] == 1) {
                                        shuffle($actual_answers_array);
                                    }
                                    $new_question_paused = $actual_question_inf['ugj_paused'];
                                    if ($new_question_paused == 0) {
                                        //Mark actual question as `in_progress` and set start time
                                        $marking_actual_question = $this->objUserJournalDB->markActualQuestion($actual_question_utj_id, $actual_time, $new_question_paused, '1');
                                        if ($marking_actual_question = false) {
                                            array_push($this->actionResultMessage, array('message' => 'Не удалось обновить статус актуального вопроса',
                                                'successfully' => 'failed_adding'));
                                        }
                                        else {
                                            $show_test_list = false;
                                            $show_user_test = true;
                                            $show_answer_result = false;
                                            $show_test_result = false;
                                            $show_temp_key = false;
                                        }
                                    }
                                    else {
                                        $show_test_list = false;
                                        $show_user_test = true;
                                        $show_answer_result = false;
                                        $show_test_result = false;
                                        $show_temp_key = false;
                                    }
                                }
                            }
                        }
                    }
                    elseif ($actual_question_array == true) {
                        $show_test_list = false;
                        $show_user_test = false;
                        $show_answer_result = false;
                        $show_test_result = true;
                        $show_temp_key = false;
                        //Show user result if the test allows it
                        $final_test_result = $this->getFinalTestResult($utj_id);
                    }
                    else {
                        array_push($this->actionResultMessage, array('message' => 'При определении статуса теста возникли проблемы',
                            'successfully' => 'failed_adding'));
                    }
                }
                else {
                    $show_test_list = false;
                    $show_user_test = false;
                    $show_answer_result = true;
                    $show_test_result = false;
                    $show_temp_key = false;
                    $pictures_array = $this->objUserJournalDB->getPicturesArray($selecting_actual_question['qs_id']);
                    $shown_answers_array = $this->prepareAnswerResult($answers_for_checking, $question_type, $input_answers_radio, $input_answers_checkbox);
                }
            }
        }
        //----------------------------------
        if ($show_test_list == true) {
            $user_test_inf = $this->showQuestionList();
        }
        //----------------------------------
        $result_array['dynamic_vars']['tr_total_time_value'] = 'не контролируется';
        if (!empty($selecting_test_information['tr_total_time_value'])) {
            $result_array['dynamic_vars']['tr_total_time_value'] = $selecting_test_information['tr_total_time_value'].' мин';
        }
        $result_array['dynamic_vars']['qs_answer_time_duration'] = 'не контролируется';
        if (!empty($actual_question_inf['qs_answer_time_duration'])) {
            $result_array['dynamic_vars']['qs_answer_time_duration'] = $actual_question_inf['qs_answer_time_duration'].' мин';
        }
        $result_array['dynamic_vars']['qs_text'] = '';
        if (!empty($actual_question_inf['qs_text'])) {
            $result_array['dynamic_vars']['qs_text'] = $actual_question_inf['qs_text'];
        }
        $result_array['dynamic_vars']['utj_id'] = '';
        if (!empty($utj_id)) {
            $result_array['dynamic_vars']['utj_id'] = $utj_id;
        }

        $result_array['dynamic_vars']['qs_question_type_id'] = '100Not_exist';
        if (!empty( $actual_question_inf['qs_question_type_id'])) {
            if ( $actual_question_inf['qs_question_type_id'] == 1) {
                $result_array['dynamic_vars']['qs_question_type_id'] = '1OneAnswer';
            }
            elseif ( $actual_question_inf['qs_question_type_id'] == 2) {
                $result_array['dynamic_vars']['qs_question_type_id'] = '2SomeAnswers';
            }
        }
        //----------------------------------

        $result_array['if_vars']['show_skip_button'] = false;
        $result_array['if_vars']['show_pause_button'] = false;
        if (!empty($new_question_paused) && $new_question_paused == 1) {
            $result_array['if_vars']['show_answer_button'] = false;
            $result_array['if_vars']['show_resume_button'] = true;
            if (empty($selecting_test_information['tr_pause_test_permission'])) {
                $result_array['if_vars']['show_resume_button'] = false;
            }
        }
        else {
            $result_array['if_vars']['show_answer_button'] = true;
            if (!empty($selecting_test_information['tr_skip_questions'])) {
                $result_array['if_vars']['show_skip_button'] = true;
            }
            if (!empty($selecting_test_information['tr_pause_test_permission'])) {
                $result_array['if_vars']['show_pause_button'] = true;
            }
            $result_array['if_vars']['show_resume_button'] = false;
        }
        //----------------------------------
        $result_array['dynamic_vars']['right_answer'] = 'не правильно';
        $result_array['dynamic_vars']['answer_status_class'] = 'wrong_status';
        if (!empty($right_answer) && $right_answer == 1) {
            $result_array['dynamic_vars']['right_answer'] = 'правильно';
            $result_array['dynamic_vars']['answer_status_class'] = 'right_status';
        }
        $result_array['dynamic_vars']['question'] = '';
        if (!empty($selecting_actual_question['qs_text'])) {
            $result_array['dynamic_vars']['question'] = $selecting_actual_question['qs_text'];
        }
        $result_array['foreach_arrs']['shown_answers_array'] = [];
        if (!empty($shown_answers_array) && is_array($shown_answers_array)) {
            $result_array['foreach_arrs']['shown_answers_array'] = $shown_answers_array;
        }
        //----------------------------------
        $result_array['foreach_arrs']['actual_question_inf'] = [];
        if (!empty($actual_question_inf) && is_array($actual_question_inf)) {
            $result_array['foreach_arrs']['actual_question_inf'] = $actual_question_inf;
        }
        $result_array['foreach_arrs']['pictures_array'] = [];
        if (!empty($pictures_array) && is_array($pictures_array)) {
            $result_array['foreach_arrs']['pictures_array'] = $pictures_array;
        }
        $result_array['foreach_arrs']['actual_answers_array'] = [];
        if (!empty($actual_answers_array) && is_array($actual_answers_array)) {
            $result_array['foreach_arrs']['actual_answers_array'] = $actual_answers_array;
        }
        //----------------------------------
        $result_array['foreach_arrs']['user_test__array'] = [];
        if (!empty($user_test_inf) && is_array($user_test_inf)) {
            $result_array['foreach_arrs']['user_test__array'] = $user_test_inf;
            $result_array['if_vars']['empty_dir'] = false;
        }
        else {
            $result_array['if_vars']['empty_dir'] = true;
        }
        $result_array['if_vars']['show_user_test'] = $show_user_test;
        $result_array['if_vars']['show_test_list'] = $show_test_list;
        $result_array['if_vars']['show_answer_result'] = $show_answer_result;
        $result_array['if_vars']['show_test_result'] = $show_test_result;
        $result_array['if_vars']['show_temp_key'] = $show_temp_key;
        //----------------------------------
        $result_array['dynamic_vars']['num_answers'] = '';
        if (!empty($final_test_result['utj_num_answers'])) {
            $result_array['dynamic_vars']['num_answers'] = $final_test_result['utj_num_answers'];
        }
        $result_array['dynamic_vars']['num_right_answers'] = '';
        if (!empty($selecting_test_information['tr_show_right_answer']) && ($selecting_test_information['tr_show_right_answer'] == 1)) {
            if (!empty($final_test_result['utj_num_right_answers'])) {
                $result_array['dynamic_vars']['num_right_answers'] = $final_test_result['utj_num_right_answers'];
            }
        }
        $result_array['dynamic_vars']['time_duration'] = '';
        if (!empty($final_test_result['utj_time_duration'])) {
            $result_array['dynamic_vars']['time_duration'] = $final_test_result['utj_time_duration']/60;
        }
        $result_array['dynamic_vars']['finish_date'] = '';
        if (!empty($final_test_result['utj_finish_date'])) {
            $result_array['dynamic_vars']['finish_date'] = date("Y-m-d H:i:s", $final_test_result['utj_finish_date']);
        }
        else {
            $result_array['dynamic_vars']['num_right_answers'] = 'Отображение запрещено настройками теста';
        }
        $result_array['dynamic_vars']['final_mark'] = '';
        if (!empty($selecting_test_information['tr_show_final_mark']) && ($selecting_test_information['tr_show_final_mark'] == 1)) {
            if (!empty($final_test_result['utj_final_mark'])) {
                $result_array['dynamic_vars']['final_mark'] = $final_test_result['utj_final_mark'];
            }
        }
        else {
            $result_array['dynamic_vars']['final_mark'] = 'Отображение запрещено настройками теста';
        }
        $result_array['dynamic_vars']['final_verdict'] = '';
        if (!empty($selecting_test_information['tr_show_final_verdict']) && ($selecting_test_information['tr_show_final_verdict'] == 1)) {
            if (!empty($final_test_result['utj_final_verdict']) && $final_test_result['utj_final_verdict'] == 1) {
                $result_array['dynamic_vars']['final_verdict'] = 'Тест сдан';
            }
            else {
                $result_array['dynamic_vars']['final_verdict'] = 'Тест не сдан';
            }
        }
        else {
            $result_array['dynamic_vars']['final_verdict'] = 'Отображение запрещено настройками теста';
        }
        //----------------------------------
        $actionMessages = $this->actionResultMessage;
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        return $result_array;
    }

    /**
     * @param $action
     * @return int
     */
    public function findTest($action) {

        $search_test = 0;
        $user_id = $this->userId;
        $utj_id = null;
        if (!empty($user_id) ) {
            if (!empty($_GET['test'])) {
                $utj_id = $_GET['test'];

                $finding_user_test_result = $this->objUserJournalDB->findUserTestById($utj_id, $user_id);
                if (is_array($finding_user_test_result)) {
                    $search_test = 1;
                }
                elseif ($finding_user_test_result == true) {
                    array_push($this->actionResultMessage, array('message' => 'Запрашиваемый тест не сущестует',
                        'successfully' => 'failed_adding'));
                }
                else {
                    array_push($this->actionResultMessage, array('message' => 'При выборке теста возникли проблемы',
                        'successfully' => 'failed_adding'));
                }
            }
        }
        else {
            if (!empty($action) && ($action == 'new_test')) {
                unset($_SESSION['utj_id']);
            }
            if (empty($_SESSION['utj_id'])) {
                $temp_key = null;
                if (!empty($_POST['temp_key'])) {
                    $temp_key = $_POST['temp_key'];
                    $finding_result = $this->objUserJournalDB->findUserTestJournalId($temp_key);
                    if (is_array($finding_result)) {
                        $utj_id = $finding_result['utj_id'];
                        $search_test = 1;
                        $_SESSION['utj_id'] = $utj_id;
                    }
                    elseif ($finding_result == true) {
                        array_push($this->actionResultMessage, array('message' => 'Запрашиваемый тест не сущестует',
                            'successfully' => 'failed_adding'));
                    }
                    else {
                        array_push($this->actionResultMessage, array('message' => 'При выборке теста возникли проблемы',
                            'successfully' => 'failed_adding'));
                    }
                }
            }
            else {
                $utj_id = $_SESSION['utj_id'];
                $search_test = 1;
            }
        }
        $this->userTestJournalId = $utj_id;
        return $search_test;
    }

    /**
     *
     */
    public function showQuestionList(){

        $user_id = $this->userId;
        $user_test_inf = $this->objUserJournalDB->selectUserTest($user_id);
        if (!empty($user_test_inf) && is_array($user_test_inf)) {
            //array_push($this->actionResultMessage, array('message' => 'Список тестов успешно сформирован',
            //    'successfully' => '1'));
            return $user_test_inf;
        } elseif ($user_test_inf == true) {
            array_push($this->actionResultMessage, array('message' => 'Список назначенных тестов пуст',
                'successfully' => 'failed_adding'));
        } else {
            array_push($this->actionResultMessage, array('message' => 'При формировании списка возникли проблемы',
                'successfully' => 'failed_adding'));
        }
    }

    /**
     * @param $shown_answers_array
     * @param $question_type
     * @param $input_answers_radio
     * @param $input_answers_checkbox
     * @return mixed
     */
    public function prepareAnswerResult($shown_answers_array, $question_type, $input_answers_radio, $input_answers_checkbox) {

        if ($question_type == 1) {
            foreach ($shown_answers_array as $key => $value) {
                if ($value['av_correct'] == 0) {
                    $shown_answers_array[$key]['answer_class'] = 'wrong_answer';
                } else {
                    $shown_answers_array[$key]['answer_class'] = 'right_answer';
                }
                if ($value['av_id'] == $input_answers_radio) {
                    $shown_answers_array[$key]['user_answer'] = 'Выбран';
                } else {
                    $shown_answers_array[$key]['user_answer'] = '';
                }
            }
        }
        elseif ($question_type == 2) {
            foreach ($shown_answers_array as $key => $value) {
                if ($value['av_correct'] == 0) {
                    $shown_answers_array[$key]['answer_class'] = 'wrong_answer';
                } else {
                    $shown_answers_array[$key]['answer_class'] = 'right_answer';
                }
                if (array_key_exists($value['av_id'], $input_answers_checkbox)) {
                    $shown_answers_array[$key]['user_answer'] = 'Выбран';
                } else {
                    $shown_answers_array[$key]['user_answer'] = '';
                }
            }
        }
        return $shown_answers_array;
    }

    /**
     * @param $selecting_test_information
     * @param $selecting_actual_question
     * @param $answering_duration
     * @param $actual_time
     * @param $question_paused
     */
    public function doActionSkip($selecting_test_information, $selecting_actual_question, $answering_duration, $actual_time, $question_paused) {

        if ($selecting_test_information['tr_skip_questions'] == 1) {
            $skipping_question = $this->objUserJournalDB->changeQuestionJournalSetting($selecting_actual_question['uqj_id'], $answering_duration, $actual_time, '4', $question_paused);
            if ($skipping_question == false) {
                array_push($this->actionResultMessage, array('message' => 'При попытке пометить вопрос как пропущенный возникли проблемы',
                    'successfully' => 'failed_adding'));
            } else {
                array_push($this->actionResultMessage, array('message' => 'Вопрос был успешно пропущен',
                    'successfully' => 'failed_adding'));
            }
        } else {
            array_push($this->actionResultMessage, array('message' => 'В данном тесте пропускать вопросы нельзя',
                'successfully' => 'failed_adding'));
        }
    }

    /**
     * @param $selecting_test_information
     * @param $selecting_actual_question
     * @param $answering_duration
     * @param $actual_time
     */
    public function doActionPause($selecting_test_information, $selecting_actual_question, $answering_duration, $actual_time) {

        if ($selecting_test_information['tr_pause_test_permission'] == 1) {
            $paused_question = $this->objUserJournalDB->changeQuestionJournalSetting($selecting_actual_question['uqj_id'], $answering_duration, $actual_time, '1', '1');
            if ($paused_question == false) {
                array_push($this->actionResultMessage, array('message' => 'При попытке поставить вопрос на паузу возникли проблемы',
                    'successfully' => 'failed_adding'));
            } else {
                array_push($this->actionResultMessage, array('message' => 'Вопрос был успешно поставлен на паузу',
                    'successfully' => 'failed_adding'));
            }
        } else {
            array_push($this->actionResultMessage, array('message' => 'Данный тест ставить на паузу нельзя',
                'successfully' => 'failed_adding'));
        }
    }

    /**
     * @param $selecting_test_information
     * @param $selecting_actual_question
     * @param $actual_time
     */
    public function doActionResume($selecting_test_information, $selecting_actual_question, $actual_time) {

        if ($selecting_test_information['tr_pause_test_permission'] == 1) {
            $resumed_question = $this->objUserJournalDB->markActualQuestion($selecting_actual_question['uqj_id'], $actual_time, '0', '2');
            if ($resumed_question == false) {
                array_push($this->actionResultMessage, array('message' => 'При попытке снять вопрос с паузы возникли проблемы',
                    'successfully' => 'failed_adding'));
            } else {
                array_push($this->actionResultMessage, array('message' => 'Вопрос был успешно снят с паузы',
                    'successfully' => 'failed_adding'));
            }
        } else {
            array_push($this->actionResultMessage, array('message' => 'Данный тест ставить на паузу/ снимать с паузы нельзя',
                'successfully' => 'failed_adding'));
        }
    }

    /**
     * @param $actual_question_array
     * @return null
     */
    public function getActualQuestionId($actual_question_array) {

        if (array_key_exists('in_progress', $actual_question_array)) {
            $actual_question_utj_id = $actual_question_array['in_progress'][0]['uqj_id'];
        }
        elseif (array_key_exists('rusemed', $actual_question_array)) {
            $actual_question_utj_id = $actual_question_array['rusemed'][0]['uqj_id'];
        }
        elseif (array_key_exists('appointed', $actual_question_array)) {
            $actual_question_utj_id = $actual_question_array['appointed'][0]['uqj_id'];
        }
        elseif (array_key_exists('skipped', $actual_question_array)) {
            $latest_time_val = null;
            $latest_time_id = null;
            foreach ($actual_question_array['skipped'] as $key => $value) {
                if (is_null($latest_time_val)) {
                    $latest_time_val = $value['uqj_last_answering_finish'];
                    $latest_time_id = $value['uqj_id'];
                } else {
                    if ($value['uqj_last_answering_finish'] < $latest_time_val) {
                        $latest_time_val = $value['uqj_last_answering_finish'];
                        $latest_time_id = $value['uqj_id'];
                    }
                }
            }
            $actual_question_utj_id = $latest_time_id;
        }
        return $actual_question_utj_id;
    }

    /**
     * @param $answers_for_checking
     * @param $input_answers_radio
     * @return string
     */
    public function getFinalVerdictQuestionType1($answers_for_checking, $input_answers_radio) {

        $right_answer = '0';
        foreach ($answers_for_checking as $key => $value) {
            if ($value['av_id'] == $input_answers_radio && $value['av_correct'] == 1) {
                $right_answer = '1';
            }
        }
        return $right_answer;
    }

    /**
     * @param $input_answers_checkbox
     * @param $answers_for_checking
     * @return int
     */
    public function getFinalVerdictQuestionType2($input_answers_checkbox, $answers_for_checking) {

        $right_answer = 1;
        $users_answers_copy = $input_answers_checkbox;
        foreach ($answers_for_checking as $key => $value) {
            if (($value['av_correct'] == 1) && (!array_key_exists($value['av_id'], $users_answers_copy))) {
                $right_answer = 0;
            } elseif (($value['av_correct'] == 1) && (array_key_exists($value['av_id'], $users_answers_copy))) {
                if (isset($users_answers_copy[$value['av_id']])) {
                    unset($users_answers_copy[$value['av_id']]);
                }
            }
        }
        if (count($users_answers_copy) > 0) {
            $right_answer = 0;
            return $right_answer;
        }
        return $right_answer;
    }

    /**
     * @param $utj_id
     * @param $selecting_test_information
     */
    public function markTestStatIfFinished($utj_id, $selecting_test_information) {

        $uncomplited_test = $this->objUserJournalDB->selectUncomplitedTest($utj_id);
        if ($uncomplited_test == false) {
            array_push($this->actionResultMessage, array('message' => 'При попытке посчитать неотвеченные вопросы возникли проблемы',
                'successfully' => 'failed_adding'));
        }
        else {
            if ($uncomplited_test['sum'] < 1) {

                $num_answers = $this->objUserJournalDB->countAnswers($utj_id);
                $num_answers = $num_answers[0]['sum'];
                $num_right_answers = $this->objUserJournalDB->countRightAnswers($utj_id);
                $num_right_answers = $num_right_answers[0]['sum'];
                if ($selecting_test_information['tr_success_value_num'] == 0) {
                    $tr_success_value_num = $num_answers;
                }
                else {
                    $tr_success_value_num = $selecting_test_information['tr_success_value_num'];
                }
                $final_verdict = 0;
                //Final result and mark (%)
                if ($selecting_test_information['tr_success_value_type'] == 1) {
                    $final_mark = round($num_right_answers / $num_answers * 10, 2);
                    if ($num_right_answers >= $tr_success_value_num) {
                        $final_verdict = 1;
                    }
                }
                elseif ($selecting_test_information['tr_success_value_type'] == 2) {
                    $final_mark = round($num_right_answers / $num_answers * 10, 2);
                    $final_percent = round($num_right_answers / $num_answers * 100);
                    if ($final_percent >= $tr_success_value_num) {
                        $final_verdict = 1;
                    }
                }
                elseif ($selecting_test_information['tr_success_value_type'] == 3) {
                    $weight_answers = $this->objUserJournalDB->countAnsWeight($utj_id);
                    $weight_answers = $weight_answers[0]['sum'];
                    $weight_right_answers = $this->objUserJournalDB->countRightAnsWeight($utj_id);
                    $weight_right_answers = $weight_right_answers[0]['sum'];
                    $final_mark = round($weight_right_answers / $weight_answers * 10, 2);
                    if ($weight_right_answers >= $tr_success_value_num) {
                        $final_verdict = 1;
                    }
                }
                $finish_date = time();
                $result = $this->objUserJournalDB->markTestAsCompleted($utj_id, $finish_date, $num_answers, $num_right_answers, $final_mark, $final_verdict);
                if ($result == false) {
                    array_push($this->actionResultMessage, array('message' => 'При попытке пометить тест как пройденный, возникли проблемы',
                        'successfully' => 'failed_adding'));
                }
            }
        }
    }

    /**
     * @param $utj_id
     * @return mixed
     */
    public function getFinalTestResult($utj_id) {

        $finished_test_stat = $this->objUserJournalDB->getFinalTestResult($utj_id);
        if ($finished_test_stat == false) {
            array_push($this->actionResultMessage, array('message' => 'При попытке получить статистику по завершенному тесту вопросы возникли проблемы',
                'successfully' => 'failed_adding'));
        }
        else {
            return $finished_test_stat;
        }
    }
}
