<?php

/**
 * Class TestJournalController
 */
class TestJournalController {

    /**
     * @var null
     */
    protected $objTestJournalDB = null;

    /**
     * @var array
     */
    protected $actionResultMessage = [];

    public function __construct($obj_test_journal_DB) {

        $this->objTestJournalDB = $obj_test_journal_DB;
    }

    /**
     * @param $user_right
     * @param $user_id
     * @return mixed
     */
    public function getTplArray($user_right, $user_id) {

        $include_appointments = true;
        $include_appointed_group_stat = false;
        $one_appointment_stat_array = [];
        $groups_stat_array= [];
        if(!empty($_GET['at_id'])) {
            $at_id = $_GET['at_id'];
            $one_appointment_stat_array = $this->getOneAppointmentStatArray($user_id, $user_right, $at_id);
            if (is_array($one_appointment_stat_array)) {
                $include_appointments = false;
                $include_appointed_group_stat = true;
            }
            else {
                $groups_stat_array = $this->getGroupsStatArray($user_id, $user_right);
            }
        }
        else {
            $groups_stat_array = $this->getGroupsStatArray($user_id, $user_right);

        }

        $one_appointment_stat_array = $this->prepareTplOneAppointmentStatArray($one_appointment_stat_array);
        $groups_stat_array = $this->prepareTplGroupsStatArray($groups_stat_array);

        $result_array['if_vars']['include_appointments'] = $include_appointments;
        $result_array['if_vars']['include_appointed_group_stat'] = $include_appointed_group_stat;

        $result_array['foreach_arrs']['one_appointment_stat_array'] = [];
        if (!empty($one_appointment_stat_array) && is_array($one_appointment_stat_array)) {
            $result_array['foreach_arrs']['one_appointment_stat_array'] = $one_appointment_stat_array;
        }
        $result_array['foreach_arrs']['groups_stat_array'] = [];
        if (!empty($groups_stat_array) && is_array($groups_stat_array)) {
            $result_array['foreach_arrs']['groups_stat_array'] = $groups_stat_array;
        }

        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($this->actionResultMessage)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $this->actionResultMessage;
        }
        return $result_array;
    }

    /**
     * @param $user_id
     * @param $user_right
     * @return mixed
     */
    protected function getGroupsStatArray($user_id, $user_right) {

        if ($user_right & Router::ADMIN_RIGHT) {
            $groups_stat_array = $this->objTestJournalDB->getAllGroupsStatArray();
            if ($groups_stat_array == false) {
                array_push($this->actionResultMessage, array('message' => 'Не удалось извлечь список назначенных тестов',
                    'successfully' => 'error_getting_tests'));
            }
            elseif ($groups_stat_array === true){
                array_push($this->actionResultMessage, array('message' => 'В базе отсутсвуют назначенные тесты',
                    'successfully' => 'empty_appointed_tests'));
            }
        }
        else {
            $groups_stat_array = $this->objTestJournalDB->getGroupsStatArray($user_id);
            if ($groups_stat_array == false) {
                array_push($this->actionResultMessage, array('message' => 'Не удалось извлечь список назначенных тестов пользователей',
                    'successfully' => 'error_getting_tests'));
            }
            elseif ($groups_stat_array === true){
                array_push($this->actionResultMessage, array('message' => 'Вы не назначали не одного теста пользователям',
                    'successfully' => 'empty_appointed_tests'));
            }
        }
        return $groups_stat_array;
    }

    /**
     * @param $user_id
     * @param $user_right
     * @param $at_id
     * @return bool|mixed
     */
    protected function getOneAppointmentStatArray($user_id, $user_right, $at_id) {

        if ($user_right & Router::ADMIN_RIGHT) {
            $one_appointment_stat_array = $this->getOneAppointmentStatArrayFromDB($at_id);
        }
        else {
            $checking_result = $this->objTestJournalDB->checkArrayForCheckingUserRightsForAppointment($user_id, $at_id);
            if (is_array($checking_result)) {
                $one_appointment_stat_array = $this->getOneAppointmentStatArrayFromDB($at_id);
            }
            else {
                array_push($this->actionResultMessage, array('message' => 'У вас нет прав на просмостр назначенных тестов',
                    'successfully' => 'error_no_rights'));
                $one_appointment_stat_array = false;
            }
        }
        return $one_appointment_stat_array;
    }

    /**
     * @param $at_id
     * @return mixed
     */
    protected function getOneAppointmentStatArrayFromDB($at_id) {

        $one_appointment_stat_array = $this->objTestJournalDB->getOneAppointmentStatArray($at_id);
        if ($one_appointment_stat_array == false) {
            array_push($this->actionResultMessage, array('message' => 'Не удалось извлечь информацию о назначенных тестах',
                'successfully' => 'empty_one_appointment_stat_array'));
        }
        elseif ($one_appointment_stat_array === true){
            array_push($this->actionResultMessage, array('message' => 'Для данного назначения отсутствуют соотвествующие тесты, что скорее всего является ошибкой, обратитесь к администратору',
                'successfully' => 'error_no_tests'));
        }
        return $one_appointment_stat_array;
    }

    /**
     * @param $one_appointment_stat_array
     * @return array
     */
    protected function prepareTplOneAppointmentStatArray($one_appointment_stat_array) {

        if(is_array($one_appointment_stat_array)) {
            foreach ($one_appointment_stat_array as $key=>$value) {
                if (!empty($value['ru_name']) || (!empty($value['ru_surname']))) {
                    $one_appointment_stat_array[$key]['user_or_key_number'] = $value['ru_name'].' '.$value['ru_surname'];
                }
                elseif (!empty($value['tu_key'])) {
                    $one_appointment_stat_array[$key]['user_or_key_number'] = $value['tu_key'];
                }
                else {
                    $one_appointment_stat_array[$key]['user_or_key_number'] = '-------------';
                }
                if (empty($value['utj_finish_date'])) {
                    $one_appointment_stat_array[$key]['utj_finish_date'] = '---';
                }
                else {
                    $one_appointment_stat_array[$key]['utj_finish_date'] = date("Y-m-d H:i:s", $value['utj_finish_date']);
                }
                if (empty($value['tu_comment'])) {
                    $one_appointment_stat_array[$key]['tu_comment'] = '---';
                }
                if ($value['utj_is_finished'] == 1) {
                    $one_appointment_stat_array[$key]['utj_is_finished'] = 'Да';
                }
                else {
                    $one_appointment_stat_array[$key]['utj_is_finished'] = 'Нет';
                }
                if ($value['utj_is_finished'] == 1) {
                    if (empty($value['utj_final_verdict'])) {
                        $one_appointment_stat_array[$key]['utj_final_verdict'] = 'Не сдан';
                    }
                    else {
                        $one_appointment_stat_array[$key]['utj_final_verdict'] = 'Сдан';
                    }
                }
                else {
                    $one_appointment_stat_array[$key]['utj_final_verdict'] = '---';
                }
            }
        }
        return $one_appointment_stat_array;
    }

    /**
     * @param $groups_stat_array
     * @return array
     */
    protected function prepareTplGroupsStatArray($groups_stat_array) {

        if(is_array($groups_stat_array)) {
            foreach ($groups_stat_array as $key=>$value) {
                if (!empty($value['at_creating_time'])) {
                    $groups_stat_array[$key]['at_creating_time'] = date("Y-m-d H:i:s", $value['at_creating_time']);
                }
                else {
                    $groups_stat_array[$key]['user_or_key_number'] = '-------------';
                }
            }
        }
        return $groups_stat_array;
    }
}
