<?php

/**
 * Class UGroupMemberController
 */
class UGroupMemberController {

    /**
     * @var null
     */
    protected $objUGroupMemberDB = null;

    /**
     * @var array
     */
    protected $userGroupArray = [];

    /**
     * @var array
     */
    protected $actionResultMessage = [];

    public function __construct($obj_ugroup_member_DB, $user_group_array) {

        $this->objUGroupMemberDB = $obj_ugroup_member_DB;
        $this->userGroupArray = $user_group_array;
    }

    /**
     * @return array
     */
    public function getActionResultMessage() {
        $actionResultMessage = $this->actionResultMessage;
        return $actionResultMessage;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getTplArray($user_id) {

        $user_group_DB = $this->userGroupArray;
        $user_group_tpl_search = [];
        if ($user_group_DB) {
            $user_group_tpl_search = $user_group_DB;
            foreach ($user_group_tpl_search as $key => $value) {
                $user_group_tpl_search[$key]['selected_user_group'] = null;
            }
        }
        $user_group_tpl_action = $user_group_tpl_search;
        //----------------------------------
        $user_group_input = 0;
        if (isset($_GET['user_group'])) {
            $user_group_input = $_GET['user_group'];
            foreach ($user_group_tpl_search as $key => $value) {
                if ($value['ug_id'] == $user_group_input) {
                    $user_group_tpl_search[$key]['selected_user_group'] = "selected='selected'";
                }
            }
        }
        //----------------------------------
        $action = null;
        if (!empty($_GET['action'])) {
            $action = $_GET['action'];
        }
        $search_type = 1;
        if (!empty($_GET['search_type'])) {
            $search_type = $_GET['search_type'];
        }
        $u_action = 1;
        if (isset($_POST['u_action'])) {
            $u_action = $_POST['u_action'];
        }
        $checked_user = [];
        if (!empty($_POST['user'])) {
            $checked_user = $_POST['user'];
        }
        $submit_pressed = null;
        if (!empty($_POST['submit_pressed'])) {
            $submit_pressed = $_POST['submit_pressed'];
        }
        $u_group_search = null;
        if (isset($_POST['u_group_action'])) {
            $u_group_search = $_POST['u_group_action'];
            foreach ($user_group_tpl_action as $key => $value) {
                if ($value['ug_id'] == $u_group_search) {
                    $user_group_tpl_action[$key]['selected_user_group'] = "selected='selected'";
                }
            }
        }
        //----------------------------------
        $result_array['if_vars']['include_search_form'] = false;
        $result_array['if_vars']['include_result_form'] = false;
        //----------------------------------
        if ($action == 'search') {
            $result_array['if_vars']['include_search_form'] = true;
            if ($search_type == 1) {
                $search_form_type = "1AllUsers";
                $result_array['if_vars']['include_result_form'] = true;
                $user_array = $this->selectUsers();
                if (!empty($user_array) && is_array($user_array)) {
                    if (!empty($submit_pressed)) {
                        $result_checked_user = $this->checkAdmitedUsers($checked_user);
                        if ($result_checked_user == 1) {
                            foreach ($checked_user as $key => $value) {
                                $checked_user[$key] = $key;
                            }
                            if (isset($u_action)) {
                                $chekingResult = $this->checkInuptDataForAction($u_action, $u_group_search);
                                if ($chekingResult == 1) {
                                    $this->doAction($u_action, $u_group_search, $checked_user, $search_type);
                                }
                            }
                            $user_array = $this->selectUsers();
                            if (!empty($user_array) && is_array($user_array)) {
                                $user_array = $this->prepareCheckedQuestion($checked_user, $user_array, 'ru_id');
                            }
                        }
                    }
                }
  
            }
            elseif ($search_type == 2) {
                $search_form_type = "2ByGroup";
                $result_array['if_vars']['include_result_form'] = true;
                $user_array = $this->selectUserByGroup($user_group_input);
                if (!empty($user_array) && is_array($user_array)) {
                    if (!empty($submit_pressed)) {
                        $result_checked_user = $this->checkAdmitedUsers($checked_user);
                        if ($result_checked_user == 1) {
                            $checked_user = $this->getCheckedUserId($checked_user, $user_array, 'um_id', 'um_registered_user_id');
                            if (isset($u_action)) {
                                $chekingResult = $this->checkInuptDataForAction($u_action, $u_group_search);
                                if ($chekingResult == 1) {
                                    $this->doAction($u_action, $u_group_search, $checked_user, $search_type);
                                }
                            }
                            $user_array = $this->selectUserByGroup($user_group_input);
                            if (!empty($user_array) && is_array($user_array)) {
                                $user_array = $this->prepareCheckedQuestion($checked_user, $user_array, 'um_id');
                            }
                        }
                    }
                }
            }
            else {
                array_push($this->actionResultMessage, array('message' => 'Необходимо выбрать один из вариантов поиска',
                    'successfully' => 'bad_search_variant'));
                $result_array['if_vars']['include_search_form'] = false;
            }

        }
        $result_array['dynamic_vars']['checked_action_1'] = "";
        $result_array['dynamic_vars']['checked_action_2'] = "";
        $result_array['dynamic_vars']['checked_action_3'] = "";
        if(isset($u_action)) {
            $result_array['dynamic_vars']['checked_action_'.$u_action] = 'checked="checked"';
        }
        //----------------------------------
        $result_array['dynamic_vars']['get_user_group'] = "";
        if(isset($user_group_input)) {
            $result_array['dynamic_vars']['get_user_group'] = "&user_group=" . $user_group_input;
        }
        //----------------------------------
        $result_array['dynamic_vars']['checked_1_search_type'] = '';
        $result_array['dynamic_vars']['checked_2_search_type'] = '';
        if  (in_array($search_type, array(1, 2))) {
            $result_array['dynamic_vars']['checked_'.$search_type.'_search_type'] = 'checked="checked"';
        }
        //----------------------------------
        $result_array['foreach_arrs']['user_array'] = [];
        $result_array['if_vars']['empty_dir'] = false;
        if (!empty($user_array) && is_array($user_array)) {
            $result_array['foreach_arrs']['user_array'] = $user_array;
        }
        else {
            $result_array['if_vars']['empty_dir'] = true;
        }
        //----------------------------------
        $result_array['dynamic_vars']['search_form_type'] = '';
        if (!empty($search_form_type)){
            $result_array['dynamic_vars']['search_form_type'] = $search_form_type;
        }
        $result_array['foreach_arrs']['user_group_array_search'] = $user_group_tpl_search;
        $result_array['foreach_arrs']['user_group_array_action'] = $user_group_tpl_action;
        //----------------------------------
        $actionMessages = $this->actionResultMessage;
        $result_array['foreach_arrs']['actionResultMessage'] = [];
        if (!empty($actionMessages)) {
            $result_array['foreach_arrs']['actionResultMessage'] = $actionMessages;
        }
        return $result_array;
    }

    /**
     * @return mixed
     */
    public function selectUsers() {

        $result = $this->objUGroupMemberDB->selectAllRegisteredUsers();
        if ($result === false) {
            array_push ($this->actionResultMessage, array('message' =>  'При формировании списка пользователей возникли проблемы',
                'successfully' => 'error_questions_list'));
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' =>  'В базе отсуствуют пользователи',
                'successfully' => 'error_empty_array'));
        }
        else {
            foreach ($result as $key => $value) {
                $result[$key]['user_checked'] = null;
            }
            //array_push ($this->actionResultMessage, array('message' =>  'Список вопросов сформирован',
            //    'successfully' => '1'));
        }
        return $result;
    }

    /**
     * @param $um_user_group_id
     * @return mixed
     */
    public function selectUserByGroup($um_user_group_id) {

        if ($um_user_group_id == 0) {
            $result = $this->objUGroupMemberDB->selectUserByAllGroup();
        }
        else {
            $result = $this->objUGroupMemberDB->selectUserByOneGroup($um_user_group_id);
        }
        if ($result === false) {
            array_push ($this->actionResultMessage, array('message' =>  'При формировании списка пользователей возникли проблемы',
                'successfully' => 'error_questions_list'));
        }
        elseif ($result === true) {
            array_push ($this->actionResultMessage, array('message' =>  'В базе отсуствуют пользователи',
                'successfully' => 'error_empty_array'));
        }
        else {
            foreach ($result as $key => $value) {
                $result[$key]['user_checked'] = null;
            }
            //array_push ($this->actionResultMessage, array('message' =>  'Список вопросов сформирован',
            //    'successfully' => '1'));
        }
        return $result;
    }

    /**
     * @param $checked_user
     * @param $user_array
     * @param $group_index
     * @param $user_index
     * @return mixed
     */
    public function getCheckedUserId ($checked_user, $user_array, $group_index, $user_index) {

        foreach ($user_array as $key => $value) {
            $user_id = $value[$group_index];
            if (array_key_exists($user_id, $checked_user)) {
                $checked_user[$user_id] = $value[$user_index];
            }
        }
        return $checked_user;
    }

    public function prepareCheckedQuestion($checked_user, $user_array, $el_id_index){

        if (!empty($checked_user)) {
            $checked_user = $_POST['user'];
            foreach ($user_array as $key => $value) {
                if (array_key_exists($value[$el_id_index], $checked_user)) {
                    $user_array[$key]['user_checked'] = "checked='checked'";
                }
            }
        }
        return $user_array;
    }

    /**
     * @param $u_action
     * @param $user_group_tpl_search
     * @return int
     */
    public function checkInuptDataForAction($u_action, $user_group_tpl_search) {

        $correct_input_data = 1;
        if (empty($u_action)) {
            array_push($this->actionResultMessage, array('message' => 'Поле с необходимым действием не заполнено',
                'successfully' => 'empty_action'));
            $correct_input_data = 0;
        }
        if (($u_action <> 3) && (empty($user_group_tpl_search))) {
            array_push($this->actionResultMessage, array('message' => 'Поле с группой куда копируется (перемещается) пользователь не заполнено',
                'successfully' => 'empty_q_or_t_array'));
            $correct_input_data = 0;
        }
        return $correct_input_data;
    }

    /**
     * @param $checked_user
     * @return int
     */
    public function checkAdmitedUsers($checked_user) {

        $correct_input_data = 1;
        if (empty($checked_user) || !is_array($checked_user)) {
            array_push($this->actionResultMessage, array('message' => 'В табличной части не отмечены пользователи, с которыми необходимо совершить действие',
                'successfully' => 'empty_question_array'));
            $correct_input_data = 0;
        }
        return $correct_input_data;
    }

    /**
     * @param $u_action
     * @param $user_group_tpl_search
     * @param $checked_user
     * @param $search_type
     */
    public function doAction($u_action, $user_group_tpl_search, $checked_user, $search_type) {

        $error_adding_counter = 0;
        $error_deleting_counter = 0;
        if ($u_action == 1) {//Копировать в тест
            foreach ($checked_user as $key => $value) {
                $adding_result = $this->objUGroupMemberDB->addUserToUGroupMember($value, $user_group_tpl_search);
                if ($adding_result == false) {
                    $error_adding_counter++;
                }
            }
            if ($error_adding_counter === 0) {
                array_push ($this->actionResultMessage, array('message' => 'Пользователи былы успешно скопированы в новую группу',
                    'successfully' => '1'));
            }
            else {
                array_push ($this->actionResultMessage, array('message' => 'Не удалось скопировать пользователей в новую группу',
                    'successfully' => 'error_adding_repository'));
            }
        }
        elseif ($u_action == 2) {//Переместить в тест
            foreach ($checked_user as $key => $value) {
                $adding_result = $this->objUGroupMemberDB->addUserToUGroupMember($value, $user_group_tpl_search);
                if ($adding_result == false) {
                    $error_adding_counter++;
                }
                $deleting_result = $this->deleteUserFromUGroupMember($key);
                if ($deleting_result == false) {
                    $error_deleting_counter++;
                }
            }
            if ($error_adding_counter === 0) {
                array_push ($this->actionResultMessage, array('message' => 'Пользователи былы успешно добавлены в новую группу',
                    'successfully' => '1'));
            }
            else {
                array_push ($this->actionResultMessage, array('message' => 'Не удалось добавить пользователей в новую группу группу',
                    'successfully' => 'error_adding_repository'));
            }
            if ($error_deleting_counter == 0) {
                array_push ($this->actionResultMessage, array('message' => 'Пользователи были успешно удалены из перованачальной группы',
                    'successfully' => '1'));
            }
            else {
                array_push ($this->actionResultMessage, array('message' => 'Не удалось удалить пользователей из перованачальной группы',
                    'successfully' => 'error_delete'));
            }
        }
        elseif ($u_action == 3) {//Удалить из тек.набора
            foreach ($checked_user as $key => $value) {
                $deleting_result = $this->deleteUserFromUGroupMember($key);
                if ($deleting_result == false) {
                    $error_deleting_counter++;
                }
            }
            if ($error_deleting_counter == 0) {
                array_push ($this->actionResultMessage, array('message' => 'Пользователи были успешно удалены из первоначальной группы',
                    'successfully' => '1'));
            }
            else {
                array_push ($this->actionResultMessage, array('message' => 'Не удалось удалить пользователей из перовначальной группы',
                    'successfully' => 'error_delete'));
            }
        }
    }

    /**
     * @param $key
     * @return mixed
     */
    protected function deleteUserFromUGroupMember($key){

            $result = $this->objUGroupMemberDB->deleteUserFromUGroupMember($key);
            return $result;
    }
}