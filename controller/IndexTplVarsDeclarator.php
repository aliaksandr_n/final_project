<?php

/**
 * Class IndexTplVarsDeclarator
 *
 * This class generate main (common) data for templates
 */
class IndexTplVarsDeclarator {

    /**
     * Centre method, which operate main vars groups.
     *
     * @param $page_access_denied
     *  Access to the page (true- 1 or false -0).
     * @param $user_right
     *  Number, witch contains user right's.
     * @return array
     *  Array for the template.
     */
    public function getTplArray($page_access_denied, $user_right, $labels) {

        $result_array = [];
        $result_array['dynamic_vars'] = $this->prepareDynamicVars($page_access_denied);
        $result_array['label_vars'] = $this->prepareLabelVars($labels);
        $result_array['config_vars'] = $this->prepareConfigVars();
        $result_array['foreach_arrs'] = $this->prepareForeachVars();
        $result_array['if_vars'] = $this->prepareIfVars($user_right);
        return $result_array;
    }

    /**
     * The method prepare the array with dynamic vars.
     *
     * @param $page_access_denied
     *  Access to the page (true- 1 or false -0).
     * @return array
     *  Array with dynamic vars for the template.
     */
    public function prepareDynamicVars ($page_access_denied) {

        $dynamic_vars = [];
        if (!empty($_GET['page']) && ($page_access_denied == 0)) {
            $dynamic_vars['page'] = $_GET['page'];
            $dynamic_vars['page'] = preg_replace("/[_]/", "", $dynamic_vars['page']);
        }
        else {
            $dynamic_vars['page'] = 'Main';
        }
        return $dynamic_vars;
    }

    /**
     * The method prepare the array with label vars.
     *
     * @return array
     *  Array with label vars for the template.
     */
    public function prepareLabelVars ($labels) {

        $label_vars = $labels;
        return $label_vars;
    }

    /**
     * The method prepare the array with config vars.
     *
     * @return array
     *  Array with config vars for the template.
     */
    public function prepareConfigVars () {

        $config_vars = [];
        return $config_vars;
    }

    /**
     * The method prepare the array with foreach vars.
     *
     * @return array
     *  Array with foreach vars for the template.
     */
    public function prepareForeachVars () {

        $foreach_arrs = [];
        return $foreach_arrs;
    }

    /**
     * The method prepare the array with if vars.
     *
     * @param $user_right
     * @return array
     *  Array with if vars for the template.
     */
    public function prepareIfVars ($user_right) {

        $if_vars = [];
        $if_vars = ['show_user_segment' => false,
            'show_trainer_segment' => false,
            'show_manager_segment' => false,
            'show_admin_segment' => false];
        if (($user_right & Router::USER_RIGHT) || ($user_right & Router::ADMIN_RIGHT)) {
            $if_vars['show_user_segment'] = true;
        }
        if (($user_right & Router::TRAINER_RIGHT) || ($user_right & Router::ADMIN_RIGHT)) {
            $if_vars['show_trainer_segment'] = true;
        }
        if (($user_right & Router::MANAGER_RIGHT) || ($user_right & Router::ADMIN_RIGHT)) {
            $if_vars['show_manager_segment'] = true;
        }
        if ($user_right & Router::ADMIN_RIGHT) {
            $if_vars['show_admin_segment'] = true;
        }
        return $if_vars;
    }
}
