<?php

/**
 * Class TemplatesEngine
 */
class TemplatesEngine {


    function __construct($index_tpl_file) {
        $result = file_get_contents($index_tpl_file);
        $this->tpl_replace($result);
    }

    /**
     * @param $result
     */
    function tpl_replace($result) {
        while (
            preg_match("/{DV=\"(.*)\"}/U", $result) ||
            preg_match("/{LABEL=\"(.*)\"}/U", $result) ||
            preg_match("/{CV=\"(.*)\"}/U", $result) ||
            preg_match("/{FOREACH=\"(.*)\"}(.*){FOREACH_END}/Us", $result) ||
            preg_match("/{IF=\"(.*)\"}{(.*)}(.*){END_IF}/Us", $result) ||
            preg_match("/{FILE=\"(.*)\"}/U", $result)
        ) {
            $result = preg_replace_callback("/{DV=\"(.*)\"}/U", array($this, 'process_dv'), $result);
            $result = preg_replace_callback("/{LABEL=\"(.*)\"}/U", array($this, 'process_lv'), $result);
            $result = preg_replace_callback("/{CV=\"(.*)\"}/U", array($this, 'process_cv'), $result);
            $result = preg_replace_callback("/{FOREACH=\"(.*)\"}(.*){FOREACH_END}/Us", array($this, 'process_foreach'), $result);
            $result = preg_replace_callback("/{IF=\"(.*)\"}{(.*)}{(.*)}(.*){END_IF}/Us", array($this, 'process_if'), $result);
            $result = preg_replace_callback("/{FILE=\"(.*)\"}/U", array($this, 'process_file'), $result);
        }
        echo $result;
    }

    /**
     * @param $dv
     * @throws Exception
     */
    function process_dv($dv) {
        $dvars = &$GLOBALS['tpl_vars']['dynamic_vars'];
        $dv_name = $dv[1];
        if (isset($dvars[$dv_name])) {
            return $dvars[$dv_name];
        }
        else {
            throw new Exception('Dynamic var ['.$dv_name.'] not defined!');
        }
    }

    /**
     * @param $lv
     * @throws Exception
     */
    function process_lv($lv) {
        $labelvars = &$GLOBALS['tpl_vars']['label_vars'];
        $lv_name = $lv[1];
        if (isset($labelvars[$lv_name])) {
            return $labelvars[$lv_name];
        }
        else {
            throw new Exception('Label var ['.$lv_name.'] not defined!');
        }
    }

    /**
     * @param $cv
     * @throws Exception
     */
    function process_cv($cv) {
        $cvars = &$GLOBALS['tpl_vars']['config_vars'];
        $cv_name = $cv[1];
        if (isset($cvars[$cv_name])) {
            return $cvars[$cv_name];
        }
        else {
            throw new Exception('Config var [' . $cv_name . '] not defined!');
        }
    }

    /**
     * @param $file
     * @throws Exception
     */
    function process_file($file) {
        $file_name = $file[1];
        if (is_file($file_name)) {
            return file_get_contents($file_name);
        }
        else {
            throw new Exception('File [' . $file_name . '] not found!');
        }
    }

    /**
     * @param $if
     * @throws Exception
     */
    function process_if($if) {
        $exp = &$GLOBALS['tpl_vars']['if_vars'];
        $if_name = $if[1];
        if (isset($exp[$if_name])) {
            if ((($if[2] == "==") && ($exp[$if_name] == $if[3])) ||
                (($if[2] == "===") && ($exp[$if_name] === $if[3])) ||
                (($if[2] == "!=") && ($exp[$if_name] != $if[3])) ||
                (($if[2] == "<>") && ($exp[$if_name] <> $if[3])) ||
                (($if[2] == "<") && ($exp[$if_name] < $if[3])) ||
                (($if[2] == ">") && ($exp[$if_name] > $if[3])) ||
                (($if[2] == "<=") && ($exp[$if_name] <= $if[3])) ||
                (($if[2] == ">=") && ($exp[$if_name] >= $if[3]))
            ) {
                return $if[4];
            }
        }
        else {
            throw new Exception('If var ['.$if_name.'] not defined!');
        }
    }

    /**
     * @param $foreach
     * @throws Exception
     */
    function process_foreach($foreach) {
        $data_array = &$GLOBALS['tpl_vars']['foreach_arrs'];
        $array_name_from_foreach_ = $foreach[1];
        if (isset($data_array[$array_name_from_foreach_])) {
            $new_html_code = '';
            foreach ($data_array[$array_name_from_foreach_] as $el_array_name_from_foreach) {
                $between_foreach_text = $foreach[2];
                //$between_foreach_text = preg_match_all("/{FR_ELEMENT=\"(.*)\"}/U", $keys_name_from_between_foreach, $between_foreach_text);
                preg_match_all("/{FR_ELEMENT=\"(.*)\"}/U", $between_foreach_text, $matches, PREG_PATTERN_ORDER);
                $index_name_from_between_foreach = [];
                foreach ($matches[1] as $index_name){
                    $index_name_from_between_foreach[] = $el_array_name_from_foreach[$index_name];
                }
                preg_match_all("/{FR_ELEMENT=\"(.*)\"}/U", $between_foreach_text, $matches, PREG_PATTERN_ORDER);
                $index_teg_name_from_foreach = [];
                foreach ($matches[0] as $index_teg) {
                    $index_teg_name_from_foreach[] = '/' . $index_teg . '/';
                }
                $between_foreach_text = preg_replace($index_teg_name_from_foreach, $index_name_from_between_foreach, $between_foreach_text);
                $new_html_code .= $between_foreach_text;
            }
            return $new_html_code;
        }
        else {
            throw new Exception('Foreach var [' . $array_name_from_foreach_ . '] not defined!');
        }
    }
}
