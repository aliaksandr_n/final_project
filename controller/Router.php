<?php

/**
 * Class Router
 *
 * This class includes necessary pages' classes and
 * manages the availability of pages depending on the user rights.
 */
class Router {

    const USER_RIGHT = 1;
    const TRAINER_RIGHT = 2;
    const MANAGER_RIGHT = 4;
    const ADMIN_RIGHT = 8;

    /**
     * Access to the page (true- 1 or false -0).
     *
     * @var int
     */
    protected $accessDenied = 1;

    /**
     * Getter of "accessDenied" property
     *
     * @return int
     */
    public function getAccessDenied() {
        return $this->accessDenied;
    }

    /**
     * The method creates necessary classes according to the requeried page and
     * manages the availability of pages depending on the user rights.
     *
     * @param $user_id
     *  Authorized user's id.
     * @param $user_right
     *  Number, witch contains user right's.
     * @return array
     *  Array for the page's templates.
     */
    public function startController($user_id, $user_right) {

        $tpl_main_vars = [];
        if (!empty($_GET['page'])) {
            switch ($_GET['page']) {
                case 'user_journal':
                    $this->accessDenied = 0;
                    $obj_user_journal_DB = new UserJournalDB;
                    $obj_user_journal_controller = new UserJournalController($obj_user_journal_DB, $user_id);
                    $tpl_main_vars = $obj_user_journal_controller->getTplArray();
                    break;
                case 'user_group':
                    if (($user_right & self::TRAINER_RIGHT) || ($user_right & self::ADMIN_RIGHT)) {
                        $this->accessDenied = 0;
                        $obj_user_group_DB = new UserGroupDB;
                        $obj_user_group_controller = new UserGroupController($obj_user_group_DB);
                        $tpl_main_vars = $obj_user_group_controller->getTplArray($user_id);
                    }
                    break;
                case 'ugroup_member':
                    if (($user_right & self::TRAINER_RIGHT) || ($user_right & self::ADMIN_RIGHT)) {
                        $this->accessDenied = 0;
                        $obj_user_group_DB = new UserGroupDB;
                        $obj_user_group_controller = new UserGroupController($obj_user_group_DB);
                        $obj_user_group_controller->readUserGroup();
                        $user_group_array = $obj_user_group_controller->getUserGroupArray();

                        $obj_ugroup_member_DB = new UGroupMemberDB;
                        $obj_ugroup_member_controller = new UGroupMemberController($obj_ugroup_member_DB, $user_group_array);
                        $tpl_main_vars = $obj_ugroup_member_controller->getTplArray($user_id);
                    }
                    break;
                case 'appointed_test':
                    if (($user_right & self::TRAINER_RIGHT) || ($user_right & self::ADMIN_RIGHT)) {
                        $this->accessDenied = 0;
                        $obj_test_category_DB = new TestCategoryDB;
                        $obj_test_category_controller = new TestCategoryController($obj_test_category_DB);
                        $obj_test_category_controller->readTestCategory();
                        $test_category_array = $obj_test_category_controller->getTestCategoryArray();

                        $obj_test_repository_DB = new TestRepositoryDB;
                        //$obj_test_repository_controller = new TestRepositoryController($obj_test_repository_DB);
                        $test_repository_array = $obj_test_repository_DB->selectTest();

                        $obj_user_group_DB = new UserGroupDB;
                        $obj_user_group_controller = new UserGroupController($obj_user_group_DB);
                        $obj_user_group_controller->readUserGroup();
                        $user_group_array = $obj_user_group_controller->getUserGroupArray();

                        $obj_appointed_test_DB = new AppointedTestDB;
                        $obj_appointed_test_controller = new AppointedTestController($obj_appointed_test_DB, $user_group_array, $test_category_array, $test_repository_array);
                        $tpl_main_vars = $obj_appointed_test_controller->getTplArray($user_id);
                    }
                    break;
                case 'temp_user':
                    if (($user_right & self::TRAINER_RIGHT) || ($user_right & self::ADMIN_RIGHT)) {
                        $this->accessDenied = 0;
                        $obj_temp_user_DB = new TempUserDB;
                        $obj_temp_user_controller = new TempUserController($obj_temp_user_DB);
                        $tpl_main_vars = $obj_temp_user_controller->getTplArray();
                    }
                    break;
                case 'test_journal':
                    if (($user_right & self::TRAINER_RIGHT) || ($user_right & self::ADMIN_RIGHT)) {
                        $this->accessDenied = 0;
                        $obj_test_journal_DB = new TestJournalDB;
                        $obj_test_journal_controller = new TestJournalController($obj_test_journal_DB);
                        $tpl_main_vars = $obj_test_journal_controller->getTplArray($user_right, $user_id);
                    }
                    break;
                case 'trainer_stat':
                    if (($user_right & self::TRAINER_RIGHT) || ($user_right & self::ADMIN_RIGHT)) {
                        $this->accessDenied = 0;
                        $obj_trainer_stat_DB = new TrainerStatDB;
                        $obj_trainer_stat_controller = new TrainerStatController($obj_trainer_stat_DB);
                        $tpl_main_vars = $obj_trainer_stat_controller->getTplArray();
                    }
                    break;
                case 'question_category':
                    if (($user_right & self::MANAGER_RIGHT) || ($user_right & self::ADMIN_RIGHT)) {
                        $this->accessDenied = 0;
                        $obj_question_category_DB = new QuestionCategoryDB;
                        $obj_question_category_controller = new QuestionCategoryController($obj_question_category_DB);
                        $tpl_main_vars = $obj_question_category_controller->getTplArray();
                    }
                    break;
                case 'question_setting':
                    if (($user_right & self::MANAGER_RIGHT) || ($user_right & self::ADMIN_RIGHT)) {
                        $this->accessDenied = 0;
                        $obj_question_category_DB = new QuestionCategoryDB;
                        $obj_question_category_controller = new QuestionCategoryController($obj_question_category_DB);
                        $obj_question_category_controller->readQuestionCategory();
                        $obj_question_category_controller->getQuestionCategoryArray();

                        $obj_question_setting_DB = new QuestionSettingDB;
                        $obj_question_setting_controller = new QuestionSettingController($obj_question_setting_DB);
                        $obj_question_setting_controller->readQuestionType();

                        $tpl_main_vars = $obj_question_setting_controller->getTplArray($obj_question_category_controller, $user_id);
                    }
                    break;
                case 'question_repository':
                    if (($user_right & self::MANAGER_RIGHT) || ($user_right & self::ADMIN_RIGHT)) {
                        $this->accessDenied = 0;
                        $obj_question_repository_DB = new QuestionRepositoryDB;
                        $obj_question_repository_controller = new QuestionRepositoryController($obj_question_repository_DB);
                        $tpl_main_vars = $obj_question_repository_controller->getTplArray();
                    }
                    break;
                case 'test_category':
                    if (($user_right & self::MANAGER_RIGHT) || ($user_right & self::ADMIN_RIGHT)) {
                        $this->accessDenied = 0;
                        $obj_test_category_DB = new TestCategoryDB;
                        $obj_test_category_controller = new TestCategoryController($obj_test_category_DB);
                        $tpl_main_vars = $obj_test_category_controller->getTplArray();
                    }
                    break;
                case 'test_repository':
                    if (($user_right & self::MANAGER_RIGHT) || ($user_right & self::ADMIN_RIGHT)) {
                        $this->accessDenied = 0;
                        $obj_test_category_DB = new TestCategoryDB;
                        $obj_test_category_controller = new TestCategoryController($obj_test_category_DB);
                        $obj_test_category_controller->readTestCategory();
                        $test_category_array = $obj_test_category_controller->getTestCategoryArray();

                        $obj_test_repository_DB = new TestRepositoryDB;
                        $obj_test_repository_controller = new TestRepositoryController($obj_test_repository_DB, $test_category_array);
                        $tpl_main_vars = $obj_test_repository_controller->getTplArray($user_id);
                    }
                    break;
                case 'question_manager':
                    if (($user_right & self::MANAGER_RIGHT) || ($user_right & self::ADMIN_RIGHT)) {
                        $this->accessDenied = 0;
                        $obj_question_category_DB = new QuestionCategoryDB;
                        $obj_question_category_controller = new QuestionCategoryController($obj_question_category_DB);
                        $obj_question_category_controller->readQuestionCategory();
                        $question_category_array = $obj_question_category_controller->getQuestionCategoryArray();

                        $obj_question_repository_DB = new QuestionRepositoryDB;
                        $obj_question_repository_controller = new QuestionRepositoryController($obj_question_repository_DB);
                        $obj_question_repository_controller->readQuestionRepository();
                        $question_repository_array = $obj_question_repository_controller->getQuestionRepositoryArray();

                        $obj_test_category_DB = new TestCategoryDB;
                        $obj_test_category_controller = new TestCategoryController($obj_test_category_DB);
                        $obj_test_category_controller->readTestCategory();
                        $test_category_array = $obj_test_category_controller->getTestCategoryArray();

                        $obj_question_manager_DB = new QuestionManagerDB;
                        $obj_question_manager_controller = new QuestionManagerController($obj_question_manager_DB, $question_category_array, $question_repository_array, $test_category_array);
                        $tpl_main_vars = $obj_question_manager_controller->getTplArray();
                    }
                    break;
                case 'user_right':
                    if ($user_right & self::ADMIN_RIGHT) {
                        $this->accessDenied = 0;
                        $obj_user_right_DB = new UserRightDB;
                        $obj_user_right_controller = new UserRightController($obj_user_right_DB);
                        $tpl_main_vars = $obj_user_right_controller->getTplArray();
                    }
                    break;
                case 'registration':
                    $this->accessDenied = 0;
                    $obj_user_group_DB = new UserGroupDB;
                    $obj_user_group_controller = new UserGroupController($obj_user_group_DB);
                    $obj_user_group_controller->readUserGroup();
                    $user_group_array = $obj_user_group_controller->getUserGroupArray();

                    $obj_registration_DB = new RegistrationDB;
                    $obj_registration_controller = new RegistrationController($obj_registration_DB, $user_group_array);
                    $tpl_main_vars = $obj_registration_controller->getTplArray();
                    break;
                case 'verification':
                    $this->accessDenied = 0;
                    $obj_verification_DB = new VerificationDB;
                    $obj_verification_controller = new VerificationController($obj_verification_DB);
                    $tpl_main_vars = $obj_verification_controller->getTplArray();
                    break;
                case 'forget_pass':
                    $this->accessDenied = 0;
                    $obj_forget_pass_DB = new ForgetPassDB;
                    $obj_forget_pass_controller = new ForgetPassController($obj_forget_pass_DB);
                    $tpl_main_vars = $obj_forget_pass_controller->getTplArray();
                    break;
                case 'recovery_pass':
                    $this->accessDenied = 0;
                    $obj_recovery_pass_DB = new RecoveryPassDB;
                    $obj_recovery_pass_controller = new RecoveryPassController($obj_recovery_pass_DB);
                    $tpl_main_vars = $obj_recovery_pass_controller->getTplArray();
                    break;
                case 'login':
                    $this->accessDenied = 0;
                    break;
            }
        }
        return $tpl_main_vars;
    }
}
