<?php

/**
 * Class Language
 */
class Language {

    /**
     * Language setter
     */
    public function setActualLanguage() {

        if(!empty($_GET['page']) && ($_GET['page'] == 'language')) {
            if (!empty($_GET['lang'])) {
                if($_GET['lang'] == 'ru') {
                    setcookie('language', 'ru', time()+1209600);
                    $_SESSION['language'] = 'ru';
                }
                elseif ($_GET['lang'] == 'eng') {
                    setcookie('language', 'eng', time()+1209600);
                    $_SESSION['language'] = 'eng';
                }
            }
            header("Location: ".$_SERVER['HTTP_REFERER']);
            exit;
        }
    }

    /**
     * Language getter
     * @return string
     */
    public function getActualLanguage() {

        $selected_language = 'ru';
        if (!empty($_COOKIE['language'])) {
            if ($_COOKIE['language'] == 'ru') {
                $selected_language = 'ru';
            }
            elseif ($_COOKIE['language'] == 'eng') {
                $selected_language = 'eng';
            }
        }
        else {
            if (!empty($_SESSION['language'])) {
                if ($_SESSION['language'] == 'ru') {
                    $selected_language = 'ru';
                }
                elseif ($_SESSION['language'] == 'eng') {
                    $selected_language = 'eng';
                }
            }
        }
        return $selected_language;
    }
}
