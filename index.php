<?php
ob_start();
session_start();
header('Content-Type:text/html; charset=UTF-8');
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
error_reporting(E_ALL);

function __autoload($c) {

    $controller_path = "controller/".$c.".php";
    $controller_page_path = "controller/page/".$c.".php";
    $model_path = "model/".$c.".php";
    $model_page_path = "model/page/".$c.".php";
    $tpl_path = "tpl/".$c.".php";
    $main_path = $c.".php";

    if(file_exists($controller_path)) {
        include_once $controller_path;
    }
    elseif(file_exists($controller_page_path)) {
        include_once $controller_page_path;
    }
    elseif(file_exists($model_path)) {
        include_once $model_path;
    }
    elseif(file_exists($model_page_path)) {
        include_once $model_page_path;
    }
    elseif(file_exists($tpl_path)) {
        include_once $tpl_path;
    }
    elseif(file_exists($main_path)) {
        include_once $main_path;
    }
    else {
        exit("Файл с классом ".$c." не найден");
    }
}

require_once('config.php');

$obj_connectionToDB = ConnectionToDBAndStat::getInstance();
$obj_main_DB = new MainDB;

$obj_language_DB = new Language;
$obj_language_DB->setActualLanguage();
$selected_language = $obj_language_DB->getActualLanguage();
if ($selected_language == 'ru') {
    $labels = $obj_main_DB->getRuLabelsForTpl($selected_language);
}
elseif ($selected_language == 'eng') {
    $labels = $obj_main_DB->getEngLabelsForTpl($selected_language);
}

$obj_login_DB = new LoginDB;
$obj_login_controller = new LoginController($obj_login_DB);
$tpl_login_vars = $obj_login_controller->getTplArray();
$user_id = $obj_login_controller->getUserId();
$user_right = $obj_login_controller->getUserRight();


$obj_router = new Router($user_id);
$tpl_main_vars = $obj_router->startController($user_id, $user_right);
$page_access_denied = $obj_router->getAccessDenied();

$obj_tpl_vars_index = new IndexTplVarsDeclarator();
$tpl_index_vars =$obj_tpl_vars_index->getTplArray($page_access_denied, $user_right, $labels);

$tpl_vars = array_merge_recursive($tpl_main_vars, $tpl_index_vars, $tpl_login_vars);
new TemplatesEngine(INDEX_TPL_FILE);

//Queries statistic
/*echo "<p>QUERIES STATISTIC</p>";
var_dump($obj_connectionToDB->getQueryStat());
var_dump($obj_connectionToDB->getQueryList());*/
