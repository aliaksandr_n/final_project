$(document).ready(function() {

    $('#addNewRadioLable').click(function(index) {
        return addNewRadio();
    });

    $('#addNewCheckboxLable').click(function(index) {
        return addNewCheckbox();
    });

    $('#addNewPictureInput').click(function(index) {
        return addNewPicture();
    });

    showFileName();

//--------------- QUESTION MANAGER SEARCH MENU

    var selectedSearchType = $('.search_q_manager_type:checked').val();

    if (selectedSearchType == '1') {
        $('#search_q_cat_block').show();
        $('#search_q_rep_block').hide();
        $('#search_t_rep_block').hide();
    }
    else if (selectedSearchType == '2') {
        $('#search_q_cat_block').hide();
        $('#search_q_rep_block').show();
        $('#search_t_rep_block').hide();
    }
    else if (selectedSearchType == '3') {
        $('#search_q_cat_block').hide();
        $('#search_q_rep_block').hide();
        $('#search_t_rep_block').show();
    }
    else {
        $('#search_q_cat_block').hide();
        $('#search_q_rep_block').hide();
        $('#search_t_rep_block').hide();
    }

    $('.search_q_manager_type').change(function(index) {
        var selectedSearchType = $(this).val();
        if (selectedSearchType == '1') {
            $('#search_q_cat_block').show();
            $('#search_q_rep_block').hide();
            $('#search_q_rep').val('0');
            $('#search_t_rep_block').hide();
            $('#search_t_rep').val('0');
        }
        else if (selectedSearchType == '2') {
            $('#search_q_cat_block').hide();
            $('#search_q_cat').val('0');
            $('#search_q_rep_block').show();
            $('#search_t_rep_block').hide();
            $('#search_t_rep').val('0');
        }
        else if (selectedSearchType == '3') {
            $('#search_q_cat_block').hide();
            $('#search_q_cat').val('0');
            $('#search_q_rep_block').hide();
            $('#search_q_rep').val('0');
            $('#search_t_rep_block').show();
        }
    });

//--------------- QUESTION MANAGER ACTION

    var selectedAction = $('.search_q_manager_action:checked').val();
    if (selectedAction == '1' || selectedAction == '3') {
        $('#goal_q_repository').show();
        $('#goal_t_repository').hide();

    }
    else if (selectedAction == '2' || selectedAction == '4') {
        $('#goal_q_repository').hide();
        $('#goal_t_repository').show();
    }
    else {
        $('#goal_q_repository').hide();
        $('#goal_t_repository').hide();
    }

    $('.search_q_manager_action').change(function(index) {
        var selectedAction = $(this).val();
        if (selectedAction == '1' || selectedAction == '3') {
            $('#goal_q_repository').show();
            $('#goal_t_repository').hide();
            $('#select_t_rep').val('0');
        }
        else if (selectedAction == '2' || selectedAction == '4') {
            $('#goal_q_repository').hide();
            $('#select_q_rep').val('0');
            $('#goal_t_repository').show();
        }
        else if (selectedAction == '5') {
            $('#goal_q_repository').hide();
            $('#select_q_rep').val('0');
            $('#goal_t_repository').hide();
            $('#select_t_rep').val('0')
        }
    });


//--------------- USER GROUP MEMBER SEARCH MENU

    var selectedSearchType = $('.search_ugroup_member_type:checked').val();

    if (selectedSearchType == '1') {
        $('#search_ugroup_member_block').hide();
    }
    else if (selectedSearchType == '2') {
        $('#search_ugroup_member_block').show();
    }
    else {
        $('#search_ugroup_member_block').hide();
    }

    $('.search_ugroup_member_type').change(function(index) {
        var selectedSearchType = $(this).val();
        if (selectedSearchType == '1') {
            $('#search_ugroup_member_block').hide();
            $('#search_ugroup_member').val('0');
        }
        else if (selectedSearchType == '2') {
            $('#search_ugroup_member_block').show();
        }
        else if (selectedSearchType == '3') {
            $('#search_ugroup_member_block').hide();
            $('#search_ugroup_member').val('0');
        }
    });

//--------------- USER GROUP MEMBER ACTION

    var selectedAction = $('.search_ugroup_member_action:checked').val();

    if (selectedAction == '1' || selectedAction == '2') {
        $('#u_group').show();
    }
    else if (selectedAction == '3') {
        $('#u_group').hide();
        $('#u_group_action').val('0');
    }

    $('.search_ugroup_member_action').change(function(index) {
        var selectedAction = $(this).val();
        if (selectedAction == '1' || selectedAction == '2') {
            $('#goal_u_group').show();
        }
        else if (selectedAction == '3') {
            $('#goal_u_group').hide();
            $('#search_u_group').val('0');
        }
    });

//--------------- APPOINTED TEST SEARCH MENU

    var selectedUserTypeSearch = $('.search_appointed_test_type:checked').val();

    if (selectedUserTypeSearch == '1') {
        $('#search_by_ugroup_member_block').hide();
        $('#search_number_genereted_keys_block').hide();
    }
    else if (selectedUserTypeSearch == '2') {
        $('#search_by_ugroup_member_block').show();
        $('#search_number_genereted_keys_block').hide();
    }
    else if (selectedUserTypeSearch == '3') {
        $('#search_by_ugroup_member_block').hide();
        $('#search_number_genereted_keys_block').show();
    }
    else {
        $('#search_by_ugroup_member_block').hide();
        $('#search_number_genereted_keys_block').hide();
    }

    $('.search_appointed_test_type').change(function(index) {
        var selectedSearchType = $(this).val();
        if (selectedSearchType == '1') {
            $('#search_by_ugroup_member_block').hide();
            $('#search_by_ugroup_member').val('0');
            $('#search_number_genereted_keys_block').hide();
            $('#search_number_genereted_keys').val('0');
        }
        else if (selectedSearchType == '2') {
            $('#search_by_ugroup_member_block').show();
            $('#search_number_genereted_keys_block').hide();
            $('#search_number_genereted_keys').val('0');
        }
        else if (selectedSearchType == '3') {
            $('#search_by_ugroup_member_block').hide();
            $('#search_by_ugroup_member').val('0');
            $('#search_number_genereted_keys_block').show();
            $('#search_number_genereted_keys').val('5');
        }
    });
//--------------- QUESTION SETTING

    $('form[name*="questionForm"]').submit(function() {

        if ($('textarea').val() === '') {
            var actionResultMessage = 'Поле с текстом вопроса не заполнено';
            alert(actionResultMessage);
            return false;
        }
        if ($('input[name="qt_id"]').val() == '1') {
            if ($("input:radio[name='qs_correct']").is(":checked")== false) {
                var actionResultMessage = 'Правильный ответ не указан';
                alert(actionResultMessage);
                return false;
            }
            else if (($("input:text[name='av_text[" + $("input:radio[name='qs_correct']:checked").val() + "]']").val()) === '') {
                var actionResultMessage = 'Для правильного ответа не написан вопрос';
                alert(actionResultMessage);
                return false;
            }
        }
        else if ($('input[name="qt_id"]').val() == '2') {
            if ($('#answers input:checked').length == false) {
                var actionResultMessage = 'Правильный ответ не указан';
                alert(actionResultMessage);
                return false;
            }
            answerLength = $('#answers input:checkbox').length;
            var errorCounter = 0;
            for (var i = 1; i <= answerLength; i++) {
                if (($("#answers input:checked[name='qs_correct[" + i + "]']").is(":checked"))
                    && ($("input:text[name='av_text[" + i + "]']").val() === ''))  {
                    errorCounter = 1;
                }
            }
            if (errorCounter == 1) {
                var actionResultMessage = 'Для правильного ответа не написан вопрос';
                alert(actionResultMessage);
                return false;
            }
        }
        if ($('input[name="qs_weight"]').val() === '') {
            var actionResultMessage = 'Поле с весом вопроса не заполнено';
            alert(actionResultMessage);
            return false;
        }
        else if (($('input[name="qs_weight"]').val() != $('input[name="qs_weight"]').val().replace(/[^0-9\.]/g, ''))
            || $('input[name="qs_weight"]').val() < 0
            || $('input[name="qs_weight"]').val() > 100) {
            var actionResultMessage = 'Поле с весом должно быть числом в диапазоне от 0 до 100';
            alert(actionResultMessage);
            return false;
        }
        else if (($('input[name="qs_answer_time_duration"]').val() != $('input[name="qs_answer_time_duration"]').val().replace(/[^0-9\.]/g, ''))
            || $('input[name="qs_answer_time_duration"]').val() < 0) {
            var actionResultMessage = 'Поле с временем ответа должно быть положительным числом';
            alert(actionResultMessage);
            return false;
        }
        else if ($('select[name="qCategory"]').val() == null) {
            var actionResultMessage = 'Категория вопроса не указана';
            alert(actionResultMessage);
            return false;
        }
        else {
            var bad_ext;
            $('.inputFileText').each(function(i) {
                if ($(this).val() != '') {
                    if ($.inArray($(this).val().split('.').pop().toLowerCase(), ['png', 'jpg', 'jpeg', 'gif', 'bmp']) == -1) {
                        bad_ext = 1;
                    }
                }
            });
            if (bad_ext == 1) {
                var actionResultMessage = 'Рисунки с данным расширением не поддерживаются';
                alert(actionResultMessage);
                return false;
            }
        }
        return true;
    });

//--------------- TEST REPOSITORY
    $('form[name*="testForm"]').submit(function() {

        if ($('#tr_name').val() === '') {
            var actionResultMessage = 'Поле с названием теста не заполнено';
            alert(actionResultMessage);
            return false;
        }
        else if ($('select[name="tCategory"]').val() == null) {
            var actionResultMessage = 'Категория теста не указана';
            alert(actionResultMessage);
            return false;
        }
        else if (($('input[name="tr_number_shown_questions"]').val() != $('input[name="tr_number_shown_questions"]').val().replace(/[^0-9\.]/g, ''))
            || $('input[name="tr_number_shown_questions"]').val() < 0) {
            var actionResultMessage = 'Поле с количeством вопросов в тесте должно быть положительным числом';
            alert(actionResultMessage);
            return false;
        }
        else if (($('input[name="tr_number_shown_questions"]').val() == $('input[name="tr_number_shown_questions"]').val().replace(/[^0-9\.]/g, ''))
            && $('input[name="tr_number_shown_questions"]').val() > 0
            && $('select[name="tr_success_value_type"]').val() == 3) {
            var actionResultMessage = 'Правильный ответ не может определяться по "весу вопросов", если в тесте выводятся не все вопросы, а некоторое их число';
            alert(actionResultMessage);
            return false;
        }
        else if ($('select[name="tr_success_value_type"]').val() == null) {
            var actionResultMessage = 'В поле "Определение прав. ответа" не выбрано значение';
            alert(actionResultMessage);
            return false;
        }
        else if (($('input[name="tr_success_value_num"]').val() != $('input[name="tr_success_value_num"]').val().replace(/[^0-9\.]/g, ''))
            || $('input[name="tr_success_value_num"]').val() < 0) {
            var actionResultMessage = 'Поле со значением, необходимым для прохождения теста должно быть положительным числом';
            alert(actionResultMessage);
            return false;
        }
        else if (($('select[name="tr_control_test_time_per_questions"]').val() == '0') && ($('#tr_total_time_value').val() == '' || $('#tr_total_time_value').val() == '0')) {
            var actionResultMessage = 'Поле со значением времени на тест должно быть заполнено, т.к. контролируется "общее время теста"';
            alert(actionResultMessage);
            return false;
        }
        else if (($('input[name="tr_total_time_value"]').val() != $('input[name="tr_total_time_value"]').val().replace(/[^0-9\.]/g, ''))
            || $('input[name="tr_total_time_value"]').val() < 0) {
            var actionResultMessage = 'Поле со значением времени на тест должно быть положительным числом';
            alert(actionResultMessage);
            return false;
        }
        return true;
    });

//--------------- QUESTION MANAGER

    $('#form_question_serch_type').submit(function() {

        if ($('.search_q_manager_type:checked').val() == null) {
            var actionResultMessage = 'Необходимо выбрать один из вариантов поиска';
            alert(actionResultMessage);
            return false;
        }
    });

    $('form[name*="questionManagerAction"]').submit(function() {

        if ($('.search_q_manager_action').val() == null) {
            var actionResultMessage = 'Поле с необходимым действием не заполнено';
            alert(actionResultMessage);
            return false;
        }
        else if ($('.search_q_manager_action:checked').val() != '5' &&
            ($('select[name="q_repository_action"]').val() == null &&
            $('select[name="t_repository_action"]').val() == null)) {
            var actionResultMessage = 'Поле 4с информацией куда копируется (перемещается) вопрос не заполнено';
            alert(actionResultMessage);
            return false;
        }
        else {
            var checkedCheckboxes=$('tbody input:checked').length;
            if (checkedCheckboxes == 0) {
                var actionResultMessage = 'В табличной части не отмечены вопросы, с которыми необходимо совершить действие';
                alert(actionResultMessage);
                return false;
            }
        }
        return true;
    });

//--------------- USER GROUP MEMBER

    $('#form_ugroup_search_type').submit(function() {

        if ($('.search_ugroup_member_type:checked').val() == null) {
            var actionResultMessage = 'Необходимо выбрать один из вариантов поиска';
            alert(actionResultMessage);
            return false;
        }
    });

    $('form[name*="uGroupMemberAction"]').submit(function() {

        if ($('.search_ugroup_member_action:checked').val() == null) {
            var actionResultMessage = 'Поле с необходимым действием не заполнено';
            alert(actionResultMessage);
            return false;
        }
        else if ($('.search_ugroup_member_action:checked').val() != '3' &&
            $('select[name="u_group_action"]').val() == null) {
            var actionResultMessage = 'Поле с информацией куда копируется (перемещается) вопрос не заполнено';
            alert(actionResultMessage);
            return false;
        }
        else {
            var checkedCheckboxes=$('tbody input:checked').length;
            if (checkedCheckboxes == 0) {
                var actionResultMessage = 'В табличной части не отмечены вопросы, с которыми необходимо совершить действие';
                alert(actionResultMessage);
                return false;
            }
        }
        return true;
    });

//--------------- APPOINTED TEST

    $('#form_user_search_type').submit(function() {

        if ($('.search_appointed_test_type:checked').val() == null) {
            var actionResultMessage = 'Необходимо выбрать один из вариантов поиска';
            alert(actionResultMessage);
            return false;
        }
    });

    $('form[name*="appointedTestAction"]').submit(function() {

        if ($('select[name="appointed_test"]').val() == null) {
            var actionResultMessage = 'Не указан назначаемый тест';
            alert(actionResultMessage);
            return false;
        }
        else {
            var checkedCheckboxes=$('tbody input:checked').length;
            if (checkedCheckboxes == 0) {
                var actionResultMessage = 'В табличной части не отмечены вопросы, с которыми необходимо совершить действие';
                alert(actionResultMessage);
                return false;
            }
        }
        return true;
    });

//--------------- USER GROUP
    $('form[name*="userGroupForm"]').submit(function() {

        if ($('input[name="ug_name"]').val() == '') {
            var actionResultMessage = 'Поле с названием группы не заполнено';
            alert(actionResultMessage);
            return false;
        }
        return true;
    });



});

// Button  uploadButton- get files name
function showFileName() {
    $('.inputFile').find('.fileInput').each(function (i) {
        $(this).change(function (index) {
            return($(this).parent().find('.inputFileText').val($(this).val()));
        });
    });
}
//<= Button  uploadButton- get files name

function addNewRadio() {

    var newNumberRadio= $('input:radio').length + 1;
    $('#answers')
        .append('Answer-'+newNumberRadio)
        .append(': (correct ')
        .append($('<input>')
            .attr({type: 'radio', name: 'qs_correct', value: newNumberRadio})
    )
        .append(') ')
        .append($('<input>')
            .attr({type: 'text', name: 'av_text['+newNumberRadio+']', size: '50'})
    )
        .append('<br />')
    ;

}

function addNewCheckbox() {

    var newNumberCheckbox= $('#answers input:checkbox').length + 1;
    $('#answers')
        .append('Answer-'+newNumberCheckbox)
        .append(': (correct ')
        .append($('<input>')
            .attr({type: 'checkbox', name: 'qs_correct['+newNumberCheckbox+']', id: 'checkbox-id'+newNumberCheckbox+''})
        )
        .append($('<label>')
            .attr({for: 'checkbox-id'+newNumberCheckbox+''})
        )
        .append(') ')
        .append($('<input>')
            .attr({type: 'text', name: 'av_text['+newNumberCheckbox+']', size: '50'})
        )
        .append('<br />')
    ;
}

function addNewPicture() {

    var newNumberFile= $('input:file').length + 1;
    $('#pictures')
        .append('Picture-'+newNumberFile)
        .append(': ')
        .append($('<span>')
            .attr({class: 'inputFile'})
            .append($('<input>')
                .attr({type: 'text', class: 'inputFileText'})
        )
            .append($('<input>')
                .attr({type: 'file', class: 'fileInput', name: 'qs_name_in_db['+newNumberFile+']'})
        )
    )
        .append('<br />')
    ;
    showFileName();
}
