<div>
    <div class="color_block">
        {LABEL="step"} 2
        <br />
        {LABEL="select_rows_ in_table_below"}
    </div>
    <form action="index.php?page=question_manager&action=search&search_type=3{DV="get_string_t_repository"}" target="_self" method="post" name="questionManagerAction" enctype="multipart/form-data">
        <table id="managerTable" >
            <thead>
                <td class="name">{LABEL="question"}</td>
                <td class="test_repos">{LABEL="test_name"}</td>
                <td class="checkbox_in_table">{LABEL="check"}</td>
            </thead>
            <tbody id="testTBody">
                {FOREACH="question_array"}
                    <tr id = '{FR_ELEMENT="trg_id"}'>
                        <th class="name"><a href="index.php?page=question_setting&action=show&qs_id={FR_ELEMENT="qs_id"}" target="_blank">{FR_ELEMENT="qs_text"}</a></th>
                        <th class="test_repos"><a href="index.php?page=test_repository&action=modify&tr_id={FR_ELEMENT="tr_id"}" target="_blank">{FR_ELEMENT="tr_name"}</a></th>
                        <th class="checkbox_in_table">
                            <input type="checkbox" name="question[{FR_ELEMENT="trg_id"}]" id="question[{FR_ELEMENT="trg_id"}]" {FR_ELEMENT="question_checked"}/><label for="question[{FR_ELEMENT="trg_id"}]">&nbsp</label>
                        </th>
                    </tr>
                {FOREACH_END}
            </tbody>
        </table>
        {IF="empty_dir"}{==}{true}
            <span class="message_empty_table">{LABEL="the_table_is_empty"}<span><br/>
        {END_IF}
        <div class="color_block">
            {LABEL="step"} 3
            <br />
            {LABEL="do_action_with_selected_questions"}:
            <br/>
            <input type="radio" name="q_action" class="search_q_manager_action" value="1" {DV="checked_action_1"}/>{LABEL="copy_to_set"}<br />
            <input type="radio" name="q_action" class="search_q_manager_action" value="2" {DV="checked_action_2"}/>{LABEL="copy_to_test"}<br />
            <input type="radio" name="q_action" class="search_q_manager_action" value="3" {DV="checked_action_3"}/>{LABEL="move_to_set"}<br />
            <input type="radio" name="q_action" class="search_q_manager_action" value="4" {DV="checked_action_4"}/>{LABEL="move_to_test"}<br />
            <input type="radio" name="q_action" class="search_q_manager_action" value="5" {DV="checked_action_5"}/>{LABEL="delete_from_current_set"}<br />
            <br/>
            <div id="goal_q_repository">
                {LABEL="specify_the_set_which_will_be_field_by_questions"}:<br />
                <select name="q_repository_action" size="1" id="select_q_rep">
                    <option value='0' selected='selected' disabled='disabled'></option>
                    {FOREACH="question_repository_array_action"}
                    <option value='{FR_ELEMENT="qr_id"}' {FR_ELEMENT="selected_question_repository"}>{FR_ELEMENT="qr_open_value"}{FR_ELEMENT="qr_name"}</option>
                    {FOREACH_END}
                </select>
            </div>
            <div id="goal_t_repository">
                {LABEL="specify_the_test_which_will_be_field_by_questions"}:<br />
                <select name="t_repository_action" size="1" id="select_t_rep">
                    <option value='0' selected='selected' disabled='disabled'></option>
                    {FOREACH="test_repository_array_action"}
                    <option value='{FR_ELEMENT="tr_id"}' {FR_ELEMENT="selected_test_category"}>{FR_ELEMENT="tr_name"}</option>
                    {FOREACH_END}
                </select>
            </div>
            <input type="hidden" name="submit_pressed" value="1">
            <input type="image" src="img/buttonDo.jpg" id="copyFileButton"><br />
        </div>
    </form>
</div>