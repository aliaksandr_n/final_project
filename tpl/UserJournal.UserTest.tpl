<h4>
    {LABEL="answer_the_question"}
</h4>
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
<span>{LABEL="time_for_test_answer"}: {DV="tr_total_time_value"}.</span><br />
<span>{LABEL="time_for_question_answer"}: {DV="qs_answer_time_duration"}.</span><br />
<br />
<div>
    <form action="index.php?page=user_journal&test={DV="utj_id"}" target="_self" method="post" enctype="multipart/form-data">
        {FILE="tpl/UserJournal.UserTest.{DV="qs_question_type_id"}.tpl"}
        <br />
        <input type="hidden" name="action" value="answer">
        {IF="show_answer_button"}{==}{true}
            <input type="image" src="img/buttonAnswer.jpg">
        {END_IF}
    </form>
    {IF="show_skip_button"}{==}{true}
        <form action="index.php?page=user_journal&test={DV="utj_id"}" target="_self" method="post" enctype="multipart/form-data">
            <input type="hidden" name="action" value="skip">
            <input type="image" src="img/buttonSkip.jpg">
        </form>
    {END_IF}
    {IF="show_pause_button"}{==}{true}
        <form action="index.php?page=user_journal&test={DV="utj_id"}" target="_self" method="post" enctype="multipart/form-data">
            <input type="hidden" name="action" value="pause">
            <input type="image" src="img/buttonPause.jpg">
        </form>
    {END_IF}
    {IF="show_resume_button"}{==}{true}
    <form action="index.php?page=user_journal&test={DV="utj_id"}" target="_self" method="post" enctype="multipart/form-data">
        <input type="hidden" name="action" value="resume">
        <input type="image" src="img/buttonResume.jpg">
    </form>
    {END_IF}
</div>




