<h4>
    {LABEL="appointed_and_passed_tests"}
</h4>
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
<div>
    <table id="managerTable" >
        <thead>
            <td class="test_name">{LABEL="test"}</td>
            <td class="test_author">{LABEL="person_whom_was_test_appoinet"}</td>
            <td class="all_test_sum">{LABEL="questions_number_in_test"}</td>
            <td class="num_done_q">{LABEL="number_of_solved_questions"}</td>
            <td class="answered_test_sum">{LABEL="pause_ability"}</td>
            <td class="do_test">{LABEL="to_solve"}</td>
        </thead>
        <tbody id="user_test_TBody">
            {FOREACH="user_test__array"}
                <tr id = '{FR_ELEMENT="utj_id"}'>
                    <th class="test_name">{FR_ELEMENT="tr_name"}&nbsp;</th>
                    <th class="test_author">{FR_ELEMENT="ru_name"} {FR_ELEMENT="ru_surname"}&nbsp;</th>
                    <th class="all_test_sum">{FR_ELEMENT="all_test_sum"}&nbsp;</th>
                    <th class="answered_test_sum">{FR_ELEMENT="answered_test_sum"}&nbsp;</th>
                    <th class="stop_test_permission">{FR_ELEMENT="tr_pause_test_permission"}&nbsp;</th>
                    <th class="do_test"><a href="index.php?page=user_journal&test={FR_ELEMENT="utj_id"}">{LABEL="to_solve"}</a>&nbsp;</th>
                </tr>
            {FOREACH_END}
        </tbody>
    </table>
</div>
{IF="empty_dir"}{==}{true}
    <span class="message_empty_table">{LABEL="the_table_is_empty"}<span><br/>
{END_IF}