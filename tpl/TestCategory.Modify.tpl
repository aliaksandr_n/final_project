<div class="modify_form">
    <img src="img/rowToChild.jpg"/>
    <form class="modifyTestCategoryForm" action="index.php?page=test_category&action=modify" target="_self" method="post" enctype="multipart/form-data">
        <input type="text" name="tc_name" value='{DV="selectedModifiedTestCategory"}' />
        <select name="tc_parent_id" size="1">
            <option value='0'></option>
            {FOREACH="test_category_array"}
            <option value='{FR_ELEMENT="tc_id"}'
                    {IF="tc_parent_id"}{==}{{FR_ELEMENT="tc_id"}}
                    selected='selected'
                    {END_IF}
                    >
                {FR_ELEMENT="tc_open_value"}{FR_ELEMENT="tc_name"}</option>
            {FOREACH_END}
        </select>
        <input hidden='hidden' name='tc_id' value='{DV="tc_id"}'>
        <input type="image" src="img/buttonModify.jpg" id="copyFileButton">
    </form>
</div>