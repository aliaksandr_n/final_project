<div class="color_block">
    {LABEL="step"} 3
    <br />
    {LABEL="select_appointed_test"}:<br/>
    <select name="appointed_test" size="1" id="search_t_repository">
        <option value='0' selected='selected' disabled='disabled'></option>
        {FOREACH="test_repository_array"}
        <option value='{FR_ELEMENT="tr_id"}' {FR_ELEMENT="selected_test_repository"}>{FR_ELEMENT="tr_name"}</option>
        {FOREACH_END}
    </select>
    <br />
    <br />
    <input type="hidden" name="submit_pressed" value="1">
    <input type="image" src="img/buttonFilter.jpg" id="copyFileButton">
</div>