<h4>
    {LABEL="tests"}
</h4>
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
{IF="include_modify_form"}{==}{true}
    {FILE="tpl/TestRepository.Modify.tpl"}
{END_IF}
{IF="include_search_form"}{==}{true}
    <div class="color_block">
        <a href="index.php?page=test_repository&action=open-test-form">{LABEL="add_test"}</a>
    </div>
    {FILE="tpl/TestRepository.Search.tpl"}
{END_IF}
    {IF="include_list_form"}{==}{true}
    {FILE="tpl/TestRepository.List.tpl"}
{END_IF}
