<div>
    <form action="index.php?page=appointed_test&action=search&search_type=1{DV="get_user_group"}" target="_self" method="post" name="appointedTestAction" enctype="multipart/form-data">
        <div class="color_block">
            {LABEL="step"} 2
            <br />
            {LABEL="select_rows_ in_table_below"}
        </div>
        <table id="managerTable">
            <thead>
                <td class="name_surname">{LABEL="user"}</td>
                <td class="checkbox_in_table">{LABEL="check"}</td>
            </thead>
            <tbody id="user_group_TBody">
                {FOREACH="user_array"}
                    <tr id = '{FR_ELEMENT="ru_id"}'>
                        <th class="name">{FR_ELEMENT="ru_name"}&nbsp;{FR_ELEMENT="ru_surname"}</th>
                        <th class="checkbox_in_table">
                            <input type="checkbox" name="user[{FR_ELEMENT="ru_id"}]" id="user[{FR_ELEMENT="ru_id"}]" {FR_ELEMENT="user_checked"}/><label for="user[{FR_ELEMENT="ru_id"}]">&nbsp</label>
                        </th>
                    </tr>
                {FOREACH_END}
            </tbody>
        </table>
        {IF="empty_dir"}{==}{true}
            <span class="message_empty_table">{LABEL="the_table_is_empty"}<span><br/>
        {END_IF}
        <br />
        {FILE="tpl/AppointedTest.DoAppointment.tpl"}
    </form>
</div>