<h4>
    {LABEL="questions"}
</h4>
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
{IF="include_search_panel"}{==}{true}
    <div class="color_block">
        <a href="index.php?page=question_setting&action=add-or-modify">{LABEL="add_question"}</a>
    </div>
    {FILE="tpl/QuestionSetting.SearchPanel.tpl"}
{END_IF}
{IF="include_search_result"}{==}{true}
    {FILE="tpl/QuestionSetting.SearchResult.tpl"}
{END_IF}
{IF="include_question_table"}{==}{true}
    {FILE="tpl/QuestionSetting.Add.tpl"}
{END_IF}
