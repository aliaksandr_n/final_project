<div>
    <table id="testTable" >
        <thead>
            <tr class="first_row">
                <td class="name" rowspan="2">{LABEL="test"}</td>
                <td class="adding_time" rowspan="2">{LABEL="adding_time"}</td>
                <td class="сategory">{LABEL="test_category"}</td>
                <td class="stop_test_permission">{LABEL="allowed_to_stop_test"}</td>
                <td class="show_right_answer">{LABEL="showing_right_answer"}</td>
                <td class="show_final_mark">{LABEL="showing_final_mark"}</td>
                <td class="show_final_verdict">{LABEL="showing_final_verdict"}</td>
                <td class="number_shown_questions">{LABEL="number_questions_in_test"}</td>
                <td class="skip_questions">{LABEL="ability_to_skip_question"}</td>
            </tr>
            <tr class="second_row">
                <td class="success_value_type">{LABEL="success_value_type"}</td>
                <td class="success_value_num">{LABEL="value_to_pass"}</td>
                <td class="control_test_time_per_questions">{LABEL="control_test_time_per_questions"}</td>
                <td class="total_time_value">{LABEL="time_per_test"}</td>
                <td class="author_id">{LABEL="author"}</td>
                <td class="modifyCol">{LABEL="modify"}</td>
                <td class="delCol">{LABEL="delete"}</td>
            </tr>
        </thead>
        <tbody id="testTBody">
        {FOREACH="testsArray"}
            <tr id = '{FR_ELEMENT="tr_id"}' class="first_row">
                <th class="name" rowspan="2">{FR_ELEMENT="tr_name"}&nbsp;</th>
                <th class="adding_time" rowspan="2">{FR_ELEMENT="tr_adding_time"}&nbsp;</th>
                <th class="сategory">{FR_ELEMENT="tc_name"}&nbsp;</th>
                <th class="stop_test_permission">{FR_ELEMENT="tr_pause_test_permission"}&nbsp;</th>
                <th class="show_right_answer">{FR_ELEMENT="tr_show_right_answer"}&nbsp;</th>
                <th class="show_final_mark">{FR_ELEMENT="tr_show_final_mark"}&nbsp;</th>
                <th class="show_final_verdict">{FR_ELEMENT="tr_show_final_verdict"}&nbsp;</th>
                <th class="number_shown_questions">{FR_ELEMENT="tr_number_shown_questions"}&nbsp;</th>
                <th class="skip_questions">{FR_ELEMENT="tr_skip_questions"}&nbsp;</th>
            </tr>
            <tr id = '{FR_ELEMENT="tr_id"}' class="second_row">
                <th class="success_value_type">{FR_ELEMENT="tr_success_value_type"}&nbsp;</th>
                <th class="success_value_num">{FR_ELEMENT="tr_success_value_num"}&nbsp;</th>
                <th class="control_test_time_per_questions">{FR_ELEMENT="tr_control_test_time_per_questions"}&nbsp;</th>
                <th class="total_time_value">{FR_ELEMENT="tr_total_time_value"}&nbsp;</th>
                <th class="author_id">{FR_ELEMENT="tr_author_id"}&nbsp;</th>
                <th class="modifyCol"><a href="index.php?page=test_repository&action=modify&tr_id={FR_ELEMENT="tr_id"}">{LABEL="modify"}</a></th>
                <th class="delCol"><a href="index.php?page=test_repository&action=delete&tr_id={FR_ELEMENT="tr_id"}">{LABEL="delete"}</a></th>
            </tr>
        {FOREACH_END}
        </tbody>
    </table>
</div>