<h4>
    {LABEL="question_set"}
</h4>
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
<div class="color_block">
    <form action="index.php?page=question_repository&action=add" target="_self" method="post" enctype="multipart/form-data">
        {LABEL="adding"}<br />
        <input type="text" name="qr_name" id="url"/>
        <select name="qr_parent_id" size="1">
            <option value='0' selected='selected'></option>
            {FOREACH="question_repository_array"}
            <option value='{FR_ELEMENT="qr_id"}'>{FR_ELEMENT="qr_open_value"}{FR_ELEMENT="qr_name"}</option>
            {FOREACH_END}
        </select>
        <input type="image" src="img/buttonAdd.jpg" id="copyFileButton"><br />
    </form>
</div>
<div>
    {FOREACH="question_repository_array"}
        {FR_ELEMENT="qr_open_value"}{FR_ELEMENT="qr_name"}
        || <a href='index.php?page=question_repository&action=delete&qr_id={FR_ELEMENT="qr_id"}'>{LABEL="delete"}</a>
        || <a href='index.php?page=question_repository&action=modify_tlp&qr_id={FR_ELEMENT="qr_id"}'>{LABEL="modify"}</a>
        {IF="question_repository_action"}{==}{{FR_ELEMENT="qr_id"}}
            <br />{FILE="tpl/QuestionRepository.Modify.tpl"}
        {END_IF}<br />
    {FOREACH_END}
</div>