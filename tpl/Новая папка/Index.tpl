<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <script src="js/jquery-2.1.3.js" type="text/javascript"></script>
    <script src="js/functions.js" type="text/javascript"></script>
</head>
<body>
    <div id="wrapper">
        <div id="header">
            <div id="change_lang_ref">
                {LABEL="change_language"}
                <a href="index.php?page=language&lang=ru">RU</a>
                <a href="index.php?page=language&lang=eng">ENG</a>
            </div>
            <div>
                <h3>
                    {LABEL="page_head_information"}
                </h3>
            </div>
        </div>
        <div id="navigation">
            {FILE="tpl/Login.{DV="log_in"}.tpl"}
        </div>
        <div id="main">
            <div id="left">
                {FILE="tpl/Menu.tpl"}
            </div>
            <div id="content">
                {FILE="tpl/{DV="page"}.tpl"}
            </div>
            <div id="right">

            </div>
        </div>
        <div id="clear">
        </div>
        <div id="footer">

        </div>
    </div>
</body>
</html>