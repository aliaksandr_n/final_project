<div>
    <table id="test_journal_table" >
        <thead>
            <td class="test_name">{LABEL="test"}</td>
            <td class="question_list">{LABEL="question_list"}</td>
            <td class="date_of_appointment">{LABEL="date_of_appointment"}</td>
            <td class="trainer">{LABEL="trainer"}</td>
            <td class="number_users">{LABEL="number_users"}</td>
            <td class="finished_users">{LABEL="finished_users"}</td>
            <td class="more_info">{LABEL="more_info"}</td>
        </thead>
        <tbody id="answerTBody">
        {FOREACH="groups_stat_array"}
        <tr id = '{FR_ELEMENT="atid"}'>
            <th class="test_name">{FR_ELEMENT="tr_name"}</th>
            <th class="question_list"><a target="_blank" href="index.php?page=question_manager&action=search&search_type=3&t_repository={FR_ELEMENT="tr_id"}">{LABEL="question_list"}</a></th>
            <th class="date_of_appointment">{FR_ELEMENT="at_creating_time"}</th>
            <th class="trainer">{FR_ELEMENT="ru_name"} {FR_ELEMENT="ru_surname"}</th>
            <th class="number_users">{FR_ELEMENT="users_num"}</th>
            <th class="finished_users">{FR_ELEMENT="finished_users"}</th>
            <th class="more_info"><a href="index.php?page=test_journal&at_id={FR_ELEMENT="atid"}">{LABEL="more_info"}</a></th>
        </tr>
        {FOREACH_END}
        </tbody>
    </table>
</div>