<div>
    {IF="login_status"}{==}{log_out}
        <span class="login_message">{LABEL="you_have_left_the_system"}</span><br />
    {END_IF}
    {IF="login_status"}{==}{login_failed}
        <span class="login_message">{LABEL="unable_to_enter"}</span><br />
    {END_IF}
    {IF="login_status"}{<>}{accepted}
        <div class="contentChildBlock">
            <form action="index.php?page=login" method="post">
                <div class="rowAuth">
                    <div class="auth">
                        {LABEL="login"}&nbsp
                    </div>
                    <input type="text" name="login" value="{DV="user_login"}" /> user1
                </div>
                <div class="rowAuth">
                    <div class="auth">
                        {LABEL="password"}&nbsp
                    </div>
                    <input type="password" name="pass" /> password1
                </div>
                <div class="rowAuth">
                    <div class="auth">
                        {LABEL="remember"}&nbsp
                    </div>
                    <input type="checkbox" name="remember_me" id="checkbox-id" {DV="checked_button"} />
                    <label for="checkbox-id">&nbsp</label>
                </div>
                <div class="rowAuth">
                    <div class="auth">
                        &nbsp
                    </div>
                    <input type="image" src="img/buttonLogin.jpg">
                </div>
                <div class="rowAuth">
                    <div class="auth">
                        &nbsp
                    </div>
                    <a href="index.php?page=registration">{LABEL="registration"}</a>
                    <a href="index.php?page=forget_pass">{LABEL="forget_password"}?</a>
                </div>
            </form>
        </div>
    {END_IF}
    {IF="login_status"}{==}{accepted}
        {LABEL="welcome_to_the_site"}, {DV="user_name"} {DV="user_surname"}.
    {END_IF}
</div>