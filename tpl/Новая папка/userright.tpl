<h4>
    {LABEL="user_rights_management"}
</h4>
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
<form action="index.php?page=user_right" target="_self" method="post" enctype="multipart/form-data">
    <table id="testTable" >
        <thead>
            <td class="user_name">{LABEL="user"}</td>
            <td class="trainer_right">{LABEL="trainer"}</td>
            <td class="manager_right">{LABEL="manager"}</td>
            <td class="admin_right">{LABEL="administrator"}</td>
        </thead>
        <tbody id="testTBody">
        {FOREACH="user_right_array"}
            <tr id = '{FR_ELEMENT="ru_id"}'>
                <th class="user_name">
                    {FR_ELEMENT="ru_name"}&nbsp;{FR_ELEMENT="ru_surname"}
                </th>
                <th class="trainer_right">
                    <input type="checkbox" name="trainer_right[{FR_ELEMENT="ru_id"}]" {FR_ELEMENT="trainer_right_checked"} id="trainer_right[{FR_ELEMENT="ru_id"}]"/><label for="trainer_right[{FR_ELEMENT="ru_id"}]">&nbsp</label>
                </th>
                <th class="manager_right">
                    <input type="checkbox" name="manager_right[{FR_ELEMENT="ru_id"}]" {FR_ELEMENT="manager_right_checked"} id="manager_right[{FR_ELEMENT="ru_id"}]"/><label for="manager_right[{FR_ELEMENT="ru_id"}]">&nbsp</label>
                </th>
                <th class="admin_right">
                    <input type="checkbox" name="admin_right[{FR_ELEMENT="ru_id"}]" {FR_ELEMENT="admin_right_checked"} id="admin_right[{FR_ELEMENT="ru_id"}]"/><label for="admin_right[{FR_ELEMENT="ru_id"}]">&nbsp</label>
                </th>
            </tr>
        <input type="hidden" name="all_users[{FR_ELEMENT="ru_id"}]" value="on">
        {FOREACH_END}
        </tbody>
    </table>
    <input type="hidden" name="action" value="change">
    <input type="image" src="img/buttonSave.jpg"><br />
</form>
<br />
*{LABEL="all_registered_users_have_student_right"}
