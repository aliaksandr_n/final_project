<div class="modify_form">
    <img src="img/rowToChild.jpg"/>
    <form class="modifyQuestionRepositoryForm" action="index.php?page=question_repository&action=modify" target="_self" method="post" enctype="multipart/form-data">
        <input type="text" name="qr_name" value='{DV="selectedModifiedQuestionRepository"}' />
        <select name="qr_parent_id" size="1">
            <option value='0'></option>
            {FOREACH="question_repository_array"}
            <option value='{FR_ELEMENT="qr_id"}'
                    {IF="qr_parent_id"}{==}{{FR_ELEMENT="qr_id"}}
                    selected='selected'
                    {END_IF}
                    >
                {FR_ELEMENT="qr_open_value"}{FR_ELEMENT="qr_name"}</option>
            {FOREACH_END}
        </select>
        <input hidden='hidden' name='qr_id' value='{DV="qr_id"}'>
        <input type="image" src="img/buttonModify.jpg" id="copyFileButton">
    </form>
</div>