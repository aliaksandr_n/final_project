<div class="color_block">
    <form action="index.php?page=question_setting&action=search" target="_self" method="get" enctype="multipart/form-data">
        {LABEL="select_question_by_category"}<br />
        <input type="hidden" name="page" value="question_setting">
        <input type="hidden" name="action" value="search">
        <select name="qCategory" size="1">
            <option value='0' selected='selected'>{LABEL="all"}</option>
            {FOREACH="question_category_array"}
                <option value='{FR_ELEMENT="qc_id"}' {FR_ELEMENT="selected_question_category"}>{FR_ELEMENT="qc_open_value"}{FR_ELEMENT="qc_name"}</option>
            {FOREACH_END}
        </select>
        <input type="image" src="img/buttonFilter.jpg" id="copyFileButton"><br />
    </form>
</div>
