<div>
    <form action="index.php?page=appointed_test&action=search&search_type=3{DV="get_key_number"}" target="_self" method="post" name="appointedTestAction" enctype="multipart/form-data">
        <div class="color_block">
            {LABEL="step"} 2
            <br />
            {LABEL="select_rows_ in_table_below"}
        </div>
        <table id="managerTable">
            <thead>
            <td class="tu_comment">{LABEL="comment_not_necessary"}</td>
            <td class="tu_key">{LABEL="key_can_be_modified"}</td>
            <td class="checkbox_in_table">{LABEL="check"}</td>
            </thead>
            <tbody id="ugroup_member_TBody">
            {FOREACH="temp_key_array"}
                <tr id = '{FR_ELEMENT="key_index"}'>
                    <th class="comment">
                        <input type="text" value="{FR_ELEMENT="key_comment"}" name="temp_key_comment[{FR_ELEMENT="key_index"}]" id="temp_key_comment[{FR_ELEMENT="key_index"}]" size="80"/>
                    </th>
                    <th class="key">
                        <input type="text" value="{FR_ELEMENT="key_name"}" name="temp_key_name[{FR_ELEMENT="key_index"}]" id="temp_key_name[{FR_ELEMENT="key_index"}]" size="47"/>
                    </th>
                    <th class="checkbox_in_table">
                        <input type="checkbox" name="temp_key_checked[{FR_ELEMENT="key_index"}]" id="temp_key_checked[{FR_ELEMENT="key_index"}]" {FR_ELEMENT="key_checked"}/><label for="temp_key_checked[{FR_ELEMENT="key_index"}]">&nbsp</label>
                    </th>
                </tr>
            {FOREACH_END}
            </tbody>
        </table>
        {IF="empty_dir"}{==}{true}
            <span class="message_empty_table">{LABEL="the_table_is_empty"}<span><br/>
        {END_IF}
        <br />
        {FILE="tpl/AppointedTest.DoAppointment.tpl"}
        <input type="hidden" name="key_number" value="{DV="key_number"}">
    </form>
</div>