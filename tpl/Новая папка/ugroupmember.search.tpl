<div class="color_block">
    {LABEL="step"} 1<br />
    <form action="index.php?page=ugroup_member&action=search" target="_self" method="get" enctype="multipart/form-data" id="form_ugroup_search_type">
        <input type="hidden" name="page" value="ugroup_member">
        <input type="hidden" name="action" value="search">
        <div id="search_type_block">
            {LABEL="select_the_source_user_search"}:<br />
            <input type="radio" name="search_type" class="search_ugroup_member_type" value="1" {DV="checked_1_search_type"}/>{LABEL="all_system_users"}<br />
            <input type="radio" name="search_type" class="search_ugroup_member_type" value="2" {DV="checked_2_search_type"}/>{LABEL="user_added_to_any_group"}
        </div>
        <div id="search_ugroup_member_block">
            {LABEL="additional_selection_for_the_group"}:
            <select name="user_group" size="1" id="search_ugroup_member">
                <option value='0' selected='selected'></option>
                {FOREACH="user_group_array_search"}
                <option value='{FR_ELEMENT="ug_id"}' {FR_ELEMENT="selected_user_group"}>{FR_ELEMENT="ug_name"}</option>
                {FOREACH_END}
            </select>
        </div>
        <input type="image" src="img/buttonFilter.jpg" id="copyFileButton"><br />
    </form>
</div>
