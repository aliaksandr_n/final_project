<h4>
    {LABEL="question_management"}
</h4>
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
{FILE="tpl/QuestionManager.Search.tpl"}
{IF="include_result_form"}{==}{true}
    {FILE="tpl/QuestionManager.{DV="search_form_type"}.tpl"}
{END_IF}
