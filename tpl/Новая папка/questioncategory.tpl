<h4>
    {LABEL="question_categoies"}
</h4>
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
<div class="color_block">
    <form action="index.php?page=question_category&action=add" target="_self" method="post" enctype="multipart/form-data">
        {LABEL="adding"}<br />
        <input type="text" name="qc_name"/>
        <select name="qc_parent_id" size="1">
            <option value='0' selected='selected'></option>
            {FOREACH="question_category_array"}
            <option value='{FR_ELEMENT="qc_id"}'>{FR_ELEMENT="qc_open_value"}{FR_ELEMENT="qc_name"}</option>
            {FOREACH_END}
        </select>
        <input type="image" src="img/buttonAdd.jpg" id="copyFileButton"><br />
    </form>
</div>
<div>
    {FOREACH="question_category_array"}
        {FR_ELEMENT="qc_open_value"}{FR_ELEMENT="qc_name"}
        || <a href='index.php?page=question_category&action=delete&qc_id={FR_ELEMENT="qc_id"}'>{LABEL="delete"}</a>
        || <a href='index.php?page=question_category&action=modify_tlp&qc_id={FR_ELEMENT="qc_id"}'>{LABEL="modify"}</a>
        {IF="question_category_action"}{==}{{FR_ELEMENT="qc_id"}}
            {FILE="tpl/QuestionCategory.Modify.tpl"}
        {END_IF}<br />
    {FOREACH_END}
</div>