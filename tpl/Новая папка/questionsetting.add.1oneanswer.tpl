<div>
    <form enctype="multipart/form-data" action="index.php?page=question_setting&action=add-or-modify" method="post" name="questionForm">
        <br />
        {LABEL="eng_question"}: <textarea name="qs_text" cols="30" rows="7">{DV="qs_text"}</textarea><br />
        <div id="pictures">
            {FOREACH="pictures_array"}
            {LABEL="eng_picture"}-{FR_ELEMENT="qp_number_in_question"}: <span class="inputFile">
                    <input type="text" class="inputFileText"/>
                    <input type="file" class="fileInput" name="qs_name_in_db[{FR_ELEMENT="qp_number_in_question"}]"/>
                    </span><br />
            {FOREACH_END}
        </div>
        <span id="addNewPictureInput">{LABEL="eng_click_to_add_another_picture"}</span><br />
        <div id="answers">
            {FOREACH="radio_button_array"}
                {LABEL="eng_answer"}-{FR_ELEMENT="av_order_value"}: ({LABEL="eng_correct"} <input type="radio" name="qs_correct" {FR_ELEMENT="check_correct"} value="{FR_ELEMENT="av_order_value"}">) <input type="text" name="av_text[{FR_ELEMENT="av_order_value"}]" size="50" value="{FR_ELEMENT="answer_text"}"><br />
            {FOREACH_END}
        </div>
        <span id="addNewRadioLable">{LABEL="eng_click_to_add_another_answer"}</span><br /><br />

        {LABEL="eng_question_weight"}: <input type="text" name="qs_weight"  value="{DV="qs_weight"}"/><br />
        {LABEL="eng_answer_time_duration"}: <input type="text" name="qs_answer_time_duration"  value="{DV="qs_answer_time_duration"}"/><br />
        {LABEL="eng_show_random_question"}: <input type="checkbox"  name="qs_show_random_answers" id="checkbox-id" {DV="qs_show_random_answers"}/>
        <label for="checkbox-id">&nbsp</label><br />
        {LABEL="eng_select_category"}:
        <select name="qCategory" size="1">
            <option value='0' selected="selected" disabled="disabled"></option>
            {FOREACH="question_category_array"}
                <option value='{FR_ELEMENT="qc_id"}' {FR_ELEMENT="selected_question_category"}>{FR_ELEMENT="qc_open_value"}{FR_ELEMENT="qc_name"}</option>
            {FOREACH_END}
        </select><br />
        <input type="hidden" name="button_pressed" value="1">
        <input type="hidden" name="qt_id" value="{DV="question_type"}">
        <input type="hidden" name="qs_id" value="{DV="qs_id"}">
        <input type="hidden" name="first_action" value="{DV="first_action"}">
        <input type="image" src="img/buttonSave.jpg">
    </form>
</div>