<div>
    <h4>
        {LABEL="password_recovery"}
    </h4>
    {FOREACH="actionResultMessage"}
        <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
    {FOREACH_END}
    {IF="email_form"}{==}{-1}
        <form action="index.php?page=forget_pass" method="post" target="_self" name="registration">
            <div>
                <label>{LABEL="enter_your_email"}: <span class="redStar" title="{LABEL="necessarily_for_field"}">*</span>:<br />
                    <input type="text" size="23" name="email" value="{DV="email"}"/>
                </label>
            </div>
            <br />
            <input type="hidden" name="submit_pressed" value="1"/>
            <input type="image" src="img/buttonRecover.jpg">
        </form>
    {END_IF}
</div