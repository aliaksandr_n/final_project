<div>
    <table id="test_journal_table" >
        <thead>
            <tr class="first_row">
                <td class="user_or_key_number">{LABEL="user_or_key"}</td>
                <td class="is_finished">{LABEL="is_finished"}</td>
                <td class="overtimed">{LABEL="overtimed"}</td>
                <td class="correct_answers">{LABEL="correct_answers"}</td>
                <td class="finish_date">{LABEL="finish_date"}</td>
                <td class="final_mark" rowspan="2">{LABEL="final_mark"}</td>
            </tr>
            <tr class="second_row">
                <td class="key_comments">{LABEL="key_comments"}</td>
                <td class="question_number">{LABEL="question_number"}</td>
                <td class="answered">{LABEL="answered"}</td>
                <td class="final_verdict">{LABEL="final_verdict"}</td>
                <td class="answering_duration" >{LABEL="answering_duration"}</td>
            </tr>
        </thead>
        <tbody id="answerTBody">
        {FOREACH="one_appointment_stat_array"}
        <tr id = '{FR_ELEMENT="utj_appointed_testid"}' class="first_row">
            <th class="user_or_key_number">{FR_ELEMENT="user_or_key_number"}</th>
            <th class="is_finished">{FR_ELEMENT="utj_is_finished"}</th>
            <th class="overtimed">{FR_ELEMENT="overtimed"}</th>
            <th class="correct_answers">{FR_ELEMENT="correct_answers"}</th>
            <th class="finish_date">{FR_ELEMENT="utj_finish_date"}</th>
            <th class="final_mark" rowspan="2">{FR_ELEMENT="utj_final_mark"}</th>
        </tr>
        <tr class="second_row">
            <th class="key_comments">{FR_ELEMENT="tu_comment"}</th>
            <th class="question_number">{FR_ELEMENT="question_number"}</th>
            <th class="answered">{FR_ELEMENT="answered"}</th>
            <th class="final_verdict">{FR_ELEMENT="utj_final_verdict"}</th>
            <th class="answering_duration">{FR_ELEMENT="utj_time_duration"}</th>
        </tr>
        {FOREACH_END}
        </tbody>
    </table>
</div>