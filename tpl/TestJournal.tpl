<h4>
    {LABEL="appointed_tests_journal"}
</h4>
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
{IF="include_appointments"}{==}{true}
    {FILE="tpl/TestJournal.Appointments.tpl"}
{END_IF}
{IF="include_appointed_group_stat"}{==}{true}
    {FILE="tpl/TestJournal.AppointedGroupStat.tpl"}
{END_IF}
