<div>
    <h4>
        {LABEL="registration_on_site"}
    </h4>
    {FOREACH="actionResultMessage"}
        <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
    {FOREACH_END}
    <br />
    {IF="registered"}{==}{-1}
        <form action="index.php?page=registration" method="post" target="_self" name="registration">
            <div>
                <label>{LABEL="login_min_symbol_description"}<span class="redStar" title="{LABEL="necessarily_for_field"}">*</span>:<br />
                    <input type="text" size="23" name="login" value="{DV="login"}"/>
                </label>
            </div>
            <div>
                <label>{LABEL="password_max_symbol"}<span class="redStar" title="{LABEL="necessarily_for_field"}">*</span>:<br />
                    <input type="password" size="23" name="password" value="{DV="password"}"/>
                </label>
            </div>
            <div>
                <label>{LABEL="password_again"}<span class="redStar" title="{LABEL="necessarily_for_field"}">*</span>:<br />
                    <input type="password" size="23" name="password2" value="{DV="password2"}"/>
                </label>
            </div>
            <div>
                <label>{LABEL="email"}<span class="redStar" title="{LABEL="necessarily_for_field"}">*</span>:<br />
                    <input type="text" size="23" name="email" value="{DV="email"}"/>
                </label>
            </div>
            <div>
                <label>{LABEL="name"}<span class="redStar" title="{LABEL="necessarily_for_field"}">*</span>:<br />
                    <input type="text" size="23" name="name" value="{DV="name"}"/>
                </label>
            </div>
            <div>
                <label>{LABEL="surname"}<span class="redStar" title="{LABEL="necessarily_for_field"}">*</span>:<br />
                    <input type="text" size="23" name="surname" value="{DV="surname"}"/>
                </label>
            </div>
            <div>
                <label>{LABEL="group"}<span class="redStar" title="{LABEL="necessarily_for_field"}">*</span>:<br />
                <select name="user_group" size="1">
                    <option value='0' selected='selected' disabled="disabled"></option>
                    {FOREACH="user_group_array"}
                    <option value='{FR_ELEMENT="ug_id"}' {FR_ELEMENT="selected_group"}>{FR_ELEMENT="ug_name"}</option>
                    {FOREACH_END}
                </select>
                </label>
            </div>
            <input type="hidden" name="submit_pressed" value="1"/>
            <input type="image" src="img/buttonRegistraition.jpg">
        </form>
    {END_IF}
</div