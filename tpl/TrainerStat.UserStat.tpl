<div>
    <table id="trainer_stat_table" >
        <thead>
        <td class="user_name">{LABEL="name"} {LABEL="surname"}</td>
        <td class="tests_appointed">{LABEL="tests_appointed"}</td>
        <td class="passed_test">{LABEL="passed_test"}</td>
        <td class="number_of_finished_successfully">{LABEL="number_of_finished_successfully"}</td>
        </thead>
        <tbody id="answerTBody">
        {FOREACH="groups_stat_array"}
        <tr id = '{FR_ELEMENT="ruid"}'>
            <th class="user_name">{FR_ELEMENT="ru_name"} {FR_ELEMENT="ru_surname"}</th>
            <th class="tests_appointed">{FR_ELEMENT="tests_appointed"}</th>
            <th class="passed_test">{FR_ELEMENT="passed_test"}</th>
            <th class="number_of_finished_successfully">{FR_ELEMENT="number_of_finished_successfully"}</th>
        </tr>
        {FOREACH_END}
        </tbody>
    </table>
</div>