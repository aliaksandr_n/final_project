<div>
    <h4>
        {LABEL="pass_changing"}
    </h4>
    {FOREACH="actionResultMessage"}
        <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
    {FOREACH_END}
    {IF="new_password_form"}{==}{1}
        <form action="index.php?page=recovery_pass&key={DV="key"}" method="post" target="_self" name="registration">
            <div>{LABEL="enter_your_new_pass"}</div>
            <div>
                <label>{LABEL="password_max_symbol"}<span class="redStar" title="{LABEL="necessarily_for_field"}">*</span>:<br />
                    <input type="password" size="23" name="password" value="{DV="password"}"/>
                </label>
            </div>
            <div>
                <label>{LABEL="password_again"}<span class="redStar" title="{LABEL="necessarily_for_field"}">*</span>:<br />
                    <input type="password" size="23" name="password2" value="{DV="password2"}"/>
                </label>
            </div>
            <br />
            <input type="hidden" name="key" value="{DV="key"}" />
            <input type="hidden" name="submit_pressed" value="1"/>
            <input type="image" src="img/buttonSave.jpg">
        </form>
    {END_IF}
</div