<h4>
    {LABEL="answer_question_results"}
</h4>
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
<p>{LABEL="question"}: {DV="question"}</p>
{FOREACH="pictures_array"}
<img class="view_q_picture" src="question_file_dir/question_id_{FR_ELEMENT="qp_question_setting_id"}/{FR_ELEMENT="qp_name_in_db"}"/>
<br />
{FOREACH_END}
<br />
<table id="user_answer_Table" >
    <thead>
        <td class="question">{LABEL="question"}</td>
        <td class="answer">{LABEL="answers_selected_by_the_user"}</td>
    </thead>
    <tbody id="answerTBody">
        {FOREACH="shown_answers_array"}
            <tr>
                <th class="question"><span class="{FR_ELEMENT="answer_class"}">{FR_ELEMENT="av_text"}</span></th>
                <th class="answer"><span class="user_answer">{FR_ELEMENT="user_answer"}</span></th>
            </tr>
        {FOREACH_END}
    </tbody>
</table>
<p>{LABEL="your_question_answer_result"} <span class="{DV="answer_status_class"}">{DV="right_answer"}</span>.</p>
<form action="index.php?page=user_journal&test={DV="utj_id"}" target="_self" method="post" enctype="multipart/form-data">
    <input type="image" src="img/buttonResume.jpg">
</form>
