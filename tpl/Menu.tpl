<div class="menu_block">
    <a href='index.php'>{LABEL="main"}</a>
</div>
{IF="show_admin_segment"}{==}{true}
    <div class="menu_block">
        <h4>
            {LABEL="menu_admin_block_name"}
        </h4>
        <a href='index.php?page=user_right'>{LABEL="menu_section_user_right"}</a>
    </div>
{END_IF}
{IF="show_manager_segment"}{==}{true}
    <div class="menu_block">
        <h4>
            {LABEL="menu_manager_block_name"}
        </h4>
        <a href='index.php?page=question_category'>{LABEL="menu_section_question_category"}</a><br />
        <a href='index.php?page=question_repository'>{LABEL="menu_section_question_repository"}</a><br />
        <a href='index.php?page=test_category'>{LABEL="menu_section_test_category"}</a><br />
        <a href='index.php?page=question_setting'>{LABEL="menu_section_question_setting"}</a><br />
        <a href='index.php?page=test_repository'>{LABEL="menu_section_test_repository"}</a><br />
        <a href='index.php?page=question_manager'>{LABEL="menu_section_question_manager"}</a>
    </div>
{END_IF}
{IF="show_trainer_segment"}{==}{true}
    <div class="menu_block">
        <h4>
            {LABEL="menu_trainer_block_name"}
        </h4>
        <a href='index.php?page=temp_user'>{LABEL="menu_section_temp_user"}</a><br />
        <a href='index.php?page=user_group'>{LABEL="menu_section_user_group"}</a><br />
        <a href='index.php?page=ugroup_member'>{LABEL="menu_section_ugroup_member"}</a><br />
        <a href='index.php?page=appointed_test'>{LABEL="menu_section_appointed_test"}</a><br />
        <a href='index.php?page=test_journal'>{LABEL="menu_section_test_journal"}</a>
        <a href='index.php?page=trainer_stat'>{LABEL="menu_section_trainer_stat"}</a>
    </div>
{END_IF}
{IF="show_user_segment"}{==}{true}
    <div class="menu_block">
        <h4>
            {LABEL="menu_user_block_name"}
        </h4>
        <a href='index.php?page=user_journal'>{LABEL="menu_section_user_journal"}</a>
    </div>
{END_IF}
{IF="show_user_segment"}{<>}{true}
    <div class="menu_block">
        <h4>
            {LABEL="menu_guest_block_name"}
        </h4>
            <a href='index.php?page=user_journal&action=new_test'>{LABEL="menu_section_user_journal_guest"}</a>
    </div>
{END_IF}