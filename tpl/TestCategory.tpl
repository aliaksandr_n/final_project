<h4>
    {LABEL="test_categories"}
</h4>
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
<div class="color_block">
    <form action="index.php?page=test_category&action=add" target="_self" method="post" enctype="multipart/form-data">
        {LABEL="adding"}<br />
        <input type="text" name="tc_name"/>
        <select name="tc_parent_id" size="1">
            <option value='0' selected='selected'></option>
            {FOREACH="test_category_array"}
            <option value='{FR_ELEMENT="tc_id"}'>{FR_ELEMENT="tc_open_value"}{FR_ELEMENT="tc_name"}</option>
            {FOREACH_END}
        </select>
        <input type="image" src="img/buttonAdd.jpg" id="copyFileButton"><br />
    </form>
</div>
<div>
    {FOREACH="test_category_array"}
        {FR_ELEMENT="tc_open_value"}{FR_ELEMENT="tc_name"}
        || <a href='index.php?page=test_category&action=delete&tc_id={FR_ELEMENT="tc_id"}'>{LABEL="delete"}</a>
        || <a href='index.php?page=test_category&action=modify_tlp&tc_id={FR_ELEMENT="tc_id"}'>{LABEL="modify"}</a>
        {IF="test_category_action"}{==}{{FR_ELEMENT="tc_id"}}
            <br />{FILE="tpl/TestCategory.Modify.tpl"}
        {END_IF}<br />
    {FOREACH_END}
</div>