<h4>
    {LABEL="appoint_test_to_users"}
</h4>
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
{FILE="tpl/AppointedTest.SearchUser.tpl"}
{IF="include_result_form"}{==}{true}
    {FILE="tpl/AppointedTest.{DV="search_form_type"}.tpl"}
{END_IF}
