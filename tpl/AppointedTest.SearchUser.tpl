<div class="color_block">
    {LABEL="step"} 1<br />
    <form action="index.php?page=appointed_test&action=search" target="_self" method="get" enctype="multipart/form-data" id="form_user_search_type">
        <input type="hidden" name="page" value="appointed_test">
        <input type="hidden" name="action" value="search">
        <div id="search_type_block">
            {LABEL="select_the_source_user_search"}:<br />
            <input type="radio" name="search_type" class="search_appointed_test_type" value="1" {DV="checked_1_search_type"}/>{LABEL="all_system_users"}<br />
            <input type="radio" name="search_type" class="search_appointed_test_type" value="2" {DV="checked_2_search_type"}/>{LABEL="users_assigned_to_groups"}<br />
            <input type="radio" name="search_type" class="search_appointed_test_type" value="3" {DV="checked_3_search_type"}/>{LABEL="generate_one_time_keys"}
        </div>
        <div id="search_by_ugroup_member_block">
            {LABEL="selection_by_group"}:
            <select name="user_group" size="1" id="search_by_ugroup_member">
                <option value='0' selected='selected'></option>
                {FOREACH="user_group_array_search"}
                <option value='{FR_ELEMENT="ug_id"}' {FR_ELEMENT="selected_user_group"}>{FR_ELEMENT="ug_name"}</option>
                {FOREACH_END}
            </select>
        </div>
        <div id="search_number_genereted_keys_block">
            {LABEL="amount_of_generated_keys_maximum_"}:
            <input type="text" name="key_number" value="{DV="key_number"}" id="search_number_genereted_keys">
        </div>
        <input type="image" src="img/buttonDo.jpg" id="copyFileButton"><br />
    </form>
</div>
