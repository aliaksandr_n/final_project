<div>
    <table id="userGroupTable" >
        <thead>
            <td class="userGroup">{LABEL="training_group"}</td>
            <td class="gDescription">{LABEL="description"}</td>
            <td class="gAuthor">{LABEL="author"}</td>
            <td class="modifyCol">{LABEL="modify"}</td>
            <td class="delCol">{LABEL="delete"}</td>
        </thead>
        <tbody id="answerTBody">
        {FOREACH="user_group_array"}
            <tr id = '{FR_ELEMENT="ug_id"}'>
                <th class="userGroup">{FR_ELEMENT="ug_name"}</th>
                <th class="gDescription">{FR_ELEMENT="ug_description"}</th>
                <th class="gAuthor">{FR_ELEMENT="ug_author_user_id"}</th>
                <th class="modifyCol"><a href="index.php?page=user_group&action=modify&ug_id={FR_ELEMENT="ug_id"}">{LABEL="modify"}</a></th>
                <th class="delCol"><a href="index.php?page=user_group&action=delete&ug_id={FR_ELEMENT="ug_id"}">{LABEL="delete"}</a></th>
            </tr>
        {FOREACH_END}
        </tbody>
    </table>
</div>