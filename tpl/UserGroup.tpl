<h4>
    {LABEL="training_groups"}
</h4>
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
{IF="include_search_result"}{==}{true}
    <div class="color_block">
        <a href="index.php?page=user_group&action=add-or-modify">{LABEL="add_group"}</a>
    </div>
    {FILE="tpl/UserGroup.SearchResult.tpl"}
{END_IF}
{IF="include_user_group_form"}{==}{true}
    {FILE="tpl/UserGroup.Add.tpl"}
{END_IF}
