<div>
    <table id="answerTable" >
        <thead>
            <td class="question">{LABEL="question"}</td>
            <td class="qCategoryName">{LABEL="question_category"}</td>
            <td class="qType">{LABEL="question_type"}</td>
            <td class="qWeight">{LABEL="weight"}</td>
            <td class="qTimeResponding">{LABEL="responding_time"}</td>
            <td class="qRandomAnswers">{LABEL="random_answers"}</td>
            <td class="qAddingTime">{LABEL="adding_time"}</td>
            <td class="qAuthor">{LABEL="author"}</td>
            <td class="modifyCol">{LABEL="modify"}</td>
            <td class="delCol">{LABEL="delete"}</td>
        </thead>
        <tbody id="answerTBody">
        {FOREACH="questionArray"}
            <tr id = '{FR_ELEMENT="qs_id"}'>
                <th class="question">{FR_ELEMENT="qs_text"}</th>
                <th class="qCategoryName">{FR_ELEMENT="qc_name"}</th>
                <th class="qType">{FR_ELEMENT="qt_name"}</th>
                <th class="qWeight">{FR_ELEMENT="qs_weight"}</th>
                <th class="qTimeResponding">{FR_ELEMENT="qs_answer_time_duration"}</th>
                <th class="qRandomAnswers">{FR_ELEMENT="qs_show_random_answers"}</th>
                <th class="qAddingTime">{FR_ELEMENT="qs_adding_time"}</th>
                <th class="qAuthor">{FR_ELEMENT="qs_author_user_id"}</th>
                <th class="modifyCol"><a href="index.php?page=question_setting&action=show&qs_id={FR_ELEMENT="qs_id"}">{LABEL="modify"}</a></th>
                <th class="delCol"><a href="index.php?page=question_setting&action=delete&qs_id={FR_ELEMENT="qs_id"}">{LABEL="delete"}</a></th>
            </tr>
        {FOREACH_END}
        </tbody>
    </table>
</div>