<div>
    <table id="trainer_stat_table" >
        <thead>
        <td class="test_name">{LABEL="test_name"}</td>
        <td class="number_of_appointments">{LABEL="number_of_appointments"}</td>
        <td class="number_of_finished">{LABEL="number_of_finished"}</td>
        <td class="passed_successfully">{LABEL="passed_successfully"}</td>
        </thead>
        <tbody id="answerTBody">
        {FOREACH="groups_stat_array"}
        <tr id = '{FR_ELEMENT="trid"}'>
            <th class="test_name">{FR_ELEMENT="tr_name"}</th>
            <th class="number_of_appointments">{FR_ELEMENT="number_of_appointments"}</th>
            <th class="number_of_finished">{FR_ELEMENT="number_of_finished"}</th>
            <th class="passed_successfully">{FR_ELEMENT="passed_successfully"}</th>
        </tr>
        {FOREACH_END}
        </tbody>
    </table>
</div>