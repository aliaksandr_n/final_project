<h4>
    {LABEL="test_was_passed"}
</h4>
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
<p>{LABEL="number_question_in_test"}: {DV="num_answers"}.<p/>
<p>{LABEL="right_answers_number"}: {DV="num_right_answers"}.<p/>
<p>{LABEL="final_mark"}: {DV="final_mark"}.<p/>
<p>{LABEL="final_verdict"}: {DV="final_verdict"}.<p/>
<p>{LABEL="time_duration"}: {DV="time_duration"}.<p/>
<p>{LABEL="finish_date"}: {DV="finish_date"}.<p/>
