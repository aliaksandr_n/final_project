<div class="color_block">
    <form action="index.php?page=question_manager&action=search" target="_self" method="get" enctype="multipart/form-data" id="form_question_serch_type">
        <input type="hidden" name="page" value="question_manager">
        <input type="hidden" name="action" value="search">
        <div id="search_type_block">
            {LABEL="step"} 1<br />
            {LABEL="select_the_source_for_the_questions_search"}:<br />
            <input type="radio" name="search_type" class="search_q_manager_type" value="1" {DV="checked_1_search_type"}/>{LABEL="all_questions_by_category"}<br />
            <input type="radio" name="search_type" class="search_q_manager_type" value="2" {DV="checked_2_search_type"}/>{LABEL="questions_from_sets"}<br />
            <input type="radio" name="search_type" class="search_q_manager_type" value="3" {DV="checked_3_search_type"}/>{LABEL="questions_from_tests_and_sets"}
        </div>
        <div id="search_q_cat_block">
            {LABEL="additional_selection_by_categories_of_questions"}:
            <select name="q_category" size="1" id="search_q_cat">
                <option value='0' selected='selected'></option>
                {FOREACH="question_category_array_search"}
                <option value='{FR_ELEMENT="qc_id"}' {FR_ELEMENT="selected_question_type"}>{FR_ELEMENT="qc_open_value"}{FR_ELEMENT="qc_name"}</option>
                {FOREACH_END}
            </select>
        </div>
        <div id="search_q_rep_block">
            {LABEL="additional_selection_on_the_set"}:
            <select name="q_repository" size="1" id="search_q_rep">
                <option value='0' selected='selected'></option>
                {FOREACH="question_repository_array_search"}
                <option value='{FR_ELEMENT="qr_id"}' {FR_ELEMENT="selected_question_repository"}>{FR_ELEMENT="qr_open_value"}{FR_ELEMENT="qr_name"}</option>
                {FOREACH_END}
            </select>
        </div>
        <div id="search_t_rep_block">
            {LABEL="additional_selection_on_the_test"}:
            <select name="t_repository" size="1" id="search_t_rep">
                <option value='0' selected='selected'></option>
                {FOREACH="test_repository_array_search"}
                <option value='{FR_ELEMENT="tr_id"}' {FR_ELEMENT="selected_test_category"}>{FR_ELEMENT="tr_name"}</option>
                {FOREACH_END}
            </select>
        </div>
        <input type="image" src="img/buttonFilter.jpg" id="copyFileButton"><br />
    </form>
</div>
