<h4>
    {IF="include_user_stat"}{==}{true}
        {LABEL="users_stat"}
    {END_IF}
    {IF="include_test_stat"}{==}{true}
        {LABEL="tests_stat"}
    {END_IF}
</h4>
{FILE="tpl/TrainerStat.Search.tpl"}
{FOREACH="actionResultMessage"}
    <span class="action_result_message">{FR_ELEMENT="message"}</span><br />
{FOREACH_END}
{IF="include_user_stat"}{==}{true}
    {FILE="tpl/TrainerStat.UserStat.tpl"}
{END_IF}
{IF="include_test_stat"}{==}{true}
    {FILE="tpl/TrainerStat.TestStat.tpl"}
{END_IF}