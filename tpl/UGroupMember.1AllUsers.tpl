<div>
    <form action="index.php?page=ugroup_member&action=search&search_type=1{DV="get_user_group"}" target="_self" method="post" name="uGroupMemberAction" enctype="multipart/form-data">
        <div class="color_block">
            {LABEL="step"} 2
            <br />
            {LABEL="select_rows_ in_table_below"}
        </div>
        <table id="managerTable">
            <thead>
                <td class="name_surname">{LABEL="user"}</td>
                <td class="checkbox_in_table">{LABEL="check"}</td>
            </thead>
            <tbody id="user_group_TBody">
                {FOREACH="user_array"}
                    <tr id = '{FR_ELEMENT="ru_id"}'>
                        <th class="name">{FR_ELEMENT="ru_name"}&nbsp;{FR_ELEMENT="ru_surname"}</th>
                        <th class="checkbox_in_table">
                            <input type="checkbox" name="user[{FR_ELEMENT="ru_id"}]" id="user[{FR_ELEMENT="ru_id"}]" {FR_ELEMENT="user_checked"}/><label for="user[{FR_ELEMENT="ru_id"}]">&nbsp</label>
                        </th>
                    </tr>
                {FOREACH_END}
            </tbody>
        </table>
        {IF="empty_dir"}{==}{true}
            <span class="message_empty_table">{LABEL="the_table_is_empty"}<span><br/>
        {END_IF}
        <div class="color_block">
            {LABEL="step"} 3
            <br />
            {LABEL="do_action_with_selected_user"}:<br/>
            <input type="radio" name="u_action" class="search_ugroup_member_action" value="1" {DV="checked_action_1"}/>{LABEL="copy_user_to_group"}<br />
            <br />
            <div id="goal_u_group">
                {LABEL="group"}:
                <select name="u_group_action" size="1" id="search_u_group">
                    <option value='0' selected='selected' disabled='disabled'></option>
                    {FOREACH="user_group_array_action"}
                    <option value='{FR_ELEMENT="ug_id"}' {FR_ELEMENT="selected_user_group"}>{FR_ELEMENT="ug_name"}</option>
                    {FOREACH_END}
                </select>
            </div>
            <br />
            <input type="hidden" name="submit_pressed" value="1">
            <input type="image" src="img/buttonFilter.jpg" id="copyFileButton">
        </form>
    </div>
</div>