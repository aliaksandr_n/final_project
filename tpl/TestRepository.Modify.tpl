<div>
    <form enctype="multipart/form-data" action="index.php?page=test_repository&action=open-test-form" method="post" name="testForm">
        {LABEL="test"}: <input type="text" name="tr_name"  id="tr_name" value="{DV="tr_name"}"/><br />
        {LABEL="test_category"}:
        <select name="tCategory" size="1" id="tCategory">
            <option value='0' selected="selected" disabled="disabled"></option>
            {FOREACH="test_category_array"}
            <option value='{FR_ELEMENT="tc_id"}' {FR_ELEMENT="selected_test_category"}>{FR_ELEMENT="tc_open_value"}{FR_ELEMENT="tc_name"}</option>
            {FOREACH_END}
        </select><br />
        {LABEL="allowed_to_stop_test"}: <input type="checkbox" name="tr_pause_test_permission" id="tr_pause_test_permission" {DV="selected_stop_test_permission"}/><label for="tr_pause_test_permission">&nbsp</label><br />
        {LABEL="showing_right_answer"}: <input type="checkbox" name="tr_show_right_answer" id="tr_show_right_answer" {DV="selected_show_right_answer"}/><label for="tr_show_right_answer">&nbsp</label><br />
        {LABEL="showing_final_mark"}: <input type="checkbox" name="tr_show_final_mark" id="tr_show_final_mark" {DV="selected_show_final_mark"} /><label for="tr_show_final_mark">&nbsp</label><br />
        {LABEL="showing_final_verdict"}: <input type="checkbox" name="tr_show_final_verdict"  id="tr_show_final_verdict" {DV="selected_show_final_verdict"}/><label for="tr_show_final_verdict">&nbsp</label><br />
        {LABEL="number_questions_in_test"} {LABEL="if_all_not_fill"}: <input type="text" name="tr_number_shown_questions"  value="{DV="tr_number_shown_questions"}"/><br />
        {LABEL="ability_to_skip_question"}: <input type="checkbox" name="tr_skip_questions"  id="tr_skip_questions" {DV="selected_skip_questions"} /><label for="tr_skip_questions">&nbsp</label><br />
        {LABEL="success_value_type"}:
        <select name="tr_success_value_type" size="1" id="tr_success_value_type">
            <option value='0' selected="selected" disabled="disabled"></option>
            <option value='1' {DV="selected_success_value_type1"}>{LABEL="by_question_number"}</option>
            <option value='2' {DV="selected_success_value_type2"}>{LABEL="by_question_percent"}</option>
            <option value='3' {DV="selected_success_value_type3"}>{LABEL="by_question_weight"}</option>
        </select><br />
        {LABEL="to_pass_test_get_value"}:<input type="text" name="tr_success_value_num"  id="tr_success_value_num" value="{DV="tr_success_value_num"}" /><label for="tr_success_value_num">&nbsp</label><br />
        {LABEL="to_control"}:
        <select name="tr_control_test_time_per_questions" size="1" id="tr_control_test_time_per_questions">
            <option value='0' {DV="selected_success_value_type1"} {DV="selected_control_test_time_per_questions0"}>{LABEL="total_test_time"}</option>
            <option value='1' {DV="selected_success_value_type2"} {DV="selected_control_test_time_per_questions1"}>{LABEL="time_per_question"}</option>
        </select><br />
        {LABEL="time_per_test"}: <input type="text" name="tr_total_time_value" id="tr_total_time_value" value="{DV="tr_total_time_value"}"/><br />
        <input type="hidden" name="button_pressed" value="1">
        <input type="hidden" name="tr_id" value="{DV="tr_id"}">
        <input type="hidden" name="first_action" value="{DV="first_action"}">
        <input type="image" src="img/buttonSave.jpg">
    </form>
</div>