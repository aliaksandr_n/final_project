<div class="color_block">
    <form action="index.php?page=test_repository&action=search" target="_self" method="get" enctype="multipart/form-data">
        {LABEL="select_test_by_category"}<br />
        <input type="hidden" name="page" value="test_repository">
        <input type="hidden" name="action" value="search">
        <select name="tCategory" size="1">
            <option value='0' selected='selected'>{LABEL="all"}</option>
            {FOREACH="test_category_array"}
                <option value='{FR_ELEMENT="tc_id"}' {FR_ELEMENT="selected_test_category"}>{FR_ELEMENT="tc_open_value"}{FR_ELEMENT="tc_name"}</option>
            {FOREACH_END}
        </select>
        <input type="image" src="img/buttonFilter.jpg" id="copyFileButton"><br />
    </form>
</div>