-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.38-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных final
CREATE DATABASE IF NOT EXISTS `final` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `final`;


-- Дамп структуры для таблица final.account_recovery_inf
CREATE TABLE IF NOT EXISTS `account_recovery_inf` (
  `ari_id` int(11) NOT NULL AUTO_INCREMENT,
  `ari_registered_user_id` int(11) NOT NULL,
  `ari_recovery_key` char(40) NOT NULL,
  `ari_recovery_date` int(11) NOT NULL,
  `ari_recovered` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ari_id`),
  KEY `FK_account_recovery_inf_registered_user` (`ari_registered_user_id`),
  KEY `Index_ari_recovery_key` (`ari_recovery_key`),
  CONSTRAINT `FK_account_recovery_inf_registered_user` FOREIGN KEY (`ari_registered_user_id`) REFERENCES `registered_user` (`ru_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='This table is used for saving temporary key, which is used in password''s recovery\r\n';

-- Дамп данных таблицы final.account_recovery_inf: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `account_recovery_inf` DISABLE KEYS */;
INSERT INTO `account_recovery_inf` (`ari_id`, `ari_registered_user_id`, `ari_recovery_key`, `ari_recovery_date`, `ari_recovered`) VALUES
	(1, 31, 'f93dc5a66259e487b8c4628e993de44372b5bfa4', 1422816105, 1);
/*!40000 ALTER TABLE `account_recovery_inf` ENABLE KEYS */;


-- Дамп структуры для таблица final.answer_variant
CREATE TABLE IF NOT EXISTS `answer_variant` (
  `av_id` int(11) NOT NULL AUTO_INCREMENT,
  `av_question_setting_id` int(11) NOT NULL,
  `av_correct` tinyint(4) NOT NULL DEFAULT '0',
  `av_text` varchar(250) NOT NULL,
  `av_order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`av_id`),
  KEY `FK_answer_variant_question_setting` (`av_question_setting_id`),
  CONSTRAINT `FK_answer_variant_question_setting` FOREIGN KEY (`av_question_setting_id`) REFERENCES `question_setting` (`qs_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.answer_variant: ~55 rows (приблизительно)
/*!40000 ALTER TABLE `answer_variant` DISABLE KEYS */;
INSERT INTO `answer_variant` (`av_id`, `av_question_setting_id`, `av_correct`, `av_text`, `av_order`) VALUES
	(43, 13, 0, 'Основатель компании "Форд"', 1),
	(44, 13, 0, 'Основатель компании "Кодак"', 2),
	(45, 13, 0, 'Основатель компании "Ксерокс"', 3),
	(46, 13, 0, 'Основатель компании "Макдональдс"', 4),
	(47, 13, 1, 'Основатель компании "IBM"', 5),
	(61, 16, 1, '4', 1),
	(62, 16, 0, '32', 2),
	(63, 16, 0, '64', 3),
	(64, 16, 0, '128', 4),
	(65, 16, 0, '256', 5),
	(66, 16, 0, '512', 6),
	(67, 16, 0, '1024', 7),
	(68, 16, 0, '3', 8),
	(69, 17, 1, 'Лимонный', 1),
	(70, 17, 1, 'Салатовый', 2),
	(71, 17, 0, 'Красный', 3),
	(72, 17, 0, 'Синий', 4),
	(73, 17, 0, 'Фиолетовый', 7),
	(100, 20, 0, 'Синий', 1),
	(101, 20, 0, 'Не зеленый', 2),
	(102, 20, 1, 'Зеленый', 3),
	(103, 20, 0, 'Оранжевый', 4),
	(111, 18, 0, 'Светло-синий', 1),
	(112, 18, 0, 'Фиолетовый', 2),
	(113, 18, 0, 'Черный', 3),
	(114, 18, 1, 'Красный', 4),
	(115, 18, 0, 'Желтый', 5),
	(123, 15, 0, '8', 1),
	(124, 15, 0, '15', 2),
	(125, 15, 0, '3', 3),
	(126, 15, 0, '4', 4),
	(127, 15, 1, '5', 6),
	(128, 15, 0, '7', 7),
	(129, 27, 0, 'Зеленый', 1),
	(130, 27, 1, 'Светло-зеленый', 2),
	(131, 27, 0, 'Темно-зеленый', 3),
	(132, 27, 0, 'Не зеленый', 4),
	(133, 12, 0, 'Марк Цукерберг (основатель Facebook)', 1),
	(134, 12, 0, 'Актер из ', 2),
	(135, 12, 0, 'Билл Клинтон', 3),
	(136, 12, 1, 'Билл Гейтс', 4),
	(137, 12, 0, 'Анатоний Чубайс', 5),
	(138, 12, 0, 'Арнольд Шварценеггер', 6),
	(139, 14, 0, 'Желтый', 1),
	(140, 14, 1, 'Зеленый', 2),
	(141, 14, 0, 'Черный', 3),
	(142, 14, 0, 'Синий', 4),
	(143, 14, 0, 'Красный', 5),
	(144, 14, 0, 'Фиолетовый', 6),
	(145, 14, 0, 'Оранжевый', 7),
	(146, 19, 1, '4', 1),
	(147, 19, 0, '3', 2),
	(148, 19, 1, '6 минус 2', 3),
	(149, 19, 0, '15', 4),
	(150, 19, 0, '2', 5);
/*!40000 ALTER TABLE `answer_variant` ENABLE KEYS */;


-- Дамп структуры для таблица final.appointed_test
CREATE TABLE IF NOT EXISTS `appointed_test` (
  `at_id` int(11) NOT NULL AUTO_INCREMENT,
  `at_test_repository_id` int(11) NOT NULL,
  `at_creating_time` int(11) NOT NULL,
  `at_creator_regestered_user_id` int(11) NOT NULL,
  PRIMARY KEY (`at_id`),
  KEY `FK_appointed_test_registered_user` (`at_creator_regestered_user_id`),
  KEY `FK_appointed_test_test_repository` (`at_test_repository_id`),
  CONSTRAINT `FK_appointed_test_registered_user` FOREIGN KEY (`at_creator_regestered_user_id`) REFERENCES `registered_user` (`ru_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_appointed_test_test_repository` FOREIGN KEY (`at_test_repository_id`) REFERENCES `test_repository` (`tr_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.appointed_test: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `appointed_test` DISABLE KEYS */;
INSERT INTO `appointed_test` (`at_id`, `at_test_repository_id`, `at_creating_time`, `at_creator_regestered_user_id`) VALUES
	(182, 91, 1422864508, 1),
	(183, 91, 1422864518, 1),
	(184, 91, 1422864557, 1),
	(185, 90, 1422864564, 1),
	(186, 94, 1422864570, 1),
	(187, 94, 1422864916, 1),
	(188, 91, 1422865277, 20),
	(189, 90, 1422865358, 1),
	(190, 90, 1422865372, 1),
	(191, 92, 1422865594, 1),
	(192, 90, 1422866339, 1),
	(193, 91, 1422866465, 31);
/*!40000 ALTER TABLE `appointed_test` ENABLE KEYS */;


-- Дамп структуры для таблица final.cofig
CREATE TABLE IF NOT EXISTS `cofig` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_name` varchar(100) NOT NULL,
  `c_value` varchar(255) NOT NULL,
  PRIMARY KEY (`c_id`),
  UNIQUE KEY `UQ_config_c_name` (`c_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.cofig: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `cofig` DISABLE KEYS */;
/*!40000 ALTER TABLE `cofig` ENABLE KEYS */;


-- Дамп структуры для таблица final.label
CREATE TABLE IF NOT EXISTS `label` (
  `l_id` int(11) NOT NULL AUTO_INCREMENT,
  `l_name` varchar(100) NOT NULL,
  `l_ru_value` varchar(1000) NOT NULL,
  `l_eng_value` varchar(1000) NOT NULL,
  `l_class` varchar(50) NOT NULL,
  PRIMARY KEY (`l_id`),
  UNIQUE KEY `UQ_lable_l_name` (`l_name`)
) ENGINE=InnoDB AUTO_INCREMENT=586 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.label: ~115 rows (приблизительно)
/*!40000 ALTER TABLE `label` DISABLE KEYS */;
INSERT INTO `label` (`l_id`, `l_name`, `l_ru_value`, `l_eng_value`, `l_class`) VALUES
	(172, 'modify', 'Изменить', 'Modify', 'COMMON_LABELS'),
	(173, 'delete', 'Удалить', 'Delete', 'COMMON_LABELS'),
	(174, 'check', 'Выделить', 'Check', 'COMMON_LABELS'),
	(175, 'the_table_is_empty', 'Табличная часть пуста', 'The table is empty', 'COMMON_LABELS'),
	(176, 'adding', 'Добавление нового элемента', 'Adding a new element', 'COMMON_LABELS'),
	(177, 'author', 'Автор', 'Author', 'COMMON_LABELS'),
	(178, 'question', 'Вопрос', 'Question', 'COMMON_LABELS'),
	(179, 'all', 'ВСЕ', 'All', 'COMMON_LABELS'),
	(180, 'necessarily_for_field', 'Поле обязательно к заполнению', 'Required field', 'COMMON_LABELS'),
	(181, 'group', 'Группа', 'Group', 'COMMON_LABELS'),
	(182, 'name', 'Имя', 'Name', 'COMMON_LABELS'),
	(183, 'surname', 'Фамилия', 'Surname', 'COMMON_LABELS'),
	(184, 'email', 'E-mail', 'Email', 'COMMON_LABELS'),
	(185, 'password_again', 'Пароль еще раз', 'Password again', 'COMMON_LABELS'),
	(186, 'password_max_symbol', 'Пароль (6 и более символов)', 'Password (6 or more characters)', 'COMMON_LABELS'),
	(187, 'adding_time', 'Время добавления', 'Time when added', 'COMMON_LABELS'),
	(188, 'user', 'Пользователь', 'User', 'COMMON_LABELS'),
	(189, 'description', 'Описание', 'Description', 'COMMON_LABELS'),
	(190, 'training_group', 'Группа тренинга', 'Training group', 'COMMON_LABELS'),
	(191, 'eng_question', 'Вопрос', 'Question', 'COMMON_LABELS'),
	(192, 'test', 'Тест', 'Test', 'COMMON_LABELS'),
	(193, 'page_head_information', 'Проверь свои знания качественно', 'Check your knowledge qualitatively', 'Index'),
	(194, 'main_page_message', 'Добро пожаловать на сайт тестирования знаний', 'Welcome to the site of testing your knowledge', 'Main'),
	(195, 'main', 'Главная', 'Main', 'Menu'),
	(196, 'menu_admin_block_name', 'Администратору', 'To administrator', 'Menu'),
	(197, 'menu_manager_block_name', 'Менеджеру', 'To manager', 'Menu'),
	(198, 'menu_trainer_block_name', 'Тренеру', 'To trainer', 'Menu'),
	(199, 'menu_user_block_name', 'Слушателю', 'To user', 'Menu'),
	(200, 'menu_guest_block_name', 'Гостю', 'To guest', 'Menu'),
	(201, 'menu_section_user_right', 'Права пользователей', 'User rights', 'Menu'),
	(202, 'menu_section_question_category', 'Категории вопросов', 'Categories of questions', 'Menu'),
	(203, 'menu_section_question_repository', 'Наборы вопросов', 'Sets of questions', 'Menu'),
	(204, 'menu_section_test_category', 'Категории тестов', 'Categories of tests', 'Menu'),
	(205, 'menu_section_question_setting', 'Вопросы', 'Questions', 'Menu'),
	(206, 'menu_section_test_repository', 'Тесты', 'Tests', 'Menu'),
	(207, 'menu_section_question_manager', 'Управление составом наборов и тестов', 'To manage sets and tests', 'Menu'),
	(208, 'menu_section_temp_user', 'Одноразовые ключи', 'One-time keys', 'Menu'),
	(209, 'menu_section_user_group', 'Группы тренинга', 'Training groups', 'Menu'),
	(210, 'menu_section_ugroup_member', 'Cформировать группу', 'To form a group', 'Menu'),
	(211, 'menu_section_appointed_test', 'Назначить тест пользователям', 'To appoint the test to users', 'Menu'),
	(212, 'menu_section_user_journal', 'Назначенные и пройденные тесты', 'Appointed and passed test', 'Menu'),
	(213, 'menu_section_user_journal_guest', 'Прохождение теста по одноразовому ключу', 'Passing the test for one-time key', 'Menu'),
	(214, 'password_recovery', 'Восстановление пароля', 'Password recovery', 'ForgetPass'),
	(215, 'enter_your_email', 'Введите ваш email', 'Enter your email', 'ForgetPass'),
	(216, 'you_entered_the_system_as', 'Вы вошли в систему как ', 'You are logged in as ', 'Login'),
	(217, 'log_out', 'Выйти', 'Log out', 'Login'),
	(218, 'enter', 'Войти', 'Log in', 'Login'),
	(219, 'welcome_to_the_site', 'Добро пожаловать на сайт', 'Welcome to the site', 'Login'),
	(220, 'registration', 'Регистрация', 'Registration', 'Login'),
	(221, 'forget_password', 'Забыли пароль', 'Forget the password', 'Login'),
	(222, 'password', 'Пароль', 'Password', 'Login'),
	(223, 'remember', 'Запомнить', 'Remember', 'Login'),
	(224, 'login', 'Логин', 'Login', 'Login'),
	(225, 'you_have_left_the_system', 'Вы вышли из системы', 'You are logged out', 'Login'),
	(226, 'unable_to_enter', 'Не удалось войти', 'Unable to enter', 'Login'),
	(227, 'appoint_test_to_users', 'Назначить тест пользователям', 'To appoint the test to users', 'AppointedTest'),
	(229, 'comment_not_necessary', 'Комментарий (необязательно к заполнению)', 'Comment (optional to fill in)', 'AppointedTest'),
	(230, 'key_can_be_modified', 'Ключ (возможно редактирование)', 'Key (can be modified)', 'AppointedTest'),
	(231, 'select_appointed_test', 'Выбирите назначаемый тест', 'Choose an appointed test', 'AppointedTest'),
	(232, 'selection_by_group', 'Дополнительный отбор по группе (необязательно)', 'Additional selection for the group (optional)', 'AppointedTest'),
	(233, 'select_the_source_user_search', 'Выбирите источник поиска пользователей', 'Select the source user search', 'COMMON_LABELS'),
	(234, 'amount_of_generated_keys_maximum_', 'Количество генерируемых ключей (35 максимум)', 'The amount of generated keys (maximum 35)', 'AppointedTest'),
	(235, 'question_categoies', 'Категории вопросов', 'Categories of questions', 'QuestionCategory'),
	(236, 'question_management', 'Управление составом наборов и тестов', 'To manage sets and tests', 'QuestionManager'),
	(237, 'select_the_source_for_the_questions_search', 'Выбирите источник поиска вопросов', 'Select the source of the search questions', 'QuestionManager'),
	(238, 'additional_selection_by_categories_of_questions', 'Дополнительный отбор по категории вопросов (необязательно)', 'Additional selection by categories of questions (optional)', 'QuestionManager'),
	(239, 'additional_selection_on_the_set', 'Дополнительный отбор по набору (необязательно)', 'Additional selection on the set (optional)', 'QuestionManager'),
	(240, 'additional_selection_on_the_test', 'Дополнительный отбор по тесту (необязательно)', 'Additional selection on the test (optional)', 'QuestionManager'),
	(241, 'all_questions_by_category', 'Все вопросы по категориям', 'All questions by category', 'QuestionManager'),
	(242, 'questions_from_sets', 'Вопросы из наборов', 'Questions from the sets', 'QuestionManager'),
	(243, 'questions_from_tests_and_sets', 'Вопросы из тестов', 'Questions from the tests and the sets', 'QuestionManager'),
	(244, 'test_name', 'Название теста', 'Name of the test', 'QuestionManager'),
	(245, 'do_action_with_selected_questions', 'Выбирите необходимое действие', 'Select the necessary action', 'QuestionManager'),
	(246, 'specify_the_set_which_will_be_field_by_questions', 'Укажите набор, куда будут скопированы вопросы', 'Specify the set, which will be copied to questions', 'QuestionManager'),
	(247, 'specify_the_test_which_will_be_field_by_questions', 'Укажите тест, куда будут скопированы вопросы', 'Specify the test, which will be copied to questions', 'QuestionManager'),
	(248, 'copy_to_set', 'Копировать в набор', 'Copy to the set', 'QuestionManager'),
	(249, 'copy_to_test', 'Копировать в тест', 'Copy to the test', 'QuestionManager'),
	(250, 'move_to_set', 'Переместить в набор', 'Move to the set', 'QuestionManager'),
	(251, 'move_to_test', 'Переместить в тест', 'Move to the test', 'QuestionManager'),
	(252, 'delete_from_current_set', 'Удалить из тек.набора', 'Remove from the current set', 'QuestionManager'),
	(253, 'category', 'Категория', 'Category', 'QuestionManager'),
	(254, 'set_name', 'Название набора', 'Set name', 'QuestionManager'),
	(256, 'question_set', 'Наборы вопросов', 'Sets of questions', 'QuestionManager'),
	(257, 'eng_picture', 'Picture', 'Picture', 'QuestionSetting'),
	(258, 'eng_click_to_add_another_picture', 'Нажмите, чтобы добавить еще картинку', 'Click to add another picture', 'QuestionSetting'),
	(259, 'eng_correct', 'сorrect', 'сorrect', 'QuestionSetting'),
	(260, 'eng_click_to_add_another_answer', 'Нажмите, чтобы добавить еще вариант ответа', 'Click to add another answer', 'QuestionSetting'),
	(261, 'eng_question_weight', 'Вес вопроса', 'Question weight', 'QuestionSetting'),
	(262, 'eng_answer_time_duration', 'Время ответа, мин.', 'Answer time duration, min', 'QuestionSetting'),
	(263, 'eng_show_random_question', 'Показывать ответы в произвольном порядке', 'Show questions randomly', 'QuestionSetting'),
	(264, 'eng_select_category', 'Выберите категорию', 'Select category', 'QuestionSetting'),
	(265, 'only_two_kind_of_questions_are_supported', 'На сегодня поддерживается добавление вопросов первых двух категорий', 'Only two kinds og question type are supported at present', 'QuestionSetting'),
	(266, 'for_question_adding_select_question_type', 'Для добавления вопроса- выбирите тип вопроса', 'To add a question - select a type of question', 'QuestionSetting'),
	(267, 'random_answers', 'Произвольные ответы', 'Random answers', 'QuestionSetting'),
	(268, 'responding_time', 'Время ответа, мин', 'Response time, min', 'QuestionSetting'),
	(269, 'weight', 'Вес', 'Weight', 'QuestionSetting'),
	(270, 'question_category', 'Категория вопроса', 'Question category', 'QuestionSetting'),
	(271, 'question_type', 'Тип вопроса', 'Type of question', 'QuestionSetting'),
	(272, 'select_question_by_category', 'Отобрать вопросы по категории', 'Select questions by category', 'QuestionSetting'),
	(273, 'pass_changing', 'Изменение пароля', 'Change the password', 'RecoveryPass'),
	(274, 'enter_your_new_pass', 'Введите ваш новый пароль', 'Enter your new password', 'RecoveryPass'),
	(275, 'registration_on_site', 'Регистрация на сайте', 'Register on the site', 'Registraition'),
	(276, 'login_min_symbol_description', 'Логин- только буквы, цифры и подчеркивание, 3 символа и больше (используется при авторизации)', 'Login- only letters, numbers and underscore, 3 characters or more (used for authentication)', 'Registraition'),
	(277, 'temp_key', 'Одноразовые ключи', 'Temp-keys', 'TempUser'),
	(278, 'comment', 'Комментарий', 'Comment', 'TempUser'),
	(279, 'key', 'Ключ', 'Key', 'TempUser'),
	(280, 'test_categories', 'Категории тестов', 'Categories of tests', 'TestCategory'),
	(281, 'tests', 'Тесты', 'Tests', 'TestRepository'),
	(282, 'add_test', 'Добавить тест', 'Add a test', 'TestRepository'),
	(283, 'select_test_by_category', 'Отобрать тесты по категории', 'Select tests by category', 'TestRepository'),
	(284, 'test_category', 'Категория теста', 'Test category ', 'TestRepository'),
	(285, 'time_per_test', 'Время на тест, мин', 'Test time, min', 'TestRepository'),
	(286, 'control_test_time_per_questions', 'Контроль времени на вопрос', 'Control answer time per the question', 'TestRepository'),
	(287, 'allowed_to_stop_test', 'Возможно ставить на паузу', 'Pause is possible', 'TestRepository'),
	(288, 'showing_right_answer', 'Показ правильных ответов', 'Show the correct answers', 'TestRepository'),
	(289, 'showing_final_mark', 'Показ итоговой оценки', 'Show the final grade', 'TestRepository'),
	(290, 'showing_final_verdict', 'Показ финального вердикта', 'Show the final verdict', 'TestRepository'),
	(291, 'number_questions_in_test', 'Количество вопросов в тесте', 'The number of questions in the test', 'TestRepository'),
	(292, 'if_all_not_fill', '(если все- поле заполнять не нужно)', '(if all-field do not need to fill in)', 'TestRepository'),
	(293, 'ability_to_skip_question', 'Возм.пропуска вопросов', 'The possibility of skipping questions', 'TestRepository'),
	(294, 'success_value_type', 'Определение прав. ответа', 'A method of determination of rights answer', 'TestRepository'),
	(295, 'value_to_pass', 'Значение для прохождения', 'The value for passing', 'TestRepository'),
	(296, 'by_question_number', 'По количеству вопросов', 'By the number of questions', 'TestRepository'),
	(297, 'by_question_percent', 'По проценту вопросов', 'By the percentage  of questions', 'TestRepository'),
	(298, 'by_question_weight', 'По весу вопросов', 'By the weight of questions', 'TestRepository'),
	(299, 'to_control', 'Контролировать', 'Control', 'TestRepository'),
	(300, 'to_pass_test_get_value', 'Для прохождения теста нужно набрать', 'To pass the test, you need to score', 'TestRepository'),
	(301, 'total_test_time', 'Общее время теста', 'The total test time', 'TestRepository'),
	(302, 'time_per_question', 'Время по каждому вопросу', 'Time for each question', 'TestRepository'),
	(303, 'form_group', 'Сформировать группу', 'To form a group', 'UGroupMember'),
	(304, 'do_action_with_selected_user', 'Выполнить действие с выделенными пользователями', 'Perform an action with selected users', 'UGroupMember'),
	(305, 'copy_user_to_group', 'Копировать пользователей в группу', 'Copy users to the group', 'UGroupMember'),
	(306, 'move_user_to_group', 'Переместить пользователей в группу', 'Move users to the group', 'UGroupMember'),
	(307, 'delete_user_from_group', 'Удалить пользователей из группы', 'Remove users from a group', 'UGroupMember'),
	(309, 'additional_selection_for_the_group', 'Дополнительный отбор по группе (необязательно)', 'Additional selection for the group (optional)', 'UGroupMember'),
	(310, 'all_system_users', 'Все пользователи системы', 'All users of the system', 'COMMON_LABELS'),
	(311, 'user_added_to_any_group', 'Пользователи, отнесенные минимум к одной группе', 'Users assigned to at least one group', 'UGroupMember'),
	(321, 'training_groups', 'Группы тренинга', 'Training groups', 'UserGroup'),
	(322, 'add_group', 'Добавить группу', 'Add a group', 'UserGroup'),
	(323, 'title', 'Название', 'Name', 'UserGroup'),
	(324, 'answer_the_question', 'Ответ на вопрос', 'Answer to the question', 'UserJournal'),
	(325, 'unable_to_display_the_question', 'Не удалось отобразить вопрос', 'Unable to display the question', 'UserJournal'),
	(326, 'time_for_test_answer', 'Время ответа на тест', 'Response time for the test', 'UserJournal'),
	(327, 'time_for_question_answer', 'Время ответа на вопрос', 'Response time for the question', 'UserJournal'),
	(328, 'test_was_passed', 'Тест пройден', 'The test_is_passed', 'UserJournal'),
	(329, 'number_question_in_test', 'Всего вопросов в тесте', 'Total number in the test', 'UserJournal'),
	(330, 'right_answers_number', 'Количество правильных ответов', 'Number of correct answers', 'UserJournal'),
	(331, 'final_mark', 'Итоговая оценка', 'Final mark', 'COMMON_LABELS'),
	(332, 'final_verdict', 'Финальный вердикт', 'Final verdict', 'COMMON_LABELS'),
	(333, 'enter_key_number', 'Введите номер ключа', 'Enter the key number', 'UserJournal'),
	(334, 'passing_the_test_for_one_time_key', 'Прохождение теста по одноразовому ключу', 'Passing the test for one-time key', 'UserJournal'),
	(335, 'answer_question_results', 'Результаты ответа на вопрос', 'The results of answer to the question', 'UserJournal'),
	(336, 'answers_selected_by_the_user', 'Ответы, выбранные пользователем', 'The answers chosen by the user', 'UserJournal'),
	(337, 'your_question_answer_result', 'Итого: на вопрос вы ответили', 'Finally: your answer to the question is', 'UserJournal'),
	(338, 'to_solve', 'Решать/ посмотреть результат', 'To solve/ see result', 'UserJournal'),
	(339, 'appointed_and_passed_tests', 'Назначенные и пройденные тесты', 'Appointed and passed tests', 'UserJournal'),
	(340, 'questions_number_in_test', 'Количество вопросов в тесте', 'The number of questions in the test', 'UserJournal'),
	(341, 'number_of_solved_questions', 'Количество отвеченных вопросов', 'The number of answered questions', 'UserJournal'),
	(342, 'pause_ability', 'Возможность паузы', 'The ability of pause', 'UserJournal'),
	(343, 'person_whom_was_test_appoinet', 'Кем назначен тест', 'A person who was appointed to the test', 'UserJournal'),
	(344, 'user_rights_management', 'Управление правами пользователей', 'User rights management', 'UserRight'),
	(345, 'trainer', 'Тренер', 'Trainer', 'COMMON_LABELS'),
	(346, 'manager', 'Менеджер', 'Manager', 'UserRight'),
	(347, 'administrator', 'Администратор', 'Administrator', 'UserRight'),
	(348, 'all_registered_users_have_student_right', 'По умолчанию, у всех зарегистрированных пользователей уже есть право "Слушателя курсов"', 'All registered users already have the right "trainees", by default', 'UserRight'),
	(349, 'confirmation_of_registration', 'Подтверждение регистрации', 'Confirmation of registration', 'Verification'),
	(351, 'change_language', 'Изменить язык', 'Choose your language', 'Index'),
	(540, 'add_question', 'Добавить вопрос', 'Add a question', 'QuestionSetting'),
	(541, 'questions', 'Вопросы', 'Questions', 'COMMON_LABELS'),
	(542, 'users_assigned_to_groups', 'Пользователи,отнесенные к группам', 'Users assigned to groups', 'AppointedTest'),
	(543, 'generate_one_time_keys', 'Сгенерировать одноразовые ключи', 'Generate a one-time keys', 'AppointedTest'),
	(544, 'menu_section_test_journal', 'Журнал назначенных тестов', 'Journal of appointed tests', 'Menu'),
	(545, 'appointed_tests_journal', 'Журнал назначенных тестов', 'Journal of appointed tests', 'TestJournal'),
	(546, 'date_of_appointment', 'Дата назначения', 'Date of appointment', 'TestJournal'),
	(548, 'number_users', 'Скольким пользователям назначен', 'How many users assigned', 'TestJournal'),
	(549, 'finished_users', 'Количество пользователей завершивших тест', 'Users completed the test', 'TestJournal'),
	(550, 'more_info', 'Результаты пользователей                               ', 'Users results', 'TestJournal'),
	(551, 'user_or_key', 'Пользователь или ключ', 'Username or temp-key', 'TestJournal'),
	(552, 'key_comments', 'Комментарий к ключу', 'keys comment', 'TestJournal'),
	(553, 'is_finished', 'Завершен                     ', 'Is finished', 'TestJournal'),
	(556, 'answering_duration', 'Время ответа', 'Time of answering', 'TestJournal'),
	(557, 'question_number', 'Количество вопросов', 'Number of questions', 'TestJournal'),
	(558, 'answered_question', 'Отвеченных вопросов', 'Answered question', 'TestJournal'),
	(559, 'overtimed_question', 'Просроченных вопросов', 'Overtimed question', 'TestJournal'),
	(560, 'correct_answers', 'Корректно-отвеченных вопросов', 'Correct answers', 'TestJournal'),
	(561, 'answered', 'Отвечено', 'Answered', 'TestJournal'),
	(562, 'overtimed', 'Превышено время', 'overtimed', 'TestJournal'),
	(563, 'eng_answer', 'Answer', 'Answer', 'QuestionSetting'),
	(566, 'step', 'Шаг', 'Step', 'COMMON_LABELS'),
	(567, 'select_rows_ in_table_below', 'Выделите необходимые элементы в  таблице ниже', 'Select the necessary rows in the table below', 'COMMON_LABELS'),
	(568, 'question_list', 'Вопросы теста', 'Tests question', 'TestJournal'),
	(569, 'time_duration', 'Длительность проходжения теста, мин.', 'Time duration', 'COMMON_LABELS'),
	(570, 'finish_date', 'Дата окончания теста', 'Test finish date', 'COMMON_LABELS'),
	(572, 'menu_section_trainer_stat', 'Статистика (тесты/ пользователи)', 'Statistics (tests / users)', 'TrainerStat'),
	(573, 'statistics_for_trainer', 'Статистика', 'Statistics (tests / users)', 'TrainerStat'),
	(574, 'users_stat', 'Статистика пользователей (минимальнаый набор)', 'Users stat', 'TrainerStat'),
	(575, 'tests_stat', 'Статистика тестов  (минимальнаый набор)', 'Tests stat', 'TrainerStat'),
	(576, 'select_stat_type', 'Укажите вид требуемой статистики', 'Select the required stat type', 'TrainerStat'),
	(577, 'number_of_appointments', 'Количество назначений', 'Number of appointments', 'TrainerStat'),
	(578, 'passed_test', 'Завершенo до конца', 'Passed', 'TrainerStat'),
	(581, 'passed_successfully', 'Пройден успешно', 'Passed_successfully', 'TrainerStat'),
	(582, 'tests_appointed', 'Тестов назначено', 'Tests appointed', 'TrainerStat'),
	(584, 'number_of_finished', 'Завершен до конца', 'Passed', 'TrainerStat'),
	(585, 'number_of_finished_successfully', 'Пройдено успешно', 'Passed_successfully', 'TrainerStat');
/*!40000 ALTER TABLE `label` ENABLE KEYS */;


-- Дамп структуры для таблица final.message
CREATE TABLE IF NOT EXISTS `message` (
  `m_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_name` varchar(100) NOT NULL,
  `m_value` varchar(1000) NOT NULL,
  PRIMARY KEY (`m_id`),
  UNIQUE KEY `UQ_message_m_name` (`m_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.message: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;


-- Дамп структуры для таблица final.question_category
CREATE TABLE IF NOT EXISTS `question_category` (
  `qc_id` int(11) NOT NULL AUTO_INCREMENT,
  `qc_name` varchar(150) DEFAULT NULL,
  `qc_parent_id` int(11) DEFAULT '0',
  PRIMARY KEY (`qc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.question_category: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `question_category` DISABLE KEYS */;
INSERT INTO `question_category` (`qc_id`, `qc_name`, `qc_parent_id`) VALUES
	(29, 'MySQL', 0),
	(30, 'Java', 0),
	(31, 'PHP', 0),
	(33, 'Переменные', 31),
	(34, 'Массивы', 31),
	(35, 'select', 29),
	(46, 'Software Testing', 0),
	(47, 'Tast cases', 46);
/*!40000 ALTER TABLE `question_category` ENABLE KEYS */;


-- Дамп структуры для таблица final.question_cat_group
CREATE TABLE IF NOT EXISTS `question_cat_group` (
  `qcg_id` int(11) NOT NULL AUTO_INCREMENT,
  `qcg_question_setting_id` int(11) NOT NULL,
  `qcg_question_category_id` int(11) NOT NULL,
  PRIMARY KEY (`qcg_id`),
  UNIQUE KEY `Индекс 4` (`qcg_question_setting_id`,`qcg_question_category_id`),
  KEY `FK_question_group_question_category` (`qcg_question_category_id`),
  CONSTRAINT `FK_question_cat_group_question_setting` FOREIGN KEY (`qcg_question_setting_id`) REFERENCES `question_setting` (`qs_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_question_group_question_category` FOREIGN KEY (`qcg_question_category_id`) REFERENCES `question_category` (`qc_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.question_cat_group: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `question_cat_group` DISABLE KEYS */;
INSERT INTO `question_cat_group` (`qcg_id`, `qcg_question_setting_id`, `qcg_question_category_id`) VALUES
	(13, 12, 35),
	(14, 13, 35),
	(15, 14, 29),
	(16, 15, 35),
	(17, 16, 35),
	(18, 17, 35),
	(19, 18, 35),
	(20, 19, 35),
	(21, 20, 29),
	(28, 27, 35);
/*!40000 ALTER TABLE `question_cat_group` ENABLE KEYS */;


-- Дамп структуры для таблица final.question_picture
CREATE TABLE IF NOT EXISTS `question_picture` (
  `qp_id` int(11) NOT NULL AUTO_INCREMENT,
  `qp_question_setting_id` int(11) NOT NULL DEFAULT '0',
  `qp_order_in_question` int(11) NOT NULL,
  `qp_name_in_db` varchar(100) NOT NULL,
  `qp_origin_name` varchar(200) NOT NULL,
  `qp_file_size` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`qp_id`),
  KEY `FK_question_picture_question_setting` (`qp_question_setting_id`),
  CONSTRAINT `FK_question_picture_question_setting` FOREIGN KEY (`qp_question_setting_id`) REFERENCES `question_setting` (`qs_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.question_picture: ~13 rows (приблизительно)
/*!40000 ALTER TABLE `question_picture` DISABLE KEYS */;
INSERT INTO `question_picture` (`qp_id`, `qp_question_setting_id`, `qp_order_in_question`, `qp_name_in_db`, `qp_origin_name`, `qp_file_size`) VALUES
	(5, 12, 1, 'c692d6a10598e0a801576fdd4ecf3c37e45bfbc4.jpg', 'bill.jpg', 0),
	(6, 13, 1, '0ceb7a81c6cce60efb46e27d86f466e0ebce37b3.jpg', 'TomasYotsan.jpg', 0),
	(7, 14, 1, '466bc8cef3e71de796ec483e212724a2c2044c68.bmp', 'black.bmp', 0),
	(8, 14, 2, '4c9a82ce72ca2519f38d0af0abbb4cecb9fceca9.bmp', 'blue.bmp', 0),
	(9, 14, 3, '3ef7ff59133fea8394d70bc06f0b9e11ae4f95aa.bmp', 'geen-light.bmp', 0),
	(10, 14, 4, 'bc74f4f071a5a33f00ab88a6d6385b5e6638b86c.bmp', 'green.bmp', 0),
	(11, 14, 5, '78988010b890ce6f4d2136481f392787ec6d6106.bmp', 'red.bmp', 0),
	(12, 14, 6, 'adb8fb74b4b25517ab254764ea8a3d5e30e29527.bmp', 'yesllow.bmp', 0),
	(13, 15, 1, '641a3a20acb43be9b97b798154d805e357bceaba.bmp', 'curcle.bmp', 0),
	(14, 17, 1, '3ef7ff59133fea8394d70bc06f0b9e11ae4f95aa.bmp', 'geen-light.bmp', 0),
	(15, 18, 1, '78988010b890ce6f4d2136481f392787ec6d6106.bmp', 'red.bmp', 0),
	(16, 20, 1, 'bc74f4f071a5a33f00ab88a6d6385b5e6638b86c.bmp', 'green.bmp', 0),
	(17, 27, 1, '3ef7ff59133fea8394d70bc06f0b9e11ae4f95aa.bmp', 'geen-light.bmp', 0);
/*!40000 ALTER TABLE `question_picture` ENABLE KEYS */;


-- Дамп структуры для таблица final.question_repository
CREATE TABLE IF NOT EXISTS `question_repository` (
  `qr_id` int(11) NOT NULL AUTO_INCREMENT,
  `qr_name` varchar(150) DEFAULT NULL,
  `qr_parent_id` int(11) DEFAULT '0',
  PRIMARY KEY (`qr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.question_repository: ~11 rows (приблизительно)
/*!40000 ALTER TABLE `question_repository` DISABLE KEYS */;
INSERT INTO `question_repository` (`qr_id`, `qr_name`, `qr_parent_id`) VALUES
	(1, 'Для внешних слушателей', 0),
	(2, 'Для внутренних слушателей', 0),
	(3, 'Без опыта работы', 1),
	(4, 'Повышение квалификации', 2),
	(5, 'Изучение смежных технологий ', 2),
	(11, 'Набор для теста', 0),
	(12, 'Разные вопросы', 11),
	(13, 'Прочее', 0),
	(14, 'Цвета и фугуры', 13),
	(15, 'Фото', 13),
	(16, 'Разное', 13);
/*!40000 ALTER TABLE `question_repository` ENABLE KEYS */;


-- Дамп структуры для таблица final.question_repos_group
CREATE TABLE IF NOT EXISTS `question_repos_group` (
  `qrg_id` int(11) NOT NULL AUTO_INCREMENT,
  `qrg_question_setting_id` int(11) NOT NULL,
  `qrg_question_repository_id` int(11) NOT NULL,
  PRIMARY KEY (`qrg_id`),
  UNIQUE KEY `Индекс 4` (`qrg_question_repository_id`,`qrg_question_setting_id`),
  KEY `FK_question_repos_group_question_setting` (`qrg_question_setting_id`),
  CONSTRAINT `FK_question_repos_group_question_repository` FOREIGN KEY (`qrg_question_repository_id`) REFERENCES `question_repository` (`qr_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_question_repos_group_question_setting` FOREIGN KEY (`qrg_question_setting_id`) REFERENCES `question_setting` (`qs_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.question_repos_group: ~19 rows (приблизительно)
/*!40000 ALTER TABLE `question_repos_group` DISABLE KEYS */;
INSERT INTO `question_repos_group` (`qrg_id`, `qrg_question_setting_id`, `qrg_question_repository_id`) VALUES
	(197, 19, 4),
	(198, 20, 4),
	(176, 14, 14),
	(178, 15, 14),
	(179, 17, 14),
	(180, 18, 14),
	(177, 20, 14),
	(181, 12, 15),
	(182, 13, 15),
	(183, 15, 15),
	(186, 12, 16),
	(187, 13, 16),
	(184, 14, 16),
	(188, 15, 16),
	(189, 16, 16),
	(190, 17, 16),
	(191, 18, 16),
	(192, 19, 16),
	(185, 20, 16);
/*!40000 ALTER TABLE `question_repos_group` ENABLE KEYS */;


-- Дамп структуры для таблица final.question_setting
CREATE TABLE IF NOT EXISTS `question_setting` (
  `qs_id` int(11) NOT NULL AUTO_INCREMENT,
  `qs_question_type_id` int(11) NOT NULL,
  `qs_text` varchar(50) NOT NULL,
  `qs_weight` int(11) DEFAULT '50',
  `qs_answer_time_duration` int(11) NOT NULL DEFAULT '0',
  `qs_show_random_answers` tinyint(4) NOT NULL DEFAULT '1',
  `qs_adding_time` int(10) NOT NULL,
  `qs_author_user_id` varchar(50) NOT NULL,
  PRIMARY KEY (`qs_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.question_setting: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `question_setting` DISABLE KEYS */;
INSERT INTO `question_setting` (`qs_id`, `qs_question_type_id`, `qs_text`, `qs_weight`, `qs_answer_time_duration`, `qs_show_random_answers`, `qs_adding_time`, `qs_author_user_id`) VALUES
	(12, 1, 'Кто изображен внизу слева?', 50, 7, 1, 1422864853, '1'),
	(13, 1, 'Кем является показанный на фото человек?', 50, 0, 0, 1422818255, '1'),
	(14, 1, 'Кокой цвет представлен дважды (разные оттенки)?', 50, 2, 1, 1422864863, '1'),
	(15, 1, 'Сколько кругов изображено на картинке', 50, 1, 0, 1422833426, '1'),
	(16, 1, 'Сколько будет 2+2', 50, 2, 1, 1422818563, '1'),
	(17, 2, 'Как можно назвать указанный цвет?', 50, 0, 1, 1422818651, '1'),
	(18, 1, 'Как можно назвать указанный цвет?', 50, 4, 0, 1422833215, '1'),
	(19, 2, '2+2 равно следующему значению', 50, 3, 1, 1422864884, '1'),
	(20, 2, 'Какой цвет на рисунке', 50, 2, 1, 1422821764, '1'),
	(27, 1, 'Какой цвет на картинке?', 50, 7, 1, 1422863892, '1');
/*!40000 ALTER TABLE `question_setting` ENABLE KEYS */;


-- Дамп структуры для таблица final.question_type
CREATE TABLE IF NOT EXISTS `question_type` (
  `qt_id` int(11) NOT NULL AUTO_INCREMENT,
  `qt_name` varchar(50) NOT NULL,
  `qt_tpl_name` varchar(50) NOT NULL COMMENT 'delete, need for developing',
  PRIMARY KEY (`qt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.question_type: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `question_type` DISABLE KEYS */;
INSERT INTO `question_type` (`qt_id`, `qt_name`, `qt_tpl_name`) VALUES
	(1, 'Вопрос с одним ответом', ''),
	(2, 'Вопрос с несколькими ответами', ''),
	(3, 'Вопрос на соответствие', ''),
	(4, 'Вопрос на классификацию', ''),
	(5, 'Вопрос на упорядочивание', ''),
	(6, 'Вопрос на заполнение', ''),
	(7, 'Вопрос со свободным ответом', ''),
	(8, 'Вопрос с картинкой', ''),
	(9, 'Вопрос на перемещение фрагментов («мозаика ', '');
/*!40000 ALTER TABLE `question_type` ENABLE KEYS */;


-- Дамп структуры для таблица final.registered_user
CREATE TABLE IF NOT EXISTS `registered_user` (
  `ru_id` int(11) NOT NULL AUTO_INCREMENT,
  `ru_login` varchar(100) NOT NULL,
  `ru_password` char(40) NOT NULL,
  `ru_email` varchar(100) NOT NULL,
  `ru_name` varchar(75) NOT NULL,
  `ru_surname` varchar(75) NOT NULL,
  `ru_reg_date` int(11) NOT NULL,
  `ru_auth_key` char(40) NOT NULL,
  `ru_user_right` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ru_id`),
  UNIQUE KEY `UQ_registered_user_ru_login` (`ru_login`),
  UNIQUE KEY `UQ_registered_user_ru_email` (`ru_email`),
  KEY `Index_ru_login` (`ru_login`),
  KEY `Index_ru_password` (`ru_password`),
  KEY `Index_ru_auth_key` (`ru_auth_key`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.registered_user: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `registered_user` DISABLE KEYS */;
INSERT INTO `registered_user` (`ru_id`, `ru_login`, `ru_password`, `ru_email`, `ru_name`, `ru_surname`, `ru_reg_date`, `ru_auth_key`, `ru_user_right`) VALUES
	(1, 'user1', 'e38ad214943daad1d64c102faec29de4afe9da3d', 'kj@jk.tui', 'Администратор', 'Администраторов', 0, '', 15),
	(20, 'ivan', 'e38ad214943daad1d64c102faec29de4afe9da3d', '', 'Иван', 'Иванов', 0, '', 5),
	(31, 'pupil1', 'e38ad214943daad1d64c102faec29de4afe9da3d', 'ser@e.example', 'Василий', 'Васильев', 1422815932, '', 3),
	(32, 'pupil2', 'e38ad214943daad1d64c102faec29de4afe9da3d', 'ser@e.exhample', 'Юрий', 'Петров', 1422866213, '', 1);
/*!40000 ALTER TABLE `registered_user` ENABLE KEYS */;


-- Дамп структуры для таблица final.temp_user
CREATE TABLE IF NOT EXISTS `temp_user` (
  `tu_id` int(11) NOT NULL AUTO_INCREMENT,
  `tu_key` char(40) NOT NULL,
  `tu_comment` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`tu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.temp_user: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `temp_user` DISABLE KEYS */;
INSERT INTO `temp_user` (`tu_id`, `tu_key`, `tu_comment`) VALUES
	(125, '12345', 'Для Федорова Висилия '),
	(126, '0d056f8e607c266f410e217ae84b46bdb21c6401', 'Для Ивана Ивановича'),
	(127, '9fb645c12c0698a892f25a4c7d3911e4161045ac', 'Запасной'),
	(128, '7eb37ee1170f76814072d600eeab8d062b7c7950', ''),
	(129, '9b4658c8487866cf14d9bdb502b980e9a0dd0970', ''),
	(130, '12345', 'Для Федорова Висилия '),
	(131, '0d056f8e607c266f410e217ae84b46bdb21c6401', 'Для Ивана Ивановича'),
	(132, '9fb645c12c0698a892f25a4c7d3911e4161045ac', 'Запасной'),
	(133, '7eb37ee1170f76814072d600eeab8d062b7c7950', ''),
	(134, '9b4658c8487866cf14d9bdb502b980e9a0dd0970', '');
/*!40000 ALTER TABLE `temp_user` ENABLE KEYS */;


-- Дамп структуры для таблица final.test_category
CREATE TABLE IF NOT EXISTS `test_category` (
  `tc_id` int(11) NOT NULL AUTO_INCREMENT,
  `tc_name` varchar(150) NOT NULL,
  `tc_parent_id` int(11) DEFAULT '0',
  PRIMARY KEY (`tc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.test_category: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `test_category` DISABLE KEYS */;
INSERT INTO `test_category` (`tc_id`, `tc_name`, `tc_parent_id`) VALUES
	(74, 'Для внутренних курсов', 0),
	(75, 'Для внешних курсов', 0),
	(76, 'Для джуниоров', 74),
	(77, 'Для middle users', 74),
	(82, 'Для продвинутых пользователей ПК', 0);
/*!40000 ALTER TABLE `test_category` ENABLE KEYS */;


-- Дамп структуры для таблица final.test_cat_group
CREATE TABLE IF NOT EXISTS `test_cat_group` (
  `tcg_id` int(11) NOT NULL AUTO_INCREMENT,
  `tcg_test_repository_id` int(11) NOT NULL,
  `tcg_test_category_id` int(11) NOT NULL,
  PRIMARY KEY (`tcg_id`),
  KEY `FK_test_cat_group_test_repository` (`tcg_test_repository_id`),
  KEY `FK_test_cat_group_test_category` (`tcg_test_category_id`),
  CONSTRAINT `FK_test_cat_group_test_category` FOREIGN KEY (`tcg_test_category_id`) REFERENCES `test_category` (`tc_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_test_cat_group_test_repository` FOREIGN KEY (`tcg_test_repository_id`) REFERENCES `test_repository` (`tr_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.test_cat_group: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `test_cat_group` DISABLE KEYS */;
INSERT INTO `test_cat_group` (`tcg_id`, `tcg_test_repository_id`, `tcg_test_category_id`) VALUES
	(80, 90, 74),
	(81, 91, 76),
	(82, 92, 82),
	(84, 94, 76);
/*!40000 ALTER TABLE `test_cat_group` ENABLE KEYS */;


-- Дамп структуры для таблица final.test_repository
CREATE TABLE IF NOT EXISTS `test_repository` (
  `tr_id` int(11) NOT NULL AUTO_INCREMENT,
  `tr_name` varchar(150) NOT NULL,
  `tr_pause_test_permission` tinyint(4) NOT NULL DEFAULT '0',
  `tr_show_right_answer` int(11) NOT NULL DEFAULT '0',
  `tr_show_final_mark` tinyint(4) NOT NULL DEFAULT '0',
  `tr_show_final_verdict` tinyint(4) NOT NULL DEFAULT '0',
  `tr_number_shown_questions` int(11) NOT NULL DEFAULT '0',
  `tr_success_value_type` int(11) NOT NULL DEFAULT '0',
  `tr_success_value_num` int(11) NOT NULL DEFAULT '0',
  `tr_skip_questions` tinyint(4) NOT NULL DEFAULT '1',
  `tr_control_test_time_per_questions` tinyint(4) NOT NULL DEFAULT '0',
  `tr_total_time_value` int(11) DEFAULT NULL,
  `tr_author_id` varchar(50) DEFAULT NULL,
  `tr_adding_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`tr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.test_repository: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `test_repository` DISABLE KEYS */;
INSERT INTO `test_repository` (`tr_id`, `tr_name`, `tr_pause_test_permission`, `tr_show_right_answer`, `tr_show_final_mark`, `tr_show_final_verdict`, `tr_number_shown_questions`, `tr_success_value_type`, `tr_success_value_num`, `tr_skip_questions`, `tr_control_test_time_per_questions`, `tr_total_time_value`, `tr_author_id`, `tr_adding_time`) VALUES
	(90, 'Тест с разными вопросами', 1, 1, 1, 1, 0, 1, 0, 1, 0, 77, '1', 1422822161),
	(91, 'Тест с вопросами', 1, 1, 1, 1, 0, 1, 30, 0, 1, 8, '1', 1422822235),
	(92, 'Все вопросы', 1, 1, 1, 1, 0, 1, 4, 1, 0, 10, '1', 1422824364),
	(94, 'Тест с 4-мя вопросами', 1, 1, 1, 1, 4, 2, 0, 1, 1, 15, '1', 1422864899);
/*!40000 ALTER TABLE `test_repository` ENABLE KEYS */;


-- Дамп структуры для таблица final.test_repos_group
CREATE TABLE IF NOT EXISTS `test_repos_group` (
  `trg_id` int(11) NOT NULL AUTO_INCREMENT,
  `trg_question_setting_id` int(11) NOT NULL,
  `trg_test_repository_id` int(11) NOT NULL,
  PRIMARY KEY (`trg_id`),
  UNIQUE KEY `Индекс 4` (`trg_question_setting_id`,`trg_test_repository_id`),
  KEY `FK_test_repos_group_test_repository` (`trg_test_repository_id`),
  CONSTRAINT `FK_test_repos_group_question_setting` FOREIGN KEY (`trg_question_setting_id`) REFERENCES `question_setting` (`qs_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_test_repos_group_test_repository` FOREIGN KEY (`trg_test_repository_id`) REFERENCES `test_repository` (`tr_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.test_repos_group: ~27 rows (приблизительно)
/*!40000 ALTER TABLE `test_repos_group` DISABLE KEYS */;
INSERT INTO `test_repos_group` (`trg_id`, `trg_question_setting_id`, `trg_test_repository_id`) VALUES
	(60, 12, 90),
	(69, 12, 92),
	(87, 12, 94),
	(61, 13, 90),
	(70, 13, 92),
	(58, 14, 90),
	(76, 14, 91),
	(67, 14, 92),
	(86, 14, 94),
	(62, 15, 90),
	(71, 15, 92),
	(63, 16, 90),
	(78, 16, 91),
	(72, 16, 92),
	(64, 17, 90),
	(73, 17, 92),
	(65, 18, 90),
	(79, 18, 91),
	(74, 18, 92),
	(89, 18, 94),
	(66, 19, 90),
	(75, 19, 92),
	(82, 19, 94),
	(59, 20, 90),
	(77, 20, 91),
	(68, 20, 92),
	(83, 20, 94);
/*!40000 ALTER TABLE `test_repos_group` ENABLE KEYS */;


-- Дамп структуры для таблица final.ugroup_member
CREATE TABLE IF NOT EXISTS `ugroup_member` (
  `um_id` int(11) NOT NULL AUTO_INCREMENT,
  `um_registered_user_id` int(11) NOT NULL,
  `um_user_group_id` int(11) NOT NULL,
  PRIMARY KEY (`um_id`),
  KEY `FK_ugroup_member_registered_user` (`um_registered_user_id`),
  KEY `FK_ugroup_member_user_group` (`um_user_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.ugroup_member: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `ugroup_member` DISABLE KEYS */;
INSERT INTO `ugroup_member` (`um_id`, `um_registered_user_id`, `um_user_group_id`) VALUES
	(187, 20, 22),
	(189, 1, 20),
	(197, 20, 31),
	(198, 1, 21),
	(199, 1, 21),
	(200, 31, 22),
	(201, 21, 32);
/*!40000 ALTER TABLE `ugroup_member` ENABLE KEYS */;


-- Дамп структуры для таблица final.user_answer_journal
CREATE TABLE IF NOT EXISTS `user_answer_journal` (
  `uaj_id` int(11) NOT NULL AUTO_INCREMENT,
  `uaj_user_question_journal_id` int(11) NOT NULL,
  `uaj_answer_variant_id` int(11) NOT NULL,
  PRIMARY KEY (`uaj_id`),
  UNIQUE KEY `Индекс 3` (`uaj_user_question_journal_id`,`uaj_answer_variant_id`),
  CONSTRAINT `FK_user_answer_journal_user_question_journal` FOREIGN KEY (`uaj_user_question_journal_id`) REFERENCES `user_question_journal` (`uqj_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=655 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.user_answer_journal: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `user_answer_journal` DISABLE KEYS */;
INSERT INTO `user_answer_journal` (`uaj_id`, `uaj_user_question_journal_id`, `uaj_answer_variant_id`) VALUES
	(615, 393, 61),
	(628, 394, 0),
	(629, 394, 1),
	(630, 394, 2),
	(631, 394, 3),
	(632, 395, 113),
	(633, 396, 145),
	(623, 433, 0),
	(624, 433, 1),
	(625, 433, 2),
	(626, 433, 3),
	(627, 434, 144),
	(634, 454, 0),
	(635, 454, 1),
	(636, 454, 2),
	(637, 454, 3),
	(638, 454, 4),
	(639, 455, 0),
	(640, 455, 1),
	(641, 455, 2),
	(642, 455, 3),
	(643, 455, 4),
	(644, 456, 0),
	(645, 456, 1),
	(646, 456, 2),
	(647, 456, 3),
	(616, 469, 141),
	(617, 470, 114),
	(618, 471, 136),
	(619, 472, 0),
	(620, 472, 1),
	(621, 472, 2),
	(622, 472, 3),
	(648, 481, 114),
	(649, 482, 144),
	(650, 483, 61),
	(651, 484, 0),
	(652, 484, 1),
	(653, 484, 2),
	(654, 484, 3);
/*!40000 ALTER TABLE `user_answer_journal` ENABLE KEYS */;


-- Дамп структуры для таблица final.user_group
CREATE TABLE IF NOT EXISTS `user_group` (
  `ug_id` int(11) NOT NULL AUTO_INCREMENT,
  `ug_name` varchar(100) NOT NULL,
  `ug_description` varchar(150) NOT NULL,
  `ug_author_user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ug_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.user_group: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` (`ug_id`, `ug_name`, `ug_description`, `ug_author_user_id`) VALUES
	(20, 'Группа 1', 'Описание группы 1', 1),
	(21, 'Группа 2', 'ok', 1),
	(22, 'Группа 3', 'Группа сформирована 01.01.2015', 1);
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;


-- Дамп структуры для таблица final.user_question_journal
CREATE TABLE IF NOT EXISTS `user_question_journal` (
  `uqj_id` int(11) NOT NULL AUTO_INCREMENT,
  `uqj_user_test_journal_id` int(11) NOT NULL,
  `uqj_question_setting_id` int(11) NOT NULL,
  `uqj_user_question_status_id` int(11) NOT NULL DEFAULT '3',
  `ugj_paused` int(11) NOT NULL DEFAULT '0',
  `uqj_user_answer` int(11) NOT NULL,
  `uqj_answering_duration` int(11) NOT NULL,
  `uqj_q_order_in_user_test` int(11) NOT NULL,
  `uqj_answer_is_correct` int(11) NOT NULL DEFAULT '0',
  `uqj_last_answering_start` int(11) NOT NULL DEFAULT '0',
  `uqj_last_answering_finish` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uqj_id`),
  KEY `FK_user_question_journal_user_test_journal` (`uqj_user_test_journal_id`),
  KEY `FK_user_question_journal_question_setting` (`uqj_question_setting_id`),
  KEY `FK_user_question_journal_user_question_status` (`uqj_user_question_status_id`),
  CONSTRAINT `FK_user_question_journal_question_setting` FOREIGN KEY (`uqj_question_setting_id`) REFERENCES `question_setting` (`qs_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_question_journal_user_question_status` FOREIGN KEY (`uqj_user_question_status_id`) REFERENCES `user_question_status` (`uqs_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_question_journal_user_test_journal` FOREIGN KEY (`uqj_user_test_journal_id`) REFERENCES `user_test_journal` (`utj_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=525 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.user_question_journal: ~84 rows (приблизительно)
/*!40000 ALTER TABLE `user_question_journal` DISABLE KEYS */;
INSERT INTO `user_question_journal` (`uqj_id`, `uqj_user_test_journal_id`, `uqj_question_setting_id`, `uqj_user_question_status_id`, `ugj_paused`, `uqj_user_answer`, `uqj_answering_duration`, `uqj_q_order_in_user_test`, `uqj_answer_is_correct`, `uqj_last_answering_start`, `uqj_last_answering_finish`) VALUES
	(393, 363, 16, 5, 0, 0, 4, 1, 1, 1422864602, 1422864606),
	(394, 363, 20, 5, 0, 0, 4, 2, 1, 1422865096, 1422865100),
	(395, 363, 18, 5, 0, 0, 4, 3, 0, 1422865102, 1422865106),
	(396, 363, 14, 5, 0, 0, 5, 4, 0, 1422865108, 1422865113),
	(397, 364, 16, 3, 0, 0, 0, 1, 0, 0, 0),
	(398, 364, 20, 3, 0, 0, 0, 2, 0, 0, 0),
	(399, 364, 18, 3, 0, 0, 0, 3, 0, 0, 0),
	(400, 364, 14, 3, 0, 0, 0, 4, 0, 0, 0),
	(401, 365, 16, 3, 0, 0, 0, 1, 0, 0, 0),
	(402, 365, 20, 3, 0, 0, 0, 2, 0, 0, 0),
	(403, 365, 18, 3, 0, 0, 0, 3, 0, 0, 0),
	(404, 365, 14, 3, 0, 0, 0, 4, 0, 0, 0),
	(405, 366, 16, 3, 0, 0, 0, 1, 0, 0, 0),
	(406, 366, 20, 3, 0, 0, 0, 2, 0, 0, 0),
	(407, 366, 18, 3, 0, 0, 0, 3, 0, 0, 0),
	(408, 366, 14, 3, 0, 0, 0, 4, 0, 0, 0),
	(409, 367, 16, 3, 0, 0, 0, 1, 0, 0, 0),
	(410, 367, 20, 3, 0, 0, 0, 2, 0, 0, 0),
	(411, 367, 18, 3, 0, 0, 0, 3, 0, 0, 0),
	(412, 367, 14, 3, 0, 0, 0, 4, 0, 0, 0),
	(413, 368, 14, 3, 0, 0, 0, 1, 0, 0, 0),
	(414, 368, 18, 3, 0, 0, 0, 2, 0, 0, 0),
	(415, 368, 16, 3, 0, 0, 0, 3, 0, 0, 0),
	(416, 368, 20, 3, 0, 0, 0, 4, 0, 0, 0),
	(417, 369, 14, 3, 0, 0, 0, 1, 0, 0, 0),
	(418, 369, 18, 3, 0, 0, 0, 2, 0, 0, 0),
	(419, 369, 16, 3, 0, 0, 0, 3, 0, 0, 0),
	(420, 369, 20, 3, 0, 0, 0, 4, 0, 0, 0),
	(421, 370, 14, 3, 0, 0, 0, 1, 0, 0, 0),
	(422, 370, 18, 3, 0, 0, 0, 2, 0, 0, 0),
	(423, 370, 16, 3, 0, 0, 0, 3, 0, 0, 0),
	(424, 370, 20, 3, 0, 0, 0, 4, 0, 0, 0),
	(425, 371, 14, 3, 0, 0, 0, 1, 0, 0, 0),
	(426, 371, 18, 3, 0, 0, 0, 2, 0, 0, 0),
	(427, 371, 16, 3, 0, 0, 0, 3, 0, 0, 0),
	(428, 371, 20, 3, 0, 0, 0, 4, 0, 0, 0),
	(429, 372, 14, 3, 0, 0, 0, 1, 0, 0, 0),
	(430, 372, 18, 3, 0, 0, 0, 2, 0, 0, 0),
	(431, 372, 16, 3, 0, 0, 0, 3, 0, 0, 0),
	(432, 372, 20, 3, 0, 0, 0, 4, 0, 0, 0),
	(433, 373, 20, 5, 0, 0, 5, 1, 0, 1422865048, 1422865053),
	(434, 373, 14, 5, 0, 0, 5, 2, 0, 1422865056, 1422865061),
	(435, 373, 18, 1, 1, 0, 3, 3, 0, 1422865064, 1422865067),
	(436, 373, 16, 3, 0, 0, 0, 4, 0, 0, 0),
	(437, 374, 20, 3, 0, 0, 0, 1, 0, 0, 0),
	(438, 374, 14, 3, 0, 0, 0, 2, 0, 0, 0),
	(439, 374, 18, 3, 0, 0, 0, 3, 0, 0, 0),
	(440, 374, 16, 3, 0, 0, 0, 4, 0, 0, 0),
	(441, 375, 20, 3, 0, 0, 0, 1, 0, 0, 0),
	(442, 375, 14, 3, 0, 0, 0, 2, 0, 0, 0),
	(443, 375, 18, 3, 0, 0, 0, 3, 0, 0, 0),
	(444, 375, 16, 3, 0, 0, 0, 4, 0, 0, 0),
	(445, 376, 17, 1, 0, 0, 0, 1, 0, 1422865076, 0),
	(446, 376, 19, 3, 0, 0, 0, 2, 0, 0, 0),
	(447, 376, 20, 3, 0, 0, 0, 3, 0, 0, 0),
	(448, 376, 16, 3, 0, 0, 0, 4, 0, 0, 0),
	(449, 376, 15, 3, 0, 0, 0, 5, 0, 0, 0),
	(450, 376, 12, 3, 0, 0, 0, 6, 0, 0, 0),
	(451, 376, 18, 3, 0, 0, 0, 7, 0, 0, 0),
	(452, 376, 14, 3, 0, 0, 0, 8, 0, 0, 0),
	(453, 376, 13, 3, 0, 0, 0, 9, 0, 0, 0),
	(454, 377, 17, 5, 0, 0, 6, 1, 0, 1422865153, 1422865159),
	(455, 377, 19, 5, 0, 0, 5, 2, 1, 1422865161, 1422865166),
	(456, 377, 20, 5, 0, 0, 4, 3, 1, 1422865168, 1422865172),
	(457, 377, 16, 1, 1, 0, 2, 4, 0, 1422865175, 1422865177),
	(458, 377, 15, 3, 0, 0, 0, 5, 0, 0, 0),
	(459, 377, 12, 3, 0, 0, 0, 6, 0, 0, 0),
	(460, 377, 18, 3, 0, 0, 0, 7, 0, 0, 0),
	(461, 377, 14, 3, 0, 0, 0, 8, 0, 0, 0),
	(462, 377, 13, 3, 0, 0, 0, 9, 0, 0, 0),
	(463, 378, 20, 3, 0, 0, 0, 1, 0, 0, 0),
	(464, 378, 19, 3, 0, 0, 0, 2, 0, 0, 0),
	(465, 379, 20, 3, 0, 0, 0, 1, 0, 0, 0),
	(466, 379, 19, 3, 0, 0, 0, 2, 0, 0, 0),
	(467, 380, 20, 3, 0, 0, 0, 1, 0, 0, 0),
	(468, 380, 19, 3, 0, 0, 0, 2, 0, 0, 0),
	(469, 381, 14, 5, 0, 0, 7, 1, 0, 1422864949, 1422864956),
	(470, 381, 18, 5, 0, 0, 6, 2, 1, 1422864967, 1422864970),
	(471, 381, 12, 5, 0, 0, 9, 3, 1, 1422864970, 1422864975),
	(472, 381, 20, 5, 0, 0, 7, 4, 1, 1422864975, 1422864978),
	(473, 382, 14, 3, 0, 0, 0, 1, 0, 0, 0),
	(474, 382, 18, 3, 0, 0, 0, 2, 0, 0, 0),
	(475, 382, 12, 3, 0, 0, 0, 3, 0, 0, 0),
	(476, 382, 20, 3, 0, 0, 0, 4, 0, 0, 0),
	(477, 383, 18, 3, 0, 0, 0, 1, 0, 0, 0),
	(478, 383, 14, 3, 0, 0, 0, 2, 0, 0, 0),
	(479, 383, 16, 3, 0, 0, 0, 3, 0, 0, 0),
	(480, 383, 20, 3, 0, 0, 0, 4, 0, 0, 0),
	(481, 384, 18, 5, 0, 0, 4, 1, 1, 1422865425, 1422865429),
	(482, 384, 14, 5, 0, 0, 5, 2, 0, 1422865431, 1422865436),
	(483, 384, 16, 5, 0, 0, 4, 3, 1, 1422865439, 1422865443),
	(484, 384, 20, 5, 0, 0, 4, 4, 1, 1422865445, 1422865449),
	(485, 385, 12, 3, 0, 0, 0, 1, 0, 0, 0),
	(486, 385, 18, 3, 0, 0, 0, 2, 0, 0, 0),
	(487, 385, 15, 3, 0, 0, 0, 3, 0, 0, 0),
	(488, 385, 16, 3, 0, 0, 0, 4, 0, 0, 0),
	(489, 385, 17, 3, 0, 0, 0, 5, 0, 0, 0),
	(490, 385, 19, 3, 0, 0, 0, 6, 0, 0, 0),
	(491, 385, 20, 3, 0, 0, 0, 7, 0, 0, 0),
	(492, 385, 14, 3, 0, 0, 0, 8, 0, 0, 0),
	(493, 385, 13, 3, 0, 0, 0, 9, 0, 0, 0),
	(494, 386, 16, 3, 0, 0, 0, 1, 0, 0, 0),
	(495, 386, 13, 3, 0, 0, 0, 2, 0, 0, 0),
	(496, 386, 14, 3, 0, 0, 0, 3, 0, 0, 0),
	(497, 386, 15, 3, 0, 0, 0, 4, 0, 0, 0),
	(498, 386, 20, 3, 0, 0, 0, 5, 0, 0, 0),
	(499, 386, 19, 3, 0, 0, 0, 6, 0, 0, 0),
	(500, 386, 18, 3, 0, 0, 0, 7, 0, 0, 0),
	(501, 386, 17, 3, 0, 0, 0, 8, 0, 0, 0),
	(502, 386, 12, 3, 0, 0, 0, 9, 0, 0, 0),
	(503, 387, 13, 3, 0, 0, 0, 1, 0, 0, 0),
	(504, 387, 19, 3, 0, 0, 0, 2, 0, 0, 0),
	(505, 387, 18, 3, 0, 0, 0, 3, 0, 0, 0),
	(506, 387, 12, 3, 0, 0, 0, 4, 0, 0, 0),
	(507, 387, 17, 3, 0, 0, 0, 5, 0, 0, 0),
	(508, 387, 14, 3, 0, 0, 0, 6, 0, 0, 0),
	(509, 387, 20, 3, 0, 0, 0, 7, 0, 0, 0),
	(510, 387, 15, 3, 0, 0, 0, 8, 0, 0, 0),
	(511, 387, 16, 3, 0, 0, 0, 9, 0, 0, 0),
	(512, 388, 18, 3, 0, 0, 0, 1, 0, 0, 0),
	(513, 388, 12, 3, 0, 0, 0, 2, 0, 0, 0),
	(514, 388, 20, 3, 0, 0, 0, 3, 0, 0, 0),
	(515, 388, 13, 3, 0, 0, 0, 4, 0, 0, 0),
	(516, 388, 19, 3, 0, 0, 0, 5, 0, 0, 0),
	(517, 388, 15, 3, 0, 0, 0, 6, 0, 0, 0),
	(518, 388, 16, 3, 0, 0, 0, 7, 0, 0, 0),
	(519, 388, 14, 3, 0, 0, 0, 8, 0, 0, 0),
	(520, 388, 17, 3, 0, 0, 0, 9, 0, 0, 0),
	(521, 389, 16, 3, 0, 0, 0, 1, 0, 0, 0),
	(522, 389, 18, 3, 0, 0, 0, 2, 0, 0, 0),
	(523, 389, 14, 3, 0, 0, 0, 3, 0, 0, 0),
	(524, 389, 20, 3, 0, 0, 0, 4, 0, 0, 0);
/*!40000 ALTER TABLE `user_question_journal` ENABLE KEYS */;


-- Дамп структуры для таблица final.user_question_status
CREATE TABLE IF NOT EXISTS `user_question_status` (
  `uqs_id` int(11) NOT NULL AUTO_INCREMENT,
  `uqs_name` varchar(50) NOT NULL,
  `uqs_comment` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`uqs_id`),
  KEY `uqs_name` (`uqs_name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.user_question_status: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `user_question_status` DISABLE KEYS */;
INSERT INTO `user_question_status` (`uqs_id`, `uqs_name`, `uqs_comment`) VALUES
	(1, 'in_progress', '1- priority'),
	(2, 'rusemed', '2- priority'),
	(3, 'appointed', '3- priority'),
	(4, 'skipped', '4- priority'),
	(5, 'answered', 'closed'),
	(6, 'overtimed', 'closed');
/*!40000 ALTER TABLE `user_question_status` ENABLE KEYS */;


-- Дамп структуры для таблица final.user_registration_inf
CREATE TABLE IF NOT EXISTS `user_registration_inf` (
  `uri_id` int(11) NOT NULL AUTO_INCREMENT,
  `uri_login` varchar(100) NOT NULL,
  `uri_password` char(40) NOT NULL,
  `uri_email` varchar(100) NOT NULL,
  `uri_name` varchar(75) NOT NULL,
  `uri_surname` varchar(75) NOT NULL,
  `uri_user_group_id` int(11) NOT NULL,
  `uri_reg_date` int(11) NOT NULL,
  `uri_registration_key` char(40) NOT NULL,
  `uri_activated` tinyint(4) NOT NULL DEFAULT '0',
  `uri_activated_date` int(11) DEFAULT NULL,
  `uri_registered_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`uri_id`),
  KEY `Index_uri_registration_key` (`uri_registration_key`),
  KEY `FK_user_registration_inf_registered_user` (`uri_registered_user_id`),
  CONSTRAINT `FK_user_registration_inf_registered_user` FOREIGN KEY (`uri_registered_user_id`) REFERENCES `registered_user` (`ru_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COMMENT='This table is used for for saving information about user before account activation by email';

-- Дамп данных таблицы final.user_registration_inf: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `user_registration_inf` DISABLE KEYS */;
INSERT INTO `user_registration_inf` (`uri_id`, `uri_login`, `uri_password`, `uri_email`, `uri_name`, `uri_surname`, `uri_user_group_id`, `uri_reg_date`, `uri_registration_key`, `uri_activated`, `uri_activated_date`, `uri_registered_user_id`) VALUES
	(34, 'pupil_1', 'c5974fa2cb5a78bcfeec4f071bfb7282274cfa80', 'ser@e.example', 'Василий', 'Васильев', 20, 1422815932, 'cf2ba2581e3124352fca6d446dc73dac0572d4af', 1, 1422815942, 31),
	(35, 'pupil_2', 'e38ad214943daad1d64c102faec29de4afe9da3d', 'ser@e.exhample', 'Юрий', 'Петров', 21, 1422866213, '9377f75a6cc2d06e954333c16b12875fe74404a8', 1, 1422866233, 32);
/*!40000 ALTER TABLE `user_registration_inf` ENABLE KEYS */;


-- Дамп структуры для таблица final.user_test_journal
CREATE TABLE IF NOT EXISTS `user_test_journal` (
  `utj_id` int(11) NOT NULL AUTO_INCREMENT,
  `utj_appointed_test_id` int(11) NOT NULL,
  `utj_registered_user_id` int(11) DEFAULT NULL,
  `utj_temp_user_id` int(11) DEFAULT NULL,
  `utj_time_duration` int(11) NOT NULL,
  `utj_is_finished` int(11) NOT NULL DEFAULT '0',
  `utj_final_mark` float NOT NULL DEFAULT '0',
  `utj_final_verdict` int(11) NOT NULL DEFAULT '0',
  `utj_finish_date` int(11) DEFAULT NULL,
  `utj_num_answers` int(11) NOT NULL DEFAULT '0',
  `utj_num_right_answers` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`utj_id`),
  UNIQUE KEY `Индекс 5` (`utj_appointed_test_id`,`utj_registered_user_id`),
  UNIQUE KEY `Индекс 6` (`utj_appointed_test_id`,`utj_temp_user_id`),
  KEY `FK_user_test_journal_registered_user` (`utj_registered_user_id`),
  KEY `FK_user_test_journal_temp_user` (`utj_temp_user_id`),
  CONSTRAINT `FK_user_test_journal_appointed_test` FOREIGN KEY (`utj_appointed_test_id`) REFERENCES `appointed_test` (`at_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_test_journal_registered_user` FOREIGN KEY (`utj_registered_user_id`) REFERENCES `registered_user` (`ru_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_user_test_journal_temp_user` FOREIGN KEY (`utj_temp_user_id`) REFERENCES `temp_user` (`tu_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=390 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы final.user_test_journal: ~20 rows (приблизительно)
/*!40000 ALTER TABLE `user_test_journal` DISABLE KEYS */;
INSERT INTO `user_test_journal` (`utj_id`, `utj_appointed_test_id`, `utj_registered_user_id`, `utj_temp_user_id`, `utj_time_duration`, `utj_is_finished`, `utj_final_mark`, `utj_final_verdict`, `utj_finish_date`, `utj_num_answers`, `utj_num_right_answers`) VALUES
	(363, 182, NULL, 125, 0, 1, 5, 0, 1422865113, 4, 2),
	(364, 182, NULL, 126, 0, 0, 0, 0, NULL, 0, 0),
	(365, 182, NULL, 127, 0, 0, 0, 0, NULL, 0, 0),
	(366, 182, NULL, 128, 0, 0, 0, 0, NULL, 0, 0),
	(367, 182, NULL, 129, 0, 0, 0, 0, NULL, 0, 0),
	(368, 183, NULL, 130, 0, 0, 0, 0, NULL, 0, 0),
	(369, 183, NULL, 131, 0, 0, 0, 0, NULL, 0, 0),
	(370, 183, NULL, 132, 0, 0, 0, 0, NULL, 0, 0),
	(371, 183, NULL, 133, 0, 0, 0, 0, NULL, 0, 0),
	(372, 183, NULL, 134, 0, 0, 0, 0, NULL, 0, 0),
	(373, 184, 1, NULL, 0, 0, 0, 0, NULL, 0, 0),
	(374, 184, 20, NULL, 0, 0, 0, 0, NULL, 0, 0),
	(375, 184, 31, NULL, 0, 0, 0, 0, NULL, 0, 0),
	(376, 185, 1, NULL, 0, 0, 0, 0, NULL, 0, 0),
	(377, 185, 20, NULL, 0, 0, 0, 0, NULL, 0, 0),
	(378, 186, 1, NULL, 0, 0, 0, 0, NULL, 0, 0),
	(379, 186, 20, NULL, 0, 0, 0, 0, NULL, 0, 0),
	(380, 186, 31, NULL, 0, 0, 0, 0, NULL, 0, 0),
	(381, 187, 1, NULL, 0, 1, 7.5, 1, 1422864978, 4, 3),
	(382, 187, 31, NULL, 0, 0, 0, 0, NULL, 0, 0),
	(383, 188, 1, NULL, 0, 0, 0, 0, NULL, 0, 0),
	(384, 188, 31, NULL, 0, 1, 7.5, 0, 1422865449, 4, 3),
	(385, 189, 31, NULL, 0, 0, 0, 0, NULL, 0, 0),
	(386, 190, 31, NULL, 0, 0, 0, 0, NULL, 0, 0),
	(387, 191, 31, NULL, 0, 0, 0, 0, NULL, 0, 0),
	(388, 192, 32, NULL, 0, 0, 0, 0, NULL, 0, 0),
	(389, 193, 32, NULL, 0, 0, 0, 0, NULL, 0, 0);
/*!40000 ALTER TABLE `user_test_journal` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
